﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EmployeePortal.Models;

namespace EmployeePortal.Controllers
{
    public class BaseController : Controller
    {
        const string UserIdSessionKey = "_UserId";
        const string UserLevelSessionKey = "_UserLevel";
        const string UserEmailSessionKey = "_UserEmail";
        const string UserNameSessionKey = "_UserName";
        const string JobTitleSessionKey = "_JobTitle";
        const string UserEmailADSessionKey = "_UserEmailAD";
        const string UserCompanySessionKey = "_UserCompany";
        const string UserDepartementSessionKey = "_UserDepartement";

        protected string UserId
        {
            get { return HttpContext.Session?.GetString(UserIdSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserIdSessionKey, value); }
        }
        protected string UserLevel
        {
            get { return HttpContext.Session?.GetString(UserLevelSessionKey) ?? ""; }   
            set { HttpContext.Session.SetString(UserLevelSessionKey, value); }
        }

        protected string UserEmail
        {
            get { return HttpContext.Session?.GetString(UserEmailSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserEmailSessionKey, value); }
        }

        protected string UserName
        {
            get { return HttpContext.Session?.GetString(UserNameSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserNameSessionKey, value); }
        }

        protected string JobTitle
        {
            get { return HttpContext.Session?.GetString(JobTitleSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(JobTitleSessionKey, value); }
        }

        protected string UserEmailAD
        {
            get { return HttpContext.Session?.GetString(UserEmailADSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserEmailADSessionKey, value); }
        }
        protected string UserCompany
        {
            get { return HttpContext.Session?.GetString(UserCompanySessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserCompanySessionKey, value); }
        }

        protected string UserDepartement
        {
            get { return HttpContext.Session?.GetString(UserDepartementSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserDepartementSessionKey, value); }
        }

        protected void SetCookie(int userId, int userLevel)
        {
            UserId = userId.ToString();

            switch (userLevel)
            {
                case 0:
                    UserLevel = "ADMIN";
                    break;
                case 2:
                    UserLevel = "VENDOR";
                    break;
                case 3:
                    UserLevel = "CUSTOMER";
                    break;
                case 4:
                    UserLevel = "JOB_APPLICANT";
                    break;
                case 11:
                    UserLevel = "USER_FR";
                    break;
                case 12:
                    UserLevel = "USER_FAP";
                    break;
                default:
                    break;
            }
        }

        protected void SetCookieUserId(string userId)
        {
            UserEmail = userId;
        }

        protected void SetCookieUsername(string email)
        {
            UserName = email;
        }

        protected void SetCookieJobTitle(string jobTitle)
        {
            JobTitle = jobTitle;
        }

        protected void SetCookieUserEmailAD(string email)
        {
            UserEmailAD = email;
        }

        protected void SetCookieUserCompany(string company)
        {
            UserCompany = company;
        }

        protected void SetCookieUserDepartement(string departement)
        {
            UserDepartement = departement;
        }
    }
}