﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EmployeePortal.Models;

namespace EmployeePortal.Controllers
{
    [Route("[controller]/[action]")]
    public class HomeController : BaseController
    {
        IConfiguration _configuration;
        readonly string _baseHRISServerKey;
        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
            _baseHRISServerKey = _configuration.GetValue<string>("BaseServer") + "HRISAPIEndpoint";
        }

        [Route("/")]
        public IActionResult Index()
        {
            ViewData["VendorUrl"] = _configuration.GetValue<string>("VendorUrlContext:BaseUrl");
            return View();
        }

        public async Task<string> GetUrlApiWebPortal()
        {
            return await Task.FromResult(_configuration.GetValue<string>("WebPortalAPIEndpoint:BaseUrl"));
        }

        public async Task<string> GetUrlApiHRIS()
        {
            return await Task.FromResult(_configuration.GetValue<string>(_baseHRISServerKey + ":BaseUrl"));
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}

        [Route("/Error")]
        public IActionResult Error()
        {
            return View();
        }
    }
}
