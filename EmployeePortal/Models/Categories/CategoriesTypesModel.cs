﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Models.Categories
{
    public class CategoriesTypesModel
    {
        public int CategoryId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public List<CategoriesModel> CategoryField { get; set; }
    }
}