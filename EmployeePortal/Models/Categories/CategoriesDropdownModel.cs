﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Models.Categories
{
    public class CategoriesDropdownModel
    {
        public string id { get; set; }
        public string text { get; set; }
        public int level { get; set; }
    }
}
