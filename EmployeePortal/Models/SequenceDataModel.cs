﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Models
{
    public class SequenceDataModel
    {
        public long Id { get; set; }
        public int Seq { get; set; }

        public static int SeqExt { get; set; }

        public void SeqIncr()
        {
            SeqExt++;
            Seq = SeqExt;
        }

        public static void SeqReset()
        {
            SeqExt = 0;
        }

    }
}
