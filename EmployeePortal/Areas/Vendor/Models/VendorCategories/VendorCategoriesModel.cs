﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Models.VendorCategories
{
    public class VendorCategoriesModel
    {
        public int VendorId { get; set; }
        public string Status { get; set; }
        public string EmailCompany { get; set; }
        public string EmailPic { get; set; }
        public string Messages { get; set; }
        public int MasterDataUserId { get; set; }
        public List<int> ListCategoryId { get; set; }
    }
}