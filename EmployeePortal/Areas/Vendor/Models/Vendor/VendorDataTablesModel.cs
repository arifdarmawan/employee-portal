﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.Vendor
{
    public class VendorDataTablesModel
    {
        public int Id { get; set; }
        public int VendorUserId { get; set; }
        public string BusinessTypeName { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string CompanyPhone { get; set; }
        public string Status { get; set; }
        public string BusinessFields { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
