﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.Vendor
{
    public class VendorBasicInfoModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string BusinessTypeName { get; set; }
        public int MasterDataBusinessTypeId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string MailingAddress { get; set; }
        public int MasterDataCountryId { get; set; }
        public string Country { get; set; }
        public int MasterDataRegionId { get; set; }
        public string Region { get; set; }
        public int MasterDataCityId { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }
        public string Pic { get; set; }
        public string Picemail { get; set; }
        public string IdCardNumber { get; set; }
        public string Picphone { get; set; }
        public bool? IntegrityPact { get; set; }
        public bool? MasterAgreement { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<int> MasterBusinessFieldsIdArray { get; set; }
        public List<BusinessFields.BusinessFieldsModel> BusinessFields { get; set; }
    }
}
