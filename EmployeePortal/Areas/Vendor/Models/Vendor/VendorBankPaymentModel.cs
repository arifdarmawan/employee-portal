﻿using EmployeePortal.Areas.Vendor.Models.BankAccount;
using EmployeePortal.Areas.Vendor.Models.VendorPayment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.Vendor
{
    public class VendorBankPaymentModel
    {
        public VendorBankAccountModel VendorBankAccount { get; set; }
        public VendorPaymentModel VendorPaymentModel { get; set; }
        public VendorBankPaymentModel() { }
        public VendorBankPaymentModel(VendorBankAccountModel vendorBankAccount, VendorPaymentModel vendorPaymentModel)
        {
            VendorBankAccount = vendorBankAccount;
            VendorPaymentModel = vendorPaymentModel;
        }
    }
}
