﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.Vendor
{
    public class VendorDetailViewModel
    {
        public string BusinessType { get; set; }
        public string CompanyName { get; set; }
        public List<BusinessFields.BusinessFieldsModel> BusinessFields { get; set; }
        public string Address { get; set; }
        public string MailingAddress { get; set; }
        public string Country { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }
        public string Pic { get; set; }
        public string IdCardNumber { get; set; }
        public string PicEmail { get; set; }
        public string Picphone { get; set; }
        public BankAccount BankAccount { get; set; }
        public BankPayment BankPayment { get; set; }
    }

    public class BankAccount
    {
        public string Currency { get; set; }
        public string BankName { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
    }

    public class BankPayment
    {
        public int Top { get; set; }
        public decimal? DownPayment { get; set; }
        public decimal? Progressive { get; set; }
        public decimal? FullPayment { get; set; }
        public decimal? Retention { get; set; }
    }
}
