﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.VendorTax
{
    public class VendorTaxModel
    {
        public int Id { get; set; }
        public string TypeNpwp { get; set; }
        public string Npwp { get; set; }
        public string TypePkp { get; set; }
        public string Pkp { get; set; }
        public string TypeBkp { get; set; }
        public string Ppn { get; set; }
        public int? MasterDataBkpCategoryId { get; set; }
        public string BkpDesc { get; set; }
    }
}
