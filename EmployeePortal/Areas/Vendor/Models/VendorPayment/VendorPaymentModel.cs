﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.VendorPayment
{
    public class VendorPaymentModel
    {
        public int Top { get; set; }
        public decimal? DownPayment { get; set; }
        public decimal? Progressive { get; set; }
        public decimal? FullPayment { get; set; }
        public decimal? Retention { get; set; }
    }
}
