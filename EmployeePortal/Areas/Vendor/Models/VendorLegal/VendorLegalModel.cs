﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.VendorLegal
{
    public class VendorLegalModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string Nib { get; set; }
        public string Nibattachment { get; set; }
        public string BusinessPermit { get; set; }
        public string BusinessPermitAttachment { get; set; }
        public string Siup { get; set; }
        public DateTime? SiupexpiredDate { get; set; }
        public string Siupattachment { get; set; }
        public string Tdp { get; set; }
        public DateTime? TdpexpiredDate { get; set; }
        public string Tdpattachment { get; set; }
        public string MemorandumOfAssociation { get; set; }
        public string MemorandumOfAssociationAttachment { get; set; }
        public string DecissionLetterMenkumham { get; set; }
        public string DecissionLetterMenkumhamAttachment { get; set; }
        public string MemorandumOfAssociationChanging { get; set; }
        public string MemorandumOfAssociationChangingAttachment { get; set; }
        public string DecissionLetterMenkumhamChanging { get; set; }
        public string DecissionLetterMenkumhamChangingAttachment { get; set; }
        public string Signing { get; set; }
        public string ManagementStructure { get; set; }
        public int? SigningId { get; set; }
        public string SigningName { get; set; }
        public string SigningAttachment { get; set; }
        public string DocumentOther { get; set; }
        public string OtherAttachment { get; set; }
        public string[] JobTitle { get; set; }
        public RootObject StructureOrg { get; set; }
        public RootObject ConstructStructureOrg(List<JobTitles.JobTitlesModel> jobTitles, RootObject rootObject)
        {            
            foreach(var single in rootObject.Data)
            {
                var checkJobTitles = jobTitles.FirstOrDefault(n => n.Id == Int32.Parse(single.JobTitleId));

                if (Int32.Parse(single.JobTitleId) == checkJobTitles.Id)
                {
                    single.JobTitleName = checkJobTitles.Title;
                }
            }
            
            return rootObject;
        }

        //public void ConstructPropertyDateFormat(this VendorLegalModel vendorModel)
        //{
        //    vendorModel.SiupexpiredDate = DateTime.ParseExact(vendorModel.SiupexpiredDate.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
        //    vendorModel.TdpexpiredDate = DateTime.ParseExact(vendorModel.SiupexpiredDate.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
        //}
    }
}
