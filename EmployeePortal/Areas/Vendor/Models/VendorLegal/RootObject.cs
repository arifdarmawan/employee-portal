﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.VendorLegal
{
    public class RootObject
    {
        public List<ComponentObject> Data { get; set; }
    }

    public class ComponentObject
    {
        public string Name { get; set; }
        public string JobTitleId { get; set; }
        public string JobTitleName { get; set; }
    }
}
