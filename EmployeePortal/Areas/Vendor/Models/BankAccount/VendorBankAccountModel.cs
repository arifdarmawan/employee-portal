﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.BankAccount
{
    public class VendorBankAccountModel
    {
        public int Id { get; set; }
        public string Currency { get; set; }
        public string BankName { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string Status { get; set; }
    }
}
