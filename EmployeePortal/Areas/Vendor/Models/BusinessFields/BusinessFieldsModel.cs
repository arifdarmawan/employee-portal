﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Vendor.Models.BusinessFields
{
    public class BusinessFieldsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Kbli { get; set; }
    }
}
