﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using EmployeePortal.Controllers;
using EmployeePortal.Helper;
using EmployeePortal.Service;
using EmployeePortal.Models.ResponseModel;
using System.Net;
using FirstResource.System.Library.EnumMaster;
using EmployeePortal.Models;
using Microsoft.AspNetCore.Hosting;
using EmployeePortal.Models.VendorCategories;
using EmployeePortal.Models.Categories;
using System.Text;

namespace EmployeePortal.Areas.Vendor.Controllers
{
    [Area("Vendor")]
    //[NoDirectAccess]
    public class VendorController : BaseController
    {
        PortalService _portalService;
        IConfiguration _configuration;
        IHostingEnvironment _environment;

        public VendorController(PortalService portalService, IConfiguration configuration, IHostingEnvironment environment)
        {
            _portalService = portalService;
            _configuration = configuration;
            _environment = environment;
        }

        [Route("/Vendor")]
        [Route("/Vendor/Index")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("/Vendor/GetAllVendor")]
        public async Task<string> GetAllVendor()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetAllVendor"));
            var dataTables = new List<Vendor.Models.Vendor.VendorDataTablesModel>();

            List<Vendor.Models.Vendor.VendorBasicInfoModel> vendors = response.Data != null ? JsonConvert.DeserializeObject<List<Vendor.Models.Vendor.VendorBasicInfoModel>>(Convert.ToString(response.Data)) : null;

            foreach (var vendor in vendors)
            {
                var businessFieldsArray = vendor.BusinessFields.Select(n => n.Name).ToArray();

                dataTables.Add(new Vendor.Models.Vendor.VendorDataTablesModel
                {
                    Id = vendor.Id,
                    VendorUserId = vendor.MasterDataUserId,
                    BusinessTypeName = vendor.BusinessTypeName,
                    Name = vendor.BusinessTypeName + " " + vendor.Name,
                    Address = vendor.Address,
                    CompanyPhone = vendor.CompanyPhone,
                    BusinessFields = GlobalHelper.ConstructSeparatedComma(businessFieldsArray),
                    Status = !string.IsNullOrEmpty(vendor.Status) ? vendor.Status : VendorStatus.PENDING.ToString(),
                    CreatedDate = vendor.CreatedDate
                });
            }

            return JsonConvert.SerializeObject(dataTables);
        }

        [Route("/Vendor/VendorDetails/{companyName}/{vendorStatus}/{vendorUserId}")]
        public IActionResult VendorDetails(string companyName, string vendorStatus, int vendorUserId)
        {
            ViewData["VendorUserId"] = vendorUserId;
            ViewData["CompanyName"] = companyName;
            ViewData["VendorStatus"] = vendorStatus;

            return View();
        }

        [Route("/Vendor/GetBasicInfo/{vendorUserId}")]
        public async Task<PartialViewResult> BasicInfo(int vendorUserId)
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "?userId=" + vendorUserId);
            Vendor.Models.Vendor.VendorBasicInfoModel model = response.Data != null ? JsonConvert.DeserializeObject<Vendor.Models.Vendor.VendorBasicInfoModel>(Convert.ToString(response.Data)) : null;

            return PartialView(model);
        }

        [Route("/Vendor/DownloadFile/{fileName}")]
        public async Task<BaseResponse> DownloadFile(string fileName)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:DownloadFile") + "?fileName=" + fileName);
                response.Data = result;
            }
            catch (Exception ex)
            {
                throw;
            }

            return response.Data;
        }

        [Route("/Vendor/GetBankPayment/{vendorUserId}")]
        public async Task<PartialViewResult> BankPayment(int vendorUserId)
        {
            var resBankAccount = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorAccountPrimary") + "?userId=" + vendorUserId);
            var resVendorPayment = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorPaymentTypeInfo") + "?masterDataUserId=" + vendorUserId);

            Vendor.Models.BankAccount.VendorBankAccountModel modelBankAccount = resBankAccount.Data != null ? JsonConvert.DeserializeObject<Vendor.Models.BankAccount.VendorBankAccountModel>(Convert.ToString(resBankAccount.Data)) : null;
            Vendor.Models.VendorPayment.VendorPaymentModel modelVendorPayment = resVendorPayment.Data != null ? JsonConvert.DeserializeObject<Vendor.Models.VendorPayment.VendorPaymentModel>(Convert.ToString(resVendorPayment.Data)) : null;
            Vendor.Models.Vendor.VendorBankPaymentModel modelBankPayment = new Models.Vendor.VendorBankPaymentModel(modelBankAccount, modelVendorPayment);

            return PartialView(modelBankPayment);
        }

        [Route("/Vendor/GetVendorLegal/{vendorUserId}")]
        public async Task<PartialViewResult> VendorLegal(int vendorUserId)
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorLegal") + "?masterDataUserId=" + vendorUserId);
            var resJobTitles = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetJobTitles"));

            Vendor.Models.VendorLegal.VendorLegalModel model = response.Data != null ? JsonConvert.DeserializeObject<Vendor.Models.VendorLegal.VendorLegalModel>(Convert.ToString(response.Data)) : null;
            List<Vendor.Models.JobTitles.JobTitlesModel> jobTitlesModel = resJobTitles.Data != null ? JsonConvert.DeserializeObject<List<Vendor.Models.JobTitles.JobTitlesModel>>(Convert.ToString(resJobTitles.Data)) : null;

            if (model != null)
            {
                Vendor.Models.VendorLegal.RootObject structureOrgObject = !string.IsNullOrEmpty(model.ManagementStructure) ? JsonConvert.DeserializeObject<Vendor.Models.VendorLegal.RootObject>(model.ManagementStructure) : null;
                model.StructureOrg = model.ConstructStructureOrg(jobTitlesModel, structureOrgObject);

                if (model.SigningId == 1)
                {
                    model.SigningName = "Direktur";
                }
                else if (model.SigningId == 2)
                {
                    model.SigningName = "Kuasa Direktur";
                }
            }

            return PartialView(model);
        }

        [Route("/Vendor/GetVendorTax/{vendorUserId}")]
        public async Task<PartialViewResult> VendorTax(int vendorUserId)
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorTax") + "?masterDataUserId=" + vendorUserId);
            Vendor.Models.VendorTax.VendorTaxModel model = response.Data != null ? JsonConvert.DeserializeObject<Vendor.Models.VendorTax.VendorTaxModel>(Convert.ToString(response.Data)) : null;

            return PartialView(model);
        }

        [Route("/Vendor/GetVendorAgreement/{vendorUserId}")]
        public async Task<PartialViewResult> VendorMasterAgreement(int vendorUserId)
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "?userId=" + vendorUserId);
            Vendor.Models.Vendor.VendorBasicInfoModel model = response.Data != null ? JsonConvert.DeserializeObject<Vendor.Models.Vendor.VendorBasicInfoModel>(Convert.ToString(response.Data)) : null;

            return PartialView(model);
        }

        [HttpPost]
        [Route("/Vendor/UpdateVendorStatus")]
        public async Task<BaseResponse> UpdateVendorStatus(VendorCategoriesModel param)
        {
            BaseResponse response = new BaseResponse();

            Models.Vendor.VendorBasicInfoModel model = null;

            if (param.Status == "FEEDBACK")
            {
                param.Status = "";
            }

            try
            {
                var responseUpdateVendorstatus = await _portalService.PutAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateVendorStatus") + "?masterDataUserId=" + param.VendorId + "&status=" + param.Status + "&createdBy=" + UserId, model);

                if (responseUpdateVendorstatus.Code == (int)HttpStatusCode.OK)
                {

                    // insert vendor categories
                    if (param.Status == "APPROVED")
                    {
                        var resVendorCategories = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorCategories"), param);
                    }

                    // insert web notification
                    var resInsertNotifWeb = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorNotification") + "?masterDataUserId=" + param.VendorId + "&messages=" + param.Messages + "&notificationType=" + NotificationType.WEB.ToString() + "&createdBy=" + UserId, model);
                    if (resInsertNotifWeb.Code == (int)HttpStatusCode.OK)
                    {
                        // insert email notification
                        var resInsertNotifEmail = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorNotification") + "?masterDataUserId=" + param.VendorId + "&messages=" + param.Messages + "&notificationType=" + NotificationType.EMAIL.ToString() + "&createdBy=" + UserId, model);
                        if (resInsertNotifEmail.Code == (int)HttpStatusCode.OK)
                        {
                            // handle duplicate email
                            if (param.EmailCompany != param.EmailPic)
                            {
                                // sending email 1
                                if (!string.IsNullOrEmpty(param.EmailCompany))
                                {
                                    var emailModel = await ConstructApprovalVendorFormatModel(param.Messages, param.EmailCompany, param.Status);

                                    MailService.SendMail(emailModel, emailModel.ConstructEmailBody(emailModel));
                                }

                                // sending email 2
                                if (!string.IsNullOrEmpty(param.EmailPic))
                                {
                                    var emailModel = await ConstructApprovalVendorFormatModel(param.Messages, param.EmailPic, param.Status);

                                    MailService.SendMail(emailModel, emailModel.ConstructEmailBody(emailModel));
                                }
                            }
                            else
                            {
                                var emailModel = await ConstructApprovalVendorFormatModel(param.Messages, param.EmailCompany, param.Status);

                                MailService.SendMail(emailModel, emailModel.ConstructEmailBody(emailModel));
                            }

                            
                            

                            response.Code = (int)HttpStatusCode.OK;
                            response.Status = HttpStatusCode.OK.ToString();
                            response.Message = "Proses Perubahan Data Vendor Berhasil.";
                        }
                        else
                        {
                            response.Code = resInsertNotifEmail.Code;
                            response.Status = resInsertNotifEmail.Status;
                            response.Message = resInsertNotifEmail.Message;
                        }

                    }
                    else
                    {
                        response.Code = resInsertNotifWeb.Code;
                        response.Status = resInsertNotifWeb.Status;
                        response.Message = resInsertNotifWeb.Message;
                    }
                }
                else
                {
                    response.Code = responseUpdateVendorstatus.Code;
                    response.Status = responseUpdateVendorstatus.Status;
                    response.Message = responseUpdateVendorstatus.Message;
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Message = ex.ToString();
            }

            return response;
        }

        [Route("/Vendor/GetVendorCategories/{vendorUserId}")]
        public async Task<PartialViewResult> VendorCategories(int vendorUserId)
        {
            var resAllCategories = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetAllCategories"));
            var resVendorCategories = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorCategories") + "?masterDataUserId=" + vendorUserId);

            List<CategoriesModel> categories = resAllCategories.Data != null ? JsonConvert.DeserializeObject<List<CategoriesModel>>(Convert.ToString(resAllCategories.Data)) : null;
            List<int> tes = JsonConvert.DeserializeObject<List<int>>(Convert.ToString(resVendorCategories.Data));
            var data = Convert.ToString(resVendorCategories.Data);


            ViewBag.MenuCategories = JsonConvert.SerializeObject(ConstructDropdownCategories(categories));
            ViewBag.VendorCategories = data;

            return PartialView();
        }

        [HttpPost]
        [Route("/Vendor/InsertVendorCategories")]
        public async Task<BaseResponse> InsertVendorCategories(VendorCategoriesModel model)
        {
            var response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorCategories"), model);

            return response;
        }

        #region helper
        private async Task<EmailFormatViewModel> ConstructApprovalVendorFormatModel(string messages, string email, string vendorStatus)
        {
            EmailFormatViewModel formatEmailModel = new EmailFormatViewModel(_configuration, _environment);

            string vendorSiteUrl = _configuration.GetValue<string>("VendorUrlContext:BaseUrl");

            formatEmailModel.Subject = "Vendor Verification";
            formatEmailModel.EmailRecipient = email;
            formatEmailModel.Link = vendorSiteUrl;
            formatEmailModel.LinkMessage = "Login";
            if (vendorStatus == VendorStatus.APPROVED.ToString())
                formatEmailModel.Message1 = "Congratulations, your data has been approved.";
            else if (vendorStatus == VendorStatus.REJECTED.ToString())
                formatEmailModel.Message1 = "Sorry, your data has been rejected.";
            else if (vendorStatus == "")
                formatEmailModel.Message1 = "Feedback.";
            else
                formatEmailModel.Message1 = "Congratulations, your data has been disabled.";
            formatEmailModel.Message2 = !string.IsNullOrEmpty(messages) ? messages : "-";

            return await Task.FromResult(formatEmailModel);
        }

        public List<CategoriesDropdownModel> ConstructDropdownCategories(List<CategoriesModel> categories)
        {
            int startLevel = 1;

            List<CategoriesDropdownModel> dropdown = new List<CategoriesDropdownModel>();
            List<CategoriesModel> parentItems = (from a in categories where a.ParentCategoryId == 0 select a).ToList();

            dropdown.Add(new CategoriesDropdownModel
            {
                id = "",
                text = "-Select Category-",
                level = 0
            });

            foreach (var parentcat in parentItems)
            {
                dropdown.Add(new CategoriesDropdownModel
                {
                    id = parentcat.CategoryId.ToString(),
                    text = parentcat.CategoryName,
                    level = startLevel
                });

                List<CategoriesModel> childItems = (from a in categories where a.ParentCategoryId == parentcat.CategoryId select a).ToList();
                if (childItems.Count > 0)
                    ConstructChildItem(categories, dropdown, parentcat, startLevel);
            }

            return dropdown;
        }

        private void ConstructChildItem(List<CategoriesModel> categories, List<CategoriesDropdownModel> dropdown, CategoriesModel childItem, int level)
        {
            int childLevel = level + 1;

            List<CategoriesModel> childItems = (from a in categories where a.ParentCategoryId == childItem.CategoryId select a).ToList();
            foreach (var cItem in childItems)
            {
                dropdown.Add(new CategoriesDropdownModel
                {
                    id = cItem.CategoryId.ToString(),
                    text = cItem.CategoryName,
                    level = childLevel
                });

                List<CategoriesModel> subChilds = (from a in categories where a.ParentCategoryId == cItem.CategoryId select a).ToList();
                if (subChilds.Count > 0)
                    ConstructChildItem(categories, dropdown, cItem, childLevel);
            }
        }
        #endregion
    }
}