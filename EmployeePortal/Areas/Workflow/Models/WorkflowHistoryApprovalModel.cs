﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Workflow.Models
{
    public class WorkflowHistoryApprovalModel
    {
        public int RequestorActionId { get; set; }
        public string RequestorActionName { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<UserApproval> UserApproval { get; set; }
    }

    public class UserApproval
    {
        public int UserActionId { get; set; }
        public string UserActionName { get; set; }
        public UserActivity Activity { get; set; }
    }

    public class UserActivity
    {
        public string Activity { get; set; }
        public string Noted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
