﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Workflow.Models
{
    public class WorkflowActionModel
    {
        public int WorkflowProcessId { get; set; }
        public int MasterDataUserId { get; set; }
        public string Noted { get; set; }
        public string ActionType { get; set; }
    }
}
