﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Workflow.Models
{
    public class WorkflowContextModel
    {
        public int WorkflowId { get; set; }
        public string WorkflowTypeCode { get; set; }
        public string WorkflowTitle { get; set; }
        public WorkflowPurchaseRequsitionContext WorkflowPurchaseRequsitionContext { get; set; }
        public List<NewFormatPRItem> NewFormatPRItem { get; set; }
    }

    public class WorkflowPurchaseRequsitionContext
    {
        public string PRNumber { get; set; }
        public DateTime RequestedDate { get; set; }
        public decimal GrandTotal { get; set; }
        public string Status { get; set; }
        public List<Item> ListItem { get; set; }
        public string LocationCode { get; set; }
        public string CompanyCode { get; set; }
        public int Year { get; set; }
        public decimal? PPN { get; set; }
        public decimal? GrandTotalAfterTax { get; set; }
    }

    public class Item
    {
        public string VendorName { get; set; }
        public string ItemName { get; set; }
        public decimal? Quantity { get; set; }
        public string Currency { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public string ItemCode { get; set; }
        public string BimsCode { get; set; }
        public decimal? TotalPriceReal { get; set; }
        public int? PPN { get; set; }
    }

    public class NewFormatPRItem
    {
        public string VendorName { get; set; }
        public List<Item> Items { get; set; }
        public int? PPN { get; set; }
        public decimal? PPNPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? GrandTotalAfterTax { get; set; }
    }
}
