﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Workflow.Models
{
    public class WorkflowRequestModel
    {
        /// <summary>
        /// Property Global
        /// </summary>
        public string WorkflowType { get; set; }
        /// <summary>
        /// Property Global
        /// </summary>
        public string WorkflowTitle { get; set; }
        /// <summary>
        /// Property for Purchase Requisition (PR)
        /// </summary>
        public string PRNumber { get; set; }
        /// <summary>
        /// Property Global
        /// </summary>
        public DateTime RequestedDate { get; set; }
        /// <summary>
        /// Property for Purchase Requisition (PR) 
        /// </summary>
        public decimal? GrandTotal { get; set; }
        /// <summary>
        /// Property Global
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Property Global
        /// </summary>
        public int WorkflowId { get; set; }
        /// <summary>
        /// Property Global
        /// </summary>
        public string ActivityStatus { get; set; }
        /// <summary>
        /// Property Global
        /// </summary>
        public string GrandTotalAfterTax { get; set; }
    }
}
