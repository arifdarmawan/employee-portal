﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeePortal.Areas.Procurement.Models.Budget;
using EmployeePortal.Areas.Procurement.Models.PurchaseRequistion;
using EmployeePortal.Areas.Workflow.Models;
using EmployeePortal.Controllers;
using EmployeePortal.GlobalParam;
using EmployeePortal.Models.ResponseModel;
using EmployeePortal.Service;
using FirstResource.System.Library.EnumMaster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace EmployeePortal.Areas.Workflow.Controllers
{
    [Area("Workflow")]
    [NoDirectAccess]
    public class ApprovalController : BaseController
    {
        readonly PortalService _portalService;
        readonly BIMSService _bimsService;
        readonly IConfiguration _configuration;
        public ApprovalController(PortalService portalService, BIMSService bimsService, IConfiguration configuration)
        {
            _portalService = portalService;
            _bimsService = bimsService;
            _configuration = configuration;
        }

        [Route("/Approval/Pending")]
        public async Task<IActionResult> Pending()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:CountWorkflows") + "?userId=" + UserId + "&status=" + FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.START.ToString());
            var totalPage = (response.Data + 5 - 1) / 5;

            ViewBag.TotalPage = totalPage;

            return View();
        }

        [Route("/Approval/PendingItem/{pageNumber}")]
        public async Task<PartialViewResult> _PendingItem(int pageNumber)
        {
            pageNumber = pageNumber - 1;

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetWorkflows")
                + "?userId=" + UserId
                + "&pageNumber=" + pageNumber
                + "&processStatus=" + FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.START.ToString());

            List<Models.WorkflowRequestModel> model = response.Data != null ? JsonConvert.DeserializeObject<List<Models.WorkflowRequestModel>>(Convert.ToString(response.Data)) : null;

            foreach (var single in model)
            {
                single.GrandTotalAfterTax = string.Format("{0:N2}", single.GrandTotal);
            }

            return PartialView(model);
        }

        [Route("/Approval/Approved")]
        public async Task<IActionResult> Approved()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:CountWorkflows") + "?userId=" + UserId + "&status=" + FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.END.ToString());
            var totalPage = (response.Data + 5 - 1) / 5;

            ViewBag.TotalPage = totalPage;

            return View();
        }

        [Route("/Approval/ApprovedItem/{pageNumber}")]
        public async Task<PartialViewResult> _ApprovedItem(int pageNumber)
        {
            pageNumber = pageNumber - 1;

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetWorkflows")
                + "?userId=" + UserId
                + "&pageNumber=" + pageNumber
                + "&processStatus=" + FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.END.ToString());
            List<Models.WorkflowRequestModel> model = response.Data != null ? JsonConvert.DeserializeObject<List<Models.WorkflowRequestModel>>(Convert.ToString(response.Data)) : null;

            foreach (var single in model)
            {
                single.GrandTotalAfterTax = string.Format("{0:N2}", single.GrandTotal);
            }

            return PartialView(model);
        }

        [Route("/Approval/Rejected")]
        public async Task<IActionResult> Rejected()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:CountWorkflows") + "?userId=" + UserId + "&status=" + FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.CANCEL.ToString());
            var totalPage = (response.Data + 5 - 1) / 5;

            ViewBag.TotalPage = totalPage;

            return View();
        }

        [Route("/Approval/RejectedItem/{pageNumber}")]
        public async Task<PartialViewResult> _RejectedItem(int pageNumber)
        {
            pageNumber = pageNumber - 1;

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetWorkflows")
                + "?userId=" + UserId
                + "&pageNumber=" + pageNumber
                + "&processStatus=" + FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.CANCEL.ToString());
            List<Models.WorkflowRequestModel> model = response.Data != null ? JsonConvert.DeserializeObject<List<Models.WorkflowRequestModel>>(Convert.ToString(response.Data)) : null;

            foreach (var single in model)
            {
                single.GrandTotalAfterTax = string.Format("{0:N2}", single.GrandTotal);
            }

            return PartialView(model);
        }

        [Route("/Approval/DetailItem/{workflowId}")]
        public async Task<PartialViewResult> _DetailItem(int workflowId, string workflowType)
        {
            var resWorkflowContext = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetWorkflowById") + "?workflowId=" + workflowId);
            var resWorkflowHistoryApproval = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetWorkflowHistoryById") + "?workflowId=" + workflowId);

            Models.WorkflowContextModel modelWorkflowContext = resWorkflowContext.Data != null ? JsonConvert.DeserializeObject<Models.WorkflowContextModel>(Convert.ToString(resWorkflowContext.Data)) : null;
            Models.WorkflowHistoryApprovalModel modelWorkflowHistoryApproval = resWorkflowHistoryApproval.Data != null ? JsonConvert.DeserializeObject<Models.WorkflowHistoryApprovalModel>(Convert.ToString(resWorkflowHistoryApproval.Data)) : null;
            var isWaitingApproval = modelWorkflowHistoryApproval.UserApproval.FirstOrDefault(n => n.UserActionId == Int32.Parse(UserId) && n.Activity.Activity == FirstResource.System.Library.Extensions.EnumExtensions.GetDescription(ActivityStatus.WAITING_APPROVAL).Result);

            var newFormatPRItem = modelWorkflowContext.WorkflowPurchaseRequsitionContext.ListItem.GroupBy(group => group.VendorName, (key, vendor) => new { VendorName = key, Items = vendor.ToList() });
            List<NewFormatPRItem> tempNewFormatPRItem = new List<NewFormatPRItem>();

            foreach (var single in newFormatPRItem)
            {
                tempNewFormatPRItem.Add(new NewFormatPRItem
                {
                    VendorName = single.VendorName,
                    Items = single.Items,
                    PPN = single.Items.Select(n => n.PPN).FirstOrDefault(),
                    PPNPrice = single.Items.Select(n => n.PPN).FirstOrDefault() * single.Items.Sum(n => n.TotalPrice) / 100,
                    TotalPrice = single.Items.Sum(n => n.TotalPrice),
                    GrandTotalAfterTax = single.Items.Sum(n => n.TotalPrice) + (single.Items.Select(n => n.PPN).FirstOrDefault() * single.Items.Sum(n => n.TotalPrice) / 100)
                });
            }

            modelWorkflowContext.NewFormatPRItem = tempNewFormatPRItem;

            ViewBag.WorkflowHistoryApproval = modelWorkflowHistoryApproval;
            ViewBag.MasterDataUserId = UserId;
            ViewBag.IsWaitingApproval = isWaitingApproval;

            return PartialView(modelWorkflowContext);
        }

        [HttpPost]
        [Route("/Approval/DetailBudget")]
        public async Task<PartialViewResult> _DetailBudget(ParamBudgetItem model)
        {
            var resBudget = await _bimsService.GetAsync(_configuration.GetValue<string>("FAPBIMSAPIEndpoint:GetItemBudget") + "?companyCode=" + model.CompanyCode + "&locationCode=" + model.LocationCode + "&budgetYear=" + model.Year + "&itemCode=" + model.BimsCode);
            var resItemRemaining = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRealizationBudgetItem") + "?bimsCode=" + model.BimsCode
                + "&year=" + model.Year);

            List<CreatePurchaseRequisitionItemModel> itemModel = resItemRemaining.Data != null ? JsonConvert.DeserializeObject<List<CreatePurchaseRequisitionItemModel>>(Convert.ToString(resItemRemaining.Data)) : null;
            BudgetModel budgetModel = resBudget.Data != null ? JsonConvert.DeserializeObject<BudgetModel>(Convert.ToString(resBudget.Data)) : null;

            if (budgetModel != null)
            {
                // Quantity
                budgetModel.QtyRealization = itemModel.Sum(t => Convert.ToDecimal(t.Qty));
                budgetModel.QtyRemaining = budgetModel.QtyBudget - budgetModel.QtyRealization;
                if (budgetModel.QtyRemaining < 0)
                {
                    budgetModel.QtyRemaining = 0;
                }
                budgetModel.QtyRequest = Convert.ToDecimal(model.Quantity);
                budgetModel.QtyRemainingTotal = budgetModel.QtyRemaining - budgetModel.QtyRequest;

                // Price
                budgetModel.TotalBudget = budgetModel.QtyBudget * budgetModel.ItemPrice;
                budgetModel.TotalBudgetRealization = Convert.ToDecimal(itemModel.Sum(t => t.TotalPrice));
                budgetModel.BudgetRemaining = budgetModel.TotalBudget - budgetModel.TotalBudgetRealization;
                if (budgetModel.BudgetRemaining < 0)
                {
                    budgetModel.BudgetRemaining = 0;
                }
                budgetModel.BudgetRequest =  Convert.ToDecimal(model.TotalPrice);
                budgetModel.BudgetRemainingTotal = budgetModel.BudgetRemaining - budgetModel.BudgetRequest;

                if (budgetModel.QtyRemainingTotal < 0)
                {
                    var tempQty = Convert.ToString(budgetModel.QtyRemainingTotal);
                    string qtyString = tempQty.Substring(1);
                    budgetModel.QtyRemainingTotal = Convert.ToDecimal(qtyString);

                    var tempBudget = Convert.ToString(budgetModel.BudgetRemainingTotal);
                    string budgetString = tempBudget.Substring(1);
                    budgetModel.BudgetRemainingTotal = Convert.ToDecimal(budgetString);

                }

                if (budgetModel.QtyRemaining > budgetModel.QtyRequest)
                {
                    budgetModel.BudgetStatus = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription(PurchaseRequisitionStatus.ONBUDGET).Result;
                }
                else
                {
                    budgetModel.BudgetStatus = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription(PurchaseRequisitionStatus.PTA).Result;
                }
            }

            return PartialView(budgetModel);
        }

        [HttpPost]
        [Route("/Approval/ApprovalAction")]
        public async Task<BaseResponse> ApprovalAction(WorkflowActionModel param)
        {
            BaseResponse response = new BaseResponse();

            WorkflowActionModel model = new WorkflowActionModel
            {
                WorkflowProcessId = param.WorkflowProcessId,
                MasterDataUserId = Int32.Parse(UserId),
                Noted = param.Noted
            };

            if (param.ActionType == "APPROVED")
                response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:WorkflowActionApproved"), model);
            else
                response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:WorkflowActionRejected"), model);

            return response;
        }
    }
}