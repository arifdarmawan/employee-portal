﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EmployeePortal.Service;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using EmployeePortal.Areas.Account.Models;
using EmployeePortal.Controllers;
using EmployeePortal.Models.ResponseModel;
using FirstResource.System.Library.EnumMaster;


namespace EmployeePortal.Areas.Account.Controllers
{
    [Area("Account")]
    public class AccountController : BaseController
    {
        readonly PortalService _portalService;
        readonly IConfiguration _configuration;
        readonly IHostingEnvironment _environment;
        readonly HRISService _hrisService;
        readonly string _baseHRISServerKey;

        public AccountController(
            PortalService portalService,
            IConfiguration configuration,
            IHostingEnvironment environment,
            HRISService hrisService)
        {
            _portalService = portalService;
            _configuration = configuration;
            _environment = environment;
            _hrisService = hrisService;
            _baseHRISServerKey = _configuration.GetValue<string>("BaseServer") + "HRISAPIEndpoint";
        }

        #region Login
        [HttpPost]
        [Route("/Account/Login")]
        public async Task<BaseResponse> Login(LoginViewModel model)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                Helper.EncryptionHelper.AESEncryptionHelper(model);
                
                if (model.UserId.Contains("."))
                {
                    var userAccess = new string[] { "hilman.lukito", "helen.zhu", "arif.darmawan", "aldo.sitorus" , "akbar.primasaputra" };

                    if (!userAccess.Contains(model.UserId))
                    {
                        var firstWord = model.UserId.Substring(0, model.UserId.IndexOf("."));

                        response.Code = (int)HttpStatusCode.Forbidden;
                        response.Status = "Access Denied";
                        response.Message = "Sorry " + char.ToUpper(firstWord.First()) + firstWord.Substring(1).ToLower() + ",  you are not allowed to access this page.";
                    }
                    else
                    {
                        var resUser = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetUserByEmailOrUserId") + "/?email=" + model.UserId);
                        var userModel = resUser.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUser.Data)) : null;

                        if (resUser.Code == (int)HttpStatusCode.OK)
                        {
                            dynamic resUserAD;

                            if (userModel == null)  // insert user ad
                            {
                                resUserAD = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertUserFromAD"), model);

                                if (resUserAD.Code == (int)HttpStatusCode.OK)
                                {
                                    userModel = resUserAD.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUserAD.Data)) : null;

                                    response = resUserAD;
                                    response.SessionUserLevel = UserLevel;
                                }
                                else if (resUserAD.Code == (int)CustomHttpStatusCode.UserAdNotRegistered)
                                {
                                    response.Code = resUserAD.Code;
                                    response.Message = resUserAD.Message;
                                }
                                else
                                {
                                    response.Code = resUserAD.Code;
                                    response.Status = resUserAD.Status;
                                    response.Message = resUserAD.Message;
                                }
                            }
                            else // update user ad
                            {
                                resUserAD = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateUserFromAD"), model);

                                if (resUserAD.Code == (int)HttpStatusCode.OK)
                                {
                                    userModel = resUserAD.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUserAD.Data)) : null;

                                    response = resUserAD;
                                    response.SessionUserLevel = UserLevel;
                                }
                                else if (resUserAD.Code == (int)HttpStatusCode.BadRequest)
                                {
                                    response.Code = resUserAD.Code;
                                    response.Status = resUserAD.Status;
                                    response.Message = resUserAD.Message;
                                }
                                else
                                {
                                    response.Code = resUserAD.Code;
                                    response.Status = resUserAD.Status;
                                    response.Message = resUserAD.Message;
                                }
                            }

                            if (resUserAD.Code == (int)HttpStatusCode.OK)
                            {
                                SetCookie(userModel.Id, userModel.UserLevel);
                                SetCookieUserId(model.UserId);

                                // Set Cookie for Profile Global Setting
                                SetCookieUsername(userModel.UserName);
                                SetCookieJobTitle(userModel.Jabatan);
                                SetCookieUserEmailAD(userModel.Email);
                            }
                        }
                        else
                        {
                            response.Code = (int)HttpStatusCode.InternalServerError;
                            response.Status = resUser.Status;
                            response.Message = resUser.Message;
                        }
                    }

                }
                else
                {
                    response.Message = "Username is a not valid!";
                }

            }
            catch (Exception e)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Message = "Internat Server Error. Please Contact Administrator.";
            }
          
            return response;
        }

        [Route("/Account/Logout")]
        public IActionResult Logout()
        {
            // Clear all session
            HttpContext.Session.Clear();

            return View();
        }
        #endregion
    }
}