﻿using Microsoft.AspNetCore.Mvc;
using EmployeePortal.Controllers;

namespace EmployeePortal.Areas.Employee.Controllers
{
    [Area("Dashboard")]
    [NoDirectAccess]
    public class DashboardController : BaseController
    {
        [Route("/Dashboard")]
        [Route("/Dashboard/Index")]
        public IActionResult Index()
        {
            return View();
        }
    }
}