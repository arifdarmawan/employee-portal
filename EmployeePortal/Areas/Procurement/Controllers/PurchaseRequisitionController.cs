﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EmployeePortal.Areas.Procurement.Models.Budget;
using EmployeePortal.Areas.Procurement.Models.Item;
using EmployeePortal.Areas.Procurement.Models.PurchaseRequistion;
using EmployeePortal.Areas.Procurement.Models.WorkflowGroupMember;
using EmployeePortal.Areas.Vendor.Models.Vendor;
using EmployeePortal.Areas.Vendor.Models.VendorPayment;
using EmployeePortal.Areas.Vendor.Models.VendorTax;
using EmployeePortal.Controllers;
using EmployeePortal.Models.ResponseModel;
using EmployeePortal.Service;
using FirstResource.System.Library.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace EmployeePortal.Areas.Procurement.Controllers
{
    //[NoDirectAccess]
    [Area("Procurement")]
    public class PurchaseRequisitionController : BaseController
    {
        PortalService _portalService;
        HRISService _hrisService;
        BIMSService _bimsService;
        IConfiguration _configuration;
        IHostingEnvironment _environment;
        readonly string _baseHRISServerKey;

        public PurchaseRequisitionController(PortalService portalService, HRISService hRISService,BIMSService bIMSService,  IConfiguration configuration, IHostingEnvironment environment) 
        {
            _portalService = portalService;
            _configuration = configuration;
            _environment = environment;
            _hrisService = hRISService;
            _bimsService = bIMSService;
            _baseHRISServerKey = _configuration.GetValue<string>("BaseServer") + "HRISAPIEndpoint";
        }

        #region CREATE PR
        [Route("/PurchaseRequisition")]
        [Route("/PurchaseRequisition/Index")]
        public async Task<IActionResult> Index()
        {
            CreatePurchaseRequisitionModel model = new CreatePurchaseRequisitionModel();

            var result = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetAccessPurchaseRequisition") + "?userid=" + UserId);
            WorkflowGroupMemberModel member = result.Data != null ? JsonConvert.DeserializeObject<WorkflowGroupMemberModel>(Convert.ToString(result.Data)) : null;

            if (result.Data != null)
            {
                var reslocation = await _bimsService.GetAsync(_configuration.GetValue<string>("FAPBIMSAPIEndpoint:GetAllBusinessUnitByCompanyCode") + "?companycode=" + member.CompanyCode);
                var rescurrency = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetCurrency"));
                var resunitofmeasure = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetUnitOfMeasure"));
                var resitem = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetItem"));

                List<BusinessUnitModel> company = reslocation.Data != null ? JsonConvert.DeserializeObject<List<BusinessUnitModel>>(Convert.ToString(reslocation.Data)) : null;
                List<CurrencyModel> currency = rescurrency.Data != null ? JsonConvert.DeserializeObject<List<CurrencyModel>>(Convert.ToString(rescurrency.Data)) : null;
                List<UnitOfMeasureModel> unitofmeasure = resunitofmeasure.Data != null ? JsonConvert.DeserializeObject<List<UnitOfMeasureModel>>(Convert.ToString(resunitofmeasure.Data)) : null;
                List<ItemModel> item = resitem.Data != null ? JsonConvert.DeserializeObject<List<ItemModel>>(Convert.ToString(resitem.Data)) : null;

                model.CompanyLocationOptions = model.ConstructCompanyLocationOptions(company);
                model.CurrencyOptions = model.ConstructCurrencyOptions(currency);
                model.UnitOfMeasureOptions = model.ConstructUnitOfMeasureOptions(unitofmeasure);
                model.ItemOptions = model.ConstructItemOptions(item);

                ViewData["CurrentTime"] = DateTime.Now.ToString("dd-MM-yyyy");
                ViewData["ConfigCompanyId"] = member.ConfigCompanyId;
                ViewData["UserCompanyCode"] = member.CompanyCode;
                ViewData["UserCompany"] = member.CompanyName;
                ViewData["ConfigDivisionId"] = member.ConfigDivisionId;
                ViewData["UserDivisionCode"] = member.DivisionCode;
                ViewData["UserDivision"] = member.DivisionName;
            }
            else
            {
                result.Message = "You not have access this Page";
            }

            return View(model);
        }

        [Route("/Procurement/SearchItemByKey")]
        public async Task<JsonResult> SearchItemByKey(string query)
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:SearchItemByKey") + "?key=" + query);

            return Json(response.Data);
        }

        [Route("/Procurement/GetItemBudget/{company}/{location}/{budgetyear}/{bimsCode}/{qty}/{totalprice}")]
        public async Task<PartialViewResult> Budget(string company, string location, int budgetYear, string bimsCode, string qty, string totalprice)
        {

            decimal decimalQty = Convert.ToDecimal(qty);
            decimal decimalPrice = Convert.ToDecimal(totalprice);

            var resBudget = await _bimsService.GetAsync(_configuration.GetValue<string>("FAPBIMSAPIEndpoint:GetItemBudget") + "?companyCode=" + company + "&locationCode=" + location + "&budgetYear=" + budgetYear + "&itemCode=" + bimsCode);
            var resPrice = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorItemPrice") + "?bimsCode=" + bimsCode);
            var resItemRemaining = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRealizationBudgetItem") + "?bimsCode=" + bimsCode + "&year=" + budgetYear);

            BudgetModel model = resBudget.Data != null ? JsonConvert.DeserializeObject<BudgetModel>(Convert.ToString(resBudget.Data)) : null;
            VendorItemModel vendorPrice = resPrice.Data != null ? JsonConvert.DeserializeObject<VendorItemModel>(Convert.ToString(resPrice.Data)) : null;
            List<CreatePurchaseRequisitionItemModel> item = resItemRemaining.Data != null ? JsonConvert.DeserializeObject<List<CreatePurchaseRequisitionItemModel>>(Convert.ToString(resItemRemaining.Data)) : null;

            if (model != null)
            {
                //model.QtyBudget = 5;
                //model.ItemPrice = 1000;

                // Quantity
                model.QtyRealization = item.Sum(t => Convert.ToDecimal(t.Qty));
                model.QtyRemaining = model.QtyBudget - model.QtyRealization;
                if (model.QtyRemaining < 0) {
                    model.QtyRemaining = 0;
                }
                model.QtyRequest = decimalQty;
                model.QtyRemainingTotal = model.QtyRemaining - model.QtyRequest;

                // Price
                model.TotalBudget = model.QtyBudget * model.ItemPrice;
                model.TotalBudgetRealization = item.Sum(t => t.TotalPrice).Value;
                model.BudgetRemaining = model.TotalBudget - model.TotalBudgetRealization;
                if (model.BudgetRemaining < 0)
                {
                    model.BudgetRemaining = 0;
                }
                model.BudgetRequest = decimalPrice;
                model.BudgetRemainingTotal = model.BudgetRemaining - model.BudgetRequest;

                if (model.QtyRemainingTotal < 0)
                {
                    var tempQty = Convert.ToString(model.QtyRemainingTotal);
                    string qtyString = tempQty.Substring(1);
                    model.QtyRemainingTotal = Convert.ToDecimal(qtyString);

                    var tempBudget = Convert.ToString(model.BudgetRemainingTotal);
                    string budgetString = tempBudget.Substring(1);
                    model.BudgetRemainingTotal = Convert.ToDecimal(budgetString);

                }

                if (model.QtyRemaining > model.QtyRequest)
                {
                    model.BudgetStatus = EnumExtensions.GetDescription(FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.ONBUDGET).Result;
                }
                else 
                {
                    model.BudgetStatus = EnumExtensions.GetDescription(FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.PTA).Result;
                }
            }

            ViewData["Budget"] = model;

            return PartialView(model);
        }

        [HttpPost]
        [Route("Procurement/InsertPurchaseRequisition")]
        public async Task<BaseResponse> InsertPurchaseRequisition(CreatePurchaseRequisitionModel values)
        {
            var year = DateTime.Now.Year;
            decimal RemainingQty;
            decimal? ppnPrice;
            double grandTotal = 0;

            values.PRNumber = "PR-" + year + "-" + values.CompanyCode + "-" + values.DepartementCode;

            values.CreatedBy = UserName;
            values.CreatedDate = DateTime.Now;
            values.ModifiedBy = UserName;
            values.ModifiedDate = DateTime.Now;
            values.MasterDataUserId = Convert.ToInt32(UserId);

            List<CreatePurchaseRequisitionItemModel> itemModels = new List<CreatePurchaseRequisitionItemModel>();
            List<PPNVendorCategoryItemModel> listVendorPPNModels = new List<PPNVendorCategoryItemModel>();

            foreach (var singleItem in values.ListItem)
            {
                var resItemRemaining = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRealizationBudgetItem") + "?itemCode=" + singleItem.ItemCode + "&year=" + year);
                List<CreatePurchaseRequisitionItemModel> listPOItemModel = resItemRemaining.Data != null ? JsonConvert.DeserializeObject<List<CreatePurchaseRequisitionItemModel>>(Convert.ToString(resItemRemaining.Data)) : null;

                var resBudget = await _bimsService.GetAsync(_configuration.GetValue<string>("FAPBIMSAPIEndpoint:GetItemBudget") + "?companyCode=" + values.CompanyCode + "&locationCode=" + values.CompanyLocationCode + "&budgetYear=" + year + "&itemCode=" + singleItem.ItemCode);
                BudgetModel budgetItemModel = resBudget.Data != null ? JsonConvert.DeserializeObject<BudgetModel>(Convert.ToString(resBudget.Data)) : null;

                if (budgetItemModel != null)
                {
                    // Quantity
                    budgetItemModel.QtyRealization = listPOItemModel.Sum(t => Convert.ToDecimal(t.Qty));
                    budgetItemModel.QtyRemaining = budgetItemModel.QtyBudget - budgetItemModel.QtyRealization;

                    RemainingQty = budgetItemModel.QtyRemaining - Convert.ToDecimal(singleItem.Qty);

                    if (RemainingQty > 0)
                    {
                        singleItem.BudgetStatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.ONBUDGET.ToString();
                    }
                    else
                    {
                        singleItem.BudgetStatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.PTA.ToString();
                    }
                }
                else
                {
                    singleItem.BudgetStatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.PTA.ToString();
                }

                var resVendorItems = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetCheckVendorItem") + "?itemId=" + singleItem.MasterItemId);

                if (resVendorItems.Data != null)
                {
                    singleItem.ItemVendorStatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.VENDOR_IS_EXIST.ToString();
                    singleItem.IsRequestVendor = false;
                }
                else
                {
                    singleItem.ItemVendorStatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.REQUEST_VENDOR.ToString();
                    singleItem.IsRequestVendor = true;
                }

                var resvendorppn = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetPpnVendorItemCategories") + "?itemId=" + singleItem.MasterItemId);
                PPNVendorCategoryItemModel vendorPPNModel = resvendorppn.Data != null ? JsonConvert.DeserializeObject<PPNVendorCategoryItemModel>(Convert.ToString(resvendorppn.Data)) : null;

                if (vendorPPNModel != null)
                {
                    singleItem.PPN = vendorPPNModel.PPN;
                    ppnPrice = singleItem.TotalPrice * singleItem.PPN / 100;

                    var double_ppn = Convert.ToDouble(ppnPrice);

                    var calc = Convert.ToDouble(singleItem.TotalPrice) + double_ppn;

                    grandTotal += calc;
                }

                if (singleItem.UnitPrice == null) 
                {
                    singleItem.UnitPrice = 0;
                }
                itemModels.Add(singleItem);
            }
            if (grandTotal > 0)
            {
                values.GrandTotal = Convert.ToDecimal(grandTotal);
            }
            // check whether one of request item have status PTA
            #region SET PR STATUS
            var checkStatus = itemModels.Find(i => i.BudgetStatus == EnumExtensions.GetDescription(FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.PTA).Result);

            if (checkStatus != null)
            {
                values.Prstatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.PTA.ToString();
            }
            else
            {
                values.Prstatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.WAITING_APPROVAL.ToString();
            }
            #endregion

            var response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertPurchaseRequisition"), values);

            return response;
        }

        [Route("/Procurement/GetPurchaseRequisitionItemOnProcess/{company}/{bimsCode}")]
        public async Task<BaseResponse> GetPurchaseRequisitionItemOnProcess(string company, string bimsCode)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetPurchaseRequisitionItemOnProcess") + "/?company=" + company + "&bimsCode=" + bimsCode);
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                throw;
            }

            return response.Data;
        }
        #endregion

        #region PR STATUS
        [Route("/PurchaseRequisition")]
        [Route("/PurchaseRequisition/Status")]
        public IActionResult Status()
        {
            return View();
        }

        [Route("/Procurement/GetPurchaseRequisitionList")]
        public async Task<string> GetPurchaseRequisitionList()
        {
            var dataTables = new List<PurchaseRequisitionListHeaderModel>(); 
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetPurchaseRequisition"));

            List<PurchaseRequisitionListHeaderModel> purchaseRequisitions = response.Data != null ? JsonConvert.DeserializeObject<List<PurchaseRequisitionListHeaderModel>>(Convert.ToString(response.Data)) : null;

            foreach (var data in purchaseRequisitions)
            {
                dataTables.Add(new PurchaseRequisitionListHeaderModel
                {
                    Id = data.Id,
                    PRNumber = data.PRNumber,
                    RequestedFrom = data.RequestedFrom,
                    Currency = data.Currency,
                    StringGrandTotalAfterTax = string.Format("{0:N2}", data.GrandTotal),
                    Prstatus = data.Prstatus,
                    StringDate = string.Format("{0:dd-MM-yyyy}", data.ModifyDate)
                });
            }
            return JsonConvert.SerializeObject(dataTables);
        }

        [Route("/Procurement/PurchaseRequisitionStatusDetail/{id}/{prnumber}")]
        public async Task<IActionResult> PurchaseRequisitionStatusDetail(int id, string prnumber)
        {
            PurchaseRequisitionListHeaderModel model = new PurchaseRequisitionListHeaderModel();
            List<PurchaseRequisitionVendorCategoryItemModel> vendorModel = new List<PurchaseRequisitionVendorCategoryItemModel>(); 
            List<PurchaseRequistionListItemModel> itemModel = new List<PurchaseRequistionListItemModel>();

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetPurchaseRequisitionById") + "?id=" + id);

            if (response.Data != null)
            {
                model = response.Data != null ? JsonConvert.DeserializeObject<PurchaseRequisitionListHeaderModel>(Convert.ToString(response.Data)) : null;
                model.StringGrandTotal = string.Format("{0:N2}", model.GrandTotal);

                foreach (var item in model.ListItem) 
                {
                    PurchaseRequisitionVendorCategoryItemModel tempVendor = new PurchaseRequisitionVendorCategoryItemModel();

                    var resVendor = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetCheckVendorItem") + "?itemId=" + item.MasterItemId);
                    tempVendor = resVendor.Data != null ? JsonConvert.DeserializeObject<PurchaseRequisitionVendorCategoryItemModel>(Convert.ToString(resVendor.Data)) : null;

                    if (tempVendor != null) 
                    {
                        item.VendorName = tempVendor.VendorName;
                    }

                    itemModel.Add(item);
                }
                ViewData["List_Item"] = itemModel;

                var result = itemModel.GroupBy(o => o.VendorName)
                    .Select(g => new
                    {
                        VendorName = g.Key,
                        TotalPrice = g.Sum(i => i.TotalPrice)
                    });

                foreach (var a in result)
                {
                    var getPpn = (from res in itemModel
                                  where res.VendorName == a.VendorName
                                  select new
                                  {
                                      res.PPN
                                  }).Distinct().SingleOrDefault();

                    decimal resPricePpn = a.TotalPrice * getPpn.PPN / 100;

                    vendorModel.Add(new PurchaseRequisitionVendorCategoryItemModel
                    {
                        VendorName = a.VendorName,
                        Ppn = getPpn.PPN,
                        PpnPrice = resPricePpn,
                        TotalPrice = a.TotalPrice,
                        TotalPriceAfterTax = a.TotalPrice + resPricePpn
                    });
                }

                ViewData["Vendor_Name"] = vendorModel;
            }

            var resWorkflowHistoryApproval = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetWorkflowHistoryById") + "?workflowId=" + model.ProcessId);
            Workflow.Models.WorkflowHistoryApprovalModel modelWorkflowHistoryApproval = resWorkflowHistoryApproval.Data != null ? JsonConvert.DeserializeObject<EmployeePortal.Areas.Workflow.Models.WorkflowHistoryApprovalModel>(Convert.ToString(resWorkflowHistoryApproval.Data)) : null;

            ViewBag.WorkflowHistoryApproval = modelWorkflowHistoryApproval;
            ViewData["prNumber"] = prnumber;

            return View(model);
        }
        #endregion

        #region PR REQUEST VENDOR
        [Route("/PurchaseRequisition")]
        [Route("/PurchaseRequisition/RFQ")]
        public async Task<IActionResult> RFQ()
        {
            return View();
        }

        [Route("/Procurement/GetRFQList")]
        public async Task<string> GetRFQList()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRFQList"));
            var dataTables = new List<PurchaseRequistionListItemModel>();

            List<PurchaseRequistionListItemModel> models = response.Data != null ? JsonConvert.DeserializeObject<List<PurchaseRequistionListItemModel>>(Convert.ToString(response.Data)) : null;

            foreach (var item in models)
            {
                dataTables.Add(new PurchaseRequistionListItemModel
                {
                    MasterPurchaseRequisitionId = item.MasterPurchaseRequisitionId,
                    PRNumber = item.PRNumber,
                    MasterItemId = item.MasterItemId,
                    ItemName = item.ItemName,
                    Specification = item.Specification,
                    Qty = item.Qty,
                    TotalPrice = item.TotalPrice,
                    ItemVendorStatus = item.ItemVendorStatus
                });
            }

            return JsonConvert.SerializeObject(dataTables);
        }

        [Route("/Procurement/RFQDetail/{masterpurchaserequisitionId}/{masterItemId}")]
        public async Task<IActionResult> RFQDetail(int masterpurchaserequisitionId, int masterItemId)
        {
            PurchaseRequisitionRequestVendorModel model = new PurchaseRequisitionRequestVendorModel();
            
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRFQDetail") + "?id=" + masterpurchaserequisitionId + "&masterItemId=" + masterItemId);

            if (response.Data != null) 
            {
                model = response.Data != null ? JsonConvert.DeserializeObject<PurchaseRequisitionRequestVendorModel>(Convert.ToString(response.Data)) : null;
            }

            var resWorkflowHistoryApproval = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetWorkflowHistoryById") + "?workflowId=" + model.ProcessId);
            Workflow.Models.WorkflowHistoryApprovalModel modelWorkflowHistoryApproval = resWorkflowHistoryApproval.Data != null ? JsonConvert.DeserializeObject<Workflow.Models.WorkflowHistoryApprovalModel>(Convert.ToString(resWorkflowHistoryApproval.Data)) : null;

            ViewBag.WorkflowHistoryApproval = modelWorkflowHistoryApproval;

            return View(model);
        }

        [HttpPost]
        [Route("Procurement/BlastToVendor")]
        public async Task<BaseResponse> GenerateRequestForQuotation(RequestForQuotationModel data)
        {
            var year = DateTime.Now.Year;

            data.Rfqnumber = "RFQ-" + year + "-" + data.CompanyCode + "-" + data.DepartementCode;
            data.Qcfnumber = "QCF-" + year + "-" + data.CompanyCode + "-" + data.DepartementCode;
            data.CreatedBy = UserName;
            data.CreatedDate = DateTime.Now;
            data.ModifyBy = UserName;
            data.ModifyDate = DateTime.Now;

            var response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:BlastToVendor"), data);

            return response;
        }

        [Route("/PurchaseRequisition")]
        [Route("/PurchaseRequisition/QCF")]
        public async Task<IActionResult> QCF()
        {
            return View();
        }

        [Route("/Procurement/GetQCFList")]
        public async Task<string> GetQCFList()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetQCFList"));
            var dataTables = new List<PurchaseRequistionListItemModel>();

            List<PurchaseRequistionListItemModel> models = response.Data != null ? JsonConvert.DeserializeObject<List<PurchaseRequistionListItemModel>>(Convert.ToString(response.Data)) : null;

            foreach (var item in models)
            {
                dataTables.Add(new PurchaseRequistionListItemModel
                {
                    Id = item.Id,
                    MasterPurchaseRequisitionId = item.MasterPurchaseRequisitionId,
                    PRNumber = item.PRNumber,
                    ItemVendorStatus = item.ItemVendorStatus,
                    MasterItemId = item.MasterItemId,
                    ItemName = item.ItemName,
                    Specification = item.Specification,
                    Qty = item.Qty,
                    TotalPrice = item.TotalPrice
                });
            }

            return JsonConvert.SerializeObject(dataTables);
        }

        [Route("/Procurement/QCFDetail/{masterpurchaserequisitionId}/{masterItemId}")]
        public async Task<IActionResult> QCFDetail(int masterpurchaserequisitionId, int masterItemId)
        {
            PurchaseRequisitionRequestVendorModel model = new PurchaseRequisitionRequestVendorModel();

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetQCFDetail") + "?id=" + masterpurchaserequisitionId + "&masterItemId=" + masterItemId);

            if (response.Data != null)
            {
                model = response.Data != null ? JsonConvert.DeserializeObject<PurchaseRequisitionRequestVendorModel>(Convert.ToString(response.Data)) : null;
            }

            var resWorkflowHistoryApproval = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetWorkflowHistoryById") + "?workflowId=" + model.ProcessId);
            Workflow.Models.WorkflowHistoryApprovalModel modelWorkflowHistoryApproval = resWorkflowHistoryApproval.Data != null ? JsonConvert.DeserializeObject<Workflow.Models.WorkflowHistoryApprovalModel>(Convert.ToString(resWorkflowHistoryApproval.Data)) : null;

            ViewBag.WorkflowHistoryApproval = modelWorkflowHistoryApproval;

            var resGetRFQ = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRequestForQuotationPRId") + "?prId=" + masterpurchaserequisitionId);
            List<RequestForQuotationModel> dataRFQ = resGetRFQ.Data != null ? JsonConvert.DeserializeObject<List<RequestForQuotationModel>>(Convert.ToString(resGetRFQ.Data)) : null;

            ViewData["RequestforQuotationList"] = dataRFQ;

            return View(model);
        }


        [Route("/Procurement/getQuotationComparationFormByPrId/{prid}/{masterItemId}")]
        public async Task<BaseResponse> getQuotationComparationFormByPrId(int prid, int masterItemId) 
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:getQuotationComparationFormByPrId") + "/?company=" + prid + "&company=" + masterItemId);
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                throw;
            }

            return response.Data;
        }

        [Route("/Procurement/GetListVendor/{id}/{masterItemId}")]
        public async Task<IActionResult> QCFListVendor(int Id, int masterItemId)
        {
            var res = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRequestForQuotationPRId") + "?prId=" + Id + "&masterItemId=" + masterItemId);
            List<RequestForQuotationModel> model = res.Data != null ? JsonConvert.DeserializeObject<List<RequestForQuotationModel>>(Convert.ToString(res.Data)) : null;
            
            return PartialView(model);
        }

        [Route("/Procurement/GetDetailVendor/{rfqId}")]
        public async Task<IActionResult> QCFDetailVendor(int rfqId)
        {
            var res = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRequestForQuotationDetailByRfqId") + "?rfqId=" + rfqId);
            VendorDetailRFQModel model = res.Data != null ? JsonConvert.DeserializeObject<VendorDetailRFQModel>(Convert.ToString(res.Data)) : null;

            var resBasicInfo = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "?userId=" + model.MasterDataUserId);

            var resVendorPayment = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorPaymentTypeInfo") + "?masterDataUserId=" + model.MasterDataUserId);
            var resVendorTax = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorTax") + "?masterDataUserId=" + model.MasterDataUserId);

            VendorBasicInfoModel vendor = resBasicInfo.Data != null ? JsonConvert.DeserializeObject<VendorBasicInfoModel>(Convert.ToString(resBasicInfo.Data)) : null;
            VendorPaymentModel modelVendorPayment = resVendorPayment.Data != null ? JsonConvert.DeserializeObject<VendorPaymentModel>(Convert.ToString(resVendorPayment.Data)) : null;
            VendorTaxModel modelVendorTax = resVendorTax.Data != null ? JsonConvert.DeserializeObject<VendorTaxModel>(Convert.ToString(resVendorTax.Data)) : null;

            model.VendorName = vendor.Name;
            model.VendorAddress = vendor.Address;
            model.VendorPhone = vendor.CompanyPhone;

            model.VendorCity = vendor.City;
            model.VendorRegion = vendor.Region;
            model.VendorPostalCode = vendor.PostalCode;

            model.VendorTOP = modelVendorPayment.Top;
            model.VendorPpn = modelVendorTax.Ppn;

            return PartialView(model);
        }

        [Route("/Procurement/QCFPreview/{masterpurchaserequisitionId}/{masterItemId}")]
        public async Task<IActionResult> QCFPreview(int masterpurchaserequisitionId, int masterItemId)
        {
            int counter = 1;
            QuotationComparationFormModel model = new QuotationComparationFormModel();

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:QuotationComparationFormHeader") + "?prId=" + masterpurchaserequisitionId + "&masterItemId=" + masterItemId);

            if (response.Data != null)
            {
                model = response.Data != null ? JsonConvert.DeserializeObject<QuotationComparationFormModel>(Convert.ToString(response.Data)) : null;
            }

            var getDetail = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:QuotationComparationFormItem") + "?prId=" + masterpurchaserequisitionId + "&masterItemId=" + masterItemId);

            foreach (var res in getDetail.Data)
            {
                if (counter == 1)
                {
                    model.MasterDataVendorId1 = res.masterDataVendorId;
                    model.VendorName1 = res.vendorName;
                    model.Pic1 = res.pic;
                    model.Picphone1 = res.picphone;
                    model.Ppn1 = res.ppn;
                    model.UnitPrice1 = res.unitPrice;
                    model.TotalPrice1 = res.totalPrice;

                    model.PPNPrice1 = model.UnitPrice1 * model.Ppn1 / 100;
                    model.GrandTotalAfterTax1 = model.TotalPrice1 + model.PPNPrice1;

                    model.DeliveryTime1 = res.deliveryTime;
                }

                if (counter == 2)
                {
                    model.MasterDataVendorId2 = res.masterDataVendorId;
                    model.VendorName2 = res.vendorName;
                    model.Pic2 = res.pic;
                    model.Picphone2 = res.picphone;
                    model.Ppn2 = res.ppn;
                    model.UnitPrice2 = res.unitPrice;
                    model.TotalPrice2 = res.totalPrice;

                    model.PPNPrice2 = model.UnitPrice2 * model.Ppn2 / 100;
                    model.GrandTotalAfterTax2 = model.TotalPrice2 + model.PPNPrice2;

                    model.DeliveryTime2 = res.deliveryTime;
                }

                if (counter == 3)
                {
                    model.MasterDataVendorId3 = res.masterDataVendorId;
                    model.VendorName3 = res.vendorName;
                    model.Pic3 = res.pic;
                    model.Picphone3 = res.picphone;
                    model.Ppn3 = res.ppn;
                    model.UnitPrice3 = res.unitPrice;
                    model.TotalPrice3 = res.totalPrice;

                    model.PPNPrice3 = model.UnitPrice3 * model.Ppn3 / 100;
                    model.GrandTotalAfterTax3 = model.TotalPrice3 + model.PPNPrice3;

                    model.DeliveryTime3 = res.deliveryTime;
                }
                counter++;
            }

            return View(model);
        }

        [HttpPost]
        [Route("/Procurement/ProcessQuotationComparationForm")]
        public async Task<BaseResponse> ProcessQuotationComparationForm(QuotationComparationFormModel param)
        {
            BaseResponse response = new BaseResponse();
            
            try
            {
                RequestForQuotationModel model = null;

                var result = await _portalService.PutAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:ProcessQuotationComparationForm") + "?PrId=" + param.Prid + "&masterDataVendorId=" + param.MasterDataVendorId + "&notes=" + param.Notes + "&modifyBy=" + UserName, model);
                
                if (result.Code == (int)HttpStatusCode.OK)
                {
                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Proses QCF Berhasil.";
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Message = ex.ToString();
            }

            return response;
        }
        #endregion
    }
}
