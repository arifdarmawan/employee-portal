﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeePortal.Areas.Procurement.Models.PurchaseOrder;
using EmployeePortal.Controllers;
using EmployeePortal.Service;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace EmployeePortal.Areas.Procurement.Controllers
{
    [NoDirectAccess]
    [Area("Procurement")]
    public class PurchaseOrderController : BaseController
    {
        PortalService _portalService;
        HRISService _hrisService;
        BIMSService _bimsService;
        IConfiguration _configuration;
        IHostingEnvironment _environment;
        readonly string _baseHRISServerKey;

        public PurchaseOrderController(PortalService portalService, HRISService hRISService,BIMSService bIMSService,  IConfiguration configuration, IHostingEnvironment environment) 
        {
            _portalService = portalService;
            _configuration = configuration;
            _environment = environment;
            _hrisService = hRISService;
            _bimsService = bIMSService;
            _baseHRISServerKey = _configuration.GetValue<string>("BaseServer") + "HRISAPIEndpoint";
        }

        [Route("/PurchaseOrder")]
        [Route("/PurchaseOrder/PurchaseOrder")]
        public IActionResult PurchaseOrder()
        {
            return View();
        }

        [Route("/Procurement/GetPurchaseOrder")]
        public async Task<string> GetPurchaseOrder()
        {
            var dataTables = new List<PurchaseOrderHeaderModel>(); 
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetPurchaseOrder"));

            List<PurchaseOrderHeaderModel> purchaseOrder = response.Data != null ? JsonConvert.DeserializeObject<List<PurchaseOrderHeaderModel>>(Convert.ToString(response.Data)) : null;

            foreach (var data in purchaseOrder)
            {
                dataTables.Add(new PurchaseOrderHeaderModel
                {
                    Id = data.Id,
                    PONumber = data.PONumber,
                    PODate = string.Format("{0:dd-MM-yyyy}", data.CreatedDate),
                    POStatus = data.POStatus,
                    StringGrandTotalAfterTax = string.Format("{0:N2}", data.GrandTotal)
                });
            }
            return JsonConvert.SerializeObject(dataTables);
        }

        [Route("/Procurement/PurchaseOrderDetail/{id}")]
        public async Task<IActionResult> PurchaseOrderDetail(int id)
        {
            PurchaseOrderHeaderModel model = new PurchaseOrderHeaderModel();
            List<PurchaseOrderVendorCategoryItemModel> vendorModel = new List<PurchaseOrderVendorCategoryItemModel>();
            List<PurchaseOrderItemModel> itemModel = new List<PurchaseOrderItemModel>();

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetPurchaseOrderItemById") + "?id=" + id);

            if (response.Data != null)
            {
                model = response.Data != null ? JsonConvert.DeserializeObject<PurchaseOrderHeaderModel>(Convert.ToString(response.Data)) : null;

                decimal temptotal = model.ListItem
                 .Sum(item => item.TotalPrice);

                int tempppn = model.ListItem
                    .Select(x => x.PPN)
                    .FirstOrDefault();

                decimal ppnPrice = temptotal * tempppn / 100;
                model.GrandTotalAfterTax = model.GrandTotal + ppnPrice;

                model.StringGrandTotal= string.Format("{0:N2}", temptotal);
                model.Ppn = tempppn;
                model.stringPpn = string.Format("{0:N2}", ppnPrice);
                model.StringGrandTotalAfterTax = string.Format("{0:N2}", model.GrandTotalAfterTax);


                ViewData["List_Item"] = model.ListItem;


            }

            return View(model);
        }
    }
}