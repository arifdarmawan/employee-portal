﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class CreatePurchaseRequisitionModel
    {
        public int Id { get; set; }
        public string PRNumber { get; set; }
        public string RequestedFrom { get; set; }
        public int ConfigCompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyLocationCode { get; set; }
        public int ConfigDivisionId { get; set; }
        public string CompanyName { get; set; }
        public string DepartementCode { get; set; }
        public string DepartementName { get; set; }
        public int MasterItemId { get; set; }        
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int CurrencyId { get; set; }
        public int UomId { get; set; }
        public string Prstatus { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal GrandTotal { get; set; }
        public DateTime? DateRequired { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public int MasterDataUserId { get; set; }
        public List<CreatePurchaseRequisitionItemModel> ListItem { get; set; }
        public List<SelectListItem> CompanyLocationOptions { get; set; }
        public List<SelectListItem> CurrencyOptions { get; set; }
        public List<SelectListItem> UnitOfMeasureOptions { get; set; }
        public List<SelectListItem> ItemOptions { get; set; }

        public List<SelectListItem> ConstructCompanyLocationOptions(List<BusinessUnitModel> location)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in location)
            {
                result.Add(new SelectListItem { Value = single.LocationCode.ToString(), Text = single.Description });
            }

            return result;
        }
        public List<SelectListItem> ConstructCurrencyOptions(List<CurrencyModel> currency)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in currency)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Name });
            }

            return result;
        }
        public List<SelectListItem> ConstructUnitOfMeasureOptions(List<UnitOfMeasureModel> unitOfMeasures)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in unitOfMeasures)
            {
                result.Add(new SelectListItem { Value = single.UomId.ToString(), Text = single.Uomname });
            }

            return result;
        }
        public List<SelectListItem> ConstructItemOptions(List<ItemModel> item)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in item)
            {
                result.Add(new SelectListItem { Value = single.ItemCode.ToString(), Text = single.ItemName });
            }

            return result;
        }
    }
}