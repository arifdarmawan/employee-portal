﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class PurchaseRequisitionListHeaderModel
    {
        public int Id { get; set; }
        public string PRNumber { get; set; }
        public string RequestedFrom { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyLocationCode { get; set; }
        public string CompanyName { get; set; }
        public string DepartementCode { get; set; }
        public string DepartementName { get; set; }
        public int MasterItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Currency { get; set; }
        public string Uom { get; set; }
        public string Prstatus { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalAfterTax { get; set; }
        public decimal Ppn { get; set; }
        public decimal GrandTotal{ get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public string ModifiedBy { get; set; }
        public int MasterDataUserId { get; set; }
        public int ProcessId { get; set; }
        public List<PurchaseRequistionListItemModel> ListItem { get; set; }
        public string StringGrandTotal { get; set; }
        public string StringPpn { get; set; }
        public string StringGrandTotalAfterTax { get; set; }
        public string StringDate { get; set; } 

    }
}