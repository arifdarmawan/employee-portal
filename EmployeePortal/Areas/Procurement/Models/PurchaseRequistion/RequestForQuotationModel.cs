﻿using System;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class RequestForQuotationModel
    {
        public int Id { get; set; }
        public int Prid { get; set; }
        public int rfqId { get; set; }
        public string Rfqnumber { get; set; }
        public string Qcfnumber { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyLocationCode { get; set; }
        public string CompanyName { get; set; }
        public string DepartementCode { get; set; }
        public string DepartementName { get; set; }
        public int? MasterDataVendorId { get; set; }
        public string vendorName { get; set; }
        public int? MasterItemCategoryId { get; set; }
        public int? MasterItemId { get; set; }
        public string ItemName { get; set; }
        public decimal? Qty { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool? IsDelete { get; set; }
    }
}