﻿using System;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class PurchaseRequisitionRequestVendorModel
    {
        public int? Prid { get; set; }
        public string PRNumber { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLocationCode { get; set; }
        public string DepartementCode { get; set; }
        public string DepartementName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ProcessId { get; set; }
        public int? MasterItemCategoryId { get; set; }
        public int? MasterItemId { get; set; }
        public string ItemVendorStatus { get; set; }
        public string ItemCode { get; set; }
        public string BimsCode { get; set; }
        public string ItemName { get; set; }
        public string Specification { get; set; }
        public string PurposeDesc { get; set; }
        public decimal Qty { get; set; }
        public string UomName { get; set; }
        public string Currency { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public string UnitPriceString { get; set; }
        public string TotalPriceString { get; set; }
    }
}