﻿using System;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class QuotationComparationFormModel
    {
        public int Prid { get; set; }
        public string Qcfnumber { get; set; }
        public DateTime? QCFDate { get; set; }
        public string Prnumber { get; set; }
        public DateTime? PRDate { get; set; }
        public string CompanyName { get; set; }
        public string ItemName { get; set; }
        public Decimal Qty { get; set; }
        public string Notes { get; set; }
        
        public int MasterDataVendorId { get; set; }
        
        public int MasterDataVendorId1 { get; set; }
        public int MasterDataVendorId2 { get; set; }
        public int MasterDataVendorId3 { get; set; }

        public string VendorName1 { get; set; }
        public string VendorName2 { get; set; }
        public string VendorName3 { get; set; }

        public string Pic1 { get; set; }
        public string Pic2 { get; set; }
        public string Pic3 { get; set; }

        public string Picphone1 { get; set; }
        public string Picphone2 { get; set; }
        public string Picphone3 { get; set; }

        public int Ppn1 { get; set; }
        public int Ppn2 { get; set; }
        public int Ppn3 { get; set; }

        public Decimal PPNPrice1 { get; set; }
        public Decimal PPNPrice2 { get; set; }
        public Decimal PPNPrice3 { get; set; }

        public Decimal UnitPrice1 { get; set; }
        public Decimal UnitPrice2 { get; set; }
        public Decimal UnitPrice3 { get; set; }

        public Decimal TotalPrice1 { get; set; }
        public Decimal TotalPrice2 { get; set; }
        public Decimal TotalPrice3 { get; set; }

        public Decimal GrandTotalAfterTax1 { get; set; }
        public Decimal GrandTotalAfterTax2 { get; set; }
        public Decimal GrandTotalAfterTax3 { get; set; }

        public int DeliveryTime1 { get; set; }
        public int DeliveryTime2 { get; set; }
        public int DeliveryTime3 { get; set; }
    }
}