﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class PPNVendorCategoryItemModel
    {
        public int VendorId { get; set; }
        public int ItemId { get; set; }
        public int PPN { get; set; }
        public decimal? Price { get; set; }
    }
}