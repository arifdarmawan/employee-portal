﻿using System;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class CreatePurchaseRequisitionItemModel
    {
        public int Id { get; set; }
        public string Specification { get; set; }
        public string PurposeDesc { get; set; }
        public decimal? Qty { get; set; }
        public int UomId { get; set; }
        public int? CurrencyId { get; set; }
        public int VendorId { get; set; }
        public int PPN { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public string BudgetStatus { get; set; }
        public string ItemVendorStatus { get; set; }
        public int? MasterPurchaseRequisitionId { get; set; }
        public int? MasterItemId { get; set; }
        public string ItemCode { get; set; }
        public string BimsCode { get; set; }
        public string ItemName { get; set; }
        public DateTime? DateRequired { get; set; }
        public bool? IsRequestVendor { get; set; }
    }
}