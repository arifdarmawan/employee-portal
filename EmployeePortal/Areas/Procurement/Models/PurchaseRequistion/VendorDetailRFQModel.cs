﻿using System;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class VendorDetailRFQModel
    {
        public string Rfqnumber { get; set; }

        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int MasterDataUserId { get; set; }
        public int MasterDataVendorId { get; set; }
        public string VendorName { get; set; }
        public string VendorAddress { get; set; }
        public string VendorPhone { get; set; }
        public string VendorCity { get; set; }
        public string VendorRegion { get; set; }
        public string VendorPostalCode { get; set; }
        public int VendorTOP { get; set; }
        public string VendorPpn { get; set; }
        public int DeliveryTime { get; set; }
        

        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Specification { get; set; }
        public decimal Qty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        
        public string Notes { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}