﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class PurchaseRequisitionVendorCategoryItemModel
    {
        public int PurchaseRequisitionItemId { get; set; }
        public string MasterDataVendorId { get; set; }
        public string VendorName { get; set; }
        public string MasterItemId { get; set; }
        public int MasterItemCategoryId { get; set; }
        public decimal Ppn{ get; set; }
        public decimal PpnPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalPriceAfterTax { get; set; }
    }
}