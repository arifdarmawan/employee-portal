﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Procurement.Models.Budget
{
    public class BudgetModel
    {
        public string BudgetStatus { get; set; }
        public decimal QtyBudget { get; set; }
        public decimal QtyRealization { get; set; }
        public decimal QtyRemaining { get; set; }
        public decimal QtyRequest { get; set; }
        public decimal QtyRemainingTotal { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal TotalBudget { get; set; }
        public decimal TotalBudgetRealization { get; set; }
        public decimal BudgetRemaining { get; set; }
        public decimal BudgetRequest { get; set; }
        public decimal BudgetRemainingTotal { get; set; }
        #region property Purchase Requisition Item Calculation Table
        public string GrandTotal { get; set; }
        public string PPN { get; set; }
        public string GrandTotalFinal { get; set; }
        #endregion
    }
}