﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class ItemModel
    {
        public string ItemCode { get; set; }
        public string OracleCode { get; set; }
        public string ItemName { get; set; }
    }
}
