﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Procurement.Models.Item
{
    public class VendorItemModel
    {
        public decimal Price { get; set; }
    }
}
