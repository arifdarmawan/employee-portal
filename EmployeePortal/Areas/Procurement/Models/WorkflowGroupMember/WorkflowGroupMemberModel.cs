﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Procurement.Models.WorkflowGroupMember
{
    public class WorkflowGroupMemberModel
    {
        public int ConfigCompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public int ConfigDivisionId { get; set; }
        public string DivisionCode { get; set; }
        public string DivisionName { get; set; }
    }
}