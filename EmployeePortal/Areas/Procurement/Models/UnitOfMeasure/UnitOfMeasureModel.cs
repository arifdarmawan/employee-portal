﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class UnitOfMeasureModel
    {
        public int UomId { get; set; }
        public string Uomcode { get; set; }
        public string Uomname { get; set; }
    }
}