﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseOrder
{
    public class PurchaseOrderVendorCategoryItemModel 
    {
        public string VendorId { get; set; }
        public string VendorName { get; set; }
        public decimal Ppn{ get; set; }
        public decimal PpnPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalPriceAfterTax { get; set; }
    }
}