﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseOrder
{
    public class PurchaseOrderItemModel
    {
        public string VendorName { get; set; }
        public string Specification { get; set; }
        public string PurposeDesc { get; set; }
        public decimal Qty { get; set; }
        public string Uom { get; set; }
        public int? CurrencyId { get; set; }
        public string Currency { get; set; }
        public int PPN { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public string PRStatus { get; set; }
        public string BudgetStatus { get; set; }
        public int? MasterPurchaseRequisitionId { get; set; }
        public string PRNumber { get; set; }
        public int? MasterItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string UnitPriceString { get; set; }
        public string TotalPriceString { get; set; }
    }
}
