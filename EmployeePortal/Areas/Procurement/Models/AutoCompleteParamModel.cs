﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Procurement.Models
{
    public class AutoCompleteParamModel
    {
        public string Key { get; set; }
    }
}
