﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.Areas.Procurement.Models.PurchaseRequistion
{
    public class BusinessUnitModel
    {
        public string CompanyCode { get; set; }
        public string LocationCode { get; set; }
        public string Description { get; set; }
    }
}
