﻿using System;
using EmployeePortal.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http.Headers;
using System.Net.Http;

namespace EmployeePortal
{
    public class Startup
    {
        //const string clientSSLKey = "HttpClientWithSSLUntrusted";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddSession(options =>
            {
                options.Cookie.Name = ".EmployeePortal.Session";
                options.IdleTimeout = TimeSpan.FromDays(90);//You can set Time
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddHttpClient<PortalService>("HttpClientWithSSLUntrusted", client =>
            {
                client.BaseAddress = new Uri(Configuration.GetValue<string>("WebPortalAPIEndpoint:BaseUrl"));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => { return true; }
            });
            services.AddHttpClient<HRISService>();
            services.AddHttpClient<BIMSService>("HttpClientWithSSLUntrusted2", client =>
            {
                client.BaseAddress = new Uri(Configuration.GetValue<string>("FAPBIMSAPIEndpoint:BaseUrl"));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => { return true; }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                    name: "AccountArea",
                    areaName: "Account",
                    template: "Account/{controller=Home}/{action=Index}/{id?}");
                routes.MapAreaRoute(
                    name: "EmployeeArea",
                    areaName: "Employee",
                    template: "Employee/{controller=Home}/{action=Index}/{id?}");
                routes.MapAreaRoute(
                    name: "VendorArea",
                    areaName: "Vendor",
                    template: "Vendor/{controller=Home}/{action=Index}/{id?}");
                routes.MapAreaRoute(
                    name: "WorkflowArea",
                    areaName: "Workflow",
                    template: "Workflow/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
