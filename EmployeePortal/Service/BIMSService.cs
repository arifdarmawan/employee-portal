﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EmployeePortal.Helper;
using EmployeePortal.Models;
using EmployeePortal.Models.ResponseModel;
using EmployeePortal.Areas.Procurement.Models.PurchaseRequistion;

namespace EmployeePortal.Service
{
    public class BIMSService
    {
        HttpClient Client { get; }
        IConfiguration Configuration { get; }
        public BIMSService(HttpClient client, IConfiguration configuration)
        {

            Configuration = configuration;
            Client = client;
        }

        #region Function Global
        public async Task<BaseResponse> GetAsync(string uri)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(uri);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PostAsync<T>(string uri, T model) where T : class
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsync(uri, new JsonContent(model));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PostAsJsonAsync(string uri, string value)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(uri, value);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PutAsync<T>(string uri, T model) where T : class
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PutAsync(uri, model != null ? new JsonContent(model) : null);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion
    }
}