﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using EmployeePortal.Models;

namespace EmployeePortal.Service
{
    public class MailService
    {
        string _smtpServer = "";
        string _sender = "";
        string _password = "";

        public MailService(string smtpServer, string sender, string password)
        {
            _smtpServer = smtpServer;
            _sender = sender;
            _password = password;
        }

        public void SettingEmail(string recipient, string subject, string message)
        {
            SmtpClient client = new SmtpClient();

            client.UseDefaultCredentials = false;
            //client.Credentials = new System.Net.NetworkCredential(_sender, _password);
            client.Host = _smtpServer;
            client.Port = 25;
            //client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = false;

            try
            {
                var mail = new MailMessage(_sender.Trim(), recipient.Trim());
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public static void SendMail(EmailFormatViewModel formatEmailModel, string messages)
        {
            var sender = new MailService(formatEmailModel.SMTPServer, formatEmailModel.EmailHost, formatEmailModel.EmailHostPassword);

            sender.SettingEmail(formatEmailModel.EmailRecipient, formatEmailModel.Subject, messages);
        }
    }
}
