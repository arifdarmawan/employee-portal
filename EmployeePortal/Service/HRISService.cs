﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EmployeePortal.Helper;
using EmployeePortal.Models.ResponseModel;

namespace EmployeePortal.Service
{
    public class HRISService
    {
        HttpClient Client { get; }
        IConfiguration Configuration { get; }
        public HRISService(HttpClient client, IConfiguration configuration)
        {

            Configuration = configuration;
            var BaseServerKey = Configuration.GetValue<string>("BaseServer");
            client.BaseAddress = new Uri(Configuration.GetValue<string>(BaseServerKey+"HRISAPIEndpoint:BaseUrl"));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            Client = client;
        }

        #region Function Global
        public async Task<BaseResponse> GetAsync(string uri)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(uri);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PostAsync<T>(string uri, T model) where T : class
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsync(uri, new JsonContent(model));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PostAsJsonAsync(string uri, string value)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(uri, value);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PutAsync<T>(string uri, T model) where T : class
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PutAsync(uri, model != null ? new JsonContent(model) : null);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion
    }
}
