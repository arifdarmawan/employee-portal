﻿(function ($) {
    'use strict';

    var initalLogin = (function () {
        var loginForm, userId, lblUserId, password, lblPassword, btnSubmit, _KEY_LP;

        var registerDom = function () {
            loginForm = $('.form-login');
            userId = $('#loginUserId');
            lblUserId = $('#lblLoginUserId');
            password = $('#loginPassword');
            lblPassword = $('#lblLoginPassword');
            btnSubmit = $('.btn-submit');
            _KEY_LP = $('#_KEY_LP');
        };

        var formValidation = function () {
            var isFormValid = false,
                isUserId = false,
                isPassword = false;

            /* User Id */
            if (_KEY_LP.val() == 'EMP') {
                if (userId.val() == '') {
                    userId.addClass('invalid');
                    if ($('.errUserId').length == 0) {
                        lblUserId.after('<div class="invalid-feedback errUserId">' + 'Please fill the required field.' + '</div>');
                        isUserId = false;
                    }
                }
                else {
                    userId.removeClass('invalid');
                    $('.errUserId').remove();
                    isUserId = true;
                }
            } else {
                if (userId.val() == '') {
                    userId.addClass('invalid');
                    if ($('.errUserId').length == 0) {
                        lblUserId.after('<div class="invalid-feedback errUserId">' + 'Please fill the required field.' + '</div>');
                        isUserId = false;
                    }
                } else if (!WEBPORTAL.Utility.CheckEmail(userId.val())) {
                    $('.errUserId').remove();
                    userId.addClass('invalid');
                    lblUserId.after('<div class="invalid-feedback errUserId">' + 'Format email not valid.' + '</div>');
                    isUserId = false;
                }
                else {
                    userId.removeClass('invalid');
                    $('.errUserId').remove();
                    isUserId = true;
                }
            }

            /* Password */
            if (password.val() == '') {
                $('.errPassword').remove();
                password.addClass('invalid');
                if ($('.errPassword').length == 0) {
                    lblPassword.after('<div class="invalid-feedback errPassword">' + 'Please fill the required field.' + '</div>');
                    isPassword = false;
                }
            } else if (password.val().length < 5) {
                $('.errPassword').remove();
                password.addClass('invalid');
                lblPassword.after('<div class="invalid-feedback errPassword">' + 'Password minimum 3 characters length.' + '</div>');
                isPassword = false;
            }
            else {
                password.removeClass('invalid');
                $('.errPassword').remove();
                isPassword = true;
            }

            if (isUserId && isPassword) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };

        var formLoginValidasi = function () {
            WEBPORTAL.Utility.SubmitLoading(btnSubmit);
            if (formValidation()) {
                var params = {
                    UserId: WEBPORTAL.Utility.EncryptionHandler(userId.val()),
                    Password: WEBPORTAL.Utility.EncryptionHandler(password.val())
                };

                $.when(WEBPORTAL.Services.POSTLocal(params, WEBPORTAL.URLContext.Login)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        window.location.href = WEBPORTAL.URLContext.Dashboard;
                    } else {
                        $('.errPassword').remove();
                        password.addClass('invalid');
                        lblPassword.after('<div class="invalid-feedback errPassword">' + result.message + '</div>');

                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmit, 'SIGN IN');
                        }, 300);
                    }
                });
            } else {
                setTimeout(function () {
                    WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmit, 'SIGN IN');
                }, 300);
            }
        }

        var eventSubmitForm = function () {
            btnSubmit.click(function () {
                formLoginValidasi();
            })
        };

        var eventEnterSubmitForm = function () {
            $('#loginUserId, #loginPassword').keyup(function (e){
                if (e.keyCode === 13) {
                    formLoginValidasi();
                }
            });
        }

        var inputChangeRemoveErr = function () {
            userId.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblUserId.next().remove();
                }
            });

            password.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblPassword.next().remove();
                }
            });
        };

        var run = function () {
            registerDom();
            inputChangeRemoveErr();
            eventEnterSubmitForm();
            eventSubmitForm();
        };

        return {
            run: run
        };
    })();

    initalLogin.run();
})(jQuery);
