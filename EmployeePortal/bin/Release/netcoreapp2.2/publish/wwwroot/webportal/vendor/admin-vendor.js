﻿(function ($) {
    'use strict';

    var initialVendor = (function () {
        var grid;

        var initialDom = function () {
            grid = $('#grid');
        };

        var initialGrid = function () {
            grid.DataTable({
                ajax: { url: WEBPORTAL.URLContext.GetAllVendor, dataSrc: '' },
                //processing: true,
                //serverSide: true,
                columns: [
                    { data: 'Id', title: "Id", visible: false },
                    { data: 'VendorUserId', title: "VendorUserId", visible: false },
                    { data: 'BusinessTypeName', title: "BusinessTypeName", visible: false },
                    { data: 'Name', title: "Nama Perusahaan" },
                    { data: 'Address', title: "Alamat" },
                    { data: 'CompanyPhone', title: "Telepon" },
                    { data: 'BusinessFields', title: "Bidang Usaha", width: 180 },
                    {
                        data: 'Status',
                        title: "Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData == 'APPROVED') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData == 'REJECTED') {
                                $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                            } else if (cellData == 'DISABLED') {
                                $(td).html('<span class="badge badge-dark">' + cellData + '</span>');
                            } else {
                                $(td).html('<span class="badge badge-primary">' + cellData + '</span>');
                            }                            
                        }
                    },
                    { data: 'CreatedDate', visible: false },
                    {
                        data: null,
                        title: "",
                        width: 30,
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).html('<div class="button-list">'
                                + '<a href="#" class= "btn btn-icon waves-effect waves-light btn-custom" id="ad_vd_btn_detail"><i class="fa fa-eye"></i></a>'
                                //+ '<a href="#" class="btn btn-icon waves-effect waves-light btn-custom" data-toggle="modal" data-target="#feedbackModal"> <i class="fa fa-edit"></i></a>'
                                //+ '<a href="#" class="btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-trash"></i></a>'
                                + '</div >');

                            $(td).find('#ad_vd_btn_detail').click(function (e) {
                                window.location.href = WEBPORTAL.URLContext.VendorDetails + '/' + rowData.Name + '/' + rowData.Status + '/' + rowData.VendorUserId;
                            });
                        }
                    }
                ],
                "order": [[8, 'desc']],
                initComplete: function (settings, json) {
                    grid.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var run = function () {
            initialDom();
            initialGrid();
        };

        return {
            run: run
        }
    })(jQuery);

    initialVendor.run();
})(jQuery);