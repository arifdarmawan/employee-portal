﻿(function ($) {
    'use strict';

    var initialApprovalRejected = (function () {
        var totalPage, listItem, pagination;

        var initialDom = function () {
            totalPage = $('#totalPage_temp').attr('data-value');
            listItem = $('#list_item_rejected');
            pagination = $('#pagination-rejected');
        };

        var initialPagination = function () {
            pagination.twbsPagination({
                totalPages: totalPage <= 1 ? 2 : totalPage,
                visiblePages: 5,
                next: 'Next',
                prev: 'Prev',
                onPageClick: function (event, page) {
                    $.when(listItem.settingWrapCenter()).done(listItem.componentAddLoading());
                    $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetWorkflowRejectedItem + '/' + page)).done(function (result, status, xhr) {
                        setTimeout(function () {
                            $.when(listItem.resetWrapCenter()).done(listItem.html(result));
                        }, 500);
                    });
                }
            });
        };

        var run = function () {
            initialDom();
            initialPagination();
        };

        return {
            run: run
        }
    })(jQuery);

    initialApprovalRejected.run();
})(jQuery);