﻿(function ($) {
    'use strict';

    var initialApprovalAction = (function () {
        var modalApprovalDetail, btnActionApproved, btnActionRejected, wrapWorkflowValue, wfNoted, wrapBudgetItem, tblDetailBudget;

        var initialDom = function () {
            modalApprovalDetail = $('#ptaApprovalOutModal');
            btnActionApproved = $('#btn-action-approved');
            btnActionRejected = $('#btn-action-rejected');
            wrapWorkflowValue = $('#wrap-workflow-value');
            wfNoted = $('#wf-noted');
            tblDetailBudget = $('#tbl-detail-budget tbody tr');
            wrapBudgetItem = $('#wrap-budget-item');
        };

        var clickActionApproved = function () {
            btnActionApproved.click(function () {
                var model = {
                    WorkflowProcessId: wrapWorkflowValue.attr('data-workflow'),
                    Noted: wfNoted.val(),
                    ActionType: "APPROVED"
                };

                WEBPORTAL.Utility.SubmitLoading(btnActionApproved);
                $.when(WEBPORTAL.Services.POSTLocal(model, WEBPORTAL.URLContext.WorkflowApprovalAction)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);

                        setTimeout(function () {
                            modalApprovalDetail.modal('hide');
                            window.location.href = '/Approval/Pending';
                        }, 500)
                    } else {
                        WEBPORTAL.Utility.ConstructNotificationError(result.message);
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnActionApproved, 'APPROVED');
                    }
                });
            });
        };

        var clickActionRejected = function () {
            btnActionRejected.click(function () {
                var model = {
                    WorkflowProcessId: wrapWorkflowValue.attr('data-workflow'),
                    Noted: wfNoted.val(),
                    ActionType: "REJECTED"
                };

                WEBPORTAL.Utility.SubmitLoading(btnActionApproved);

                $.when(WEBPORTAL.Services.POSTLocal(model, WEBPORTAL.URLContext.WorkflowApprovalAction)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);

                        setTimeout(function () {
                            modalApprovalDetail.modal('hide');
                            window.location.href = '/Approval/Pending';
                        }, 500)
                    } else {
                        WEBPORTAL.Utility.ConstructNotificationError(result.message);
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnActionApproved, 'APPROVED');
                    }
                });
            });
        };

        var checkDetailBudget = function () {
            tblDetailBudget.click(function () {
                var model = {
                    CompanyCode: $(this).attr('data-company-code'),
                    LocationCode: $(this).attr('data-location-code'),
                    Year: $(this).attr('data-year'),
                    BimsCode: $(this).attr('data-item-code'),
                    Quantity: $(this).find('#col-qty').attr('data-column'),
                    TotalPrice: $(this).find('#col-totalPrice').attr('data-column'),
                    GrandTotal: $(this).attr('data-total-price')
                };

                $.when(wrapBudgetItem.settingWrapCenter()).done(wrapBudgetItem.componentAddLoading());
                $.when(WEBPORTAL.Services.POSTLocal(model, WEBPORTAL.URLContext.ApprovalDetailBudget)).done(function (result, status, xhr) {                 
                    setTimeout(function () {
                        wrapBudgetItem.resetWrapCenter();
                        wrapBudgetItem.html(result);
                    }, 500);
                });
            });
        };

        var run = function () {
            initialDom();
            clickActionApproved();
            clickActionRejected();
            checkDetailBudget();
        };

        return {
            run: run
        }
    })(jQuery);

    initialApprovalAction.run();
})(jQuery);