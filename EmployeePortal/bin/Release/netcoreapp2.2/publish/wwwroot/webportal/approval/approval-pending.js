﻿(function ($) {
    'use strict';

    var initialApprovalPending = (function () {
        var totalPage, listItem, pagination;

        var initialDom = function () {
            totalPage = $('#totalPage_temp').attr('data-value');
            listItem = $('#list_item_pending');
            pagination = $('#pagination-pending');
        };

        var initialPagination = function () {
            pagination.twbsPagination({
                totalPages: totalPage <= 1 ? 2 : totalPage,
                visiblePages: 5,
                next: 'Next',
                prev: 'Prev',
                onPageClick: function (event, page) {
                    $.when(listItem.settingWrapCenter()).done(listItem.componentAddLoading());
                    $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetWorkflowPendingItem + '/' + page)).done(function (result, status, xhr) {
                        setTimeout(function () {
                            $.when(listItem.resetWrapCenter()).done(listItem.html(result));
                        }, 500);
                    });
                }
            });
        };

        var run = function () {
            initialDom();
            initialPagination();
        };

        return {
            run: run
        }
    })(jQuery);

    initialApprovalPending.run();
})(jQuery);