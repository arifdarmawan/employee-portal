﻿var EMPLOYEEALL = EMPLOYEEALL || {};

EMPLOYEEALL.URLContext = {
    ApiEndPointBaseUrl: '',
    HRISApiEndPointBaseUrl: '',
    //GetIDCard: '/Employee/PersonalData/GetIDCardList'
    GetIDCard: '/PersonalData/GetIDCardList',
    GetFamily: '/PersonalData/GetFamilyList',
    GetEducation: '/PersonalData/GetEducationList',
    GetJobExp: '/PersonalData/GetJobExpList',
    GetBankAccount: '/PersonalData/GetBankAccountList'
};
EMPLOYEEALL.GetDomGrid = (function () {
    return $('#grid');
})();

EMPLOYEEALL.Grid = (function () {
    return $('#grid');
})();

$.get('/Home/GetUrlApiWebPortal', function (result, status, xhr) {
    WEBPORTAL.URLContext.ApiEndPointBaseUrl = result;
});

$.get('/Home/GetUrlApiHRIS', function (result, status, xhr) {
    WEBPORTAL.URLContext.HRISApiEndPointBaseUrl = result;
});

//EMPLOYEEALL.IDCard = (function () {
//    //var initIDCard = (function () {
//    var initialGrid = function () {
//        EMPLOYEEALL.Grid.DataTable({
//            ajax: { url: EMPLOYEEALL.URLContext.GetIDCard, dataSrc: '' },
//            //processing: true,
//            //serverSide: true,
//            columns: [

//                { data: 'Id', title: "Id", visible: false },
//                { data: 'Seq', title: "#" },
//                { data: 'CardTypeName', title: "Card Type Number Ess" },
//                { data: 'CardNo', title: "Card Number" },
//                { data: 'Publisher', title: "Publisher" },
//                { data: 'ExpiredDateDisp', title: "Expired Date" },
//                { data: 'Description', title: "Description" },
//                {
//                    data: "IsPostedDisp",
//                    title: "",
//                    createdCell: function (td, cellData, rowData, row, col) {
//                        var toRenderClass = cellData === "Approved" ? "badge-success" : "badge-warning";
//                        $(td).html('<span class="badge badge-pill ' + toRenderClass + '">' + cellData + '</span></td>');
//                    }
//                },
//                {
//                    data: null,
//                    title: "",
//                    createdCell: function (td, cellData, rowData, row, col) {
//                        $(td).html('<div class="btn-group">'
//                            + '<a href="#" data-toggle="modal" data-target="#editIDCardModal" class="btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-edit"></i></a>'
//                            + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom"> <i class="fa fa-trash"></i></a>'
//                            + '</div >');
//                    }
//                }
//            ]
//        });
//    };

//    var run = function () {
//        initialGrid();
//    };

//    return {
//        run: run
//    };
//    //})();
//})();

EMPLOYEEALL.Family = (function () {
    var initialGrid = function () {
        EMPLOYEEALL.Grid.DataTable({
            ajax: { url: EMPLOYEEALL.URLContext.GetFamily, dataSrc: '' },
            //processing: true,
            //serverSide: true,
            columns: [
                { data: 'Seq', title: "#" },
                { data: 'FamilyRelation', title: "Family Relation" },
                { data: 'FamilyMemberName', title: "Family Member Name" },
                { data: 'BirthPlace', title: "Birth Place" },
                { data: 'BirthDateDisp', title: "Birth Date" },
                { data: 'Gender', title: "Gender" },
                { data: 'MaritalStatus', title: "Marital Status" },
                { data: 'IDCardNo', title: "ID Card No." },
                { data: 'Occupation', title: "Occupation" },
                { data: 'OriginalAddress', title: "Original Address" },
                { data: 'ResidentialAddress', title: "Residential Address" },
                { data: 'PhoneNo', title: "Phone No." },
                { data: 'MobileNo', title: "Mobile No." },
                {
                    data: "IsPostedDisp",
                    title: "Status",
                    createdCell: function (td, cellData, rowData, row, col) {
                        var toRenderClass = cellData === "Approved" ? "badge-success" : "badge-warning";
                        $(td).html('<span class="badge badge-pill ' + toRenderClass + '">' + cellData + '</span></td>');
                    }
                },
                {
                    data: "IDEmpFamily",
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).html('<div class="btn-group">'
                            + '<a href="javascript:void(0);" class="ancFamEdit btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-edit"></i></a>'
                            + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom"> <i class="fa fa-trash"></i></a>'
                            + '</div >');
                        $(td).find('.ancFamEdit').click(function () {
                            debugger;
                            var options = { "backdrop": "static", keyboard: true };
                            $.ajax({
                                type: "GET",
                                url: "/PersonalData/FamilyEdit/" + cellData,
                                contentType: "application/json; charset=utf-8",
                                //data: { "Id": id },
                                datatype: "json",
                                success: function (data) {
                                    debugger;
                                    $('#myModalDialog').html(data);
                                    $('#myModal').modal(options);
                                    $('#myModal').modal('show');

                                },
                                error: function () {
                                    alert("Dynamic content load failed.");
                                }
                            });
                        });
                    }
                }
            ]
        });
    };

    var run = function () {
        initialGrid();
    };

    return {
        run: run
    };
})();

EMPLOYEEALL.Education = (function () {
    //var initIDCard = (function () {

    var initialGrid = function () {
        EMPLOYEEALL.Grid.DataTable({
            ajax: { url: EMPLOYEEALL.URLContext.GetEducation, dataSrc: '' },
            //processing: true,
            //serverSide: true,
            columns: [

                { data: 'Seq', title: "#" },
                { data: 'EducationStatus', title: "Education Status" },
                { data: 'EducationLevel', title: "Education Level" },
                { data: 'EducationMajor', title: "Education Major" },
                { data: 'InstitutionName', title: "Institution Name" },
                { data: 'City', title: "City" },
                { data: 'EducationResult', title: "Education Result" },
                { data: 'GraduationDisp', title: "Graduation" },
                { data: 'GradePoint', title: "Grade Point" },
                { data: 'Title', title: "Title" },
                {
                    data: "IsPostedDisp",
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        var toRenderClass = cellData === "Approved" ? "badge-success" : "badge-warning";
                        $(td).html('<span class="badge badge-pill ' + toRenderClass + '">' + cellData + '</span></td>');
                    }
                },
                {
                    data: null,
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).html('<div class="btn-group">'
                            + '<a href="#" data-toggle="modal" data-target="#editEducationModal" class="btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-edit"></i></a>'
                            + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom"> <i class="fa fa-trash"></i></a>'
                            + '</div >');
                    }
                }
            ]
        });
    };

    var run = function () {
        initialGrid();
    };

    return {
        run: run
    };
})();

EMPLOYEEALL.JoBExp = (function () {
    //var initIDCard = (function () {

    var initialGrid = function () {
        EMPLOYEEALL.Grid.DataTable({
            ajax: { url: EMPLOYEEALL.URLContext.GetJobExp, dataSrc: '' },
            //processing: true,
            //serverSide: true,
            columns: [

                { data: 'Seq', title: "#" },
                { data: 'JobTitle', title: "Job Title" },
                { data: 'StartDateDisp', title: "Start Date" },
                { data: 'EndDateDisp', title: "End Date" },
                { data: 'TerminationReason', title: "Termination Reason" },
                { data: 'CompanyName', title: "Company Name" },
                { data: 'CompanyAddress', title: "Company Address" },
                { data: 'ZipCode', title: "Zip Code" },
                { data: 'CompanyPhone', title: "Company Phone" },
                {
                    data: "IsPostedDisp",
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        var toRenderClass = cellData === "Approved" ? "badge-success" : "badge-warning";
                        $(td).html('<span class="badge badge-pill ' + toRenderClass + '">' + cellData + '</span></td>');
                    }
                },
                {
                    data: null,
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).html('<div class="btn-group">'
                            + '<a href="#" data-toggle="modal" data-target="#editEducationModal" class="btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-edit"></i></a>'
                            + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom"> <i class="fa fa-trash"></i></a>'
                            + '</div >');
                    }
                }
            ]
        });
    };

    var run = function () {
        initialGrid();
    };

    return {
        run: run
    };
})();

EMPLOYEEALL.BankAccount = (function () {
    //var initIDCard = (function () {

    var initialGrid = function () {
        EMPLOYEEALL.Grid.DataTable({
            ajax: { url: EMPLOYEEALL.URLContext.GetBankAccount, dataSrc: '' },
            //processing: true,
            //serverSide: true,
            columns: [

                { data: 'Seq', title: "#" },
                { data: 'CurrencyName', title: "Currency Name" },
                { data: 'BankGroup', title: "Bank Group" },
                { data: 'Branch', title: "Branch" },
                { data: 'AccountNo', title: "Account No." },
                { data: 'AccountName', title: "Account Name" },
                { data: 'Owner', title: "Owner" },
                {
                    data: "IsPostedDisp",
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        //var toRenderClass = cellData === "Approved" ? "badge-success" : "badge-warning";
                        $(td).html('<span class="badge badge-pill badge-success"> Approved </span></td>');
                    }
                }
                //{
                //    data: null,
                //    title: "",
                //    createdCell: function (td, cellData, rowData, row, col) {
                //        $(td).html('<div class="btn-group">'
                //            + '<a href="#" data-toggle="modal" data-target="#editEducationModal" class="btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-edit"></i></a>'
                //            + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom"> <i class="fa fa-trash"></i></a>'
                //            + '</div >');
                //    }
                //}
            ]
        });
    };

    var run = function () {
        initialGrid();
    };

    return {
        run: run
    };
})();