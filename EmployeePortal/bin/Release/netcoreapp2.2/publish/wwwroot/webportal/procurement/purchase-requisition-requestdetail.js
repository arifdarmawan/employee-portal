﻿(function ($) {
    'use strict';

    var initialVendor = (function () {
        var wrapPurchaseRequisitionItem, wrapPurchaseRequisitionBudget, grid, dataTable, prid;

        var initialDom = function () {
            wrapPurchaseRequisitionItem = $('#pr_wrap_Item_list'); wrapPurchaseRequisitionBudget = $('#pr_wrap_budget'); grid = $("#Grid");
            dataTable = grid.DataTable(); prid = $("#PRId");
        };

        var initialForm = function () {
            loadPurchaseRequisitionItem();
            gridRowOnClick();
        };

        var loadPurchaseRequisitionItem = function () {
            $.when(wrapPurchaseRequisitionItem.settingWrapCenter()).done(wrapPurchaseRequisitionItem.componentAddLoading());

            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetPurchaseRequisitionItem + "/" + prid.val())).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapPurchaseRequisitionItem.resetWrapCenter()).done(wrapPurchaseRequisitionItem.html(result));
                }, 500);
            });
        };

        var gridRowOnClick = function () {
            grid.on('click', 'tr', function () {
                $.when(wrapPurchaseRequisitionBudget.settingWrapCenter()).done(wrapPurchaseRequisitionBudget.componentAddLoading());
                $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetItemBudget + "/" + location + "/" + budgetyear + "/" + rowData.ItemCode + "/" + rowData.Qty + "/" + rowData.Price)).done(function (result, status, xhr) {
                    setTimeout(function () {
                        wrapPurchaseRequisitionBudget.removeAttr('hidden');
                        $.when(wrapPurchaseRequisitionBudget.resetWrapCenter()).done(wrapPurchaseRequisitionBudget.html(result));
                    }, 500);
                });
            });
        };

        var run = function () {
            initialDom();
            initialForm();
        };

        return {
            run: run
        };
    })(jQuery);

    initialVendor.run();
})(jQuery);