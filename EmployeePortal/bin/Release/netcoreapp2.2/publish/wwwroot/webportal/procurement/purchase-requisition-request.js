﻿(function ($) {
    'use strict';

    var initialPurchaseRequisition = (function () {
        var grid, dataTable;

        var initialDom = function () {
            grid = $('#grid');
        };

        var initialGrid = function () {
            dataTable = grid.DataTable({
                ajax: { url: WEBPORTAL.URLContext.GetPurchaseRequisitionRequest, dataSrc: '' },
                columns: [
                    { data: 'MasterPurchaseRequisitionId', visible: false },
                    { data: 'PRNumber', title: "PRNumber" },
                    { data: 'ItemName', title: "Item Name" },
                    { data: 'Specification', title: "Specification" },
                    { data: 'Qty', title: "Quantity" },
                    { data: 'TotalPrice', title: "Price", render: $.fn.dataTable.render.number(',', '.', 2) },
                    {
                        data: 'PRStatus',
                        title: "Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData == 'APPROVED') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData == 'REJECTED') {
                                $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                            } else if (cellData == 'DISABLED') {
                                $(td).html('<span class="badge badge-dark">' + cellData + '</span>');
                            } else {
                                $(td).html('<span class="badge badge-primary">' + cellData + '</span>');
                            }
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    grid.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var gridRowOnClick = function () {
            grid.on('click', 'tr', function () {
                var rowData = dataTable.row(this).data();
                window.location.href = WEBPORTAL.URLContext.PurchaseRequisitionRequestDetails + '/' + rowData.MasterPurchaseRequisitionId + '/' + rowData.PRNumber;
            });
        };
        

        var run = function () {
            initialDom();
            gridRowOnClick();
            initialGrid();
        };

        return {
            run: run
        };
    })(jQuery);

    initialPurchaseRequisition.run();
})(jQuery);