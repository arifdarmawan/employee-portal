﻿/**
 * @namespace WEBPORTAL
 * 
 * */
var WEBPORTAL = WEBPORTAL || {};

/**
 * @module Version
 * 
 * */
WEBPORTAL.Version = '1.0.0';

/**
 * @module Register
 * 
 * */
WEBPORTAL.Register = {};
WEBPORTAL.Register.MaterialSelect = $('.mdb-select').materialSelect();

/**
 * @module Global
 * 
 * */
WEBPORTAL.Global = {};
WEBPORTAL.Global.SetDropdownCaret = $('.select-wrapper.md-form.md-outline span.caret').css('z-index', '3');

/**
 * @module UserType
 * 
 * */
WEBPORTAL.UserType = {};
WEBPORTAL.UserType.Vendor = "V";

/**
 * @module URLContext
 * 
 * */
WEBPORTAL.URLContext = {
    ApiEndPointBaseUrl: '',
    Login: '/Account/Login',
    Dashboard: '/Dashboard/Index',
    GetAllVendor: '/Vendor/GetAllVendor',
    VendorDetails: '/Vendor/VendorDetails',
    GetBasicInfo: '/Vendor/GetBasicInfo',
    GetBankPayment: '/Vendor/GetBankPayment',
    GetVendorLegal: '/Vendor/GetVendorLegal',
    GetVendorTax: '/Vendor/GetVendorTax',
    GetVendorAgreement: '/Vendor/GetVendorAgreement',
    DownloadFile: '/Vendor/DownloadFile',
    UpdateVendorStatus: '/Vendor/UpdateVendorStatus',
    GetVendorCategories: '/Vendor/GetVendorCategories',
    InsertVendorCategories: '/Vendor/InsertVendorCategories',
    GetCompany: '/api/Procurement/GetCompany',
    GetAllItem: '/api/Item/Get',
    GetAllItemLookup: '/api/Item/GetAllItemLookup',
    GetDataItem: '/api/Item/GetItemByCode',    
    GetVendorItemPrice: '/api/Item/GetVendorItemPrice',

    GetPurchaseRequisitionList: '/Procurement/GetPurchaseRequisitionList',
    PurchaseRequisitionStatusDetails: '/Procurement/PurchaseRequisitionStatusDetail',
    GetPurchaseRequisitionItem: '/Procurement/GetPurchaseRequisitionItem',
    GetRFQList: '/Procurement/GetRFQList',
    RFQDetails: '/Procurement/RFQDetail',
    GetQCFList: '/Procurement/GetQCFList',
    QCFDetails: '/Procurement/QCFDetail',
    QCFPreview: '/Procurement/QCFPreview',
    GetListVendor: '/Procurement/GetListVendor',
    GetDetailVendor: '/Procurement/GetDetailVendor',
    GetItemBudget: '/Procurement/GetItemBudget',
    GetWorkflowPendingItem: '/Approval/PendingItem',
    GetWorkflowApprovedItem: '/Approval/ApprovedItem',
    GetWorkflowRejectedItem: '/Approval/RejectedItem',
    GetWorkflowApprovalDetail: '/Approval/DetailItem',
    WorkflowApprovalAction: '/Approval/ApprovalAction',
    ApprovalDetailBudget: '/Approval/DetailBudget',
    SearchItemByKey: '/Procurement/SearchItemByKey',
    GetPurchaseOrder: '/Procurement/GetPurchaseOrder',
    PurchaseOrderDetails: '/Procurement/PurchaseOrderDetail',
    GetPurchaseRequisitionItemOnProcess: '/Procurement/GetPurchaseRequisitionItemOnProcess',
    getQuotationComparationFormByPrId: '/Procurement/getQuotationComparationFormByPrId',
    ProcessQuotationComparationForm: '/Procurement/ProcessQuotationComparationForm',
};

WEBPORTAL.VendorStatus = {
    Approved: 'APPROVED',
    Rejected: 'REJECTED',
    Disabled: 'DISABLED',
    Feedback: 'FEEDBACK'
};

const URL_CONTEXT_GetPurchaseRequisitionStatus = '/PurchaseRequisition/Status';
const URL_CONTEXT_InsertPurchaseRequisition = '/Procurement/InsertPurchaseRequisition';
const URL_CONTEXT_GetPurchaseRequisitionRFQ = '/PurchaseRequisition/RFQ';
const URL_CONTEXT_BlastToVendor = '/Procurement/BlastToVendor';
const URL_CONTEXT_GetPurchaseRequisitionQCF = '/PurchaseRequisition/QCF';
const URL_CONTEXT_GetPurchaseOrder = '/PurchaseOrder/PurchaseOrder';

WEBPORTAL.GlobalMessage = {
    SubmitError: "Submit Error. Please check your form"
};

/* Get url APi */
$.get('/Home/GetUrlApiWebPortal', function (result, status, xhr) {
    WEBPORTAL.URLContext.ApiEndPointBaseUrl = result;
});

/**
 * @module Services
 * 
 */
WEBPORTAL.Services = (function () {
    function get(urlContext) {
        var deferred = $.Deferred();

        $.ajax({
            type: "GET",
            url: WEBPORTAL.URLContext.ApiEndPointBaseUrl + urlContext,
            success: function (result, status, xhr) {            
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            }
        });

        return deferred.promise();
    };

    function getLocal(urlContext) {
        var deferred = $.Deferred();

        $.ajax({
            type: "GET",
            url: urlContext,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            }
        });

        return deferred.promise();
    };

    function postLocal(data, urlContext, contentType, processData) {
        var deferred = $.Deferred();

        $.ajax({
            type: "POST",
            data: data,
            url: urlContext,
            contentType: contentType !== 'undefined' ? contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
            processData: processData !== 'undefined' ? processData : true,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    function putLocal(data, urlContext, queryString) {
        var deferred = $.Deferred();

        $.ajax({
            type: "PUT",
            data: data,
            url: urlContext + queryString,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    function deleteLocal(data, urlContext, queryString) {
        var deferred = $.Deferred();

        $.ajax({
            type: "DELETE",
            data: data,
            url: urlContext + queryString,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    function downloadFile(fileName) {
        var deferred = $.Deferred();

        $.ajax({
            url: WEBPORTAL.URLContext.ApiEndPointBaseUrl + WEBPORTAL.URLContext.DownloadFile + '?fileName=' + fileName,
            method: 'GET',
            xhrFields: {
                responseType: 'blob'
            },
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    return {
        GET: get,
        GETLocal: getLocal,
        POSTLocal: postLocal,
        PUTLocal: putLocal,
        DELETELocal: deleteLocal,
        DownloadFile: downloadFile
    };
})();

/**
 * @module Utility
 * 
 * */
WEBPORTAL.Utility = (function () {
    function showLoading(obj) {
        obj.append('<span class="loading-animation"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></span>');
    };
    function removeLoading(obj) {
        obj.remove();
    };
    function constructDropdownOptions(elem, data) {
        var option = '<option value="" disabled selected>Select</option>';

        $.each(data, function (key, value) {
            option += "<option value='" + value.id + "'>" + value.name + "</option>";
        });

        elem.html(option);
    };
    function constructDropdownGroupOptions(elem, data) {
        var tempOption = "";

        $.each(data, function (key, value) {
            var optGroup = "<optgroup label='" + value.desc + "'>";

            for (var i = 0; i < value.businessFields.length; i++) {
                optGroup += "<option value='" + value.businessFields[i].id + "'>" + value.businessFields[i].kbli + " " + value.businessFields[i].name + "</option>";
            }

            optGroup += "</optGroup>";
            tempOption += optGroup;         
        });
        elem.html(tempOption);
    };
    function checkEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        return regex.test(email);
    };
    function submitLoading(elem) {
        elem.html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Loading...').addClass('disabled');
    };
    function submitRemoveLoading(elem, title) {
        elem.find('span').remove();
        elem.removeClass('disabled');
        elem.text('Save');
        if (typeof title === 'undefined')
            elem.html('<i class="fa fa-save pr-2"></i><span>Save</span>');
        else
            elem.html('<i class="fa fa-save pr-2"></i><span>' + typeof title === 'undefined' ? 'Save' : title + '</span>');
    };
    function constructUpdateButton(elem) {
        elem.find('span').remove();
        elem.removeClass('disabled');
        elem.text('Update');
        elem.html('<i class="fa fa-edit pr-2"></i> <span>Update</span>');
    };
    function constructNotificationSuccess(msg) {
        toastr.options = {
            preventDuplicates: true,
            preventOpenDuplicates: true
        };
        toastr.success(msg, 'Success', { timeOut: 5000 });
    };
    function constructNotificationError(msg) {
        toastr.options = {
            preventDuplicates: true,
            preventOpenDuplicates: true
        };
        toastr.error(msg, 'Error', { timeOut: 5000 })
    };
    function encryptionHandler(msg) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

        var encrypted = CryptoJS.AES.encrypt(
            CryptoJS.enc.Utf8.parse(msg),
            key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

        return encrypted.toString();
    };
    return {
        ShowLoading: showLoading,
        RemoveLoading: removeLoading,
        ConstructDropdownOptions: constructDropdownOptions,
        ConstructDropdownGroupOptions: constructDropdownGroupOptions,
        CheckEmail: checkEmail,
        SubmitLoading: submitLoading,
        SubmitRemoveLoading: submitRemoveLoading,
        ConstructUpdateButton: constructUpdateButton,
        ConstructNotificationSuccess: constructNotificationSuccess,
        ConstructNotificationError: constructNotificationError,
        EncryptionHandler: encryptionHandler
    };
})();

/**
 * @module jQuery
 * 
 */
jQuery.fn.extend({
    configureMaterialSelect: function () {
        $(this).val("")
            .removeAttr('readonly').attr("placeholder", "Select ").prop('required', true)
            .addClass('form-control').css('background-color', '#fff');
    },
    configureMaterialSelectGroup: function () {
        $(this).val("")
            .removeAttr('readonly').attr("placeholder", "Select Max 5 Bussiness Field ").prop('required', true)
            .addClass('form-control').css('background-color', '#fff');
    },
    removeError: function () {
        $(this).on('change', function () {
            if ($(this).val() != '') {
                $(this).parent().next().next().remove();
            }
        });
    },
    constructUpdateButton: function() {
        $(this).find('span').remove();
        $(this).removeClass('disabled');
        $(this).text('Update');
        $(this).html('<i class="fa fa-edit pr-2"></i> <span>Update</span>');
    },
    constructSaveButton: function () {
        $(this).find('span').remove();
        $(this).removeClass('disabled');
        $(this).text('Save');
        $(this).html('<i class="fa fa-edit pr-2"></i> <span>Save</span>');
    },
    componentAddLoading: function () {
        $(this).html('<div class="d-flex justify-content-center">'
            + '<div class="spinner-grow text-dark" role="status">'
            + '<span class="sr-only">Loading...</span>'
            + '</div>'
            + '</div>');
    },
    iframeLoading: function () {
        $(this).html('<div class="spinner-grow text-dark" role="status" style="position: absolute; top: 50%; left: 50%;"><span class="sr-only">Loading...</span></div>');
    },
    titleTooltipOnApproved: function () {
        $(this).find('.vd_status_tooltip').remove('.vd_status_tooltip');
        $(this).append('<i class="fa fa-check-circle text-success pl-1 vd_status_tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Approved" data-original-title="Approved"></i>');
        $(this).find('.vd_status_tooltip').tooltip();
    },
    titleTooltipOnRejected: function () {
        $(this).find('.vd_status_tooltip').remove('.vd_status_tooltip');
        $(this).append('<i class="fa fa-times-circle text-danger pl-1 vd_status_tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Reject" data-original-title="Reject"></i>');
        $(this).find('.vd_status_tooltip').tooltip();
    },
    titleTooltipOnDisabled: function () {
        $(this).find('.vd_status_tooltip').remove('.vd_status_tooltip');
        $(this).append('<i class="fa fa-ban text-dark pl-1 vd_status_tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Disable" data-original-title="Disable"></i>');
        $(this).find('.vd_status_tooltip').tooltip();
    },
    titleTooltipOnPending: function () {
        $(this).find('.vd_status_tooltip').remove('.vd_status_tooltip');
        $(this).append('<i class="fa fa-hourglass-1 text-info pl-1 vd_status_tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Pending" data-original-title="Pending"></i>');
        $(this).find('.vd_status_tooltip').tooltip();
    },
    settingWrapCenter: function () {
        $(this).addClass('d-flex justify-content-center align-items-center mb-5');
    },
    resetWrapCenter: function () {
        $(this).removeClass('d-flex justify-content-center align-items-center mb-5');
    }
});




