﻿(function ($) {
    'use strict';

    var initialVendor = (function () {
        var wrapBasicInfo, vendorUserId, wrapBankPayment, wrapVendorLegal, wrapVendorTax, wrapVendorAgreement,
            wrapvendorcategories, modalFeedback, btnVendorfeedback, btnVendorApprove, btnVendorReject, btnVendorDisable, feedbackValue, btnFeedbackMsg,
            emailCompany, emailPic, isBasicInfoLoaded, vendorHeaderTitle, vendorStatus, vendorStatusTooltip, wrapVendorCategories,
            wrapVendorDropdownCategories;

        var initialDom = function () {
            wrapBasicInfo = $('#vd_wrap_basic_info'); vendorUserId = $("#vendorUserId"); wrapBankPayment = $("#vd_wrap_bank_payment");
            wrapVendorLegal = $('#vd_wrap_legal'); wrapVendorTax = $('#vd_wrap_tax'); wrapVendorAgreement = $('#vd_wrap_agreement');
            btnVendorApprove = $('#ad_vd_btn_approve'); wrapvendorcategories = $('#vd_wrap_vendor_categories'); modalFeedback = $('#ad_vd_mdl_feedback');
            btnVendorfeedback = $('#ad_vd_btn_feedback'); btnVendorReject = $('#ad_vd_btn_reject'); btnVendorDisable = $('#ad_vd_btn_disable');
            feedbackValue = $('#feedback_value'); btnFeedbackMsg = $('#btn_ad_feed_msg'); emailCompany = $('body').find('#ad_vd_email_comp').text();
            emailPic = $('body').find('#ad_vd_email_pic').text(); vendorStatus = ''; isBasicInfoLoaded = false;
            vendorStatusTooltip = $('.vd_status_tooltip'); $('[data-toggle="tooltip"]').tooltip(); vendorHeaderTitle = $('#ad_vd_header_title');
            wrapVendorDropdownCategories = $('#wrap-dropdown-vendor-categories');
        };

        var initialForm = function () {
            loadVendorBasicInfo();
            loadVendorBankPayment();
            loadVendorLegal();
            loadVendorTax();
            loadVendorAgreement();
            loadVendorCategories();
        };

        var loadVendorBasicInfo = function () {
            $.when(wrapBasicInfo.settingWrapCenter()).done(wrapBasicInfo.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetBasicInfo + "/" + vendorUserId.val())).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapBasicInfo.resetWrapCenter()).done(wrapBasicInfo.html(result));

                    isBasicInfoLoaded = true;

                    eventAfterBasicInfoLoaded();
                }, 500);
            });
        };

        var loadVendorBankPayment = function () {
            $.when(wrapBankPayment.settingWrapCenter()).done(wrapBankPayment.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetBankPayment + "/" + vendorUserId.val())).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapBankPayment.resetWrapCenter()).done(wrapBankPayment.html(result));
                }, 500);
            });
        };

        var loadVendorLegal = function () {
            $.when(wrapVendorLegal.settingWrapCenter()).done(wrapVendorLegal.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetVendorLegal + "/" + vendorUserId.val())).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapVendorLegal.resetWrapCenter()).done(wrapVendorLegal.html(result));
                }, 500);
            });
        };

        var loadVendorTax = function () {
            $.when(wrapVendorTax.settingWrapCenter()).done(wrapVendorTax.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetVendorTax + "/" + vendorUserId.val())).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapVendorTax.resetWrapCenter()).done(wrapVendorTax.html(result));
                }, 500);
            });
        };

        var loadVendorAgreement = function () {
            $.when(wrapVendorAgreement.settingWrapCenter()).done(wrapVendorAgreement.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetVendorAgreement + "/" + vendorUserId.val())).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapVendorAgreement.resetWrapCenter()).done(wrapVendorAgreement.html(result));
                }, 500);
            });
        };

        var loadVendorCategories = function () {
            $.when(wrapvendorcategories.settingWrapCenter()).done(wrapvendorcategories.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetVendorCategories + "/" + vendorUserId.val())).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapvendorcategories.resetWrapCenter()).done(wrapvendorcategories.html(result));
                }, 500);
            });
        };

        var openModalVendorFeedback = function () {
            btnVendorfeedback.click(function (e) {
                modalFeedback.modal('show');

                vendorStatus = WEBPORTAL.VendorStatus.Feedback;
            });
        };

        var openModalApprovalFeedback = function () {
            btnVendorApprove.click(function (e) {
                modalFeedback.modal('show');

                vendorStatus = WEBPORTAL.VendorStatus.Approved;
            });
        };

        var openModalRejectFeedback = function () {
            btnVendorReject.click(function (e) {
                modalFeedback.modal('show');

                vendorStatus = WEBPORTAL.VendorStatus.Rejected;
            });
        };

        var openModalDisableFeedback = function () {
            btnVendorDisable.click(function (e) {
                modalFeedback.modal('show');

                vendorStatus = WEBPORTAL.VendorStatus.Disabled;
            });
        };

        var submitFeedbackMessage = function () {
            btnFeedbackMsg.click(function (e) {
                WEBPORTAL.Utility.SubmitLoading(btnFeedbackMsg);

                if (vendorStatus == 'APPROVED') {
                    if ($('#categories-select-option').val().length == 0) {
                        if ($('#wrap-dropdown-vendor-categories .invalid-feedback').length == 0)
                            $('#wrap-dropdown-vendor-categories').append('<div class="invalid-feedback" style="display: block">Please choose at least one category type.</div>');
                        $(window).scrollTop($('#wrap-dropdown-vendor-categories').offset().top);

                        setTimeout(function () {
                            modalFeedback.modal('toggle');
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnFeedbackMsg, 'Send Feedback');
                        }, 400);
                    } else {
                        $('#wrap-dropdown-vendor-categories .invalid-feedback').remove();

                        var param = {
                            VendorId: vendorUserId.val(),
                            Status: vendorStatus,
                            EmailCompany: $('body').find('#ad_vd_email_comp').text(),
                            EmailPic: $('body').find('#ad_vd_email_pic').text(),
                            Messages: feedbackValue.val(),
                            MasterDataUserId: window.location.href.split('/')[7],
                            ListCategoryId: $('#categories-select-option').val()
                        };

                        $.when(WEBPORTAL.Services.POSTLocal(param, WEBPORTAL.URLContext.UpdateVendorStatus))
                            .done(function (result, status, xhr) {
                                autoChangeTitleHeader(vendorStatus);

                                setTimeout(function () {
                                    modalFeedback.modal('toggle');
                                    WEBPORTAL.Utility.SubmitRemoveLoading(btnFeedbackMsg, 'Send Feedback');
                                }, 400);

                                if (result.code == 200) {
                                    setTimeout(function () {
                                        WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                                    }, 500);
                                } else {
                                    setTimeout(function () {
                                        WEBPORTAL.Utility.ConstructNotificationError(result.message);
                                    }, 500);
                                }
                            });
                    }
                } else {
                    $('#wrap-dropdown-vendor-categories .invalid-feedback').remove();

                    var param = {
                        VendorId: vendorUserId.val(),
                        Status: vendorStatus,
                        EmailCompany: $('body').find('#ad_vd_email_comp').text(),
                        EmailPic: $('body').find('#ad_vd_email_pic').text(),
                        Messages: feedbackValue.val(),
                        MasterDataUserId: window.location.href.split('/')[7],
                        ListCategoryId: $('#categories-select-option').val()
                    };

                    $.when(WEBPORTAL.Services.POSTLocal(param, WEBPORTAL.URLContext.UpdateVendorStatus))
                        .done(function (result, status, xhr) {
                            autoChangeTitleHeader(vendorStatus);

                            setTimeout(function () {
                                modalFeedback.modal('toggle');
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnFeedbackMsg, 'Send Feedback');
                            }, 400);

                            if (result.code == 200) {
                                setTimeout(function () {
                                    WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                                }, 500);
                            } else {
                                setTimeout(function () {
                                    WEBPORTAL.Utility.ConstructNotificationError(result.message);
                                }, 500);
                            }
                        });
                }
                
            });
        };

        var resetFeedbackValue = function () {
            modalFeedback.on('hidden.bs.modal', function (e) {
                feedbackValue.val('');
            });
        };

        var eventAfterBasicInfoLoaded = function () {
            if (!isBasicInfoLoaded) {
                btnFeedbackMsg.attr('disabled', true);
            } else {
                btnFeedbackMsg.attr('disabled', false);
            }
        };

        var autoChangeTitleHeader = function (vendorStatus) {
            switch (vendorStatus) {
                case WEBPORTAL.VendorStatus.Approved:
                    vendorHeaderTitle.titleTooltipOnApproved();
                    changeUrlState(vendorStatus);
                    break;
                case WEBPORTAL.VendorStatus.Rejected:
                    vendorHeaderTitle.titleTooltipOnRejected();
                    changeUrlState(vendorStatus);
                    break;
                case WEBPORTAL.VendorStatus.Disabled:
                    vendorHeaderTitle.titleTooltipOnDisabled();
                    changeUrlState(vendorStatus);
                    break;
                default:
                    vendorHeaderTitle.titleTooltipOnPending();
                    changeUrlState(vendorStatus);
                    break;
            }
        };

        var changeUrlState = function (vendorSatus) {
            var url = window.location.href;

            window.history.replaceState(url, '', url.slice(0, url.indexOf(url.split('/')[6])) + vendorSatus + '/' + url.split('/')[7]);
        };

        var run = function () {
            initialDom();
            initialForm();
            openModalVendorFeedback();
            openModalApprovalFeedback();
            openModalRejectFeedback();
            openModalDisableFeedback();
            submitFeedbackMessage();
            resetFeedbackValue();
            eventAfterBasicInfoLoaded();
        };

        return {
            run: run
        };
    })(jQuery);

    initialVendor.run();
})(jQuery);