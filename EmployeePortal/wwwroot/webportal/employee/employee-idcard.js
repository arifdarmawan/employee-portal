﻿EMPLOYEEALL = EMPLOYEEALL || {};

EMPLOYEEALL.IDCard = (function () {
    //var initIDCard = (function () {
    var initialGrid = function () {
        EMPLOYEEALL.Grid.DataTable({
            ajax: { url: EMPLOYEEALL.URLContext.GetIDCard, dataSrc: '' },
            //processing: true,
            //serverSide: true,
            columns: [

                { data: 'Id', title: "Id", visible: false },
                { data: 'Seq', title: "#" },
                { data: 'CardTypeName', title: "Card Type Number Ess" },
                { data: 'CardNo', title: "Card Number" },
                { data: 'Publisher', title: "Publisher" },
                { data: 'ExpiredDateDisp', title: "Expired Date" },
                { data: 'Description', title: "Description" },
                {
                    data: "IsPostedDisp",
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        var toRenderClass = cellData === "Approved" ? "badge-success" : "badge-info";
                        $(td).html('<span class="badge badge-pill ' + toRenderClass + '">' + cellData + '</span></td>');
                    }
                },
                {
                    data: null,
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).html('<div class="btn-group">'
                            + '<a href="#" data-toggle="modal" data-target="#editIDCardModal" class="btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-edit"></i></a>'
                            + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom"> <i class="fa fa-trash"></i></a>'
                            + '</div >');
                    }
                }
            ]
        });
    };

    var run = function () {
        initialGrid();
    };

    return {
        run: run
    };
    //})();
})();

EMPLOYEEALL.IDCard.run();
