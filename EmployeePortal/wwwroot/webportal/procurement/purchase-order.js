﻿(function ($) {
    'use strict';

    var initialPurchaseOrder = (function () {
        var grid, dataTable;

        var initialDom = function () {
            grid = $('#grid');
        };

        var initialGrid = function () {
            dataTable = grid.DataTable({
                ajax: { url: WEBPORTAL.URLContext.GetPurchaseOrder, dataSrc: '' },
                columns: [
                    { data: 'Id', visible: false },
                    { data: 'PONumber', title: "PO Number" },
                    { data: 'PODate', title: "PO Date" },
                    { data:'StringGrandTotalAfterTax',title:"Grand Total"},
                    {
                        data: 'POStatus',
                        className: "text-center",
                        title: "Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === 'WAITING') {
                                $(td).html('<span class="badge badge-primary">' + cellData + '</span>');
                            }
                            else if (cellData === 'PROCESS') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData === 'CANCEL') {
                                $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                            }
                        }
                    }
                ],
                "order": [[1, 'desc']],
                select: true,
                initComplete: function (settings, json) {
                    grid.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var gridRowOnClick = function () {
            grid.on('click', 'tr', function () {
                var rowData = dataTable.row(this).data();
                window.location.href = WEBPORTAL.URLContext.PurchaseOrderDetails + '/' + rowData.Id;
            });
        };

        var run = function () {
            initialDom();
            initialGrid();
            gridRowOnClick();
        };

        return {
            run: run
        };
    })(jQuery);

    initialPurchaseOrder.run();
})(jQuery);