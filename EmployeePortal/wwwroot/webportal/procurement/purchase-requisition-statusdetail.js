﻿(function ($) {
    'use strict';

    var initialPurchaseRequisitionStatusDetails = (function () {
        var wrapItemBudget, budgetyear, tableDetailPR

        var initialDom = function () {
            tableDetailPR = $('#table-detail-pr tbody tr');
            wrapItemBudget = $('#pr_wrap_budget');
        };

        var gridRowOnClick = function () {
            tableDetailPR.click(function () {
                var d = new Date();
                budgetyear = d.getFullYear();

                var model = {
                    CompanyCode: $(this).attr('data-company-code'),
                    CompanyLocationCode: $(this).attr('data-location-code'),
                    ItemCode: $(this).attr('data-bims-code'),
                    Quantity: $(this).find('#col-qty').attr('data-column'),
                    TotalPrice: $(this).find('#col-totalPrice').attr('data-column'),
                };

                $.when(wrapItemBudget.settingWrapCenter()).done(wrapItemBudget.componentAddLoading());
                $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetItemBudget + "/" + model.CompanyCode + "/" + model.CompanyLocationCode + "/" + budgetyear + "/" + model.ItemCode + "/" + model.Quantity + "/" + model.TotalPrice)).done(function (result, status, xhr) {
                    setTimeout(function () {
                        wrapItemBudget.removeAttr('hidden');
                        $.when(wrapItemBudget.resetWrapCenter()).done(wrapItemBudget.html(result));
                    }, 500);
                });
            });
        };

        var run = function () {
            initialDom();
            gridRowOnClick();
        };

        return {
            run: run
        };
    })(jQuery);

    initialPurchaseRequisitionStatusDetails.run();
})(jQuery);