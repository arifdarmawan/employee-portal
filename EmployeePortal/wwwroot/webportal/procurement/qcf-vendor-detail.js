﻿(function ($) {
    'use strict';

    var initialQCFVendorDetail = (function () {
        var wrapVendorItem, modalApprovalDetail, btnViewDetail;

        var initialDom = function () {
            wrapVendorItem = $('.appr tr');
            modalApprovalDetail = $('#modalVendor');
            btnViewDetail = $('#btn-view-detail-vendor');
        };

        var clickDetailVendor = function () {
            wrapVendorItem.click(function () {
                console.log($(this).closest('tr').attr('data-rfq-id'));
                modalApprovalDetail.modal('show');
                loadApprovalDataDetail($(this).closest('tr').attr('data-rfq-id'));
            });
        };

        var loadApprovalDataDetail = function (rfqid) {
            $.when(modalApprovalDetail.find('.modal-body').settingWrapCenter()).done(modalApprovalDetail.find('.modal-body').componentAddLoading());           
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetDetailVendor + '/' + rfqid)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(modalApprovalDetail.find('.modal-body').resetWrapCenter()).done(modalApprovalDetail.find('.modal-body').html(result));
                }, 500);
            });
        };

        var run = function () {
            initialDom();
            clickDetailVendor();
        };

        return {
            run: run
        }
    })(jQuery);

    initialQCFVendorDetail.run();
})(jQuery);