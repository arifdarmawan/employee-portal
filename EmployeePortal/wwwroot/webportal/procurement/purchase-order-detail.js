﻿(function ($) {
    'use strict';

    var initialPurchaseOrderDetail = (function () {
        var companyCode, companyLocationCode, wrapItemBudget, budgetyear, grid, dataTable;

        var initialDom = function () {
            companyCode = $('#CompanyCode');
            companyLocationCode = $('#CompanyLocationCode');
            grid = $('#Grid');
            wrapItemBudget = $('#pr_wrap_budget');
            dataTable = grid.DataTable();
        };

        var initialForm = function () {
            gridRowOnClick();
        };

        var gridRowOnClick = function () {
            grid.on('click', 'tr', function () {
                var d = new Date();
                budgetyear = d.getFullYear();

                var company = companyCode.val();
                var location = companyLocationCode.val();
                var rowData = dataTable.row(this).data();

                $.when(wrapItemBudget.settingWrapCenter()).done(wrapItemBudget.componentAddLoading());
                $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetItemBudget + "/" + company + "/" + location + "/" + budgetyear + "/" + rowData[0] + "/" + rowData[4] + "/" + rowData[7])).done(function (result, status, xhr) {
                    setTimeout(function () {
                        wrapItemBudget.removeAttr('hidden');
                        $.when(wrapItemBudget.resetWrapCenter()).done(wrapItemBudget.html(result));
                    }, 500);
                });
            });
        };

        var run = function () {
            initialDom();
            initialForm();
        };

        return {
            run: run
        };
    })(jQuery);

    initialPurchaseOrderDetail.run();
})(jQuery);