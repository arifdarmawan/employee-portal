﻿(function ($) {
    'use strict';

    var initialPurchaseRequisitionStatus = (function () {
        var grid, dataTable;

        var initialDom = function () {
            grid = $('#grid');
        };

        var initialGrid = function () {
            dataTable = grid.DataTable({
                ajax: { url: WEBPORTAL.URLContext.GetPurchaseRequisitionList, dataSrc: '' },
                columns: [
                    { data: 'Id', visible: false },
                    { data: 'PRNumber', title: "PR Number" },
                    { data: 'StringDate', title: "PR Date"},
                    { data: 'RequestedFrom', title: "Requestor" },
                    //{ data: 'Currency', title: "Currency" },
                    { data: 'StringGrandTotalAfterTax', className: "text-right", title: "Grand Total" },
                    {
                        data: 'Prstatus',
                        className: "text-center",
                        title: "Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === 'WAITING_APPROVAL') {
                                $(td).html('<span class="badge badge-primary">' + "WAITING APPROVAL" + '</span>');
                            } else if (cellData === 'APPROVED') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData === 'COMPLETED') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else {
                                $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                            }
                        }
                    }
                ],
                "order": [[1, 'desc']],
                select: true,
                initComplete: function (settings, json) {
                    grid.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var gridRowOnClick = function () {
            grid.on('click', 'tr', function () {
                var rowData = dataTable.row(this).data();
                window.location.href = WEBPORTAL.URLContext.PurchaseRequisitionStatusDetails + '/' + rowData.Id + '/' + rowData.PRNumber;
            });
        };

        var run = function () {
            initialDom();
            initialGrid();
            gridRowOnClick();
        };

        return {
            run: run
        };
    })(jQuery);

    initialPurchaseRequisitionStatus.run();
})(jQuery);