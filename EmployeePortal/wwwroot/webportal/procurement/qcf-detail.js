﻿(function ($) {
    'use strict';

    var initialQCFDetail = (function () {
        var grid, prId, companyCode, departementCode, companyLocationCode, masterItemCategoryId, masterItemId, budgetyear,
            wrapPurchaseRequisitionBudget, btnpreviewqcf, btnopenmodal, modalvendordetail, griddetailvendor, listVendor;

        var initialDom = function () {
            wrapPurchaseRequisitionBudget = $('#pr_wrap_budget');
            listVendor = $('#list_item_approved');
            grid = $('#table-detail-request-pr tbody tr');
            prId = $("#Prid");
            griddetailvendor = $('#gridDetailVendor tbody tr');
            companyCode = $("#CompanyCode");
            departementCode = $("#DepartementCode");
            companyLocationCode = $("#CompanyLocationCode");
            masterItemCategoryId = $("#MasterItemCategoryId");
            masterItemId = $("#MasterItemId");
            btnpreviewqcf = $('#btn_Preview_QCF');
        };

        var initialForm = function () {
            gridRowOnClick();
        };

        var gridRowOnClick = function () {
            grid.click(function () {
                var d = new Date();
                budgetyear = d.getFullYear();

                var model = {
                    CompanyCode: $(this).attr('data-company-code'),
                    CompanyLocationCode: $(this).attr('data-location-code'),
                    ItemCode: $(this).attr('data-bims-code'),
                    Quantity: $(this).find('#col-qty').attr('data-column'),
                    TotalPrice: $(this).find('#col-totalPrice').attr('data-column'),
                };

                $.when(wrapPurchaseRequisitionBudget.settingWrapCenter()).done(wrapPurchaseRequisitionBudget.componentAddLoading());
                $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetItemBudget + "/" + model.CompanyCode + "/" + model.CompanyLocationCode + "/" + budgetyear + "/" + model.ItemCode + "/" + model.Quantity + "/" + model.TotalPrice)).done(function (result, status, xhr) {
                    setTimeout(function () {
                        wrapPurchaseRequisitionBudget.removeAttr('hidden');
                        $.when(wrapPurchaseRequisitionBudget.resetWrapCenter()).done(wrapPurchaseRequisitionBudget.html(result));
                    }, 500);
                });
            });
        };

        var PreviewQCFScreen = function () {
            btnpreviewqcf.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnpreviewqcf);
                window.open(WEBPORTAL.URLContext.QCFPreview + '/' + prId.val() + "/" + masterItemId.val(), '_blank');
                WEBPORTAL.Utility.SubmitRemoveLoading(btnpreviewqcf);

                btnpreviewqcf.find('span').text("Preview");
            });
        };

        var ViewListVendor = function () {
            $.when(listVendor.settingWrapCenter()).done(listVendor.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetListVendor + "/" + prId.val() + "/" + masterItemId.val())).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(listVendor.resetWrapCenter()).done(listVendor.html(result));
                }, 500)
            });
        };

        var run = function () {
            initialDom();
            initialForm();
            ViewListVendor();
            PreviewQCFScreen();
        };

        return {
            run: run
        };
    })(jQuery);

    initialQCFDetail.run();
})(jQuery);