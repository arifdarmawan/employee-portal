﻿(function ($) {
    'use strict';
    
    var initialPurchaseRequisitionItems = (function () {
        var grid, dataTable;

        var initialDom = function () {
            grid = $('#grid');
        };

        var initialGrid = function () {
            dataTable = grid.DataTable({
                ajax: { url: WEBPORTAL.URLContext.GetPurchaseRequisitionList, dataSrc: '' },
                columns: [
                    { data: 'Id', visible: false },
                    { data: 'PRNumber', title: "PR Number" },
                    { data: 'CreatedDate', title: "PR Date" },
                    { data: 'RequestedFrom', title: "Requestor" },
                    { data: 'GrandTotal', title: "Amount", render: $.fn.dataTable.render.number(',', '.', 2) },
                    {
                        data: null,
                        title: "Budget Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData == 'On Budget') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData == 'PTA') {
                                $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                            } else {
                                $(td).html('<span class="badge badge-primary">' + cellData + '</span>');
                            }
                        }
                    },
                    {
                        data: 'Prstatus',
                        title: "Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData == 'WAITING APPROVAL') {
                                $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                            } else if (cellData == 'APPROVED') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData == 'COMPLETED') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else {
                                $(td).html('<span class="badge badge-primary">' + cellData + '</span>');
                            }
                        }
                    }
                ],
                select: true,
                initComplete: function (settings, json) {
                    grid.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var gridRowOnClick = function () {
            grid.on('click', 'tr', function () {
                $.when(wrapPRbudget.settingWrapCenter()).done(wrapPRbudget.componentAddLoading());
                $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetItemBudget + "/" + location + "/" + budgetyear + "/" + rowData.ItemCode + "/" + rowData.Qty + "/" + rowData.Price)).done(function (result, status, xhr) {
                    setTimeout(function () {
                        wrapPRbudget.removeAttr('hidden');
                        $.when(wrapPRbudget.resetWrapCenter()).done(wrapPRbudget.html(result));
                    }, 500);
                });
            });
        };

        var run = function () {
            initialDom();
            initialGrid();
            gridRowOnClick();
        };

        return {
            run: run
        };
    })();

    initialPurchaseRequisitionItems.run();
})(jQuery);