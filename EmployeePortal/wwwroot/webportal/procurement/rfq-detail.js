﻿(function ($) {
    'use strict';

    var initialRFQDetail = (function () {
        var grid, prId, companyCode, departementCode, companyLocationCode, masterItemCategoryId, masterItemId, qty,
            itemVendorStatus, budgetyear, btnBlasttoVendor, wrapPurchaseRequisitionBudget, notificationdisable;

        var initialDom = function () {
            wrapPurchaseRequisitionBudget = $('#pr_wrap_budget');
            grid = $('#table-detail-request-pr tbody tr');
            btnBlasttoVendor = $("#pr_btn_blast_to_vendor");
            prId = $("#Prid");
            companyCode = $("#CompanyCode");
            departementCode = $("#DepartementCode");
            companyLocationCode = $("#CompanyLocationCode");
            masterItemCategoryId = $("#MasterItemCategoryId");
            masterItemId = $("#MasterItemId");
            qty = $("#Qty");
            itemVendorStatus = $('#ItemVendorStatus');
            notificationdisable = $('#notification-disable');
        };

        var initialForm = function () {
            gridRowOnClick();
        };

        var gridRowOnClick = function () {
            grid.click(function () {
                var d = new Date();
                budgetyear = d.getFullYear();

                var model = {
                    CompanyCode: $(this).attr('data-company-code'),
                    CompanyLocationCode: $(this).attr('data-location-code'),
                    ItemCode: $(this).attr('data-bims-code'),
                    Quantity: $(this).find('#col-qty').attr('data-column'),
                    TotalPrice: $(this).find('#col-totalPrice').attr('data-column'),
                };

                $.when(wrapPurchaseRequisitionBudget.settingWrapCenter()).done(wrapPurchaseRequisitionBudget.componentAddLoading());
                $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetItemBudget + "/" + model.CompanyCode + "/" + model.CompanyLocationCode + "/" + budgetyear + "/" + model.ItemCode + "/" + model.Quantity + "/" + model.TotalPrice)).done(function (result, status, xhr) {
                    setTimeout(function () {
                        wrapPurchaseRequisitionBudget.removeAttr('hidden');
                        $.when(wrapPurchaseRequisitionBudget.resetWrapCenter()).done(wrapPurchaseRequisitionBudget.html(result));
                    }, 500);
                });
            });
        };

        var blasttoVendor = function () {
            btnBlasttoVendor.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnBlasttoVendor);

                let model = {
                    Prid: prId.val(),
                    CompanyCode: companyCode.val(),
                    DepartementCode: departementCode.val(),
                    CompanyLocationCode: companyLocationCode.val(),
                    MasterItemCategoryId: masterItemCategoryId.val(),
                    MasterItemId: masterItemId.val(),
                    Qty: qty.val()
                };

                $.when(WEBPORTAL.Services.POSTLocal(model, URL_CONTEXT_BlastToVendor)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        setTimeout(function () {
                            btnBlasttoVendor.constructUpdateButton();
                            WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            window.location.href = URL_CONTEXT_GetPurchaseRequisitionQCF;
                        }, 500);
                    } else {
                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnBlasttoVendor);
                            WEBPORTAL.Utility.ConstructNotificationError(result.message);
                        }, 500);
                    }
                });
            });
        };

        var buttonBehaviour = function () {
            if (itemVendorStatus.val() === "RFQ_ON_PROCESS") {
                console.log(itemVendorStatus.val());
                $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.getQuotationComparationFormByPrId + "/" + prId.val() + "/" + masterItemId.val())).done(function (result, status, xhr) {
                    setTimeout(function () {
                        console.log(result.data);
                        if (result.data !== null) {
                            notificationdisable.prop('hidden', true);
                        } else {
                            notificationdisable.prop('hidden', false);
                        }
                    }, 500);
                });
                btnBlasttoVendor.find('span').text("RFQ on Process");
                btnBlasttoVendor.prop('disabled', true);
            }
            else if (itemVendorStatus.val() === "VENDOR_IS_EXIST") {
                btnBlasttoVendor.find('span').text("Closed");
                btnBlasttoVendor.prop('hidden', true);
            }
            else if(itemVendorStatus.val() !== "REQUEST_VENDOR") {
                btnBlasttoVendor.prop('disabled', false);
            }
        };

        var run = function () {
            initialDom();
            initialForm();
            blasttoVendor();
            buttonBehaviour();
        };

        return {
            run: run
        };
    })(jQuery);

    initialRFQDetail.run();
})(jQuery);