﻿(function ($) {
    'use strict';

    var initialRFQ = (function () {
        var grid, dataTable;

        var initialDom = function () {
            grid = $('#grid');
        };

        var initialGrid = function () {
            dataTable = grid.DataTable({
                ajax: { url: WEBPORTAL.URLContext.GetRFQList, dataSrc: '' },
                columns: [
                    { data: 'MasterPurchaseRequisitionId', visible: false },
                    { data: 'PRNumber', title: "PRNumber" },
                    { data: 'MasterItemId', visible: false },
                    { data: 'ItemName', title: "Item Name" },
                    { data: 'Specification', title: "Specification" },
                    { data: 'Qty', title: "Quantity" },
                    { data: 'TotalPrice', title: "Price", render: $.fn.dataTable.render.number(',', '.', 2) },
                    {
                        data: 'ItemVendorStatus',
                        title: "Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === 'REQUEST_VENDOR') {
                                $(td).html('<span class="badge badge-danger">' + "WAITING RFQ" + '</span>');
                            } else if (cellData === 'RFQ_ON_PROCESS') {
                                $(td).html('<span class="badge badge-primary">' + "ON PROCESS" + '</span>');
                            } else if (cellData === 'VENDOR_IS_EXIST') {
                                $(td).html('<span class="badge badge-success">' + "CLOSED" + '</span>');
                            }
                        }
                    }
                ],
                "order": [[1, 'desc']],
                select: true,
                initComplete: function (settings, json) {
                    grid.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var gridRowOnClick = function () {
            grid.on('click', 'tr', function () {
                var rowData = dataTable.row(this).data();
                window.location.href = WEBPORTAL.URLContext.RFQDetails + '/' + rowData.MasterPurchaseRequisitionId + '/' + rowData.MasterItemId;
            });
        };

        var run = function () {
            initialDom();
            gridRowOnClick();
            initialGrid();
        };

        return {
            run: run
        };
    })(jQuery);

    initialRFQ.run();
})(jQuery);