﻿(function ($) {
    'use strict';

    var initialQCFPreview = (function () {
        var prid, masterDataVendorId, notes, btnsendpo, radiowinner1, radiowinner2, radiowinner3;

        var initialDom = function () {
            prid = $("#Prid");
            masterDataVendorId = $("#MasterDataVendorId");
            radiowinner1 = $('#MasterDataVendorId1');
            radiowinner2 = $('#MasterDataVendorId2');
            radiowinner3 = $('#MasterDataVendorId3');
            notes = $("#Notes");
            btnsendpo = $('#btn-send-po');
        };

        var getVendorWinner = function () {
            $('input[name="inlineMaterialRadiosExample"]:radio').change(function () {
                masterDataVendorId.val($("input[name='inlineMaterialRadiosExample']:checked").val());
            });
        };

        var radioButtonWinnerOnload = function () {
            console.log(masterDataVendorId.val());
            if (masterDataVendorId.val() > 0) {
                if (radiowinner1.val() == masterDataVendorId.val()) {

                    radiowinner1.prop('checked', true);
                    radiowinner2.prop('disabled', true);
                    radiowinner3.prop('disabled', true);

                    btnsendpo.prop('disabled', true);
                } else if (radiowinner2.val() == masterDataVendorId.val()) {

                    radiowinner2.prop('checked', true);
                    radiowinner1.prop('disabled', true);
                    radiowinner3.prop('disabled', true);

                    btnsendpo.prop('disabled', true);
                } else if (radiowinner3.val() == masterDataVendorId.val()) {

                    radiowinner3.prop('checked', true);
                    radiowinner1.prop('disabled', true);
                    radiowinner2.prop('disabled', true);

                    btnsendpo.prop('disabled', true);
                }
            } else {

                console.log(radiowinner1.val());
                console.log(radiowinner2.val());
                console.log(radiowinner3.val());

                if (radiowinner1.val() > 0) {
                    radiowinner1.prop('disabled', false);
                    btnsendpo.prop('disabled', false);
                } else {
                    radiowinner1.prop('disabled', true);
                    btnsendpo.prop('disabled', true);
                    notes.prop('disabled', true);
                }

                if (radiowinner2.val() > 0) {
                    radiowinner2.prop('disabled', false);
                }
                else {
                    radiowinner2.prop('disabled', true);
                }

                if (radiowinner3.val() > 0) {
                    radiowinner3.prop('disabled', false);
                } else {
                    radiowinner3.prop('disabled', true);
                }
            }
        };

        var generatePO = function () {
            btnsendpo.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnsendpo);

                var param = {
                    Prid: prid.val(),
                    MasterDataVendorId: masterDataVendorId.val(),
                    Notes: notes.val()
                };

                $.when(WEBPORTAL.Services.POSTLocal(param, WEBPORTAL.URLContext.ProcessQuotationComparationForm))
                    .done(function (result, status, xhr) {

                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnsendpo);
                        }, 400);

                        if (result.code == 200) {
                            setTimeout(function () {
                                WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                                btnsendpo.prop('disabled', true);
                                window.close();
                                window.location.href = WEBPORTAL.URLContext.GetQCFList;
                            }, 500);
                        } else {
                            setTimeout(function () {
                                WEBPORTAL.Utility.ConstructNotificationError(result.message);
                                btnsendpo.prop('disabled', false);
                            }, 500);
                        }
                    });
            });
        };

        var run = function () {
            initialDom();   
            getVendorWinner();
            radioButtonWinnerOnload();
            generatePO();
        };

        return {
            run: run
        };
    })(jQuery);

    initialQCFPreview.run();
})(jQuery);