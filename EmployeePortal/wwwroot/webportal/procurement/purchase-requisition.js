﻿(function ($) {
    'use strict';

    var initialPurchaseRequisition = (function () {
        let urlContext = '';
        let id, purchaseRequisitionForm, configCompanyId, companyCode, configDivisionId, departementCode, companylocationCode, lblcompanylocationCode,
            requestedfrom, lblrequestedfrom, itemId, itemCode, itemName, lblitemName, specification, lblspecification, qty, lblqty, uomId, lbluomId,
            currency, lblcurrency, unitPrice, lblunitPrice, grandTotal, purposeDesc, lblpurposeDesc, itemStatus, dateRequired, lbldaterequired,
            gridcreateprlist, btnopenAddItemModal, modalAddItem, btnadditem, btnSavePR, wrapPRbudget, budgetyear,
            datatable, dataItem, rowNumber, getRowId, datePicker, frameModal, btnCloseFrame;

        var initialDom = function () {
            id = $('#Id');
            purchaseRequisitionForm = $('#vendor-frm-add-item');
            configCompanyId = $('#ConfigCompanyId');
            companyCode = $('#CompanyCode');
            companylocationCode = $('#CompanyLocationCode');
            lblcompanylocationCode = $('#lblCompanyLocationCode');
            configDivisionId = $('#ConfigDivisionId');
            departementCode = $('#DepartementCode');
            requestedfrom = $('#RequestedFrom');
            lblrequestedfrom = $('#lblRequestedFrom');
            itemId = $('#MasterItemId');
            itemCode = $('#ItemCode');
            itemName = $('#ItemName');
            lblitemName = $('#lblItemName');
            specification = $('#Specification');
            lblspecification = $('#lblSpecification');
            qty = $('#Qty');
            lblqty = $('#lblQty');
            uomId = $('#UomId');
            lbluomId = $('#lblUomId');
            currency = $('#CurrencyId');
            lblcurrency = $('#lblCurrencyId');
            unitPrice = $('#UnitPrice');
            lblunitPrice = $('#lblUnitPrice');
            purposeDesc = $('#PurposeDesc');
            lblpurposeDesc = $('#lblPurposeDesc');
            dateRequired = $('#DateRequired');
            lbldaterequired = $('#lblDateRequired');
            gridcreateprlist = $('#grid_create_pr_list');
            wrapPRbudget = $('#pr_wrap_budget');
            btnadditem = $('#btn_Add_Item');
            btnopenAddItemModal = $('.btn-open_Add-ItemForm');
            modalAddItem = $('#addItemModal');
            btnSavePR = $("#pr_btn-save");
            datePicker = $('.datepicker').pickadate({ formatSubmit: 'yyyy/mm/dd', min: new Date() });
            frameModal = $('#frameModalBottom');
            btnCloseFrame = $('#btncloseframeModal');
            dataItem = [];
            rowNumber = 0;
        };

        var itemAutoComplete = function () {
            itemName.autocomplete({
                serviceUrl: WEBPORTAL.URLContext.SearchItemByKey,
                minChars: 3,
                showNoSuggestionNotice: true,
                noSuggestionNotice: "No results found",
                transformResult: function (response) {
                    if (response) {
                        return {
                            suggestions: JSON.parse(response)
                        };
                    }
                    else {
                        return {
                            suggestions: []
                        };
                    }
                },
                onSelect: function (suggestion) {
                    $("#MasterItemId").val(suggestion.id);
                    $("#ItemCode").val(suggestion.code);
                    $("#ItemName").val(suggestion.value);
                    $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.ApiEndPointBaseUrl + WEBPORTAL.URLContext.GetDataItem + '/?bimsCode=' +
                        suggestion.code)).done(function (result, status, xhr) {
                            var res = result.data;
                            console.log(res);
                            if (res != null) {

                                // auto fill value
                                specification.val(res.itemSpecification.replace(/"/g, '').replace(/{/g, '').replace(/}/g, ''));
                                $('#UomId option[value="' + res.uomid + '"]').attr('selected', 'selected');
                                unitPrice.val(res.unitPrice);
                                $('#CurrencyId option[value="' + res.currencyid + '"]').attr('selected', 'selected');

                                // handling css
                                lblspecification.addClass('active');
                                lblunitPrice.addClass('active');
                                uomId.removeClass('invalid');
                                specification.removeClass('invalid');
                                $('.pr_inpt_err_uom').remove();
                                $('.pr_inpt_err_specification').remove();

                                //$.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.ApiEndPointBaseUrl + WEBPORTAL.URLContext.GetVendorItemPrice + '/?itemId=' +
                                //    suggestion.id)).done(function (result, status, xhr) {
                                //        var resPrice = result.data
                                //        if (resPrice != null) {
                                //            // auto fill value
                                //            unitPrice.val(res.price);
                                //            $('#CurrencyId option[value="' + res.currencyid + '"]').attr('selected', 'selected');

                                //            // handling css
                                //            lblunitPrice.addClass('active');
                                //        }
                                //        else {
                                //            unitPrice.val('');
                                //            $('#CurrencyId option:selected').attr('selected', false);
                                //            lblunitPrice.removeClass('active');
                                //        }
                                //    });
                            } else {
                                specification.val('');
                                $('#UomId option:selected').attr('selected', false);
                                unitPrice.val('');
                                $('#CurrencyId option:selected').attr('selected', false);

                                // handling css
                                lblspecification.removeClass('active');
                                lblunitPrice.removeClass('active');
                            }
                        });
                }
            });
            itemName.keyup(function () {
                $.when(WEBPORTAL.Services.GETLocal('/Procurement/SearchItemByKey/' + '?query=' + this.value)).done(function (result, status, xhr) {

                });
            });
        };

        var initialMDB = initialMDB || {};
        initialMDB.Configuration = {};
        initialMDB.Configuration.PurchaseRequisition = {};

        var initialgridcreateprlist = function () {
            datatable = gridcreateprlist.DataTable({
                data: dataItem,
                searching: false,
                columns: [
                    { data: "MasterItemId", visible: false },
                    { data: "ItemCode", visible: false },
                    { data: "ItemName", title: "Item/ServiceName" },
                    { data: "Specification", title: "Specification" },
                    { data: "Qty", title: "Quantity" },
                    { data: "UomId", visible: false },
                    { data: "CurrencyId", visible: false },
                    { data: "PurposeDesc", title: "Description" },
                    { data: "UnitPrice", title: "Unit Price", render: $.fn.dataTable.render.number(',', '.', 2) },
                    { data: "TotalPrice", title: "Total Price", render: $.fn.dataTable.render.number(',', '.', 2) },
                    { data: "DateRequired", visible: false },
                    {
                        data: null,
                        title: "Action",
                        width: 110,
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).html('<div class="btn-group">'
                                + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom pr_tbl_btn_viewbudget"> <i class="fa fa-dollar-sign"></i></a>'
                                + '<a href="#" class="btn btn-icon waves-effect waves-light btn-custom pr_tbl_btn_edit"> <i class="fa fa-edit"></i></a>'
                                + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom pr_tbl_btn_delete"> <i class="fa fa-trash"></i></a>'
                                + '</div>');

                            $(td).find('.pr_tbl_btn_viewbudget').click(function () {
                                var d = new Date();
                                budgetyear = d.getFullYear();
                                var company = companyCode.val();
                                var location = companylocationCode.val();

                                if (formValidationPurchaseRequisition()) {
                                    $.when(wrapPRbudget.settingWrapCenter()).done(wrapPRbudget.componentAddLoading());
                                    $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetItemBudget + "/" + company + "/" + location + "/" + budgetyear + "/" + rowData.ItemCode + "/" + rowData.Qty + "/" + rowData.TotalPrice)).done(function (result, status, xhr) {
                                        setTimeout(function () {
                                            wrapPRbudget.removeAttr('hidden');
                                            $.when(wrapPRbudget.resetWrapCenter()).done(wrapPRbudget.html(result));
                                        }, 500);
                                    });
                                } else {
                                    setTimeout(function () {
                                        WEBPORTAL.Utility.ConstructNotificationError("Please Select Company");
                                    }, 500);
                                }
                            });

                            $(td).find('.pr_tbl_btn_edit').click(function () {
                                modalAddItem.find('.modal-dialog .modal-content .modal-body span#pr-mdl__Add_Item_Title').text('Update Item/Service');
                                btnadditem.find('span').text("Edit Item");

                                getRowId = rowData.RowNumber;
                                itemStatus = "itemEdit";

                                $('#ItemName option[value="' + rowData.ItemName + '"]').attr('selected', 'selected');
                                $('#CurrencyId option[value="' + rowData.CurrencyId + '"]').attr('selected', 'selected');
                                $('#UomId option[value="' + rowData.UomId + '"]').attr('selected', 'selected');

                                lblitemName.addClass('active');
                                lblspecification.addClass('active');
                                lblqty.addClass('active');
                                lbluomId.addClass('active');
                                lblcurrency.addClass('active');
                                lblunitPrice.addClass('active');
                                lblpurposeDesc.addClass('active');

                                itemName.val(rowData.ItemName);
                                specification.val(rowData.Specification);
                                qty.val(rowData.Qty);
                                uomId.val(rowData.UomId);
                                currency.val(rowData.CurrencyId);
                                unitPrice.val(rowData.UnitPrice);
                                purposeDesc.val(rowData.PurposeDesc);
                                dateRequired.val(rowData.DateRequired);

                                wrapPRbudget.attr('hidden', true);
                                modalAddItem.modal('show');
                            });

                            $(td).find('.pr_tbl_btn_delete').click(function (e) {
                                var rowToRemove = rowData.RowNumber;

                                removeDataTableRow(dataItem, "RowNumber", rowToRemove);
                                datatable.row(rowData).remove().draw();

                                // Remove RowNumber                              
                                rowNumber = rowNumber - 1;
                                wrapPRbudget.attr('hidden', true);

                                if (dataItem.length == 0) {
                                    btnSavePR.prop('disabled', true);
                                }

                            });
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    gridcreateprlist.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var openAddItemForm = function () {
            btnopenAddItemModal.click(function () {
                if (companylocationCode.val !== null) {
                    resetPurchaseRequisitionItemForm();

                    itemStatus = "Add";

                    modalAddItem.find('.modal-dialog .modal-content .modal-body span#pr-mdl__Add_Item_Title').text('Add Item/Service');
                    modalAddItem.modal('show');
                    btnadditem.find('span').text("Add");
                }
                else {
                    WEBPORTAL.Utility.ConstructNotificationError("Please Select Company");
                }
            });
        };

        var LocationOnChange = function () {
            companylocationCode.change(function () {
                if (this.value !== "") {
                    btnopenAddItemModal.prop('disabled', false);
                } else {
                    btnopenAddItemModal.prop('disabled', true);
                }
            });
        };

        var addItemPurchaseRequisition = function () {
            btnadditem.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnadditem);
                if (formValidationPurchaseRequisitionItem()) {
                    $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetPurchaseRequisitionItemOnProcess + "/" + companyCode.val() + "/" + itemCode.val())).done(function (result, status, xhr) {
                        if (result.data > 0) {
                            frameModal.modal('show');
                        } else {
                            var totalPrice = qty.val() * unitPrice.val();
                            if (itemStatus === "itemEdit") {
                                for (var i = 0; i < dataItem.length; i++) {
                                    if (dataItem[i].RowNumber === getRowId) {
                                        dataItem[i].MasterItemId = itemId.val();
                                        dataItem[i].ItemCode = itemCode.val();
                                        dataItem[i].ItemName = itemName.val();
                                        dataItem[i].Specification = specification.val();
                                        dataItem[i].Qty = qty.val();
                                        dataItem[i].UomId = uomId.val();
                                        dataItem[i].CurrencyId = currency.val();
                                        dataItem[i].UnitPrice = unitPrice.val();
                                        dataItem[i].TotalPrice = totalPrice;
                                        dataItem[i].PurposeDesc = purposeDesc.val();
                                        dataItem[i].DateRequired = dateRequired.val();
                                        break;
                                    }
                                }
                                datatable.clear();
                                datatable.rows.add(dataItem);
                                datatable.draw();
                            }
                            else {
                                if (dataItem.length === 0) {
                                    rowNumber = 1;
                                }
                                else {
                                    rowNumber++;
                                }
                                var Jsonmodel = {
                                    RowNumber: rowNumber,
                                    MasterItemId: parseInt(itemId.val()),
                                    ItemCode: itemCode.val(),
                                    ItemName: itemName.val(),
                                    Specification: specification.val(),
                                    Qty: parseInt(qty.val()),
                                    UomId: parseInt(uomId.val()),
                                    CurrencyId: parseInt(currency.val()),
                                    UnitPrice: parseInt(unitPrice.val()),
                                    TotalPrice: totalPrice,
                                    PurposeDesc: purposeDesc.val(),
                                    DateRequired: dateRequired.val()
                                };
                                console.log(Jsonmodel);
                                var findExistingItem = dataItem.filter(function (entry) {
                                    return entry.ItemCode === Jsonmodel.ItemCode;
                                });

                                if (findExistingItem.length) {
                                    for (var i = 0; i < dataItem.length; i++) {
                                        if (dataItem[i].ItemCode === Jsonmodel.ItemCode) {
                                            dataItem[i].Qty = dataItem[i].Qty + Jsonmodel.Qty;
                                            dataItem[i].CurrencyId = Jsonmodel.CurrencyId;
                                            dataItem[i].PurposeDesc = Jsonmodel.PurposeDesc;
                                            dataItem[i].TotalPrice = dataItem[i].TotalPrice + Jsonmodel.TotalPrice;
                                            break;
                                        }
                                    }
                                } else {
                                    dataItem.push(Jsonmodel);
                                }

                                datatable.clear();
                                datatable.rows.add(dataItem);
                                datatable.draw();
                            }
                            $.when(modalAddItem.modal('hide')).then(resetPurchaseRequisitionItemForm());
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnadditem);
                            WEBPORTAL.Utility.ConstructNotificationSuccess();
                        }
                    });

                    btnSavePR.prop('disabled', false);
                } else {
                    setTimeout(function () {
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnadditem, 'Add Item/Service');
                    }, 300);
                }
            });
        };

        var closeframeModal = function () {
            btnCloseFrame.click(function () {
                frameModal.modal('hide');
                resetPurchaseRequisitionItemForm();
                WEBPORTAL.Utility.SubmitRemoveLoading(btnadditem);
            });
        }

        var submitPurchaseRequisition = function () {
            btnSavePR.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnSavePR);
                if (formValidationPurchaseRequisition()) {
                    let model = {
                        ConfigCompanyId: configCompanyId.val(),
                        CompanyCode: companyCode.val(),
                        CompanyLocationCode: companylocationCode.val(),
                        ConfigDivisionId: configDivisionId.val(),
                        DepartementCode: departementCode.val(),
                        RequestedFrom: requestedfrom.val(),
                        GrandTotal: dataItem.map(item => item.TotalPrice).reduce((x, y) => x + y),
                        ListItem: []
                    };
                    model.ListItem = dataItem;

                    $.when(WEBPORTAL.Services.POSTLocal(model, URL_CONTEXT_InsertPurchaseRequisition)).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            setTimeout(function () {
                                window.location.href = URL_CONTEXT_GetPurchaseRequisitionStatus;

                                btnSavePR.constructUpdateButton();
                                WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            }, 500);
                        } else {
                            setTimeout(function () {
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnSavePR);
                                WEBPORTAL.Utility.ConstructNotificationError(result.message);
                            }, 500);
                        }
                    });
                } else {
                    setTimeout(function () {
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSavePR);
                    }, 500);
                }
            });
        };

        var formValidationPurchaseRequisitionItem = function () {
            let isItemName = false,
                isSpecification = false,
                isQty = false,
                isUom = false,
                //isCurrency = false,
                //isPrice = false,
                isPurposeDesc = false,
                //isDateRequired = false,
                isFormValid = false;

            // Item Name
            if (itemName.val() === '') {
                itemName.addClass('invalid');
                if ($('.pr_inpt_err_item_name').length === 0) {
                    lblitemName.after('<div class="invalid-feedback pr_inpt_err_item_name">' + 'Please fill the required field.' + '</div>');
                    isItemName = false;
                }
            }
            else {
                itemName.removeClass('invalid');
                $('.pr_inpt_err_item_name').remove();
                isItemName = true;
            }

            // Specification
            if (specification.val() === '') {
                specification.addClass('invalid');
                if ($('.pr_inpt_err_specification').length === 0) {
                    lblspecification.after('<div class="invalid-feedback pr_inpt_err_specification">' + 'Please fill the required field.' + '</div>');
                    isSpecification = false;
                }
            }
            else {
                specification.removeClass('invalid');
                $('.pr_inpt_err_specification').remove();
                isSpecification = true;
            }

            // Quantity
            if (qty.val() === '') {
                qty.addClass('invalid');
                if ($('.pr_inpt_err_qty').length === 0) {
                    lblqty.after('<div class="invalid-feedback pr_inpt_err_qty">' + 'Please fill the required field.' + '</div>');
                    isQty = false;
                }
            }
            else {
                qty.removeClass('invalid');
                $('.pr_inpt_err_qty').remove();
                isQty = true;
            }

            // UOM
            if (uomId.val() === '' || uomId.val() === null) {
                uomId.addClass('invalid');
                if ($('.pr_inpt_err_uom').length === 0) {
                    lbluomId.after('<div class="invalid-feedback pr_inpt_err_uom">' + 'Please select one.' + '</div>');
                    isUom = false;
                }
            }
            else {
                uomId.removeClass('invalid');
                $('.pr_inpt_err_uom').remove();
                isUom = true;
            }

            // Currency
            //if (currency.val() === '' || currency.val() === null) {
            //    currency.addClass('invalid');
            //    if ($('.pr_inpt_err_currency').length === 0) {
            //        lblcurrency.after('<div class="invalid-feedback pr_inpt_err_currency">' + 'Please select one.' + '</div>');
            //        isCurrency = false;
            //    }
            //}
            //else {
            //    currency.removeClass('invalid');
            //    $('.pr_inpt_err_currency').remove();
            //    isCurrency = true;
            //}

            // Price
            //if (unitPrice.val() === '') {
            //    unitPrice.addClass('invalid');
            //    if ($('.pr_inpt_err_price').length === 0) {
            //        lblunitPrice.after('<div class="invalid-feedback pr_inpt_err_price">' + 'Please fill the required field.' + '</div>');
            //        isPrice = false;
            //    }
            //}
            //else {
            //    unitPrice.removeClass('invalid');
            //    $('.pr_inpt_err_price').remove();
            //    isPrice = true;
            //}

            // Purpose Description
            if (purposeDesc.val() === '') {
                purposeDesc.addClass('invalid');
                if ($('.pr_inpt_err_purpose').length === 0) {
                    lblpurposeDesc.after('<div class="invalid-feedback pr_inpt_err_purpose">' + 'Please fill the required field.' + '</div>');
                    isPurposeDesc = false;
                }
            }
            else {
                purposeDesc.removeClass('invalid');
                $('.pr_inpt_err_purpose').remove();
                isPurposeDesc = true;
            }

            // Date Required
            //if (dateRequired.val() === '') {
            //    dateRequired.addClass('invalid');
            //    if ($('.pr_inpt_err_dateRequired').length === 0) {
            //        lbldaterequired.after('<div class="invalid-feedback pr_inpt_err_dateRequired">' + 'Please fill the required field.' + '</div>');
            //        isDateRequired = false;
            //    }
            //}
            //else {
            //    datePicker.removeClass('invalid');
            //    $('.pr_inpt_err_dateRequired').remove();
            //    isDateRequired = true;
            //}

            if (isItemName && isSpecification && isQty && isUom /*&& isCurrency && isPrice*/ && isPurposeDesc /*&& isDateRequired*/) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };

        var formValidationPurchaseRequisition = function () {
            let isCompanyName = false,
                isRequester = false,
                isFormValid = false;

            // Company Name
            if (companylocationCode.val() === '') {
                companylocationCode.addClass('invalid');
                if ($('.pr_slct_err_location_name').length === 0) {
                    lblcompanylocationCode.after('<div class="invalid-feedback pr_slct_err_location_name">' + 'Please select one.' + '</div>');
                    isCompanyName = false;
                }
            }
            else {
                companylocationCode.removeClass('invalid');
                $('.pr_slct_err_location_name').remove();
                isCompanyName = true;
            }

            // Requester Name
            if (requestedfrom.val() === '') {
                requestedfrom.addClass('invalid');
                if ($('.pr_inpt_err_requestedfrom').length === 0) {
                    lblrequestedfrom.after('<div class="invalid-feedback pr_inpt_err_requestedfrom">' + 'Please fill the required field.' + '</div>');
                    isRequester = false;
                }
            }
            else {
                requestedfrom.removeClass('invalid');
                $('.pr_inpt_err_requestedfrom').remove();
                isRequester = true;
            }

            if (isCompanyName && isRequester) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };

        var resetPurchaseRequisitionItemForm = function () {
            id.val();
            itemName.val('');
            specification.val('');
            qty.val('');
            uomId.val('');
            $('#UomId option:selected').attr('selected', false);
            currency.val('');
            $('#CurrencyId option:selected').attr('selected', false);
            unitPrice.val('');
            purposeDesc.val('');
            dateRequired.val('');

            lblitemName.removeClass('active');
            lblspecification.removeClass('active');
            lblqty.removeClass('active');
            lblunitPrice.removeClass('active');
            lblpurposeDesc.removeClass('active');

            purchaseRequisitionForm.removeClass('was-validated');
        };

        var onChangeBehaviour = function () {
            companylocationCode.change(function (e) {
                if (this.value !== '' || this.value !== null)
                    $('.pr_slct_err_location_name').remove();
            });

            uomId.change(function (e) {
                if (this.value !== '' || this.value !== null)
                    $('.pr_inpt_err_uom').remove();
            });

            currency.change(function (e) {
                if (this.value !== '' || this.value !== null)
                    $('.pr_inpt_err_currency').remove();
            });
        };

        var inputChangeRemoveErr = function () {
            requestedfrom.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblrequestedfrom.next().remove();
                }
            });

            itemName.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblitemName.next().remove();
                }
            });

            specification.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblspecification.next().remove();
                }
            });

            qty.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblqty.next().remove();
                }
            });

            unitPrice.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblunitPrice.next().remove();
                }
            });

            purposeDesc.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblpurposeDesc.next().remove();
                }
            });
        };

        var removeDataTableRow = function (array, property, value) {
            var i, j, cur;
            for (i = array.length - 1; i >= 0; i--) {
                cur = array[i];
                if (cur[property] === value) {
                    array.splice(i, 1);
                }
            }
        }

        var keyPressNumber = function () {
            qty.on('keypress', function (event) {
                var regex = new RegExp("^[a-zA-Z0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            unitPrice.on('keypress', function (event) {
                var regex = new RegExp("^[a-zA-Z0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
        }

        var run = function () {
            initialDom();
            keyPressNumber();
            closeframeModal();
            itemAutoComplete();
            initialgridcreateprlist();
            LocationOnChange();
            onChangeBehaviour();
            openAddItemForm();
            inputChangeRemoveErr();
            addItemPurchaseRequisition();
            submitPurchaseRequisition();
        };

        return {
            run: run
        };
    })();

    initialPurchaseRequisition.run();
})(jQuery);