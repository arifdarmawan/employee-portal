﻿(function ($) {
    'use strict';

    var initialApprovalDetail = (function () {
        var wrapApprovalItem, modalApprovalDetail, btnActionApproved, btnActionRejected;

        var initialDom = function () {
            wrapApprovalItem = $('.appr li a');
            modalApprovalDetail = $('#ptaApprovalOutModal');
            btnActionApproved = $('#btn-action-approved');
        };

        var clickDetailItem = function () {
            wrapApprovalItem.click(function () {
                modalApprovalDetail.modal('show');
                loadApprovalDataDetail($(this).attr('data-workflow'));
            });
        };

        var clickActionApproved = function () {
            btnActionApproved.click(function () {
                alert('button approved clicked');
            });
        };

        var loadApprovalDataDetail = function (workflowId) {
            $.when(modalApprovalDetail.find('.modal-body').settingWrapCenter()).done(modalApprovalDetail.find('.modal-body').componentAddLoading());           
            $.when(WEBPORTAL.Services.GETLocal(WEBPORTAL.URLContext.GetWorkflowApprovalDetail + '/' + workflowId)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(modalApprovalDetail.find('.modal-body').resetWrapCenter()).done(modalApprovalDetail.find('.modal-body').html(result));
                }, 500);
            });
        };

        var run = function () {
            initialDom();
            clickDetailItem();
            clickActionApproved();
        };

        return {
            run: run
        }
    })(jQuery);

    initialApprovalDetail.run();
})(jQuery);