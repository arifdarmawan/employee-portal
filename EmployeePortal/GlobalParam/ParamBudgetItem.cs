﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeePortal.GlobalParam
{
    public class ParamBudgetItem
    {
        public string CompanyCode { get; set; }
        public string LocationCode { get; set; }
        public int Year { get; set; }
        public string BimsCode { get; set; }
        public string Quantity { get; set; }
        public string TotalPrice { get; set; }
    }
}
