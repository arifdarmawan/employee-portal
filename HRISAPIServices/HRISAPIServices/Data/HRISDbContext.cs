﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HRISAPIServices.Data
{
    public class HRISDbContext : IdentityDbContext
    {
        public HRISDbContext(DbContextOptions<HRISDbContext> options)
            : base(options)
        {
        }
    }
}
