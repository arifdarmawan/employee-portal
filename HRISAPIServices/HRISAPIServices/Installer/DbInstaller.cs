﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRISAPIServices.Data;
using HRISAPIServices.Models.Models;
using HRISAPIServices.Repository.Abstract;
using HRISAPIServices.Repository.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HRISAPIServices.Installer
{
    public class DbInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<HRISAPIServicesContext>(options =>
              options.UseSqlServer(
                  configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>()
                .AddEntityFrameworkStores<HRISAPIServicesContext>();


            services.AddSingleton(configuration);

            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
        }
    }
}
