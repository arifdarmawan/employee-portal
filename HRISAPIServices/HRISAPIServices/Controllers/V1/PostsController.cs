﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRISAPIServices.Contracts.V1;
using HRISAPIServices.Domain;
using Microsoft.AspNetCore.Mvc;

namespace HRISAPIServices.Controllers.V1
{
    public class PostsController : Controller
    {
        private List<Post> _posts;

        public PostsController()
        {
            _posts = new List<Post>();
            for (int i = 0; i < 5; i++)
            {
                _posts.Add(new Post { Id = Guid.NewGuid().ToString() });
            }
        }

        [HttpGet(ApiRoutes.Post.GetAll)]
        public IActionResult GetAll()
        {
            return Ok(_posts);
        }

        [HttpPost(ApiRoutes.Post.Create)]
        public IActionResult Create([FromBody] Post post)
        {
            if (string.IsNullOrEmpty(post.Id))
            {
                post.Id = Guid.NewGuid().ToString();
            }

            _posts.Add(post);

            var baseUrl = $"{HttpContext.Request.Scheme}//{HttpContext.Request.Host.ToUriComponent()}";
            var locationUri = baseUrl + "/" + ApiRoutes.Post.Get.Replace("{postId}", post.Id);

            return Created(locationUri, post);
        }
    }
}