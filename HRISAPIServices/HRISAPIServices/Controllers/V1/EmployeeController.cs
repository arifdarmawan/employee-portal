﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRISAPIServices.Contracts.V1;
using HRISAPIServices.Models.GeneralModels;
using HRISAPIServices.Models.Models;
using HRISAPIServices.Repository.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace HRISAPIServices.Controllers.V1
{
    //[Route("api/v1/[controller]")]
    [ApiController]
    public class EmployeeController : Controller
    {
        IEmployeeRepository _EmployeeRepository;

        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            _EmployeeRepository = employeeRepository;
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}

        //[HttpGet("GetEmployeeByEmail")]
        [HttpGet(ApiRoutes.Employee.GetDataEmployeeInfoByEmail)]
        public async Task<BaseResponse> GetEmployeeInfoByEmail(string email)
        {
            return await _EmployeeRepository.GetEmployeeInfoByEmail(email);
        }

        [HttpGet(ApiRoutes.Employee.GetDataEmployeeGeneralInfoByEmail)]
        public async Task<BaseResponse> GetEmployeeGeneralInfoByEmail(string email)
        {
            return await _EmployeeRepository.GetEmployeeGeneralInfoByEmail(email);
        }

        [HttpGet(ApiRoutes.Employee.GetDataEmployeeGeneralInfoInputByEmail)]
        public async Task<BaseResponse> GetEmployeeGeneralInfoInputByEmail(string email)
        {
            return await _EmployeeRepository.GetEmployeeGeneralInfoInputByEmail(email);
        }

        [HttpGet(ApiRoutes.Employee.GetDataEmployeeIDCardListByEmail)]
        public async Task<BaseResponse> GetEmployeeIDCardListByEmail(string email)
        {
            return await _EmployeeRepository.GetEmployeeIDCardListByEmail(email);
        }

        [HttpGet(ApiRoutes.Employee.GetDataEmployeeFamilyListByEmail)]
        public async Task<BaseResponse> GetEmployeeFamilyListByEmail(string email)
        {
            return await _EmployeeRepository.GetEmployeeFamilyListByEmail(email);
        }

        [HttpGet(ApiRoutes.Employee.GetEmployeeFamilyInputByIDEmpFamily)]
        public async Task<BaseResponse> GetEmployeeFamilyInputByIDEmpFamily(long id)
        {
            return await _EmployeeRepository.GetEmployeeFamilyInputByIDEmpFamily(id);
        }

        [HttpGet(ApiRoutes.Employee.GetDataEmployeeEducationListByEmail)]
        public async Task<BaseResponse> GetEmployeeEducationListByEmail(string email)
        {
            return await _EmployeeRepository.GetEmployeeEducationListByEmail(email);
        }

        [HttpGet(ApiRoutes.Employee.GetDataEmployeeJobExperienceListByEmail)]
        public async Task<BaseResponse> GetEmployeeJobExperienceListByEmail(string email)
        {
            return await _EmployeeRepository.GetEmployeeJobExperienceListByEmail(email);
        }

        [HttpGet(ApiRoutes.Employee.GetDataEmployeeBankAccountListByEmail)]
        public async Task<BaseResponse> GetEmployeeBankAccountListByEmail(string email)
        {
            return await _EmployeeRepository.GetEmployeeBankAccountListByEmail(email);
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterCityList)]
        public async Task<BaseResponse> GetDataMasterCityList()
        {
            return await _EmployeeRepository.GetDataMasterCityList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterBloodTypeList)]
        public async Task<BaseResponse> GetDataMasterBloodTypeList()
        {
            return await _EmployeeRepository.GetDataMasterBloodTypeList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterCountryList)]
        public async Task<BaseResponse> GetDataMasterCountryList()
        {
            return await _EmployeeRepository.GetDataMasterCountryList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterMaritalStatusList)]
        public async Task<BaseResponse> GetDataMasterMaritalStatusList()
        {
            return await _EmployeeRepository.GetDataMasterMaritalStatusList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterReligionList)]
        public async Task<BaseResponse> GetDataMasterReligionList()
        {
            return await _EmployeeRepository.GetDataMasterReligionList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterCompanyList)]
        public async Task<BaseResponse> GetDataMasterCompanyList()
        {
            return await _EmployeeRepository.GetDataMasterCompanyList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterCompanyListParam)]
        public async Task<BaseResponse> GetDataMasterCompanyListParam(string OracleCode)
        {
            return await _EmployeeRepository.GetDataMasterCompanyListParam(OracleCode);
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterFamilyRelationList)]
        public async Task<BaseResponse> GetDataMasterFamilyRelationList()
        {
            return await _EmployeeRepository.GetDataMasterFamilyRelationList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterEducationLevelList)]
        public async Task<BaseResponse> GetDataMasterEducationLevelList()
        {
            return await _EmployeeRepository.GetDataMasterEducationLevelList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterOccupationList)]
        public async Task<BaseResponse> GetDataMasterOccupationList()
        {
            return await _EmployeeRepository.GetDataMasterOccupationList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterEducationStatusList)]
        public async Task<BaseResponse> GetDataMasterEducationStatusList()
        {
            return await _EmployeeRepository.GetDataMasterEducationStatusList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterEducationMajorList)]
        public async Task<BaseResponse> GetDataMasterEducationMajorList()
        {
            return await _EmployeeRepository.GetDataMasterEducationMajorList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterInstitutionList)]
        public async Task<BaseResponse> GetDataMasterInstitutionList()
        {
            return await _EmployeeRepository.GetDataMasterInstitutionList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterEducationResultList)]
        public async Task<BaseResponse> GetDataMasterEducationResultList()
        {
            return await _EmployeeRepository.GetDataMasterEducationResultList();
        }

        [HttpGet(ApiRoutes.Employee.GetDataMasterCardTypeList)]
        public async Task<BaseResponse> GetDataMasterCardTypeList()
        {
            return await _EmployeeRepository.GetDataMasterCardTypeList();
        }

        [HttpGet(ApiRoutes.Employee.CheckData)]
        public async Task<BaseResponse> Checkdata(string email)
        {
            return await _EmployeeRepository.Checkdata(email);
        }

        [HttpPost(ApiRoutes.Employee.InsertIDCard)]
        public async Task<BaseResponse> InsertIDCard([FromBody] TTrxEssEmpIdCard data)
        {
            return await _EmployeeRepository.InsertIDCard(data);
        }

        [HttpPost(ApiRoutes.Employee.InsertFamily)]
        public async Task<BaseResponse> InsertFamily([FromBody] EmployeeFamilyInputModel data)
        {
            return await _EmployeeRepository.InsertFamily(data);
        }

        [HttpPost(ApiRoutes.Employee.UpdateGeneralInfo)]
        public async Task<BaseResponse> UpdateGeneralInfo([FromBody] GeneralInfoModel data)
        {
            return await _EmployeeRepository.UpdateGeneralInfo(data);
        }
    }
}