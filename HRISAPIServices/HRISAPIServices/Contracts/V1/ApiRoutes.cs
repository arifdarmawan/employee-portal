﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRISAPIServices.Contracts.V1
{
    public static class ApiRoutes
    {
        public const string Root = "api";

        public const string Version = "v1";

        public const string Base = Root + "/" + Version;

        public static class Post
        {
            public const string GetAll = Base + "/posts";

            public const string Get = Base + "/posts/{postId}";

            public const string Create = Base + "/posts";
        }

        public static class Employee
        {
            public const string Route = Base + "/Employee";

            //public const string GetAll = Base + "/posts";
            #region GetEndPoint
            public const string GetDataEmployeeInfoByEmail = Route + "/GetDataEmployeeInfoByEmail";
            public const string GetDataEmployeeGeneralInfoByEmail = Route + "/GetDataEmployeeGeneralInfoByEmail";
            public const string GetDataEmployeeGeneralInfoInputByEmail = Route + "/GetDataEmployeeGeneralInfoInputByEmail";
            public const string GetDataEmployeeIDCardListByEmail = Route + "/GetDataEmployeeIDCardListByEmail";
            public const string GetDataEmployeeFamilyListByEmail = Route + "/GetDataEmployeeFamilyListByEmail";
            public const string GetEmployeeFamilyInputByIDEmpFamily = Route + "/GetEmployeeFamilyInputByIDEmpFamily";
            public const string GetDataEmployeeEducationListByEmail = Route + "/GetDataEmployeeEducationListByEmail";
            public const string GetDataEmployeeJobExperienceListByEmail = Route + "/GetDataEmployeeJobExperienceListByEmail";
            public const string GetDataEmployeeBankAccountListByEmail = Route + "/GetDataEmployeeBankAccountListByEmail";
            public const string GetDataMasterCityList = Route + "/GetDataMasterCityList";
            public const string GetDataMasterBloodTypeList = Route + "/GetDataMasterBloodTypeList";
            public const string GetDataMasterCountryList = Route + "/GetDataMasterCountryList";
            public const string GetDataMasterMaritalStatusList = Route + "/GetDataMasterMaritalStatusList";
            public const string GetDataMasterReligionList = Route + "/GetDataMasterReligionList";
            public const string GetDataMasterCompanyList = Route + "/GetDataMasterCompanyList";
            public const string GetDataMasterCompanyListParam = Route + "/GetDataMasterCompanyListParam";
            public const string GetDataMasterFamilyRelationList = Route + "/GetDataMasterFamilyRelationList";
            public const string GetDataMasterEducationLevelList = Route + "/GetDataMasterEducationLevelList";
            public const string GetDataMasterEducationStatusList = Route + "/GetDataMasterEducationStatusList";
            public const string GetDataMasterEducationMajorList = Route + "/GetDataMasterEducationMajorList";
            public const string GetDataMasterInstitutionList = Route + "/GetDataMasterInstitutionList";
            public const string GetDataMasterEducationResultList = Route + "/GetDataMasterEducationResultList";
            public const string GetDataMasterCardTypeList = Route + "/GetDataMasterCardTypeList";
            public const string GetDataMasterOccupationList = Route + "/GetDataMasterOccupationList";
            //public const string GetDataEmployeeByEmail = Route + "/GetDataEmployeeByEmail";
            #endregion
            public const string CheckData = Route + "/CheckData";

            #region InsertEndPoint
            public const string InsertIDCard = Route + "/InsertIDCard";
            public const string InsertFamily = Route + "/InsertFamily";
            #endregion

            #region UpdateEndPoint
            public const string UpdateGeneralInfo = Route + "/UpdateGeneralInfo";
            #endregion
            //public const string Create = Base + "/employees";
        }
    }
}
