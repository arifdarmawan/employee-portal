﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRISAPIServices.Models.GeneralModels
{
    public class EmployeeFamilyInputModel
    {
        public long? Idemployee { get; set; }
        public int FamilyRelation { get; set; }
        public string FamilyMemberName { get; set; }
        public int BirthPlace { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public int MaritalStatus { get; set; }
        public string IDCardNo { get; set; }
        public int EducationLevel { get; set; }
        public int Occupation { get; set; }
        public string OriginalAddress { get; set; }
        public string ResidentialAddress { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public bool IsAlive { get; set; }
        public DateTime? MarriedDate { get; set; }
    }
}
