﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRISAPIServices.Models.GeneralModels
{
    public class EmployeeInputModel
    {
        public long Idemployee { get; set; }
        public int IDBirthPlace { get; set; }
        public DateTime? birthDate { get; set; }
        public int bloodType { get; set; }
        public string gender { get; set; }
        public int IDNationality { get; set; }
        public int IDMaritalStatus { get; set; }
        public string IdCardAddress { get; set; }
        public string CardidNumber { get; set; }
        public string currentAddress { get; set; }
        public string idCardNumber { get; set; }
        public string Email { get; set; }
        public string mobileNo { get; set; }
        public int IDReligion { get; set; }
    }
}
