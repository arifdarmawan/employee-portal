using System;

namespace HRISAPIServices.Models.GeneralModels
{
    public class GeneralInfoModel
    {
        public long Idemployee { get; set; }
        public int IDBirthPlace { get; set; }
        public DateTime? birthDate { get; set; }
        public int IDBloodType { get; set; }
        public string gender { get; set; }
        public int IDNationality { get; set; }
        public int IDMaritalStatus { get; set; }
        public string IdCardAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string CardidNumber { get; set; }
        public string Email { get; set; }
        public string mobileNo { get; set; }
        public int IDReligion { get; set; }
    }
}