﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Bank_Account")]
    public partial class TMasterBankAccount
    {
        public TMasterBankAccount()
        {
            TMasterCompany = new HashSet<TMasterCompany>();
            TTrxEmpBankAccount = new HashSet<TTrxEmpBankAccount>();
            TTrxEmpBankAccountChanges = new HashSet<TTrxEmpBankAccountChanges>();
            TTrxEssEmpBankAccount = new HashSet<TTrxEssEmpBankAccount>();
        }

        [Key]
        [Column("IDBankAccount")]
        public int IdbankAccount { get; set; }
        [Column("IDBankGroup")]
        public int? IdbankGroup { get; set; }
        [StringLength(150)]
        public string AccountNo { get; set; }
        [StringLength(150)]
        public string AccountName { get; set; }
        [StringLength(50)]
        public string BranchCode { get; set; }
        [StringLength(150)]
        public string Branch { get; set; }
        [Column("IDCity")]
        public int? Idcity { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("BICode")]
        [StringLength(50)]
        public string Bicode { get; set; }

        [ForeignKey("IdbankGroup")]
        [InverseProperty("TMasterBankAccount")]
        public virtual TMasterBankGroup IdbankGroupNavigation { get; set; }
        [ForeignKey("Idcity")]
        [InverseProperty("TMasterBankAccount")]
        public virtual TMasterCity IdcityNavigation { get; set; }
        [InverseProperty("IdbankAccountNavigation")]
        public virtual ICollection<TMasterCompany> TMasterCompany { get; set; }
        [InverseProperty("IdbankAccountNavigation")]
        public virtual ICollection<TTrxEmpBankAccount> TTrxEmpBankAccount { get; set; }
        [InverseProperty("IdbankAccountNavigation")]
        public virtual ICollection<TTrxEmpBankAccountChanges> TTrxEmpBankAccountChanges { get; set; }
        [InverseProperty("IdbankAccountNavigation")]
        public virtual ICollection<TTrxEssEmpBankAccount> TTrxEssEmpBankAccount { get; set; }
    }
}
