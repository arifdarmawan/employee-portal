﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Warning_Level")]
    public partial class TMasterWarningLevel
    {
        public TMasterWarningLevel()
        {
            TTrxEmpViolation = new HashSet<TTrxEmpViolation>();
        }

        [Key]
        [Column("IDWarning")]
        public int Idwarning { get; set; }
        [StringLength(10)]
        public string WarningCode { get; set; }
        [StringLength(100)]
        public string WarningName { get; set; }
        public short? Period { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdwarningNavigation")]
        public virtual ICollection<TTrxEmpViolation> TTrxEmpViolation { get; set; }
    }
}
