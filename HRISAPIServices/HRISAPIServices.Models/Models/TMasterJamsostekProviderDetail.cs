﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Jamsostek_Provider_Detail")]
    public partial class TMasterJamsostekProviderDetail
    {
        [Key]
        [Column("IDJamProviderDetail")]
        public int IdjamProviderDetail { get; set; }
        [Column("IDJamsostekProvider")]
        public int IdjamsostekProvider { get; set; }
        [Column("IDHistoryCompLoc")]
        public long IdhistoryCompLoc { get; set; }
        [Column("IDJobLevel")]
        public int IdjobLevel { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdhistoryCompLoc")]
        [InverseProperty("TMasterJamsostekProviderDetail")]
        public virtual TTrxHistoryCompanyLocation IdhistoryCompLocNavigation { get; set; }
        [ForeignKey("IdjamsostekProvider")]
        [InverseProperty("TMasterJamsostekProviderDetail")]
        public virtual TMasterJamsostekProvider IdjamsostekProviderNavigation { get; set; }
        [ForeignKey("IdjobLevel")]
        [InverseProperty("TMasterJamsostekProviderDetail")]
        public virtual TMasterJoblevel IdjobLevelNavigation { get; set; }
    }
}
