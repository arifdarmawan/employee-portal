﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Salary_Item_Type")]
    public partial class TMasterSalaryItemType
    {
        [Key]
        [Column("IDSalaryItemType")]
        public int IdsalaryItemType { get; set; }
        [StringLength(10)]
        public string ItemTypeCode { get; set; }
        [StringLength(50)]
        public string ItemTypeName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
