﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Joblevel_Group")]
    public partial class TMasterJoblevelGroup
    {
        public TMasterJoblevelGroup()
        {
            TMasterJoblevel = new HashSet<TMasterJoblevel>();
        }

        [Key]
        [Column("IDJobLevelGroup")]
        public int IdjobLevelGroup { get; set; }
        [StringLength(10)]
        public string GroupCode { get; set; }
        [StringLength(100)]
        public string GroupName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdjobLevelGroupNavigation")]
        public virtual ICollection<TMasterJoblevel> TMasterJoblevel { get; set; }
    }
}
