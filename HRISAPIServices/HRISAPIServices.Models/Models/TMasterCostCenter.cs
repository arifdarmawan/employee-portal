﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Cost_Center")]
    public partial class TMasterCostCenter
    {
        public TMasterCostCenter()
        {
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
        }

        [Key]
        [Column("IDCostCenter")]
        public int IdcostCenter { get; set; }
        [Column("IDCostCenterGroup")]
        public int? IdcostCenterGroup { get; set; }
        [StringLength(30)]
        public string CostCenterCode { get; set; }
        [StringLength(150)]
        public string CostCenterName { get; set; }
        [StringLength(250)]
        public string CostCenterInitial { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdcostCenterGroup")]
        [InverseProperty("TMasterCostCenter")]
        public virtual TMasterCostCenterGroup IdcostCenterGroupNavigation { get; set; }
        [InverseProperty("IdcostCenterNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdcostCenterNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
    }
}
