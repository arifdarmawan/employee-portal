﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Medical_Claim_Expense")]
    public partial class TTrxMedicalClaimExpense
    {
        [Key]
        [Column("IDMedExpense")]
        public long IdmedExpense { get; set; }
        [Column("IDFamRelation")]
        public int? IdfamRelation { get; set; }
        [Column("IDMedicalClaim")]
        public long IdmedicalClaim { get; set; }
        [Column("IDMedicalExpense")]
        public int IdmedicalExpense { get; set; }
        [Column("IDInstitution")]
        public int? Idinstitution { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? ClaimAmount { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? PayByComp { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? PayByEmp { get; set; }
        public bool IsEmployee { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdfamRelation")]
        [InverseProperty("TTrxMedicalClaimExpense")]
        public virtual TMasterFamilyRelation IdfamRelationNavigation { get; set; }
        [ForeignKey("Idinstitution")]
        [InverseProperty("TTrxMedicalClaimExpense")]
        public virtual TMasterMedicalInstitution IdinstitutionNavigation { get; set; }
        [ForeignKey("IdmedicalClaim")]
        [InverseProperty("TTrxMedicalClaimExpense")]
        public virtual TTrxMedicalClaim IdmedicalClaimNavigation { get; set; }
        [ForeignKey("IdmedicalExpense")]
        [InverseProperty("TTrxMedicalClaimExpense")]
        public virtual TMasterMedicalExpense IdmedicalExpenseNavigation { get; set; }
    }
}
