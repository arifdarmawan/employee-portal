﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_BPJSKes_Template")]
    public partial class TMasterBpjskesTemplate
    {
        public TMasterBpjskesTemplate()
        {
            TMasterBpjskesTmpDetail = new HashSet<TMasterBpjskesTmpDetail>();
            TTrxEmpBpjskes = new HashSet<TTrxEmpBpjskes>();
            TTrxEmpBpjskesChanges = new HashSet<TTrxEmpBpjskesChanges>();
        }

        [Key]
        [Column("IDTmpBPJSKes")]
        public int IdtmpBpjskes { get; set; }
        [StringLength(10)]
        public string TemplateCode { get; set; }
        [Required]
        [StringLength(50)]
        public string TemplateName { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdtmpBpjskesNavigation")]
        public virtual ICollection<TMasterBpjskesTmpDetail> TMasterBpjskesTmpDetail { get; set; }
        [InverseProperty("IdtmpBpjskesNavigation")]
        public virtual ICollection<TTrxEmpBpjskes> TTrxEmpBpjskes { get; set; }
        [InverseProperty("IdtmpBpjskesNavigation")]
        public virtual ICollection<TTrxEmpBpjskesChanges> TTrxEmpBpjskesChanges { get; set; }
    }
}
