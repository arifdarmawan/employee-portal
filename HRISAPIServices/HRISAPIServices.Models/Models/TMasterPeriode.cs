﻿using System;
using System.Collections.Generic;

namespace HRISAPIServices.Models.Models
{
    public partial class TMasterPeriode
    {
        public long Idperiode { get; set; }
        public string Nyear { get; set; }
        public string Nmonth { get; set; }
        public short? WorkDay { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool IsUpah { get; set; }
        public bool IsNonUpah { get; set; }
        public bool IsBonus { get; set; }
        public bool IsThr { get; set; }
        public string Status { get; set; }
        public string ProcessedBy { get; set; }
        public DateTime? ProcessedDate { get; set; }
    }
}
