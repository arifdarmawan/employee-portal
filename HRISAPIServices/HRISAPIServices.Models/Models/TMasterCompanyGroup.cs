﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Company_Group")]
    public partial class TMasterCompanyGroup
    {
        public TMasterCompanyGroup()
        {
            TTrxHistoryCompanyGroup = new HashSet<TTrxHistoryCompanyGroup>();
        }

        [Key]
        [Column("IDCompanyGroup")]
        public int IdcompanyGroup { get; set; }
        [StringLength(5)]
        public string CompanyGroupCode { get; set; }
        [StringLength(150)]
        public string CompanyGroupName { get; set; }
        [StringLength(10)]
        public string AliasCode { get; set; }
        [StringLength(150)]
        public string AliasName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdcompanyGroupNavigation")]
        public virtual ICollection<TTrxHistoryCompanyGroup> TTrxHistoryCompanyGroup { get; set; }
    }
}
