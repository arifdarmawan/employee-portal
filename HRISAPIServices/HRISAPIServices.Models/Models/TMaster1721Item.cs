﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_1721_Item")]
    public partial class TMaster1721Item
    {
        public TMaster1721Item()
        {
            TMasterPph21Component = new HashSet<TMasterPph21Component>();
            TMasterPph21Formula = new HashSet<TMasterPph21Formula>();
        }

        [Key]
        [Column("IDItem")]
        public int Iditem { get; set; }
        [StringLength(10)]
        public string ItemCode { get; set; }
        [StringLength(250)]
        public string ItemDesc { get; set; }
        public int? Idx { get; set; }
        public bool IsHeader { get; set; }
        [Required]
        public bool? IsCalculated { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IditemNavigation")]
        public virtual ICollection<TMasterPph21Component> TMasterPph21Component { get; set; }
        [InverseProperty("IditemNavigation")]
        public virtual ICollection<TMasterPph21Formula> TMasterPph21Formula { get; set; }
    }
}
