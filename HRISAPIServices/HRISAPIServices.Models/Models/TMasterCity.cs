﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_City")]
    public partial class TMasterCity
    {
        public TMasterCity()
        {
            TMasterBankAccount = new HashSet<TMasterBankAccount>();
            TMasterInstitution = new HashSet<TMasterInstitution>();
            TMasterLocation = new HashSet<TMasterLocation>();
            TMasterMedicalInstitution = new HashSet<TMasterMedicalInstitution>();
            TTrxEmpAddressIdcityONavigation = new HashSet<TTrxEmpAddress>();
            TTrxEmpAddressIdcityRNavigation = new HashSet<TTrxEmpAddress>();
            TTrxEmpEducation = new HashSet<TTrxEmpEducation>();
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmpTermination = new HashSet<TTrxEmpTermination>();
            TTrxEmployeeIdbirthPlaceNavigation = new HashSet<TTrxEmployee>();
            TTrxEmployeeIdhomeBaseNavigation = new HashSet<TTrxEmployee>();
            TTrxEmployeeIdplacesNavigation = new HashSet<TTrxEmployee>();
            TTrxEssEmpAddressIdcityONavigation = new HashSet<TTrxEssEmpAddress>();
            TTrxEssEmpAddressIdcityRNavigation = new HashSet<TTrxEssEmpAddress>();
            TTrxEssEmpEducation = new HashSet<TTrxEssEmpEducation>();
            TTrxEssEmployeeIdbirthPlaceNavigation = new HashSet<TTrxEssEmployee>();
            TTrxEssEmployeeIdhomeBaseNavigation = new HashSet<TTrxEssEmployee>();
            TTrxLoanApplication = new HashSet<TTrxLoanApplication>();
        }

        [Key]
        [Column("IDCity")]
        public int Idcity { get; set; }
        [Column("IDState")]
        public int Idstate { get; set; }
        [StringLength(50)]
        public string CityCode { get; set; }
        [StringLength(50)]
        public string CityName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idstate")]
        [InverseProperty("TMasterCity")]
        public virtual TMasterState IdstateNavigation { get; set; }
        [InverseProperty("IdcityNavigation")]
        public virtual ICollection<TMasterBankAccount> TMasterBankAccount { get; set; }
        [InverseProperty("IdcityNavigation")]
        public virtual ICollection<TMasterInstitution> TMasterInstitution { get; set; }
        [InverseProperty("IdcityNavigation")]
        public virtual ICollection<TMasterLocation> TMasterLocation { get; set; }
        [InverseProperty("IdcityNavigation")]
        public virtual ICollection<TMasterMedicalInstitution> TMasterMedicalInstitution { get; set; }
        [InverseProperty("IdcityONavigation")]
        public virtual ICollection<TTrxEmpAddress> TTrxEmpAddressIdcityONavigation { get; set; }
        [InverseProperty("IdcityRNavigation")]
        public virtual ICollection<TTrxEmpAddress> TTrxEmpAddressIdcityRNavigation { get; set; }
        [InverseProperty("IdcityNavigation")]
        public virtual ICollection<TTrxEmpEducation> TTrxEmpEducation { get; set; }
        [InverseProperty("IdcityLetterNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdcityLetterNavigation")]
        public virtual ICollection<TTrxEmpTermination> TTrxEmpTermination { get; set; }
        [InverseProperty("IdbirthPlaceNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployeeIdbirthPlaceNavigation { get; set; }
        [InverseProperty("IdhomeBaseNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployeeIdhomeBaseNavigation { get; set; }
        [InverseProperty("IdplacesNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployeeIdplacesNavigation { get; set; }
        [InverseProperty("IdcityONavigation")]
        public virtual ICollection<TTrxEssEmpAddress> TTrxEssEmpAddressIdcityONavigation { get; set; }
        [InverseProperty("IdcityRNavigation")]
        public virtual ICollection<TTrxEssEmpAddress> TTrxEssEmpAddressIdcityRNavigation { get; set; }
        [InverseProperty("IdcityNavigation")]
        public virtual ICollection<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
        [InverseProperty("IdbirthPlaceNavigation")]
        public virtual ICollection<TTrxEssEmployee> TTrxEssEmployeeIdbirthPlaceNavigation { get; set; }
        [InverseProperty("IdhomeBaseNavigation")]
        public virtual ICollection<TTrxEssEmployee> TTrxEssEmployeeIdhomeBaseNavigation { get; set; }
        [InverseProperty("IdplacesNavigation")]
        public virtual ICollection<TTrxLoanApplication> TTrxLoanApplication { get; set; }
    }
}
