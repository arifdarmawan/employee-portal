﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Salary_Detail")]
    public partial class TTrxEmpSalaryDetail
    {
        [Key]
        [Column("IDEmpSalDetail")]
        public long IdempSalDetail { get; set; }
        [Column("IDEmpSalary")]
        public long? IdempSalary { get; set; }
        [Column("IDSalaryItem")]
        public int? IdsalaryItem { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Amount { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdempSalary")]
        [InverseProperty("TTrxEmpSalaryDetail")]
        public virtual TTrxEmpSalary IdempSalaryNavigation { get; set; }
        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TTrxEmpSalaryDetail")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
    }
}
