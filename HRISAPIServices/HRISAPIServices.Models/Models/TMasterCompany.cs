﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Company")]
    public partial class TMasterCompany
    {
        public TMasterCompany()
        {
            TMasterSalaryItemLocation = new HashSet<TMasterSalaryItemLocation>();
            TMasterUserDataTrustee = new HashSet<TMasterUserDataTrustee>();
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
            TTrxHistoryCompanyGroup = new HashSet<TTrxHistoryCompanyGroup>();
            TTrxHistoryCompanyLocation = new HashSet<TTrxHistoryCompanyLocation>();
        }

        [Key]
        [Column("IDCompany")]
        public int Idcompany { get; set; }
        [Column("IDBankAccount")]
        public int? IdbankAccount { get; set; }
        [StringLength(10)]
        public string CompanyCode { get; set; }
        [StringLength(150)]
        public string CompanyName { get; set; }
        [StringLength(10)]
        public string OracleCode { get; set; }
        [Column("NPWP")]
        [StringLength(20)]
        public string Npwp { get; set; }
        [StringLength(500)]
        public string Address { get; set; }
        [StringLength(5)]
        public string ZipCode { get; set; }
        [StringLength(15)]
        public string Phone { get; set; }
        [StringLength(15)]
        public string Fax { get; set; }
        [StringLength(20)]
        public string Email { get; set; }
        [StringLength(200)]
        public string Image { get; set; }
        [StringLength(200)]
        public string Director { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        public int? AccountNo { get; set; }
        [StringLength(500)]
        public string AccountName { get; set; }

        [ForeignKey("IdbankAccount")]
        [InverseProperty("TMasterCompany")]
        public virtual TMasterBankAccount IdbankAccountNavigation { get; set; }
        [InverseProperty("IdcompanyNavigation")]
        public virtual ICollection<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        [InverseProperty("IdcompanyNavigation")]
        public virtual ICollection<TMasterUserDataTrustee> TMasterUserDataTrustee { get; set; }
        [InverseProperty("IdcompanyNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdcompanyNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
        [InverseProperty("IdcompanyNavigation")]
        public virtual ICollection<TTrxHistoryCompanyGroup> TTrxHistoryCompanyGroup { get; set; }
        [InverseProperty("IdcompanyNavigation")]
        public virtual ICollection<TTrxHistoryCompanyLocation> TTrxHistoryCompanyLocation { get; set; }
    }
}
