﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Sal_Changes_Detail")]
    public partial class TTrxEmpSalChangesDetail
    {
        [Key]
        [Column("IDEmpSalChangesDetail")]
        public long IdempSalChangesDetail { get; set; }
        [Column("IDEmpSalaryChanges")]
        public long? IdempSalaryChanges { get; set; }
        [Column("IDSalaryItem")]
        public int? IdsalaryItem { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Amount { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdempSalaryChanges")]
        [InverseProperty("TTrxEmpSalChangesDetail")]
        public virtual TTrxEmpSalaryChanges IdempSalaryChangesNavigation { get; set; }
        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TTrxEmpSalChangesDetail")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
    }
}
