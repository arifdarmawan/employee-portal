﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_THR_TmpDetail")]
    public partial class TMasterThrTmpDetail
    {
        [Key]
        [Column("IDTHRTmpDetail")]
        public long IdthrtmpDetail { get; set; }
        [Column("IDTHRTemplate")]
        public long Idthrtemplate { get; set; }
        [Column("IDSalaryItem")]
        public int IdsalaryItem { get; set; }
        [Column("IDReligion")]
        public int? Idreligion { get; set; }
        [Column("IDEmploymentType")]
        public int? IdemploymentType { get; set; }
        [StringLength(500)]
        public string Formula { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartPeriod { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndPeriod { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdemploymentType")]
        [InverseProperty("TMasterThrTmpDetail")]
        public virtual TMasterEmploymentType IdemploymentTypeNavigation { get; set; }
        [ForeignKey("Idreligion")]
        [InverseProperty("TMasterThrTmpDetail")]
        public virtual TMasterReligion IdreligionNavigation { get; set; }
        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TMasterThrTmpDetail")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
        [ForeignKey("Idthrtemplate")]
        [InverseProperty("TMasterThrTmpDetail")]
        public virtual TMasterThrTemplate IdthrtemplateNavigation { get; set; }
    }
}
