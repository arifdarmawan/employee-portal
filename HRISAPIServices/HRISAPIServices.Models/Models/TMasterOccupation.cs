﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Occupation")]
    public partial class TMasterOccupation
    {
        public TMasterOccupation()
        {
            TTrxEmpFamily = new HashSet<TTrxEmpFamily>();
            TTrxEssEmpFamily = new HashSet<TTrxEssEmpFamily>();
        }

        [Key]
        [Column("IDOccupation")]
        public int Idoccupation { get; set; }
        [StringLength(10)]
        public string OccupationCode { get; set; }
        [StringLength(50)]
        public string OccupationName { get; set; }
        public bool IsClaimable { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdoccupationNavigation")]
        public virtual ICollection<TTrxEmpFamily> TTrxEmpFamily { get; set; }
        [InverseProperty("IdoccupationNavigation")]
        public virtual ICollection<TTrxEssEmpFamily> TTrxEssEmpFamily { get; set; }
    }
}
