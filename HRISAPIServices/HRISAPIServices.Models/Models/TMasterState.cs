﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_State")]
    public partial class TMasterState
    {
        public TMasterState()
        {
            TMasterCity = new HashSet<TMasterCity>();
            TMasterLocation = new HashSet<TMasterLocation>();
            TTrxEmpAddressIdstateONavigation = new HashSet<TTrxEmpAddress>();
            TTrxEmpAddressIdstateRNavigation = new HashSet<TTrxEmpAddress>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
            TTrxEssEmpAddressIdstateONavigation = new HashSet<TTrxEssEmpAddress>();
            TTrxEssEmpAddressIdstateRNavigation = new HashSet<TTrxEssEmpAddress>();
        }

        [Column("IDState")]
        public int Idstate { get; set; }
        [Column("IDCountry")]
        public int Idcountry { get; set; }
        [Column("IDIsland")]
        public int? Idisland { get; set; }
        [StringLength(50)]
        public string StateCode { get; set; }
        [StringLength(50)]
        public string StateName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcountry")]
        [InverseProperty("TMasterState")]
        public virtual TMasterCountry IdcountryNavigation { get; set; }
        [ForeignKey("Idisland")]
        [InverseProperty("TMasterState")]
        public virtual TMasterIsland IdislandNavigation { get; set; }
        [InverseProperty("IdstateNavigation")]
        public virtual ICollection<TMasterCity> TMasterCity { get; set; }
        [InverseProperty("IdstateNavigation")]
        public virtual ICollection<TMasterLocation> TMasterLocation { get; set; }
        [InverseProperty("IdstateONavigation")]
        public virtual ICollection<TTrxEmpAddress> TTrxEmpAddressIdstateONavigation { get; set; }
        [InverseProperty("IdstateRNavigation")]
        public virtual ICollection<TTrxEmpAddress> TTrxEmpAddressIdstateRNavigation { get; set; }
        [InverseProperty("IdpoHNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
        [InverseProperty("IdstateONavigation")]
        public virtual ICollection<TTrxEssEmpAddress> TTrxEssEmpAddressIdstateONavigation { get; set; }
        [InverseProperty("IdstateRNavigation")]
        public virtual ICollection<TTrxEssEmpAddress> TTrxEssEmpAddressIdstateRNavigation { get; set; }
    }
}
