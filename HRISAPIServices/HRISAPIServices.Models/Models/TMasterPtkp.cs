﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_PTKP")]
    public partial class TMasterPtkp
    {
        public TMasterPtkp()
        {
            TMasterPtkpDetail = new HashSet<TMasterPtkpDetail>();
        }

        [Key]
        [Column("IDPTKP")]
        public int Idptkp { get; set; }
        [Column("WPTS", TypeName = "numeric(18, 2)")]
        public decimal? Wpts { get; set; }
        [Column("WPTK", TypeName = "numeric(18, 2)")]
        public decimal? Wptk { get; set; }
        [Column("WPTT", TypeName = "numeric(18, 2)")]
        public decimal? Wptt { get; set; }
        public short? MaxTanggungan { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdptkpNavigation")]
        public virtual ICollection<TMasterPtkpDetail> TMasterPtkpDetail { get; set; }
    }
}
