﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Payroll_NonUpah")]
    public partial class TTrxPayrollNonUpah
    {
        [Key]
        [Column("IDPayroll")]
        public Guid Idpayroll { get; set; }
        [StringLength(4)]
        public string Nyear { get; set; }
        [Column("NMonth")]
        [StringLength(2)]
        public string Nmonth { get; set; }
        [Column("NIK")]
        [StringLength(10)]
        public string Nik { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Allowance { get; set; }
        [Column("THP", TypeName = "numeric(18, 2)")]
        public decimal? Thp { get; set; }
        public bool IsVerified { get; set; }
        [StringLength(50)]
        public string VerifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? VerifiedDate { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        public bool IsEdited { get; set; }
        [StringLength(50)]
        public string EditedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EditedDate { get; set; }
        public bool IsPaid { get; set; }
        [StringLength(500)]
        public string Note { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [StringLength(150)]
        public string AccountNo { get; set; }
        [StringLength(10)]
        public string BankGroupCode { get; set; }
        [StringLength(150)]
        public string Owner { get; set; }
    }
}
