﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_ESS_Emp_Education")]
    public partial class TTrxEssEmpEducation
    {
        [Key]
        [Column("IDEmpEducation")]
        public long IdempEducation { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDEduStatus")]
        public int? IdeduStatus { get; set; }
        [Column("IDEduLevel")]
        public int? IdeduLevel { get; set; }
        [Column("IDEduMajor")]
        public int? IdeduMajor { get; set; }
        [Column("IDInstitution")]
        public int? Idinstitution { get; set; }
        [Column("IDCity")]
        public int? Idcity { get; set; }
        [Column("IDEduResult")]
        public int? IdeduResult { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Graduation { get; set; }
        [Column(TypeName = "numeric(3, 2)")]
        public decimal? GradePoint { get; set; }
        [StringLength(10)]
        public string Title { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Column("IDAttachment")]
        public Guid? Idattachment { get; set; }
        public bool IsLastEdu { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        public bool IsRejected { get; set; }
        [StringLength(50)]
        public string RejectedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RejectedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idattachment")]
        [InverseProperty("TTrxEssEmpEducation")]
        public virtual TTrxAttachment IdattachmentNavigation { get; set; }
        [ForeignKey("Idcity")]
        [InverseProperty("TTrxEssEmpEducation")]
        public virtual TMasterCity IdcityNavigation { get; set; }
        [ForeignKey("IdeduLevel")]
        [InverseProperty("TTrxEssEmpEducation")]
        public virtual TMasterEduLevel IdeduLevelNavigation { get; set; }
        [ForeignKey("IdeduMajor")]
        [InverseProperty("TTrxEssEmpEducation")]
        public virtual TMasterEduMajor IdeduMajorNavigation { get; set; }
        [ForeignKey("IdeduResult")]
        [InverseProperty("TTrxEssEmpEducation")]
        public virtual TMasterEduResult IdeduResultNavigation { get; set; }
        [ForeignKey("IdeduStatus")]
        [InverseProperty("TTrxEssEmpEducation")]
        public virtual TMasterEduStatus IdeduStatusNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEssEmpEducation")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("Idinstitution")]
        [InverseProperty("TTrxEssEmpEducation")]
        public virtual TMasterInstitution IdinstitutionNavigation { get; set; }
    }
}
