﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Termination")]
    public partial class TTrxEmpTermination
    {
        [Key]
        [Column("IDEmpTermination")]
        public long IdempTermination { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDTermReason")]
        public int? IdtermReason { get; set; }
        [Column("IDCityLetter")]
        public int? IdcityLetter { get; set; }
        [StringLength(50)]
        public string TerminationNo { get; set; }
        [StringLength(150)]
        public string EmpReason { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [Column(TypeName = "date")]
        public DateTime? AppliedDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? TerminationDate { get; set; }
        [StringLength(250)]
        public string ServicePeriod { get; set; }
        [StringLength(150)]
        public string ResignLetterNo { get; set; }
        [Column("BPJSLetterNo")]
        [StringLength(150)]
        public string BpjsletterNo { get; set; }
        [Column("SignedOffNIK")]
        [StringLength(10)]
        public string SignedOffNik { get; set; }
        [StringLength(250)]
        public string SignedOffName { get; set; }
        public bool IsCalcSeverance { get; set; }
        public bool IsProportionalPay { get; set; }
        public bool IsTermRehiring { get; set; }
        public bool IsPosted { get; set; }
        public bool IsResignLetter { get; set; }
        public bool IsExitInterview { get; set; }
        public bool IsExitClearance { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdcityLetter")]
        [InverseProperty("TTrxEmpTermination")]
        public virtual TMasterCity IdcityLetterNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpTermination")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdtermReason")]
        [InverseProperty("TTrxEmpTermination")]
        public virtual TMasterTerminationReason IdtermReasonNavigation { get; set; }
    }
}
