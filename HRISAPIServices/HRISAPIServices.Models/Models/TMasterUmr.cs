﻿using System;
using System.Collections.Generic;

namespace HRISAPIServices.Models.Models
{
    public partial class TMasterUmr
    {
        public int Idumr { get; set; }
        public int Idstate { get; set; }
        public int Idcity { get; set; }
        public decimal? Umramount { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual TMasterCity IdcityNavigation { get; set; }
        public virtual TMasterState IdstateNavigation { get; set; }
    }
}
