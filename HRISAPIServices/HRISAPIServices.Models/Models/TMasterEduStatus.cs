﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_EduStatus")]
    public partial class TMasterEduStatus
    {
        public TMasterEduStatus()
        {
            TMasterEduLevel = new HashSet<TMasterEduLevel>();
            TTrxEmpEducation = new HashSet<TTrxEmpEducation>();
            TTrxEssEmpEducation = new HashSet<TTrxEssEmpEducation>();
        }

        [Key]
        [Column("IDEduStatus")]
        public int IdeduStatus { get; set; }
        [StringLength(10)]
        public string EduStatusCode { get; set; }
        [StringLength(50)]
        public string EduStatusName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdeduStatusNavigation")]
        public virtual ICollection<TMasterEduLevel> TMasterEduLevel { get; set; }
        [InverseProperty("IdeduStatusNavigation")]
        public virtual ICollection<TTrxEmpEducation> TTrxEmpEducation { get; set; }
        [InverseProperty("IdeduStatusNavigation")]
        public virtual ICollection<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
    }
}
