﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Joblevel")]
    public partial class TMasterJoblevel
    {
        public TMasterJoblevel()
        {
            TMasterAllowanceJamsostek = new HashSet<TMasterAllowanceJamsostek>();
            TMasterBpjskesProviderDetail = new HashSet<TMasterBpjskesProviderDetail>();
            TMasterJamsostekProviderDetail = new HashSet<TMasterJamsostekProviderDetail>();
            TMasterJobtitle = new HashSet<TMasterJobtitle>();
            TMasterKppDetail = new HashSet<TMasterKppDetail>();
            TMasterMedicalTemplate = new HashSet<TMasterMedicalTemplate>();
            TMasterSalaryItemLocation = new HashSet<TMasterSalaryItemLocation>();
            TMasterTerminationNotice = new HashSet<TMasterTerminationNotice>();
            TMasterUserDataTrustee = new HashSet<TMasterUserDataTrustee>();
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
        }

        [Key]
        [Column("IDJoblevel")]
        public int Idjoblevel { get; set; }
        [Column("IDJobLevelGroup")]
        public int? IdjobLevelGroup { get; set; }
        [StringLength(10)]
        public string JoblevelCode { get; set; }
        [StringLength(150)]
        public string JoblevelName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdjobLevelGroup")]
        [InverseProperty("TMasterJoblevel")]
        public virtual TMasterJoblevelGroup IdjobLevelGroupNavigation { get; set; }
        [InverseProperty("IdjoblevelNavigation")]
        public virtual ICollection<TMasterAllowanceJamsostek> TMasterAllowanceJamsostek { get; set; }
        [InverseProperty("IdjobLevelNavigation")]
        public virtual ICollection<TMasterBpjskesProviderDetail> TMasterBpjskesProviderDetail { get; set; }
        [InverseProperty("IdjobLevelNavigation")]
        public virtual ICollection<TMasterJamsostekProviderDetail> TMasterJamsostekProviderDetail { get; set; }
        [InverseProperty("IdjoblevelNavigation")]
        public virtual ICollection<TMasterJobtitle> TMasterJobtitle { get; set; }
        [InverseProperty("IdjoblevelNavigation")]
        public virtual ICollection<TMasterKppDetail> TMasterKppDetail { get; set; }
        [InverseProperty("IdjoblevelNavigation")]
        public virtual ICollection<TMasterMedicalTemplate> TMasterMedicalTemplate { get; set; }
        [InverseProperty("IdjobLevelNavigation")]
        public virtual ICollection<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        [InverseProperty("IdjoblevelNavigation")]
        public virtual ICollection<TMasterTerminationNotice> TMasterTerminationNotice { get; set; }
        [InverseProperty("IdjobLevelNavigation")]
        public virtual ICollection<TMasterUserDataTrustee> TMasterUserDataTrustee { get; set; }
        [InverseProperty("IdjoblevelNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdjoblevelNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
    }
}
