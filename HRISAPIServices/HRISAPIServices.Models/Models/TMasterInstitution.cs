﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Institution")]
    public partial class TMasterInstitution
    {
        public TMasterInstitution()
        {
            TTrxEmpEducation = new HashSet<TTrxEmpEducation>();
            TTrxEssEmpEducation = new HashSet<TTrxEssEmpEducation>();
        }

        [Key]
        [Column("IDInstitution")]
        public int Idinstitution { get; set; }
        [Column("IDCity")]
        public int? Idcity { get; set; }
        [StringLength(10)]
        public string InstitutionCode { get; set; }
        [StringLength(150)]
        public string InstitutionName { get; set; }
        [StringLength(200)]
        public string Website { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcity")]
        [InverseProperty("TMasterInstitution")]
        public virtual TMasterCity IdcityNavigation { get; set; }
        [InverseProperty("IdinstitutionNavigation")]
        public virtual ICollection<TTrxEmpEducation> TTrxEmpEducation { get; set; }
        [InverseProperty("IdinstitutionNavigation")]
        public virtual ICollection<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
    }
}
