﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_PCN")]
    public partial class TTrxEmpPcn
    {
        [Key]
        [Column("IDEmpPCN")]
        public long IdempPcn { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDJobtitle")]
        public int? Idjobtitle { get; set; }
        [Column("IDJoblevel")]
        public int? Idjoblevel { get; set; }
        [Column("IDEmploymentType")]
        public int? IdemploymentType { get; set; }
        [Column("IDOrganization")]
        public int? Idorganization { get; set; }
        [Column("IDCompany")]
        public int? Idcompany { get; set; }
        [Column("IDLocation")]
        public int? Idlocation { get; set; }
        [Column("IDCostCenter")]
        public int? IdcostCenter { get; set; }
        [Column("IDPCNType")]
        public int? Idpcntype { get; set; }
        [Column("PCNNo")]
        [StringLength(150)]
        public string Pcnno { get; set; }
        [Column(TypeName = "date")]
        public DateTime? AppliedDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? JoinDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EffectiveDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string Initiator { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("IDCityLetter")]
        public int? IdcityLetter { get; set; }
        [Column("SignedOffNIK")]
        [StringLength(10)]
        public string SignedOffNik { get; set; }
        [StringLength(250)]
        public string SignedOffName { get; set; }
        [Column("SPKNo")]
        [StringLength(150)]
        public string Spkno { get; set; }

        [ForeignKey("IdcityLetter")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterCity IdcityLetterNavigation { get; set; }
        [ForeignKey("Idcompany")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterCompany IdcompanyNavigation { get; set; }
        [ForeignKey("IdcostCenter")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterCostCenter IdcostCenterNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdemploymentType")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterEmploymentType IdemploymentTypeNavigation { get; set; }
        [ForeignKey("Idjoblevel")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterJoblevel IdjoblevelNavigation { get; set; }
        [ForeignKey("Idjobtitle")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterJobtitle IdjobtitleNavigation { get; set; }
        [ForeignKey("Idlocation")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterLocation IdlocationNavigation { get; set; }
        [ForeignKey("Idorganization")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterOrganization IdorganizationNavigation { get; set; }
        [ForeignKey("Idpcntype")]
        [InverseProperty("TTrxEmpPcn")]
        public virtual TMasterPcnType IdpcntypeNavigation { get; set; }
    }
}
