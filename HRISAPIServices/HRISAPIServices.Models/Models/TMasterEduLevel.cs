﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_EduLevel")]
    public partial class TMasterEduLevel
    {
        public TMasterEduLevel()
        {
            TTrxEmpEducation = new HashSet<TTrxEmpEducation>();
            TTrxEmpFamily = new HashSet<TTrxEmpFamily>();
            TTrxEssEmpEducation = new HashSet<TTrxEssEmpEducation>();
            TTrxEssEmpFamily = new HashSet<TTrxEssEmpFamily>();
        }

        [Key]
        [Column("IDEduLevel")]
        public int IdeduLevel { get; set; }
        [Column("IDEduStatus")]
        public int? IdeduStatus { get; set; }
        [StringLength(10)]
        public string EduLevelCode { get; set; }
        [StringLength(50)]
        public string EduLevelName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdeduStatus")]
        [InverseProperty("TMasterEduLevel")]
        public virtual TMasterEduStatus IdeduStatusNavigation { get; set; }
        [InverseProperty("IdeduLevelNavigation")]
        public virtual ICollection<TTrxEmpEducation> TTrxEmpEducation { get; set; }
        [InverseProperty("IdeduLevelNavigation")]
        public virtual ICollection<TTrxEmpFamily> TTrxEmpFamily { get; set; }
        [InverseProperty("IdeduLevelNavigation")]
        public virtual ICollection<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
        [InverseProperty("IdeduLevelNavigation")]
        public virtual ICollection<TTrxEssEmpFamily> TTrxEssEmpFamily { get; set; }
    }
}
