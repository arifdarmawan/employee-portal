﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Salary_Monthly")]
    public partial class TTrxEmpSalaryMonthly
    {
        public TTrxEmpSalaryMonthly()
        {
            TTrxEmpSalaryMonthlyDetail = new HashSet<TTrxEmpSalaryMonthlyDetail>();
        }

        [Key]
        [Column("IDEmpSalaryMonthly")]
        public long IdempSalaryMonthly { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [StringLength(4)]
        public string Nyear { get; set; }
        [Column("NMonth")]
        [StringLength(2)]
        public string Nmonth { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpSalaryMonthly")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [InverseProperty("IdempSalaryMonthlyNavigation")]
        public virtual ICollection<TTrxEmpSalaryMonthlyDetail> TTrxEmpSalaryMonthlyDetail { get; set; }
    }
}
