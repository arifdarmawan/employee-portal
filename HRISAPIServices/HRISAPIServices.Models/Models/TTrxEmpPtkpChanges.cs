﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_PTKP_Changes")]
    public partial class TTrxEmpPtkpChanges
    {
        [Key]
        [Column("IDEmpPTKPChanges")]
        public long IdempPtkpchanges { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDPTKPDetail")]
        public int? Idptkpdetail { get; set; }
        [Column(TypeName = "date")]
        public DateTime? AppliedDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EffectiveDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(10)]
        public string Initiator { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpPtkpChanges")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("Idptkpdetail")]
        [InverseProperty("TTrxEmpPtkpChanges")]
        public virtual TMasterPtkpDetail IdptkpdetailNavigation { get; set; }
    }
}
