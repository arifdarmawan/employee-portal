﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Salary_Type")]
    public partial class TMasterSalaryType
    {
        public TMasterSalaryType()
        {
            TMasterSalaryItem = new HashSet<TMasterSalaryItem>();
        }

        [Key]
        [Column("IDSalaryType")]
        public int IdsalaryType { get; set; }
        [StringLength(10)]
        public string SalaryTypeCode { get; set; }
        [StringLength(50)]
        public string SalaryTypeName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdsalaryTypeNavigation")]
        public virtual ICollection<TMasterSalaryItem> TMasterSalaryItem { get; set; }
    }
}
