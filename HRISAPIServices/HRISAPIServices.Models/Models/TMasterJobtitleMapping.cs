﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Jobtitle_Mapping")]
    public partial class TMasterJobtitleMapping
    {
        [Key]
        [Column("IDJobtitleMapping")]
        public long IdjobtitleMapping { get; set; }
        [Column("IDJobtitle")]
        public int Idjobtitle { get; set; }
        [Column("IDHistoryLocGroup")]
        public long IdhistoryLocGroup { get; set; }
        [Column("IDParentJobtitle")]
        public int IdparentJobtitle { get; set; }
        [Column("TotalMPP")]
        public int TotalMpp { get; set; }
        public bool IsActive { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "date")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdhistoryLocGroup")]
        [InverseProperty("TMasterJobtitleMapping")]
        public virtual TTrxHistoryLocationGroup IdhistoryLocGroupNavigation { get; set; }
        [ForeignKey("Idjobtitle")]
        [InverseProperty("TMasterJobtitleMappingIdjobtitleNavigation")]
        public virtual TMasterJobtitle IdjobtitleNavigation { get; set; }
        [ForeignKey("IdparentJobtitle")]
        [InverseProperty("TMasterJobtitleMappingIdparentJobtitleNavigation")]
        public virtual TMasterJobtitle IdparentJobtitleNavigation { get; set; }
    }
}
