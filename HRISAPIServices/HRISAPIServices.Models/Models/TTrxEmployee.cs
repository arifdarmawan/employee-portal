﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Employee")]
    public partial class TTrxEmployee
    {
        public TTrxEmployee()
        {
            TMasterSalaryItemEmployee = new HashSet<TMasterSalaryItemEmployee>();
            TTrxEmpAddress = new HashSet<TTrxEmpAddress>();
            TTrxEmpBankAccount = new HashSet<TTrxEmpBankAccount>();
            TTrxEmpBankAccountChanges = new HashSet<TTrxEmpBankAccountChanges>();
            TTrxEmpBpjskes = new HashSet<TTrxEmpBpjskes>();
            TTrxEmpBpjskesChanges = new HashSet<TTrxEmpBpjskesChanges>();
            TTrxEmpEducation = new HashSet<TTrxEmpEducation>();
            TTrxEmpFamily = new HashSet<TTrxEmpFamily>();
            TTrxEmpIdCard = new HashSet<TTrxEmpIdCard>();
            TTrxEmpJamsostek = new HashSet<TTrxEmpJamsostek>();
            TTrxEmpJamsostekChanges = new HashSet<TTrxEmpJamsostekChanges>();
            TTrxEmpJobExp = new HashSet<TTrxEmpJobExp>();
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmpPph = new HashSet<TTrxEmpPph>();
            TTrxEmpPphChanges = new HashSet<TTrxEmpPphChanges>();
            TTrxEmpPtkpChanges = new HashSet<TTrxEmpPtkpChanges>();
            TTrxEmpSalary = new HashSet<TTrxEmpSalary>();
            TTrxEmpSalaryChanges = new HashSet<TTrxEmpSalaryChanges>();
            TTrxEmpSalaryMonthly = new HashSet<TTrxEmpSalaryMonthly>();
            TTrxEmpTermination = new HashSet<TTrxEmpTermination>();
            TTrxEmpViolation = new HashSet<TTrxEmpViolation>();
            TTrxEssEmpAddress = new HashSet<TTrxEssEmpAddress>();
            TTrxEssEmpBankAccount = new HashSet<TTrxEssEmpBankAccount>();
            TTrxEssEmpEducation = new HashSet<TTrxEssEmpEducation>();
            TTrxEssEmpFamily = new HashSet<TTrxEssEmpFamily>();
            TTrxEssEmpIdCard = new HashSet<TTrxEssEmpIdCard>();
            TTrxEssEmpJobExp = new HashSet<TTrxEssEmpJobExp>();
            TTrxEssEmployee = new HashSet<TTrxEssEmployee>();
            TTrxLoanApplication = new HashSet<TTrxLoanApplication>();
            TTrxMedicalClaim = new HashSet<TTrxMedicalClaim>();
        }

        [Key]
        [Column("IDEmployee")]
        public long Idemployee { get; set; }
        [Column("IDBloodType")]
        public int? IdbloodType { get; set; }
        [Column("IDReligion")]
        public int? Idreligion { get; set; }
        [Column("IDRace")]
        public int? Idrace { get; set; }
        [Column("IDMaritalStatus")]
        public int? IdmaritalStatus { get; set; }
        [Column("NIK")]
        [StringLength(10)]
        public string Nik { get; set; }
        [Column("OldNIK")]
        [StringLength(10)]
        public string OldNik { get; set; }
        [StringLength(250)]
        public string EmployeeName { get; set; }
        public string Password { get; set; }
        public string Pin { get; set; }
        [Column("IDBirthPlace")]
        public int? IdbirthPlace { get; set; }
        [StringLength(50)]
        public string BirthPlace { get; set; }
        [Column(TypeName = "date")]
        public DateTime? BirthDate { get; set; }
        [StringLength(1)]
        public string Gender { get; set; }
        public byte[] Image { get; set; }
        public string ImagePath { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Column("IDHomeBase")]
        public int? IdhomeBase { get; set; }
        [StringLength(50)]
        public string HomeBase { get; set; }
        [Column("IDCitizenship")]
        public int? Idcitizenship { get; set; }
        [StringLength(50)]
        public string Citizenship { get; set; }
        [StringLength(150)]
        public string OfficeMail { get; set; }
        [StringLength(150)]
        public string PrivateMail { get; set; }
        [StringLength(50)]
        public string MobileNo { get; set; }
        [Column("PCNNo")]
        [StringLength(150)]
        public string Pcnno { get; set; }
        [Column("IDPCNType")]
        public int? Idpcntype { get; set; }
        [Column("IDOrganization")]
        public int? Idorganization { get; set; }
        [Column("IDCompany")]
        public int? Idcompany { get; set; }
        [Column("IDLocation")]
        public int? Idlocation { get; set; }
        [Column("IDJobtitle")]
        public int? Idjobtitle { get; set; }
        [Column("IDJoblevel")]
        public int? Idjoblevel { get; set; }
        [Column("IDEmploymentType")]
        public int? IdemploymentType { get; set; }
        [Column("IDCostCenter")]
        public int? IdcostCenter { get; set; }
        [Column("IDPoH")]
        public int? IdpoH { get; set; }
        [StringLength(10)]
        public string EmployeeReference { get; set; }
        [StringLength(150)]
        public string RefNo { get; set; }
        [StringLength(200)]
        public string RefNote { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? JoinDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EffectiveDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string Initiator { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        public bool IsRehired { get; set; }
        [StringLength(50)]
        public string RehiredBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RehiredDate { get; set; }
        public bool IsTerminateRehired { get; set; }
        [StringLength(50)]
        public string TerminateRehiredBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? TerminateRehiredDate { get; set; }
        public bool IsTerminated { get; set; }
        [StringLength(50)]
        public string TerminatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? TerminatedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [StringLength(50)]
        public string ResetPwdBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ResetPwdDate { get; set; }
        [Column("ResetPINBy")]
        [StringLength(50)]
        public string ResetPinby { get; set; }
        [Column("ResetPINDate", TypeName = "datetime")]
        public DateTime? ResetPindate { get; set; }
        [StringLength(250)]
        public string MotherName { get; set; }
        [Column("SPKNo")]
        [StringLength(150)]
        public string Spkno { get; set; }
        [Column("SPKType")]
        [StringLength(50)]
        public string Spktype { get; set; }
        [Column("IDPlaces")]
        public int? Idplaces { get; set; }
        [Column("SignedOffNIK")]
        [StringLength(10)]
        public string SignedOffNik { get; set; }
        [StringLength(250)]
        public string SignedOffName { get; set; }
        [Column("IDReportingTo")]
        public int? IdreportingTo { get; set; }

        [ForeignKey("IdbirthPlace")]
        [InverseProperty("TTrxEmployeeIdbirthPlaceNavigation")]
        public virtual TMasterCity IdbirthPlaceNavigation { get; set; }
        [ForeignKey("IdbloodType")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterBloodType IdbloodTypeNavigation { get; set; }
        [ForeignKey("Idcitizenship")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterCountry IdcitizenshipNavigation { get; set; }
        [ForeignKey("Idcompany")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterCompany IdcompanyNavigation { get; set; }
        [ForeignKey("IdcostCenter")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterCostCenter IdcostCenterNavigation { get; set; }
        [ForeignKey("IdemploymentType")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterEmploymentType IdemploymentTypeNavigation { get; set; }
        [ForeignKey("IdhomeBase")]
        [InverseProperty("TTrxEmployeeIdhomeBaseNavigation")]
        public virtual TMasterCity IdhomeBaseNavigation { get; set; }
        [ForeignKey("Idjoblevel")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterJoblevel IdjoblevelNavigation { get; set; }
        [ForeignKey("Idjobtitle")]
        [InverseProperty("TTrxEmployeeIdjobtitleNavigation")]
        public virtual TMasterJobtitle IdjobtitleNavigation { get; set; }
        [ForeignKey("Idlocation")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterLocation IdlocationNavigation { get; set; }
        [ForeignKey("IdmaritalStatus")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterMaritalStatus IdmaritalStatusNavigation { get; set; }
        [ForeignKey("Idorganization")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterOrganization IdorganizationNavigation { get; set; }
        [ForeignKey("Idpcntype")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterPcnType IdpcntypeNavigation { get; set; }
        [ForeignKey("Idplaces")]
        [InverseProperty("TTrxEmployeeIdplacesNavigation")]
        public virtual TMasterCity IdplacesNavigation { get; set; }
        [ForeignKey("IdpoH")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterState IdpoHNavigation { get; set; }
        [ForeignKey("Idrace")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterRace IdraceNavigation { get; set; }
        [ForeignKey("Idreligion")]
        [InverseProperty("TTrxEmployee")]
        public virtual TMasterReligion IdreligionNavigation { get; set; }
        [ForeignKey("IdreportingTo")]
        [InverseProperty("TTrxEmployeeIdreportingToNavigation")]
        public virtual TMasterJobtitle IdreportingToNavigation { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TMasterSalaryItemEmployee> TMasterSalaryItemEmployee { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpAddress> TTrxEmpAddress { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpBankAccount> TTrxEmpBankAccount { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpBankAccountChanges> TTrxEmpBankAccountChanges { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpBpjskes> TTrxEmpBpjskes { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpBpjskesChanges> TTrxEmpBpjskesChanges { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpEducation> TTrxEmpEducation { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpFamily> TTrxEmpFamily { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpIdCard> TTrxEmpIdCard { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpJamsostek> TTrxEmpJamsostek { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpJamsostekChanges> TTrxEmpJamsostekChanges { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpJobExp> TTrxEmpJobExp { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpPph> TTrxEmpPph { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpPphChanges> TTrxEmpPphChanges { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpPtkpChanges> TTrxEmpPtkpChanges { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpSalary> TTrxEmpSalary { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpSalaryChanges> TTrxEmpSalaryChanges { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpSalaryMonthly> TTrxEmpSalaryMonthly { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpTermination> TTrxEmpTermination { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEmpViolation> TTrxEmpViolation { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEssEmpAddress> TTrxEssEmpAddress { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEssEmpBankAccount> TTrxEssEmpBankAccount { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEssEmpFamily> TTrxEssEmpFamily { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEssEmpIdCard> TTrxEssEmpIdCard { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEssEmpJobExp> TTrxEssEmpJobExp { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxEssEmployee> TTrxEssEmployee { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxLoanApplication> TTrxLoanApplication { get; set; }
        [InverseProperty("IdemployeeNavigation")]
        public virtual ICollection<TTrxMedicalClaim> TTrxMedicalClaim { get; set; }
    }
}
