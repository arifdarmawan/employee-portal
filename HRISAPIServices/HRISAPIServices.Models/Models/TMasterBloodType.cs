﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Blood_Type")]
    public partial class TMasterBloodType
    {
        public TMasterBloodType()
        {
            TTrxEmployee = new HashSet<TTrxEmployee>();
            TTrxEssEmployee = new HashSet<TTrxEssEmployee>();
        }

        [Key]
        [Column("IDBloodType")]
        public int IdbloodType { get; set; }
        [StringLength(10)]
        public string BloodTypeCode { get; set; }
        [StringLength(50)]
        public string BloodTypeName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdbloodTypeNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
        [InverseProperty("IdbloodTypeNavigation")]
        public virtual ICollection<TTrxEssEmployee> TTrxEssEmployee { get; set; }
    }
}
