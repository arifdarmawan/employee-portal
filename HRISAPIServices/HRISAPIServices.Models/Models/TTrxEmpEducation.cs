﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Education")]
    public partial class TTrxEmpEducation
    {
        [Key]
        [Column("IDEmpEducation")]
        public long IdempEducation { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDEduStatus")]
        public int? IdeduStatus { get; set; }
        [Column("IDEduLevel")]
        public int? IdeduLevel { get; set; }
        [Column("IDEduMajor")]
        public int? IdeduMajor { get; set; }
        [Column("IDInstitution")]
        public int? Idinstitution { get; set; }
        [Column("IDCity")]
        public int? Idcity { get; set; }
        [Column("IDEduResult")]
        public int? IdeduResult { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Graduation { get; set; }
        [Column(TypeName = "numeric(3, 2)")]
        public decimal? GradePoint { get; set; }
        [StringLength(10)]
        public string Title { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        public bool IsLastEdu { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcity")]
        [InverseProperty("TTrxEmpEducation")]
        public virtual TMasterCity IdcityNavigation { get; set; }
        [ForeignKey("IdeduLevel")]
        [InverseProperty("TTrxEmpEducation")]
        public virtual TMasterEduLevel IdeduLevelNavigation { get; set; }
        [ForeignKey("IdeduMajor")]
        [InverseProperty("TTrxEmpEducation")]
        public virtual TMasterEduMajor IdeduMajorNavigation { get; set; }
        [ForeignKey("IdeduResult")]
        [InverseProperty("TTrxEmpEducation")]
        public virtual TMasterEduResult IdeduResultNavigation { get; set; }
        [ForeignKey("IdeduStatus")]
        [InverseProperty("TTrxEmpEducation")]
        public virtual TMasterEduStatus IdeduStatusNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpEducation")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("Idinstitution")]
        [InverseProperty("TTrxEmpEducation")]
        public virtual TMasterInstitution IdinstitutionNavigation { get; set; }
    }
}
