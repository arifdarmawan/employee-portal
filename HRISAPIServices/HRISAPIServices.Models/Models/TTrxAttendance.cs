﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Attendance")]
    public partial class TTrxAttendance
    {
        [Key]
        [Column("IDAttendance")]
        public Guid Idattendance { get; set; }
        [Column("IDAttendanceType")]
        public int IdattendanceType { get; set; }
        [Column("IDAttachment")]
        public Guid? Idattachment { get; set; }
        [StringLength(16)]
        public string AttendanceCode { get; set; }
        [Column("NIK")]
        [StringLength(10)]
        public string Nik { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(500)]
        public string Notes { get; set; }
        public bool IsApproved { get; set; }
        [StringLength(50)]
        public string ApprovedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ApprovedDate { get; set; }
        public bool IsRejected { get; set; }
        [StringLength(50)]
        public string RejectedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RejectedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idattachment")]
        [InverseProperty("TTrxAttendance")]
        public virtual TTrxAttachment IdattachmentNavigation { get; set; }
        [ForeignKey("IdattendanceType")]
        [InverseProperty("TTrxAttendance")]
        public virtual TMasterAttendanceType IdattendanceTypeNavigation { get; set; }
    }
}
