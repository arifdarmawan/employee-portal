﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_PTKP_Detail")]
    public partial class TMasterPtkpDetail
    {
        public TMasterPtkpDetail()
        {
            TTrxEmpPph = new HashSet<TTrxEmpPph>();
            TTrxEmpPphChanges = new HashSet<TTrxEmpPphChanges>();
            TTrxEmpPtkpChanges = new HashSet<TTrxEmpPtkpChanges>();
        }

        [Key]
        [Column("IDPTKPDetail")]
        public int Idptkpdetail { get; set; }
        [Column("IDTmpPTKP")]
        public long? IdtmpPtkp { get; set; }
        [Column("IDPTKP")]
        public int? Idptkp { get; set; }
        [StringLength(800)]
        public string Formula { get; set; }
        [StringLength(200)]
        public string TableName { get; set; }
        [StringLength(100)]
        public string Query { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idptkp")]
        [InverseProperty("TMasterPtkpDetail")]
        public virtual TMasterPtkp IdptkpNavigation { get; set; }
        [ForeignKey("IdtmpPtkp")]
        [InverseProperty("TMasterPtkpDetail")]
        public virtual TMasterPtkpTemplate IdtmpPtkpNavigation { get; set; }
        [InverseProperty("IdptkpdetailNavigation")]
        public virtual ICollection<TTrxEmpPph> TTrxEmpPph { get; set; }
        [InverseProperty("IdptkpdetailNavigation")]
        public virtual ICollection<TTrxEmpPphChanges> TTrxEmpPphChanges { get; set; }
        [InverseProperty("IdptkpdetailNavigation")]
        public virtual ICollection<TTrxEmpPtkpChanges> TTrxEmpPtkpChanges { get; set; }
    }
}
