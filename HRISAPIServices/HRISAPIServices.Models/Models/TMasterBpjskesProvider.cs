﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_BPJSKes_Provider")]
    public partial class TMasterBpjskesProvider
    {
        public TMasterBpjskesProvider()
        {
            TMasterBpjskesProviderDetail = new HashSet<TMasterBpjskesProviderDetail>();
            TTrxEmpBpjskes = new HashSet<TTrxEmpBpjskes>();
            TTrxEmpBpjskesChanges = new HashSet<TTrxEmpBpjskesChanges>();
        }

        [Column("IDBPJSKesProvider")]
        public int IdbpjskesProvider { get; set; }
        [Required]
        [StringLength(50)]
        public string OfficeCode { get; set; }
        [Required]
        [StringLength(150)]
        public string OfficeName { get; set; }
        [StringLength(250)]
        public string Address { get; set; }
        [StringLength(50)]
        public string RegisterNo { get; set; }
        [Column("EDabuUser")]
        [StringLength(50)]
        public string EdabuUser { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdbpjskesProviderNavigation")]
        public virtual ICollection<TMasterBpjskesProviderDetail> TMasterBpjskesProviderDetail { get; set; }
        [InverseProperty("IdbpjskesProviderNavigation")]
        public virtual ICollection<TTrxEmpBpjskes> TTrxEmpBpjskes { get; set; }
        [InverseProperty("IdbpjskesProviderNavigation")]
        public virtual ICollection<TTrxEmpBpjskesChanges> TTrxEmpBpjskesChanges { get; set; }
    }
}
