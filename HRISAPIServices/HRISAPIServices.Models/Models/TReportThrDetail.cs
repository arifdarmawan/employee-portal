﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Report_THR_Detail")]
    public partial class TReportThrDetail
    {
        [Key]
        [Column("IDTHRDetail")]
        public long Idthrdetail { get; set; }
        [Column("IDTHR")]
        public Guid? Idthr { get; set; }
        [StringLength(50)]
        public string SalaryItemCode { get; set; }
        [StringLength(50)]
        public string SalaryItemName { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Amount { get; set; }
        [StringLength(50)]
        public string SalaryType { get; set; }
        [StringLength(10)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
