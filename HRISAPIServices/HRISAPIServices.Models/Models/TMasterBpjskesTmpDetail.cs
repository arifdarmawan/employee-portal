﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_BPJSKes_TmpDetail")]
    public partial class TMasterBpjskesTmpDetail
    {
        [Key]
        [Column("IDTmpBPJSKesDetail")]
        public long IdtmpBpjskesDetail { get; set; }
        [Column("IDTmpBPJSKes")]
        public int? IdtmpBpjskes { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? TotalGross { get; set; }
        [Column("Company%", TypeName = "decimal(5, 2)")]
        public decimal? Company { get; set; }
        [Column("Employee%", TypeName = "decimal(5, 2)")]
        public decimal? Employee { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal MaxPremi { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? MaxPremiC { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? MaxPremiE { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public bool? IsPayroll { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdtmpBpjskes")]
        [InverseProperty("TMasterBpjskesTmpDetail")]
        public virtual TMasterBpjskesTemplate IdtmpBpjskesNavigation { get; set; }
    }
}
