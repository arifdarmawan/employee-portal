﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Location")]
    public partial class TMasterLocation
    {
        public TMasterLocation()
        {
            TMasterSalaryItemLocation = new HashSet<TMasterSalaryItemLocation>();
            TMasterUserDataTrustee = new HashSet<TMasterUserDataTrustee>();
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
            TTrxHistoryCompanyLocation = new HashSet<TTrxHistoryCompanyLocation>();
            TTrxHistoryLocationGroup = new HashSet<TTrxHistoryLocationGroup>();
        }

        [Column("IDLocation")]
        public int Idlocation { get; set; }
        [Column("IDCountry")]
        public int? Idcountry { get; set; }
        [Column("IDState")]
        public int? Idstate { get; set; }
        [Column("IDCity")]
        public int? Idcity { get; set; }
        [StringLength(10)]
        public string LocationCode { get; set; }
        [StringLength(150)]
        public string LocationName { get; set; }
        [StringLength(10)]
        public string OracleCode { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcity")]
        [InverseProperty("TMasterLocation")]
        public virtual TMasterCity IdcityNavigation { get; set; }
        [ForeignKey("Idcountry")]
        [InverseProperty("TMasterLocation")]
        public virtual TMasterCountry IdcountryNavigation { get; set; }
        [ForeignKey("Idstate")]
        [InverseProperty("TMasterLocation")]
        public virtual TMasterState IdstateNavigation { get; set; }
        [InverseProperty("IdlocationNavigation")]
        public virtual ICollection<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        [InverseProperty("IdlocationNavigation")]
        public virtual ICollection<TMasterUserDataTrustee> TMasterUserDataTrustee { get; set; }
        [InverseProperty("IdlocationNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdlocationNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
        [InverseProperty("IdlocationNavigation")]
        public virtual ICollection<TTrxHistoryCompanyLocation> TTrxHistoryCompanyLocation { get; set; }
        [InverseProperty("IdlocationNavigation")]
        public virtual ICollection<TTrxHistoryLocationGroup> TTrxHistoryLocationGroup { get; set; }
    }
}
