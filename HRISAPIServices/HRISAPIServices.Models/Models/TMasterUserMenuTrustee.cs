﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_User_Menu_Trustee")]
    public partial class TMasterUserMenuTrustee
    {
        [Key]
        [Column("IDUserMenuTrustee")]
        public long IduserMenuTrustee { get; set; }
        [Column("IDGroup")]
        public int? Idgroup { get; set; }
        [Column("IDMenu")]
        public int? Idmenu { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idgroup")]
        [InverseProperty("TMasterUserMenuTrustee")]
        public virtual TMasterGroup IdgroupNavigation { get; set; }
        [ForeignKey("Idmenu")]
        [InverseProperty("TMasterUserMenuTrustee")]
        public virtual TMasterMenu IdmenuNavigation { get; set; }
    }
}
