﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Menu")]
    public partial class TMasterMenu
    {
        public TMasterMenu()
        {
            TMasterUserMenuTrustee = new HashSet<TMasterUserMenuTrustee>();
        }

        [Column("IDMenu")]
        public int Idmenu { get; set; }
        [Required]
        [StringLength(20)]
        public string CodeMenu { get; set; }
        [StringLength(50)]
        public string NameMenu { get; set; }
        [Column("URLMenu")]
        [StringLength(150)]
        public string Urlmenu { get; set; }
        [StringLength(50)]
        public string GroupMenu { get; set; }
        [StringLength(20)]
        public string ParentMenu { get; set; }
        [StringLength(50)]
        public string TitleMenu { get; set; }
        public bool IsLink { get; set; }
        [StringLength(100)]
        public string ImageName { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdmenuNavigation")]
        public virtual ICollection<TMasterUserMenuTrustee> TMasterUserMenuTrustee { get; set; }
    }
}
