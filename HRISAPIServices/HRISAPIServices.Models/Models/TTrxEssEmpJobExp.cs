﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_ESS_Emp_JobExp")]
    public partial class TTrxEssEmpJobExp
    {
        [Key]
        [Column("IDEmpJobExp")]
        public long IdempJobExp { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [StringLength(150)]
        public string Jobtitle { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(250)]
        public string TerminationReason { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? FirstSalary { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? LastSalary { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [StringLength(150)]
        public string CompanyName { get; set; }
        [StringLength(250)]
        public string CompanyAddress { get; set; }
        [StringLength(10)]
        public string ZipCode { get; set; }
        [StringLength(50)]
        public string CompanyPhone { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEssEmpJobExp")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
    }
}
