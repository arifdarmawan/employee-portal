﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_PPh21_Template")]
    public partial class TMasterPph21Template
    {
        public TMasterPph21Template()
        {
            TMasterPph21Component = new HashSet<TMasterPph21Component>();
            TTrxEmpPph = new HashSet<TTrxEmpPph>();
            TTrxEmpPphChanges = new HashSet<TTrxEmpPphChanges>();
        }

        [Key]
        [Column("IDPPhTemplate")]
        public int IdpphTemplate { get; set; }
        [StringLength(50)]
        public string TemplateCode { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Column(TypeName = "date")]
        public DateTime? NonActiveDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdpphTemplateNavigation")]
        public virtual ICollection<TMasterPph21Component> TMasterPph21Component { get; set; }
        [InverseProperty("IdpphTemplateNavigation")]
        public virtual ICollection<TTrxEmpPph> TTrxEmpPph { get; set; }
        [InverseProperty("IdpphTemplateNavigation")]
        public virtual ICollection<TTrxEmpPphChanges> TTrxEmpPphChanges { get; set; }
    }
}
