﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_History_Report")]
    public partial class TTrxHistoryReport
    {
        [Key]
        [Column("IDReport")]
        public Guid Idreport { get; set; }
        [StringLength(150)]
        public string ReportName { get; set; }
        [StringLength(250)]
        public string ReportNo { get; set; }
        [StringLength(50)]
        public string ReportType { get; set; }
        public int? SequenceNo { get; set; }
        [Column("NYear")]
        [StringLength(4)]
        public string Nyear { get; set; }
        [StringLength(50)]
        public string PrintedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PrintedDate { get; set; }
    }
}
