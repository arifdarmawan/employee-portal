﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Processed_PPh21_Monthly")]
    public partial class TTrxProcessedPph21Monthly
    {
        [Key]
        [Column("IDProcessPPh21")]
        public Guid IdprocessPph21 { get; set; }
        [Column("NYear")]
        [StringLength(4)]
        public string Nyear { get; set; }
        [Column("NMonth")]
        [StringLength(2)]
        public string Nmonth { get; set; }
        [Column("NIK")]
        [StringLength(10)]
        public string Nik { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
