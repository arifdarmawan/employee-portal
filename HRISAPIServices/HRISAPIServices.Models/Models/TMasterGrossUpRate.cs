﻿using System;
using System.Collections.Generic;

namespace HRISAPIServices.Models.Models
{
    public partial class TMasterGrossUpRate
    {
        public int IdgrossUpRate { get; set; }
        public string Nyear { get; set; }
        public decimal? StartValue { get; set; }
        public decimal? EndValue { get; set; }
        public decimal? PercentageNpwp { get; set; }
        public decimal? PercentageNonNpwp { get; set; }
        public decimal? DivideByNpwp { get; set; }
        public decimal? DivideByNonNpwp { get; set; }
        public decimal? AdditionBy { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
