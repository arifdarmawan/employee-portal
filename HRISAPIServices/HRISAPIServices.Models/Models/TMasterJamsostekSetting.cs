﻿using System;
using System.Collections.Generic;

namespace HRISAPIServices.Models.Models
{
    public partial class TMasterJamsostekSetting
    {
        public int IdjamsostekSetting { get; set; }
        public int? IdsalaryItem { get; set; }
        public short? MaxAge { get; set; }
        public bool IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
    }
}
