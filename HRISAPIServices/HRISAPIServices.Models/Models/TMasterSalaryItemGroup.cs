﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Salary_Item_Group")]
    public partial class TMasterSalaryItemGroup
    {
        public TMasterSalaryItemGroup()
        {
            TMasterSalaryItem = new HashSet<TMasterSalaryItem>();
        }

        [Key]
        [Column("IDSalaryItemGroup")]
        public int IdsalaryItemGroup { get; set; }
        [StringLength(10)]
        public string GroupCode { get; set; }
        [StringLength(50)]
        public string GroupName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdsalaryItemGroupNavigation")]
        public virtual ICollection<TMasterSalaryItem> TMasterSalaryItem { get; set; }
    }
}
