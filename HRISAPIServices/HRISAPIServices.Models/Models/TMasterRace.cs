﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Race")]
    public partial class TMasterRace
    {
        public TMasterRace()
        {
            TTrxEmployee = new HashSet<TTrxEmployee>();
            TTrxEssEmployee = new HashSet<TTrxEssEmployee>();
        }

        [Key]
        [Column("IDRace")]
        public int Idrace { get; set; }
        [StringLength(10)]
        public string RaceCode { get; set; }
        [StringLength(50)]
        public string RaceName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdraceNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
        [InverseProperty("IdraceNavigation")]
        public virtual ICollection<TTrxEssEmployee> TTrxEssEmployee { get; set; }
    }
}
