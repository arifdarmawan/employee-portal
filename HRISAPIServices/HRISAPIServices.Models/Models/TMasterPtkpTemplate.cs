﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_PTKP_Template")]
    public partial class TMasterPtkpTemplate
    {
        public TMasterPtkpTemplate()
        {
            TMasterPtkpDetail = new HashSet<TMasterPtkpDetail>();
        }

        [Key]
        [Column("IDTmpPTKP")]
        public long IdtmpPtkp { get; set; }
        [Column("PTKPCode")]
        [StringLength(15)]
        public string Ptkpcode { get; set; }
        [Column("PTKPName")]
        [StringLength(100)]
        public string Ptkpname { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        public short? Tanggungan { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdtmpPtkpNavigation")]
        public virtual ICollection<TMasterPtkpDetail> TMasterPtkpDetail { get; set; }
    }
}
