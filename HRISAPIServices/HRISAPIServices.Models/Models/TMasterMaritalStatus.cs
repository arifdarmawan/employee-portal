﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_MaritalStatus")]
    public partial class TMasterMaritalStatus
    {
        public TMasterMaritalStatus()
        {
            TMasterMedicalTmpDetail = new HashSet<TMasterMedicalTmpDetail>();
            TMasterSalaryItemLocation = new HashSet<TMasterSalaryItemLocation>();
            TTrxEmpFamily = new HashSet<TTrxEmpFamily>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
            TTrxEssEmpFamily = new HashSet<TTrxEssEmpFamily>();
            TTrxEssEmployee = new HashSet<TTrxEssEmployee>();
        }

        [Key]
        [Column("IDMaritalStatus")]
        public int IdmaritalStatus { get; set; }
        [StringLength(10)]
        public string MaritalStatusCode { get; set; }
        [StringLength(50)]
        public string MaritalStatusName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdmaritalStatusNavigation")]
        public virtual ICollection<TMasterMedicalTmpDetail> TMasterMedicalTmpDetail { get; set; }
        [InverseProperty("IdmaritalStatusNavigation")]
        public virtual ICollection<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        [InverseProperty("IdmaritalStatusNavigation")]
        public virtual ICollection<TTrxEmpFamily> TTrxEmpFamily { get; set; }
        [InverseProperty("IdmaritalStatusNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
        [InverseProperty("IdmaritalStatusNavigation")]
        public virtual ICollection<TTrxEssEmpFamily> TTrxEssEmpFamily { get; set; }
        [InverseProperty("IdmaritalStatusNavigation")]
        public virtual ICollection<TTrxEssEmployee> TTrxEssEmployee { get; set; }
    }
}
