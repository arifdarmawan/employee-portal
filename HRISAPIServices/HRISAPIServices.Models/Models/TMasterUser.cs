﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_User")]
    public partial class TMasterUser
    {
        public TMasterUser()
        {
            TMasterUserGroup = new HashSet<TMasterUserGroup>();
        }

        [Key]
        [Column("IDUser")]
        public long Iduser { get; set; }
        [StringLength(20)]
        public string UserName { get; set; }
        public string Password { get; set; }
        [StringLength(150)]
        public string Name { get; set; }
        [StringLength(150)]
        public string Email { get; set; }
        [StringLength(8)]
        public string Pin { get; set; }
        public Guid? Token { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ExpireToken { get; set; }
        public bool TokenIsUsed { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastLogin { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IduserNavigation")]
        public virtual ICollection<TMasterUserGroup> TMasterUserGroup { get; set; }
    }
}
