﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Allowance_Jamsostek")]
    public partial class TMasterAllowanceJamsostek
    {
        [Key]
        [Column("IDAllowanceJamsostek")]
        public long IdallowanceJamsostek { get; set; }
        [Column("IDTmpJamsostek")]
        public int IdtmpJamsostek { get; set; }
        [Column("IDTmpSalary")]
        public int? IdtmpSalary { get; set; }
        [Column("IDJoblevel")]
        public int? Idjoblevel { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Amount { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idjoblevel")]
        [InverseProperty("TMasterAllowanceJamsostek")]
        public virtual TMasterJoblevel IdjoblevelNavigation { get; set; }
        [ForeignKey("IdtmpJamsostek")]
        [InverseProperty("TMasterAllowanceJamsostek")]
        public virtual TMasterJamsostekTemplate IdtmpJamsostekNavigation { get; set; }
        [ForeignKey("IdtmpSalary")]
        [InverseProperty("TMasterAllowanceJamsostek")]
        public virtual TMasterSalaryTemplate IdtmpSalaryNavigation { get; set; }
    }
}
