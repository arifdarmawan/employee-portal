﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Address")]
    public partial class TTrxEmpAddress
    {
        [Key]
        [Column("IDEmpAddress")]
        public long IdempAddress { get; set; }
        [Column("IDEmployee")]
        public long Idemployee { get; set; }
        [Column("IDCountryO")]
        public int? IdcountryO { get; set; }
        [Column("IDStateO")]
        public int? IdstateO { get; set; }
        [Column("IDCityO")]
        public int? IdcityO { get; set; }
        [Column("OAddress")]
        [StringLength(250)]
        public string Oaddress { get; set; }
        [Column("OZipCode")]
        [StringLength(10)]
        public string OzipCode { get; set; }
        [Column("IDCountryR")]
        public int? IdcountryR { get; set; }
        [Column("IDStateR")]
        public int? IdstateR { get; set; }
        [Column("IDCityR")]
        public int? IdcityR { get; set; }
        [Column("RAddress")]
        [StringLength(250)]
        public string Raddress { get; set; }
        [Column("RZipCode")]
        [StringLength(10)]
        public string RzipCode { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdcityO")]
        [InverseProperty("TTrxEmpAddressIdcityONavigation")]
        public virtual TMasterCity IdcityONavigation { get; set; }
        [ForeignKey("IdcityR")]
        [InverseProperty("TTrxEmpAddressIdcityRNavigation")]
        public virtual TMasterCity IdcityRNavigation { get; set; }
        [ForeignKey("IdcountryO")]
        [InverseProperty("TTrxEmpAddressIdcountryONavigation")]
        public virtual TMasterCountry IdcountryONavigation { get; set; }
        [ForeignKey("IdcountryR")]
        [InverseProperty("TTrxEmpAddressIdcountryRNavigation")]
        public virtual TMasterCountry IdcountryRNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpAddress")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdstateO")]
        [InverseProperty("TTrxEmpAddressIdstateONavigation")]
        public virtual TMasterState IdstateONavigation { get; set; }
        [ForeignKey("IdstateR")]
        [InverseProperty("TTrxEmpAddressIdstateRNavigation")]
        public virtual TMasterState IdstateRNavigation { get; set; }
    }
}
