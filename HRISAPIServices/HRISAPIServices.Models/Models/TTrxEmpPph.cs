﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_PPh")]
    public partial class TTrxEmpPph
    {
        [Key]
        [Column("IDEmpPPh")]
        public long IdempPph { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDPPhMethod")]
        public int? IdpphMethod { get; set; }
        [Column("IDPTKPDetail")]
        public int? Idptkpdetail { get; set; }
        [Column("IDKPP")]
        public int? Idkpp { get; set; }
        [Column("NPWP")]
        [StringLength(50)]
        public string Npwp { get; set; }
        [Column(TypeName = "date")]
        public DateTime? RegDate { get; set; }
        [Column("IDPPhTemplate")]
        public int? IdpphTemplate { get; set; }
        [Column("NPWPAddress")]
        [StringLength(250)]
        public string Npwpaddress { get; set; }
        [Column("ProcessDatePPh", TypeName = "date")]
        public DateTime? ProcessDatePph { get; set; }
        public bool IsPayroll { get; set; }
        [StringLength(1)]
        public string Liable { get; set; }
        [Column(TypeName = "date")]
        public DateTime? ProcessDateB { get; set; }
        [Column("PTKPB", TypeName = "numeric(18, 2)")]
        public decimal? Ptkpb { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? NettoFinalB { get; set; }
        [Column("PPhFinalB", TypeName = "numeric(18, 2)")]
        public decimal? PphFinalB { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpPph")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("Idkpp")]
        [InverseProperty("TTrxEmpPph")]
        public virtual TMasterKpp IdkppNavigation { get; set; }
        [ForeignKey("IdpphMethod")]
        [InverseProperty("TTrxEmpPph")]
        public virtual TMasterPph21Method IdpphMethodNavigation { get; set; }
        [ForeignKey("IdpphTemplate")]
        [InverseProperty("TTrxEmpPph")]
        public virtual TMasterPph21Template IdpphTemplateNavigation { get; set; }
        [ForeignKey("Idptkpdetail")]
        [InverseProperty("TTrxEmpPph")]
        public virtual TMasterPtkpDetail IdptkpdetailNavigation { get; set; }
    }
}
