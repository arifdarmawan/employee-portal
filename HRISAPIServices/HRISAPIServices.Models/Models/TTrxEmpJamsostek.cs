﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Jamsostek")]
    public partial class TTrxEmpJamsostek
    {
        [Key]
        [Column("IDEmpJamsostek")]
        public long IdempJamsostek { get; set; }
        [Column("IDEmployee")]
        public long Idemployee { get; set; }
        [Column("IDTmpJamsostek")]
        public int IdtmpJamsostek { get; set; }
        [Column("IDCurrency")]
        public int Idcurrency { get; set; }
        [Column("IDJamsostekProvider")]
        public int? IdjamsostekProvider { get; set; }
        [StringLength(150)]
        public string MemberNo { get; set; }
        [Column(TypeName = "date")]
        public DateTime? MembershipDate { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? PremiBasedValue { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartPeriod { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndPeriod { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcurrency")]
        [InverseProperty("TTrxEmpJamsostek")]
        public virtual TMasterCurrency IdcurrencyNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpJamsostek")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdjamsostekProvider")]
        [InverseProperty("TTrxEmpJamsostek")]
        public virtual TMasterJamsostekProvider IdjamsostekProviderNavigation { get; set; }
        [ForeignKey("IdtmpJamsostek")]
        [InverseProperty("TTrxEmpJamsostek")]
        public virtual TMasterJamsostekTemplate IdtmpJamsostekNavigation { get; set; }
    }
}
