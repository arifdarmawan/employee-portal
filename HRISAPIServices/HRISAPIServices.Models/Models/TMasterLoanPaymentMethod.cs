﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Loan_Payment_Method")]
    public partial class TMasterLoanPaymentMethod
    {
        public TMasterLoanPaymentMethod()
        {
            TTrxLoanScheduled = new HashSet<TTrxLoanScheduled>();
        }

        [Key]
        [Column("IDPayMethod")]
        public int IdpayMethod { get; set; }
        [StringLength(10)]
        public string PayMethodCode { get; set; }
        [StringLength(150)]
        public string PayMethodName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdpayMethodNavigation")]
        public virtual ICollection<TTrxLoanScheduled> TTrxLoanScheduled { get; set; }
    }
}
