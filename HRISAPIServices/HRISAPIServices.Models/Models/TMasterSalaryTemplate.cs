﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Salary_Template")]
    public partial class TMasterSalaryTemplate
    {
        public TMasterSalaryTemplate()
        {
            TMasterAllowanceJamsostek = new HashSet<TMasterAllowanceJamsostek>();
            TMasterSalaryTmpDetail = new HashSet<TMasterSalaryTmpDetail>();
            TTrxEmpSalary = new HashSet<TTrxEmpSalary>();
            TTrxEmpSalaryChanges = new HashSet<TTrxEmpSalaryChanges>();
        }

        [Key]
        [Column("IDTmpSalary")]
        public int IdtmpSalary { get; set; }
        [Column("IDCurrency")]
        public int? Idcurrency { get; set; }
        [StringLength(10)]
        public string TemplateCode { get; set; }
        [StringLength(50)]
        public string TemplateName { get; set; }
        [StringLength(1)]
        public string Payment { get; set; }
        [StringLength(50)]
        public string Initiator { get; set; }
        [Required]
        public bool? IsDefault { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcurrency")]
        [InverseProperty("TMasterSalaryTemplate")]
        public virtual TMasterCurrency IdcurrencyNavigation { get; set; }
        [InverseProperty("IdtmpSalaryNavigation")]
        public virtual ICollection<TMasterAllowanceJamsostek> TMasterAllowanceJamsostek { get; set; }
        [InverseProperty("IdtmpSalaryNavigation")]
        public virtual ICollection<TMasterSalaryTmpDetail> TMasterSalaryTmpDetail { get; set; }
        [InverseProperty("IdtmpSalaryNavigation")]
        public virtual ICollection<TTrxEmpSalary> TTrxEmpSalary { get; set; }
        [InverseProperty("IdtmpSalaryNavigation")]
        public virtual ICollection<TTrxEmpSalaryChanges> TTrxEmpSalaryChanges { get; set; }
    }
}
