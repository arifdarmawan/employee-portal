﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Salary_Item")]
    public partial class TMasterSalaryItem
    {
        public TMasterSalaryItem()
        {
            TMasterAllowanceTemplate = new HashSet<TMasterAllowanceTemplate>();
            TMasterLoanType = new HashSet<TMasterLoanType>();
            TMasterPph21Component = new HashSet<TMasterPph21Component>();
            TMasterSalaryItemEmployee = new HashSet<TMasterSalaryItemEmployee>();
            TMasterSalaryItemLocation = new HashSet<TMasterSalaryItemLocation>();
            TMasterSalaryTmpDetail = new HashSet<TMasterSalaryTmpDetail>();
            TMasterThrTmpDetail = new HashSet<TMasterThrTmpDetail>();
            TTrxEmpSalChangesDetail = new HashSet<TTrxEmpSalChangesDetail>();
            TTrxEmpSalaryDetail = new HashSet<TTrxEmpSalaryDetail>();
            TTrxEmpSalaryMonthlyDetail = new HashSet<TTrxEmpSalaryMonthlyDetail>();
        }

        [Key]
        [Column("IDSalaryItem")]
        public int IdsalaryItem { get; set; }
        [Column("IDSalaryItemGroup")]
        public int? IdsalaryItemGroup { get; set; }
        [Column("IDSalaryType")]
        public int? IdsalaryType { get; set; }
        [StringLength(50)]
        public string SalaryItemCode { get; set; }
        [StringLength(150)]
        public string SalaryItemName { get; set; }
        public bool IsIncome { get; set; }
        public bool IsDeduction { get; set; }
        public bool IsFixedIncome { get; set; }
        public bool IsFixedDeduction { get; set; }
        [Column("IsPPh")]
        public bool IsPph { get; set; }
        public bool IsEditable { get; set; }
        public bool IsUploadable { get; set; }
        public bool IsDetail { get; set; }
        public bool IsPaymentSlip { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        public bool IsSalaryChanges { get; set; }

        [ForeignKey("IdsalaryItemGroup")]
        [InverseProperty("TMasterSalaryItem")]
        public virtual TMasterSalaryItemGroup IdsalaryItemGroupNavigation { get; set; }
        [ForeignKey("IdsalaryType")]
        [InverseProperty("TMasterSalaryItem")]
        public virtual TMasterSalaryType IdsalaryTypeNavigation { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TMasterAllowanceTemplate> TMasterAllowanceTemplate { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TMasterLoanType> TMasterLoanType { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TMasterPph21Component> TMasterPph21Component { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TMasterSalaryItemEmployee> TMasterSalaryItemEmployee { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TMasterSalaryTmpDetail> TMasterSalaryTmpDetail { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TMasterThrTmpDetail> TMasterThrTmpDetail { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TTrxEmpSalChangesDetail> TTrxEmpSalChangesDetail { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TTrxEmpSalaryDetail> TTrxEmpSalaryDetail { get; set; }
        [InverseProperty("IdsalaryItemNavigation")]
        public virtual ICollection<TTrxEmpSalaryMonthlyDetail> TTrxEmpSalaryMonthlyDetail { get; set; }
    }
}
