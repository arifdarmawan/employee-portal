﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Violation")]
    public partial class TMasterViolation
    {
        [Key]
        [Column("IDViolation")]
        public int Idviolation { get; set; }
        [StringLength(10)]
        public string ViolationCode { get; set; }
        [StringLength(150)]
        public string ViolationName { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
