﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_IdCard")]
    public partial class TTrxEmpIdCard
    {
        [Key]
        [Column("IDEmpCard")]
        public long IdempCard { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDCardType")]
        public int? IdcardType { get; set; }
        [Required]
        [StringLength(150)]
        public string CardNumber { get; set; }
        [StringLength(200)]
        public string Publisher { get; set; }
        [Column(TypeName = "date")]
        public DateTime ExpiredDate { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdcardType")]
        [InverseProperty("TTrxEmpIdCard")]
        public virtual TMasterCardType IdcardTypeNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpIdCard")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
    }
}
