﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Salary_Monthly_Detail")]
    public partial class TTrxEmpSalaryMonthlyDetail
    {
        [Key]
        [Column("IDEmpSalMDetail")]
        public long IdempSalMdetail { get; set; }
        [Column("IDEmpSalaryMonthly")]
        public long? IdempSalaryMonthly { get; set; }
        [Column("IDSalaryItem")]
        public int? IdsalaryItem { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Amount { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        public bool? IsUploaded { get; set; }

        [ForeignKey("IdempSalaryMonthly")]
        [InverseProperty("TTrxEmpSalaryMonthlyDetail")]
        public virtual TTrxEmpSalaryMonthly IdempSalaryMonthlyNavigation { get; set; }
        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TTrxEmpSalaryMonthlyDetail")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
    }
}
