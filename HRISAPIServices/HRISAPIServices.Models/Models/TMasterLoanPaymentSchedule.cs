﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Loan_Payment_Schedule")]
    public partial class TMasterLoanPaymentSchedule
    {
        public TMasterLoanPaymentSchedule()
        {
            TTrxLoanScheduled = new HashSet<TTrxLoanScheduled>();
        }

        [Key]
        [Column("IDPaySchedule")]
        public int IdpaySchedule { get; set; }
        [StringLength(10)]
        public string PayScheduleCode { get; set; }
        [StringLength(150)]
        public string PayScheduleName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdpayScheduleNavigation")]
        public virtual ICollection<TTrxLoanScheduled> TTrxLoanScheduled { get; set; }
    }
}
