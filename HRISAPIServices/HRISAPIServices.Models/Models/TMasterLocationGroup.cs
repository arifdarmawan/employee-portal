﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Location_Group")]
    public partial class TMasterLocationGroup
    {
        public TMasterLocationGroup()
        {
            TTrxHistoryLocationGroup = new HashSet<TTrxHistoryLocationGroup>();
        }

        [Key]
        [Column("IDLocationGroup")]
        public int IdlocationGroup { get; set; }
        [StringLength(50)]
        public string LocationGroupCode { get; set; }
        [StringLength(100)]
        public string LocationGroupName { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdlocationGroupNavigation")]
        public virtual ICollection<TTrxHistoryLocationGroup> TTrxHistoryLocationGroup { get; set; }
    }
}
