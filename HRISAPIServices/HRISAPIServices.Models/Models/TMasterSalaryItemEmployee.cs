﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Salary_Item_Employee")]
    public partial class TMasterSalaryItemEmployee
    {
        [Key]
        [Column("IDSalaryItemEmp")]
        public long IdsalaryItemEmp { get; set; }
        [Column("IDEmployee")]
        public long Idemployee { get; set; }
        [Column("IDSalaryItem")]
        public int IdsalaryItem { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Amount { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? TerminationDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TMasterSalaryItemEmployee")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TMasterSalaryItemEmployee")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
    }
}
