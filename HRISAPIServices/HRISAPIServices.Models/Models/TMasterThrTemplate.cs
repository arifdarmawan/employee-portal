﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_THR_Template")]
    public partial class TMasterThrTemplate
    {
        public TMasterThrTemplate()
        {
            TMasterThrTmpDetail = new HashSet<TMasterThrTmpDetail>();
        }

        [Key]
        [Column("IDTHRTemplate")]
        public long Idthrtemplate { get; set; }
        [StringLength(150)]
        public string TemplateName { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [Column("THRDateFrom")]
        [StringLength(50)]
        public string ThrdateFrom { get; set; }
        public bool IsEmploymentType { get; set; }
        public bool IsReligion { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdthrtemplateNavigation")]
        public virtual ICollection<TMasterThrTmpDetail> TMasterThrTmpDetail { get; set; }
    }
}
