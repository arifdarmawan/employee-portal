﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Currency")]
    public partial class TMasterCurrency
    {
        public TMasterCurrency()
        {
            TMasterSalaryTemplate = new HashSet<TMasterSalaryTemplate>();
            TTrxCurrencyRate = new HashSet<TTrxCurrencyRate>();
            TTrxEmpBankAccount = new HashSet<TTrxEmpBankAccount>();
            TTrxEmpBankAccountChanges = new HashSet<TTrxEmpBankAccountChanges>();
            TTrxEmpJamsostek = new HashSet<TTrxEmpJamsostek>();
            TTrxEmpJamsostekChanges = new HashSet<TTrxEmpJamsostekChanges>();
            TTrxEssEmpBankAccount = new HashSet<TTrxEssEmpBankAccount>();
            TTrxLoanApplication = new HashSet<TTrxLoanApplication>();
        }

        [Key]
        [Column("IDCurrency")]
        public int Idcurrency { get; set; }
        [StringLength(10)]
        public string CurrencyCode { get; set; }
        [StringLength(50)]
        public string CurrencyName { get; set; }
        public short? Decimal { get; set; }
        public bool IsHomeCurrency { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdcurrencyNavigation")]
        public virtual ICollection<TMasterSalaryTemplate> TMasterSalaryTemplate { get; set; }
        [InverseProperty("IdcurrencyNavigation")]
        public virtual ICollection<TTrxCurrencyRate> TTrxCurrencyRate { get; set; }
        [InverseProperty("IdcurrencyNavigation")]
        public virtual ICollection<TTrxEmpBankAccount> TTrxEmpBankAccount { get; set; }
        [InverseProperty("IdcurrencyNavigation")]
        public virtual ICollection<TTrxEmpBankAccountChanges> TTrxEmpBankAccountChanges { get; set; }
        [InverseProperty("IdcurrencyNavigation")]
        public virtual ICollection<TTrxEmpJamsostek> TTrxEmpJamsostek { get; set; }
        [InverseProperty("IdcurrencyNavigation")]
        public virtual ICollection<TTrxEmpJamsostekChanges> TTrxEmpJamsostekChanges { get; set; }
        [InverseProperty("IdcurrencyNavigation")]
        public virtual ICollection<TTrxEssEmpBankAccount> TTrxEssEmpBankAccount { get; set; }
        [InverseProperty("IdcurrencyNavigation")]
        public virtual ICollection<TTrxLoanApplication> TTrxLoanApplication { get; set; }
    }
}
