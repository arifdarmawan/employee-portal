﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Medical_Claim")]
    public partial class TTrxMedicalClaim
    {
        public TTrxMedicalClaim()
        {
            TTrxMedicalClaimExpense = new HashSet<TTrxMedicalClaimExpense>();
        }

        [Key]
        [Column("IDMedicalClaim")]
        public long IdmedicalClaim { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDMedicalType")]
        public int? IdmedicalType { get; set; }
        [StringLength(50)]
        public string MedicalNo { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? CurrentLimit { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? ClaimAmount { get; set; }
        [Column(TypeName = "date")]
        public DateTime ClaimDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime LetterDate { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        public bool IsFamClaimed { get; set; }
        public bool IsPay { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PayrollDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PaymentDate { get; set; }
        public bool IsApproved { get; set; }
        [StringLength(50)]
        public string ApprovedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ApprovedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxMedicalClaim")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdmedicalType")]
        [InverseProperty("TTrxMedicalClaim")]
        public virtual TMasterMedicalType IdmedicalTypeNavigation { get; set; }
        [InverseProperty("IdmedicalClaimNavigation")]
        public virtual ICollection<TTrxMedicalClaimExpense> TTrxMedicalClaimExpense { get; set; }
    }
}
