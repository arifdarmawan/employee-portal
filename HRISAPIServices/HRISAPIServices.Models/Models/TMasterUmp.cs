﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_UMP")]
    public partial class TMasterUmp
    {
        public TMasterUmp()
        {
            TMasterJamsostekTmpDetail = new HashSet<TMasterJamsostekTmpDetail>();
            TMasterUmpDetail = new HashSet<TMasterUmpDetail>();
        }

        [Key]
        [Column("IDUMP")]
        public int Idump { get; set; }
        [Column("UMPCode")]
        [StringLength(5)]
        public string Umpcode { get; set; }
        [Column("UMPName")]
        [StringLength(150)]
        public string Umpname { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdumpNavigation")]
        public virtual ICollection<TMasterJamsostekTmpDetail> TMasterJamsostekTmpDetail { get; set; }
        [InverseProperty("IdumpNavigation")]
        public virtual ICollection<TMasterUmpDetail> TMasterUmpDetail { get; set; }
    }
}
