﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Lookup")]
    public partial class TMasterLookup
    {
        public TMasterLookup()
        {
            TMasterJamsostekTemplate = new HashSet<TMasterJamsostekTemplate>();
        }

        [Key]
        [Column("IDLookup")]
        public int Idlookup { get; set; }
        [StringLength(10)]
        public string LookupCode { get; set; }
        [StringLength(150)]
        public string LookupName { get; set; }
        [StringLength(50)]
        public string LookupType { get; set; }
        [StringLength(200)]
        public string LookupDesc { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("TemplateByNavigation")]
        public virtual ICollection<TMasterJamsostekTemplate> TMasterJamsostekTemplate { get; set; }
    }
}
