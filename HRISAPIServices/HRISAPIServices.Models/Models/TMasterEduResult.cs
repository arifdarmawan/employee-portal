﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_EduResult")]
    public partial class TMasterEduResult
    {
        public TMasterEduResult()
        {
            TTrxEmpEducation = new HashSet<TTrxEmpEducation>();
            TTrxEssEmpEducation = new HashSet<TTrxEssEmpEducation>();
        }

        [Key]
        [Column("IDEduResult")]
        public int IdeduResult { get; set; }
        [StringLength(10)]
        public string EduResultCode { get; set; }
        [StringLength(50)]
        public string EduResultName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdeduResultNavigation")]
        public virtual ICollection<TTrxEmpEducation> TTrxEmpEducation { get; set; }
        [InverseProperty("IdeduResultNavigation")]
        public virtual ICollection<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
    }
}
