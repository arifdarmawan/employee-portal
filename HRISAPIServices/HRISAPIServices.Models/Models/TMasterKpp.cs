﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_KPP")]
    public partial class TMasterKpp
    {
        public TMasterKpp()
        {
            TMasterKppDetail = new HashSet<TMasterKppDetail>();
            TTrxEmpPph = new HashSet<TTrxEmpPph>();
            TTrxEmpPphChanges = new HashSet<TTrxEmpPphChanges>();
        }

        [Key]
        [Column("IDKPP")]
        public int Idkpp { get; set; }
        [Column("KPPCode")]
        [StringLength(20)]
        public string Kppcode { get; set; }
        [Column("KPPName")]
        [StringLength(100)]
        public string Kppname { get; set; }
        [Column("NPWP")]
        [StringLength(50)]
        public string Npwp { get; set; }
        [Column(TypeName = "date")]
        public DateTime? RegDate { get; set; }
        [StringLength(10)]
        public string EmployeeCode { get; set; }
        [StringLength(250)]
        public string ResponsiblePerson { get; set; }
        [StringLength(500)]
        public string Address { get; set; }
        [StringLength(15)]
        public string Phone { get; set; }
        [StringLength(15)]
        public string Fax { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdkppNavigation")]
        public virtual ICollection<TMasterKppDetail> TMasterKppDetail { get; set; }
        [InverseProperty("IdkppNavigation")]
        public virtual ICollection<TTrxEmpPph> TTrxEmpPph { get; set; }
        [InverseProperty("IdkppNavigation")]
        public virtual ICollection<TTrxEmpPphChanges> TTrxEmpPphChanges { get; set; }
    }
}
