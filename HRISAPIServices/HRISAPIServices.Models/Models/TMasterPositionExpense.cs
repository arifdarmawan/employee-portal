﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Position_Expense")]
    public partial class TMasterPositionExpense
    {
        [Key]
        [Column("IDPositionExpense")]
        public int IdpositionExpense { get; set; }
        [StringLength(4)]
        public string Nyear { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal? Percentage { get; set; }
        [Column("MaxYExpense", TypeName = "numeric(18, 2)")]
        public decimal? MaxYexpense { get; set; }
        [Column("MaxMExpense", TypeName = "numeric(18, 2)")]
        public decimal? MaxMexpense { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        public bool IsUsed { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
