﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_BPJSKes_Provider_Detail")]
    public partial class TMasterBpjskesProviderDetail
    {
        [Key]
        [Column("IDBPJSKesProviderDetail")]
        public int IdbpjskesProviderDetail { get; set; }
        [Column("IDBPJSKesProvider")]
        public int IdbpjskesProvider { get; set; }
        [Column("IDHistoryCompLoc")]
        public long IdhistoryCompLoc { get; set; }
        [Column("IDJobLevel")]
        public int IdjobLevel { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdbpjskesProvider")]
        [InverseProperty("TMasterBpjskesProviderDetail")]
        public virtual TMasterBpjskesProvider IdbpjskesProviderNavigation { get; set; }
        [ForeignKey("IdhistoryCompLoc")]
        [InverseProperty("TMasterBpjskesProviderDetail")]
        public virtual TTrxHistoryCompanyLocation IdhistoryCompLocNavigation { get; set; }
        [ForeignKey("IdjobLevel")]
        [InverseProperty("TMasterBpjskesProviderDetail")]
        public virtual TMasterJoblevel IdjobLevelNavigation { get; set; }
    }
}
