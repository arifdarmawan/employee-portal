﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Termination_Notice")]
    public partial class TMasterTerminationNotice
    {
        [Key]
        [Column("IDTermNotice")]
        public int IdtermNotice { get; set; }
        [Column("IDJoblevel")]
        public int? Idjoblevel { get; set; }
        [Column("IDEmploymentType")]
        public int? IdemploymentType { get; set; }
        public int? Days { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdemploymentType")]
        [InverseProperty("TMasterTerminationNotice")]
        public virtual TMasterEmploymentType IdemploymentTypeNavigation { get; set; }
        [ForeignKey("Idjoblevel")]
        [InverseProperty("TMasterTerminationNotice")]
        public virtual TMasterJoblevel IdjoblevelNavigation { get; set; }
    }
}
