﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Attachment")]
    public partial class TTrxAttachment
    {
        public TTrxAttachment()
        {
            TTrxAttendance = new HashSet<TTrxAttendance>();
            TTrxEssEmpEducation = new HashSet<TTrxEssEmpEducation>();
            TTrxEssEmpFamily = new HashSet<TTrxEssEmpFamily>();
            TTrxEssEmpIdCard = new HashSet<TTrxEssEmpIdCard>();
        }

        [Key]
        [Column("IDAttachment")]
        public Guid Idattachment { get; set; }
        [StringLength(500)]
        public string FileName { get; set; }
        [Column("FileURL")]
        [StringLength(500)]
        public string FileUrl { get; set; }
        [StringLength(50)]
        public string FileType { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [StringLength(150)]
        public string Sources { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdattachmentNavigation")]
        public virtual ICollection<TTrxAttendance> TTrxAttendance { get; set; }
        [InverseProperty("IdattachmentNavigation")]
        public virtual ICollection<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
        [InverseProperty("IdattachmentNavigation")]
        public virtual ICollection<TTrxEssEmpFamily> TTrxEssEmpFamily { get; set; }
        [InverseProperty("IdattachmentNavigation")]
        public virtual ICollection<TTrxEssEmpIdCard> TTrxEssEmpIdCard { get; set; }
    }
}
