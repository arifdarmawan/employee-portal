﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Loan_Application")]
    public partial class TTrxLoanApplication
    {
        public TTrxLoanApplication()
        {
            TTrxLoanScheduled = new HashSet<TTrxLoanScheduled>();
        }

        [Key]
        [Column("IDApplication")]
        public long Idapplication { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDLoanType")]
        public int? IdloanType { get; set; }
        [Column("IDCurrency")]
        public int? Idcurrency { get; set; }
        [StringLength(50)]
        public string ApplicationNo { get; set; }
        [StringLength(10)]
        public string ApplicantId { get; set; }
        [StringLength(150)]
        public string ApplicantName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ApplicationDate { get; set; }
        [Required]
        public bool? IsScheduled { get; set; }
        public bool IsInterest { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal? InterestRate { get; set; }
        [StringLength(1)]
        public string InterestPeriod { get; set; }
        public bool IsSubmit { get; set; }
        [StringLength(50)]
        public string SubmittedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SubmittedDate { get; set; }
        public bool IsApproved { get; set; }
        [StringLength(150)]
        public string ApprovedNo { get; set; }
        [StringLength(50)]
        public string ApprovedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ApprovedDate { get; set; }
        public bool IsRejected { get; set; }
        [StringLength(150)]
        public string RejectedNo { get; set; }
        [StringLength(50)]
        public string RejectedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RejectedDate { get; set; }
        public bool IsDisbursed { get; set; }
        [StringLength(150)]
        public string DisbursedNo { get; set; }
        [StringLength(50)]
        public string DisbursedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DisbursedDate { get; set; }
        public bool IsEliminated { get; set; }
        [StringLength(150)]
        public string EliminatedNo { get; set; }
        [StringLength(50)]
        public string EliminatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EliminatedDate { get; set; }
        [StringLength(10)]
        public string EliminatedReason { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? EliminatedAmount { get; set; }
        [StringLength(250)]
        public string EliminatedRemark { get; set; }
        public bool IsCompleted { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("IDPlaces")]
        public int? Idplaces { get; set; }
        [Column("WitnessNIK")]
        [StringLength(10)]
        public string WitnessNik { get; set; }
        [StringLength(250)]
        public string WitnessName { get; set; }
        [Column("IsSPH")]
        public bool IsSph { get; set; }

        [ForeignKey("Idcurrency")]
        [InverseProperty("TTrxLoanApplication")]
        public virtual TMasterCurrency IdcurrencyNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxLoanApplication")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdloanType")]
        [InverseProperty("TTrxLoanApplication")]
        public virtual TMasterLoanType IdloanTypeNavigation { get; set; }
        [ForeignKey("Idplaces")]
        [InverseProperty("TTrxLoanApplication")]
        public virtual TMasterCity IdplacesNavigation { get; set; }
        [InverseProperty("IdapplicationNavigation")]
        public virtual ICollection<TTrxLoanScheduled> TTrxLoanScheduled { get; set; }
    }
}
