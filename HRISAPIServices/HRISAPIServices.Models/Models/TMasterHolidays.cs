﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Holidays")]
    public partial class TMasterHolidays
    {
        [Key]
        [Column("IDHolidays")]
        public long Idholidays { get; set; }
        [Column("IDHistoryCompLoc")]
        public long IdhistoryCompLoc { get; set; }
        [StringLength(500)]
        public string HolidayName { get; set; }
        [Column("NYear")]
        [StringLength(4)]
        public string Nyear { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        public bool IsMassLeave { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdhistoryCompLoc")]
        [InverseProperty("TMasterHolidays")]
        public virtual TTrxHistoryCompanyLocation IdhistoryCompLocNavigation { get; set; }
    }
}
