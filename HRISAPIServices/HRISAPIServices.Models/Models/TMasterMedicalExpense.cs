﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Medical_Expense")]
    public partial class TMasterMedicalExpense
    {
        public TMasterMedicalExpense()
        {
            TTrxMedicalClaimExpense = new HashSet<TTrxMedicalClaimExpense>();
        }

        [Key]
        [Column("IDMedicalExpense")]
        public int IdmedicalExpense { get; set; }
        [Column("IDMedicalType")]
        public int? IdmedicalType { get; set; }
        [StringLength(10)]
        public string ExpenseCode { get; set; }
        [StringLength(150)]
        public string ExpenseName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdmedicalType")]
        [InverseProperty("TMasterMedicalExpense")]
        public virtual TMasterMedicalType IdmedicalTypeNavigation { get; set; }
        [InverseProperty("IdmedicalExpenseNavigation")]
        public virtual ICollection<TTrxMedicalClaimExpense> TTrxMedicalClaimExpense { get; set; }
    }
}
