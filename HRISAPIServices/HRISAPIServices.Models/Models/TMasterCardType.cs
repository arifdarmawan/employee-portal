﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Card_Type")]
    public partial class TMasterCardType
    {
        public TMasterCardType()
        {
            TTrxEmpIdCard = new HashSet<TTrxEmpIdCard>();
            TTrxEssEmpIdCard = new HashSet<TTrxEssEmpIdCard>();
        }

        [Key]
        [Column("IDCardType")]
        public int IdcardType { get; set; }
        [StringLength(10)]
        public string CardTypeCode { get; set; }
        [StringLength(50)]
        public string CardTypeName { get; set; }
        public bool IsCandidateCheck { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdcardTypeNavigation")]
        public virtual ICollection<TTrxEmpIdCard> TTrxEmpIdCard { get; set; }
        [InverseProperty("IdcardTypeNavigation")]
        public virtual ICollection<TTrxEssEmpIdCard> TTrxEssEmpIdCard { get; set; }
    }
}
