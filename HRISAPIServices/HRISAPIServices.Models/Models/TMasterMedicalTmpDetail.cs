﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Medical_TmpDetail")]
    public partial class TMasterMedicalTmpDetail
    {
        [Key]
        [Column("IDTmpMedDetail")]
        public int IdtmpMedDetail { get; set; }
        [Column("IDMedicalType")]
        public int? IdmedicalType { get; set; }
        [Column("IDTmpMedical")]
        public int? IdtmpMedical { get; set; }
        [Column("IDMaritalStatus")]
        public int? IdmaritalStatus { get; set; }
        [StringLength(1)]
        public string Gender { get; set; }
        public short? DueMonth { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? AnnualAmount { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdmaritalStatus")]
        [InverseProperty("TMasterMedicalTmpDetail")]
        public virtual TMasterMaritalStatus IdmaritalStatusNavigation { get; set; }
        [ForeignKey("IdmedicalType")]
        [InverseProperty("TMasterMedicalTmpDetail")]
        public virtual TMasterMedicalType IdmedicalTypeNavigation { get; set; }
        [ForeignKey("IdtmpMedical")]
        [InverseProperty("TMasterMedicalTmpDetail")]
        public virtual TMasterMedicalTemplate IdtmpMedicalNavigation { get; set; }
    }
}
