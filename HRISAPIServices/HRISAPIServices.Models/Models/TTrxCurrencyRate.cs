﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Currency_Rate")]
    public partial class TTrxCurrencyRate
    {
        [Column("IDCurrencyRate")]
        public int IdcurrencyRate { get; set; }
        [Column("IDCurrency")]
        public int Idcurrency { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Rate { get; set; }
        public bool IsActive { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EffectiveDate { get; set; }
        [StringLength(50)]
        public string UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }

        [ForeignKey("Idcurrency")]
        [InverseProperty("TTrxCurrencyRate")]
        public virtual TMasterCurrency IdcurrencyNavigation { get; set; }
    }
}
