﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Medical_Template")]
    public partial class TMasterMedicalTemplate
    {
        public TMasterMedicalTemplate()
        {
            TMasterMedicalTmpDetail = new HashSet<TMasterMedicalTmpDetail>();
        }

        [Key]
        [Column("IDTmpMedical")]
        public int IdtmpMedical { get; set; }
        [Column("IDJoblevel")]
        public int? Idjoblevel { get; set; }
        [StringLength(10)]
        public string TemplateCode { get; set; }
        [StringLength(50)]
        public string TemplateName { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string Initiator { get; set; }
        public bool IsDefault { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idjoblevel")]
        [InverseProperty("TMasterMedicalTemplate")]
        public virtual TMasterJoblevel IdjoblevelNavigation { get; set; }
        [InverseProperty("IdtmpMedicalNavigation")]
        public virtual ICollection<TMasterMedicalTmpDetail> TMasterMedicalTmpDetail { get; set; }
    }
}
