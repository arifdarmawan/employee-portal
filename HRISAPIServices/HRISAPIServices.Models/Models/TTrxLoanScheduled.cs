﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Loan_Scheduled")]
    public partial class TTrxLoanScheduled
    {
        public TTrxLoanScheduled()
        {
            TTrxLoanInstallment = new HashSet<TTrxLoanInstallment>();
        }

        [Column("IDLoanScheduled")]
        public long IdloanScheduled { get; set; }
        [Column("IDApplication")]
        public long? Idapplication { get; set; }
        [StringLength(50)]
        public string ScheduleNo { get; set; }
        [Column("IDPayMethod")]
        public int? IdpayMethod { get; set; }
        [Column("IDPaySchedule")]
        public int? IdpaySchedule { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? LoanAmount { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? PrincipalAmount { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartPeriod { get; set; }
        public short? PaymentPeriod { get; set; }
        public bool IsRescheduled { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idapplication")]
        [InverseProperty("TTrxLoanScheduled")]
        public virtual TTrxLoanApplication IdapplicationNavigation { get; set; }
        [ForeignKey("IdpayMethod")]
        [InverseProperty("TTrxLoanScheduled")]
        public virtual TMasterLoanPaymentMethod IdpayMethodNavigation { get; set; }
        [ForeignKey("IdpaySchedule")]
        [InverseProperty("TTrxLoanScheduled")]
        public virtual TMasterLoanPaymentSchedule IdpayScheduleNavigation { get; set; }
        [InverseProperty("IdloanScheduledNavigation")]
        public virtual ICollection<TTrxLoanInstallment> TTrxLoanInstallment { get; set; }
    }
}
