﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Bank_Account")]
    public partial class TTrxEmpBankAccount
    {
        [Key]
        [Column("IDEmpBankAccount")]
        public long IdempBankAccount { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDCurrency")]
        public int? Idcurrency { get; set; }
        [Column("IDBankGroup")]
        public int? IdbankGroup { get; set; }
        [Column("IDBankAccount")]
        public int? IdbankAccount { get; set; }
        [StringLength(150)]
        public string AccountNo { get; set; }
        [StringLength(150)]
        public string AccountName { get; set; }
        [StringLength(150)]
        public string Owner { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? TransferFee { get; set; }
        public bool IsDefaultAccount { get; set; }
        [Column("IsTHR")]
        public bool IsThr { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdbankAccount")]
        [InverseProperty("TTrxEmpBankAccount")]
        public virtual TMasterBankAccount IdbankAccountNavigation { get; set; }
        [ForeignKey("IdbankGroup")]
        [InverseProperty("TTrxEmpBankAccount")]
        public virtual TMasterBankGroup IdbankGroupNavigation { get; set; }
        [ForeignKey("Idcurrency")]
        [InverseProperty("TTrxEmpBankAccount")]
        public virtual TMasterCurrency IdcurrencyNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpBankAccount")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
    }
}
