﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Jamsostek_Provider")]
    public partial class TMasterJamsostekProvider
    {
        public TMasterJamsostekProvider()
        {
            TMasterJamsostekProviderDetail = new HashSet<TMasterJamsostekProviderDetail>();
            TTrxEmpJamsostek = new HashSet<TTrxEmpJamsostek>();
            TTrxEmpJamsostekChanges = new HashSet<TTrxEmpJamsostekChanges>();
        }

        [Key]
        [Column("IDJamsostekProvider")]
        public int IdjamsostekProvider { get; set; }
        [Required]
        [StringLength(50)]
        public string OfficeCode { get; set; }
        [Required]
        [StringLength(150)]
        public string OfficeName { get; set; }
        [StringLength(250)]
        public string Address { get; set; }
        [StringLength(50)]
        public string RegisterNo { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(150)]
        public string Signature { get; set; }
        [StringLength(250)]
        public string Jobtitle { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdjamsostekProviderNavigation")]
        public virtual ICollection<TMasterJamsostekProviderDetail> TMasterJamsostekProviderDetail { get; set; }
        [InverseProperty("IdjamsostekProviderNavigation")]
        public virtual ICollection<TTrxEmpJamsostek> TTrxEmpJamsostek { get; set; }
        [InverseProperty("IdjamsostekProviderNavigation")]
        public virtual ICollection<TTrxEmpJamsostekChanges> TTrxEmpJamsostekChanges { get; set; }
    }
}
