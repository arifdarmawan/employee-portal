﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_PCN_Type")]
    public partial class TMasterPcnType
    {
        public TMasterPcnType()
        {
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
        }

        [Key]
        [Column("IDPCNType")]
        public int Idpcntype { get; set; }
        [Column("PCNTypeCode")]
        [StringLength(5)]
        public string PcntypeCode { get; set; }
        [Column("PCNType")]
        [StringLength(30)]
        public string Pcntype { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdpcntypeNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdpcntypeNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
    }
}
