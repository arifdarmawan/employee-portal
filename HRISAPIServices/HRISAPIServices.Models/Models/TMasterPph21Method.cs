﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_PPh21_Method")]
    public partial class TMasterPph21Method
    {
        public TMasterPph21Method()
        {
            TMasterPph21Component = new HashSet<TMasterPph21Component>();
            TTrxEmpPph = new HashSet<TTrxEmpPph>();
            TTrxEmpPphChanges = new HashSet<TTrxEmpPphChanges>();
        }

        [Key]
        [Column("IDPPhMethod")]
        public int IdpphMethod { get; set; }
        [Column("PPhMethodCode")]
        [StringLength(10)]
        public string PphMethodCode { get; set; }
        [Column("PPhMethodName")]
        [StringLength(20)]
        public string PphMethodName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdpphMethodNavigation")]
        public virtual ICollection<TMasterPph21Component> TMasterPph21Component { get; set; }
        [InverseProperty("IdpphMethodNavigation")]
        public virtual ICollection<TTrxEmpPph> TTrxEmpPph { get; set; }
        [InverseProperty("IdpphMethodNavigation")]
        public virtual ICollection<TTrxEmpPphChanges> TTrxEmpPphChanges { get; set; }
    }
}
