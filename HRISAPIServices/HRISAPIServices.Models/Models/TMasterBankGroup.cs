﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Bank_Group")]
    public partial class TMasterBankGroup
    {
        public TMasterBankGroup()
        {
            TMasterBankAccount = new HashSet<TMasterBankAccount>();
            TTrxEmpBankAccount = new HashSet<TTrxEmpBankAccount>();
            TTrxEmpBankAccountChanges = new HashSet<TTrxEmpBankAccountChanges>();
            TTrxEssEmpBankAccount = new HashSet<TTrxEssEmpBankAccount>();
        }

        [Column("IDBankGroup")]
        public int IdbankGroup { get; set; }
        [StringLength(150)]
        public string BankGroupCode { get; set; }
        [StringLength(150)]
        public string BankGroupName { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? TransferFee { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdbankGroupNavigation")]
        public virtual ICollection<TMasterBankAccount> TMasterBankAccount { get; set; }
        [InverseProperty("IdbankGroupNavigation")]
        public virtual ICollection<TTrxEmpBankAccount> TTrxEmpBankAccount { get; set; }
        [InverseProperty("IdbankGroupNavigation")]
        public virtual ICollection<TTrxEmpBankAccountChanges> TTrxEmpBankAccountChanges { get; set; }
        [InverseProperty("IdbankGroupNavigation")]
        public virtual ICollection<TTrxEssEmpBankAccount> TTrxEssEmpBankAccount { get; set; }
    }
}
