﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_EduMajor")]
    public partial class TMasterEduMajor
    {
        public TMasterEduMajor()
        {
            TTrxEmpEducation = new HashSet<TTrxEmpEducation>();
            TTrxEssEmpEducation = new HashSet<TTrxEssEmpEducation>();
        }

        [Key]
        [Column("IDEduMajor")]
        public int IdeduMajor { get; set; }
        [StringLength(10)]
        public string EduMajorCode { get; set; }
        [StringLength(50)]
        public string EduMajorName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdeduMajorNavigation")]
        public virtual ICollection<TTrxEmpEducation> TTrxEmpEducation { get; set; }
        [InverseProperty("IdeduMajorNavigation")]
        public virtual ICollection<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
    }
}
