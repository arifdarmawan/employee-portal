﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Medical_Institution")]
    public partial class TMasterMedicalInstitution
    {
        public TMasterMedicalInstitution()
        {
            TTrxMedicalClaimExpense = new HashSet<TTrxMedicalClaimExpense>();
        }

        [Key]
        [Column("IDInstitution")]
        public int Idinstitution { get; set; }
        [Column("IDInstitutionType")]
        public int? IdinstitutionType { get; set; }
        [Column("IDCity")]
        public int? Idcity { get; set; }
        [StringLength(10)]
        public string InstitutionCode { get; set; }
        [StringLength(150)]
        public string InstitutionName { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal? ClaimDisc { get; set; }
        [StringLength(250)]
        public string Address { get; set; }
        [StringLength(10)]
        public string ZipCode { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcity")]
        [InverseProperty("TMasterMedicalInstitution")]
        public virtual TMasterCity IdcityNavigation { get; set; }
        [ForeignKey("IdinstitutionType")]
        [InverseProperty("TMasterMedicalInstitution")]
        public virtual TMasterMedicalInstitutionType IdinstitutionTypeNavigation { get; set; }
        [InverseProperty("IdinstitutionNavigation")]
        public virtual ICollection<TTrxMedicalClaimExpense> TTrxMedicalClaimExpense { get; set; }
    }
}
