﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Security_Policy")]
    public partial class TMasterSecurityPolicy
    {
        [Column("IDPolicy")]
        public int Idpolicy { get; set; }
        [StringLength(50)]
        public string Key { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        public string Value { get; set; }
        public string ValueType { get; set; }
        [Column("ValueUOM")]
        [StringLength(20)]
        public string ValueUom { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [StringLength(100)]
        public string PolicyGroup { get; set; }
        [StringLength(200)]
        public string GroupTitle { get; set; }
        public bool IsTechnical { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
