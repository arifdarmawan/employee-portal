﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_PPh21_Formula")]
    public partial class TMasterPph21Formula
    {
        [Key]
        [Column("IDPPhFormula")]
        public int IdpphFormula { get; set; }
        [Column("IDItem")]
        public int? Iditem { get; set; }
        public string Formula { get; set; }
        public string Description { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Iditem")]
        [InverseProperty("TMasterPph21Formula")]
        public virtual TMaster1721Item IditemNavigation { get; set; }
    }
}
