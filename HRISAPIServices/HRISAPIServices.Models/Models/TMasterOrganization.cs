﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Organization")]
    public partial class TMasterOrganization
    {
        public TMasterOrganization()
        {
            TMasterJobtitle = new HashSet<TMasterJobtitle>();
            TMasterSalaryItemLocation = new HashSet<TMasterSalaryItemLocation>();
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
        }

        [Key]
        [Column("IDOrganization")]
        public int Idorganization { get; set; }
        [Column("IDOrgLevel")]
        public int IdorgLevel { get; set; }
        [Column("IDOrgGroup")]
        public int IdorgGroup { get; set; }
        [StringLength(50)]
        public string OrgCode { get; set; }
        [StringLength(150)]
        public string OrgName { get; set; }
        [StringLength(50)]
        public string ParentOrgCode { get; set; }
        [StringLength(150)]
        public string ParentOrgName { get; set; }
        [StringLength(5)]
        public string ParentLevelCode { get; set; }
        [StringLength(150)]
        public string ParentLevelName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdorgGroup")]
        [InverseProperty("TMasterOrganization")]
        public virtual TMasterOrgGroup IdorgGroupNavigation { get; set; }
        [ForeignKey("IdorgLevel")]
        [InverseProperty("TMasterOrganization")]
        public virtual TMasterOrgLevel IdorgLevelNavigation { get; set; }
        [InverseProperty("IdorganizationNavigation")]
        public virtual ICollection<TMasterJobtitle> TMasterJobtitle { get; set; }
        [InverseProperty("IdorganizationNavigation")]
        public virtual ICollection<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        [InverseProperty("IdorganizationNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdorganizationNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
    }
}
