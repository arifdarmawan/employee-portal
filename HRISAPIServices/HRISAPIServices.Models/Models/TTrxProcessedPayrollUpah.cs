﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Processed_Payroll_Upah")]
    public partial class TTrxProcessedPayrollUpah
    {
        [Column("IDProcessUpah")]
        public Guid IdprocessUpah { get; set; }
        [Column("NYear")]
        [StringLength(4)]
        public string Nyear { get; set; }
        [Column("NMonth")]
        [StringLength(2)]
        public string Nmonth { get; set; }
        [Column("NIK")]
        [StringLength(10)]
        public string Nik { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
