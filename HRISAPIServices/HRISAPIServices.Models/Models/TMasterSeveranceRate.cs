﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Severance_Rate")]
    public partial class TMasterSeveranceRate
    {
        [Key]
        [Column("IDSeveranceRate")]
        public int IdseveranceRate { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? StartValue { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? EndValue { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal? Percentage { get; set; }
        [StringLength(4)]
        public string Nyear { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
