﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Medical_Type")]
    public partial class TMasterMedicalType
    {
        public TMasterMedicalType()
        {
            TMasterMedicalExpense = new HashSet<TMasterMedicalExpense>();
            TMasterMedicalTmpDetail = new HashSet<TMasterMedicalTmpDetail>();
            TTrxMedicalClaim = new HashSet<TTrxMedicalClaim>();
        }

        [Key]
        [Column("IDMedicalType")]
        public int IdmedicalType { get; set; }
        [Required]
        [StringLength(10)]
        public string MedicalTypeCode { get; set; }
        [Required]
        [StringLength(150)]
        public string MedicalTypeName { get; set; }
        [Required]
        [Column("RunID")]
        [StringLength(10)]
        public string RunId { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdmedicalTypeNavigation")]
        public virtual ICollection<TMasterMedicalExpense> TMasterMedicalExpense { get; set; }
        [InverseProperty("IdmedicalTypeNavigation")]
        public virtual ICollection<TMasterMedicalTmpDetail> TMasterMedicalTmpDetail { get; set; }
        [InverseProperty("IdmedicalTypeNavigation")]
        public virtual ICollection<TTrxMedicalClaim> TTrxMedicalClaim { get; set; }
    }
}
