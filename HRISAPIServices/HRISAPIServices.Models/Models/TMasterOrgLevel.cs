﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Org_Level")]
    public partial class TMasterOrgLevel
    {
        public TMasterOrgLevel()
        {
            TMasterOrganization = new HashSet<TMasterOrganization>();
            TMasterUserDataTrustee = new HashSet<TMasterUserDataTrustee>();
        }

        [Key]
        [Column("IDOrgLevel")]
        public int IdorgLevel { get; set; }
        [StringLength(5)]
        public string OrgLevelCode { get; set; }
        [StringLength(150)]
        public string OrgLevelName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdorgLevelNavigation")]
        public virtual ICollection<TMasterOrganization> TMasterOrganization { get; set; }
        [InverseProperty("IdorgLevelNavigation")]
        public virtual ICollection<TMasterUserDataTrustee> TMasterUserDataTrustee { get; set; }
    }
}
