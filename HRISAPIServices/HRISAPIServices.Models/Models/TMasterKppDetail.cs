﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_KPP_Detail")]
    public partial class TMasterKppDetail
    {
        [Column("IDKPPDetail")]
        public int Idkppdetail { get; set; }
        [Column("IDKPP")]
        public int Idkpp { get; set; }
        [Column("IDHistoryCompLoc")]
        public long IdhistoryCompLoc { get; set; }
        [Column("IDJoblevel")]
        public int? Idjoblevel { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdhistoryCompLoc")]
        [InverseProperty("TMasterKppDetail")]
        public virtual TTrxHistoryCompanyLocation IdhistoryCompLocNavigation { get; set; }
        [ForeignKey("Idjoblevel")]
        [InverseProperty("TMasterKppDetail")]
        public virtual TMasterJoblevel IdjoblevelNavigation { get; set; }
        [ForeignKey("Idkpp")]
        [InverseProperty("TMasterKppDetail")]
        public virtual TMasterKpp IdkppNavigation { get; set; }
    }
}
