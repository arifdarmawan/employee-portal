﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Medical_Institution_Type")]
    public partial class TMasterMedicalInstitutionType
    {
        public TMasterMedicalInstitutionType()
        {
            TMasterMedicalInstitution = new HashSet<TMasterMedicalInstitution>();
        }

        [Key]
        [Column("IDInstitutionType")]
        public int IdinstitutionType { get; set; }
        [StringLength(50)]
        public string InstitutionTypeCode { get; set; }
        [StringLength(150)]
        public string InstitutionTypeName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdinstitutionTypeNavigation")]
        public virtual ICollection<TMasterMedicalInstitution> TMasterMedicalInstitution { get; set; }
    }
}
