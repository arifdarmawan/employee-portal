﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Salary")]
    public partial class TTrxEmpSalary
    {
        public TTrxEmpSalary()
        {
            TTrxEmpSalaryDetail = new HashSet<TTrxEmpSalaryDetail>();
        }

        [Key]
        [Column("IDEmpSalary")]
        public long IdempSalary { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDTmpSalary")]
        public int? IdtmpSalary { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EffectiveDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpSalary")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdtmpSalary")]
        [InverseProperty("TTrxEmpSalary")]
        public virtual TMasterSalaryTemplate IdtmpSalaryNavigation { get; set; }
        [InverseProperty("IdempSalaryNavigation")]
        public virtual ICollection<TTrxEmpSalaryDetail> TTrxEmpSalaryDetail { get; set; }
    }
}
