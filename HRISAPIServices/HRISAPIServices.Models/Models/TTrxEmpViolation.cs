﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Violation")]
    public partial class TTrxEmpViolation
    {
        [Key]
        [Column("IDEmpViolation")]
        public long IdempViolation { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDWarning")]
        public int? Idwarning { get; set; }
        [StringLength(150)]
        public string ViolationNo { get; set; }
        [Column(TypeName = "date")]
        public DateTime? ViolationDate { get; set; }
        public string Description { get; set; }
        [StringLength(500)]
        public string Clause { get; set; }
        [Column("IDPlaces")]
        public int? Idplaces { get; set; }
        [Column("ResponsibilityNIK")]
        [StringLength(10)]
        public string ResponsibilityNik { get; set; }
        [StringLength(250)]
        public string ResponsibilityName { get; set; }
        [Column("IsSPLetter")]
        public bool IsSpletter { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpViolation")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("Idwarning")]
        [InverseProperty("TTrxEmpViolation")]
        public virtual TMasterWarningLevel IdwarningNavigation { get; set; }
    }
}
