﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_UMP_Detail")]
    public partial class TMasterUmpDetail
    {
        [Key]
        [Column("IDUMPDetail")]
        public int Idumpdetail { get; set; }
        [Column("IDUMP")]
        public int? Idump { get; set; }
        [StringLength(500)]
        public string Amount { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idump")]
        [InverseProperty("TMasterUmpDetail")]
        public virtual TMasterUmp IdumpNavigation { get; set; }
    }
}
