﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_User_Data_Trustee")]
    public partial class TMasterUserDataTrustee
    {
        [Key]
        [Column("IDUserDataTrustee")]
        public long IduserDataTrustee { get; set; }
        [Column("IDGroup")]
        public int? Idgroup { get; set; }
        [Column("IDCompany")]
        public int? Idcompany { get; set; }
        [Column("IDLocation")]
        public int? Idlocation { get; set; }
        [Column("IDJobLevel")]
        public int? IdjobLevel { get; set; }
        [Column("IDOrgLevel")]
        public int? IdorgLevel { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcompany")]
        [InverseProperty("TMasterUserDataTrustee")]
        public virtual TMasterCompany IdcompanyNavigation { get; set; }
        [ForeignKey("Idgroup")]
        [InverseProperty("TMasterUserDataTrustee")]
        public virtual TMasterGroup IdgroupNavigation { get; set; }
        [ForeignKey("IdjobLevel")]
        [InverseProperty("TMasterUserDataTrustee")]
        public virtual TMasterJoblevel IdjobLevelNavigation { get; set; }
        [ForeignKey("Idlocation")]
        [InverseProperty("TMasterUserDataTrustee")]
        public virtual TMasterLocation IdlocationNavigation { get; set; }
        [ForeignKey("IdorgLevel")]
        [InverseProperty("TMasterUserDataTrustee")]
        public virtual TMasterOrgLevel IdorgLevelNavigation { get; set; }
    }
}
