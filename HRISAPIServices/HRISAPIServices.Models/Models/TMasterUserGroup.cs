﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_User_Group")]
    public partial class TMasterUserGroup
    {
        [Column("IDUserGroup")]
        public int IduserGroup { get; set; }
        [Column("IDUser")]
        public long Iduser { get; set; }
        [Column("IDGroup")]
        public int Idgroup { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idgroup")]
        [InverseProperty("TMasterUserGroup")]
        public virtual TMasterGroup IdgroupNavigation { get; set; }
        [ForeignKey("Iduser")]
        [InverseProperty("TMasterUserGroup")]
        public virtual TMasterUser IduserNavigation { get; set; }
    }
}
