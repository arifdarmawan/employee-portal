﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Loan_Installment")]
    public partial class TTrxLoanInstallment
    {
        [Key]
        [Column("IDLoanInstallment")]
        public long IdloanInstallment { get; set; }
        [Column("IDLoanScheduled")]
        public long? IdloanScheduled { get; set; }
        public short? SequenceNo { get; set; }
        [Column(TypeName = "date")]
        public DateTime PaymentPeriod { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? StartBalance { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? PrincipalPortion { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? InterestPortion { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? OutstandingPrincipal { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? OutstandingInterest { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? ReceivedAmount { get; set; }
        [Column(TypeName = "date")]
        public DateTime? PaymentDate { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? EndBalance { get; set; }
        public bool? IsRescheduled { get; set; }
        public bool IsEliminated { get; set; }
        public bool IsCash { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdloanScheduled")]
        [InverseProperty("TTrxLoanInstallment")]
        public virtual TTrxLoanScheduled IdloanScheduledNavigation { get; set; }
    }
}
