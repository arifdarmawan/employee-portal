﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_PPh21_Component")]
    public partial class TMasterPph21Component
    {
        [Key]
        [Column("IDPPhComponent")]
        public int IdpphComponent { get; set; }
        [Column("IDPPhTemplate")]
        public int? IdpphTemplate { get; set; }
        [Column("IDPPhMethod")]
        public int? IdpphMethod { get; set; }
        [Column("IDItem")]
        public int? Iditem { get; set; }
        [Column("IDSalaryItem")]
        public int? IdsalaryItem { get; set; }
        [Column("PPhItemCode")]
        [StringLength(10)]
        public string PphItemCode { get; set; }
        [Column("PPhItemName")]
        [StringLength(150)]
        public string PphItemName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Iditem")]
        [InverseProperty("TMasterPph21Component")]
        public virtual TMaster1721Item IditemNavigation { get; set; }
        [ForeignKey("IdpphMethod")]
        [InverseProperty("TMasterPph21Component")]
        public virtual TMasterPph21Method IdpphMethodNavigation { get; set; }
        [ForeignKey("IdpphTemplate")]
        [InverseProperty("TMasterPph21Component")]
        public virtual TMasterPph21Template IdpphTemplateNavigation { get; set; }
        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TMasterPph21Component")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
    }
}
