﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Island")]
    public partial class TMasterIsland
    {
        public TMasterIsland()
        {
            TMasterState = new HashSet<TMasterState>();
        }

        [Key]
        [Column("IDIsland")]
        public int Idisland { get; set; }
        [Column("IDCountry")]
        public int Idcountry { get; set; }
        [StringLength(50)]
        public string IslandCode { get; set; }
        [StringLength(50)]
        public string IslandName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcountry")]
        [InverseProperty("TMasterIsland")]
        public virtual TMasterCountry IdcountryNavigation { get; set; }
        [InverseProperty("IdislandNavigation")]
        public virtual ICollection<TMasterState> TMasterState { get; set; }
    }
}
