﻿using System;
using System.Collections.Generic;

namespace HRISAPIServices.Models.Models
{
    public partial class TMasterUserType
    {
        public int IduserType { get; set; }
        public string UserTypeCode { get; set; }
        public string UserTypeName { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
