﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Salary_Item_Location")]
    public partial class TMasterSalaryItemLocation
    {
        [Key]
        [Column("IDSalItemLocation")]
        public int IdsalItemLocation { get; set; }
        [Column("IDSalaryItem")]
        public int? IdsalaryItem { get; set; }
        [Column("IDCompany")]
        public int? Idcompany { get; set; }
        [Column("IDLocation")]
        public int? Idlocation { get; set; }
        [Column("IDJobLevel")]
        public int? IdjobLevel { get; set; }
        [Column("IDEmploymentType")]
        public int? IdemploymentType { get; set; }
        [Column("IDOrganization")]
        public int? Idorganization { get; set; }
        [Column("IDJobtitle")]
        public int? Idjobtitle { get; set; }
        [Column("IDMaritalStatus")]
        public int? IdmaritalStatus { get; set; }
        [StringLength(1)]
        public string Gender { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Amount { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcompany")]
        [InverseProperty("TMasterSalaryItemLocation")]
        public virtual TMasterCompany IdcompanyNavigation { get; set; }
        [ForeignKey("IdemploymentType")]
        [InverseProperty("TMasterSalaryItemLocation")]
        public virtual TMasterEmploymentType IdemploymentTypeNavigation { get; set; }
        [ForeignKey("IdjobLevel")]
        [InverseProperty("TMasterSalaryItemLocation")]
        public virtual TMasterJoblevel IdjobLevelNavigation { get; set; }
        [ForeignKey("Idjobtitle")]
        [InverseProperty("TMasterSalaryItemLocation")]
        public virtual TMasterJobtitle IdjobtitleNavigation { get; set; }
        [ForeignKey("Idlocation")]
        [InverseProperty("TMasterSalaryItemLocation")]
        public virtual TMasterLocation IdlocationNavigation { get; set; }
        [ForeignKey("IdmaritalStatus")]
        [InverseProperty("TMasterSalaryItemLocation")]
        public virtual TMasterMaritalStatus IdmaritalStatusNavigation { get; set; }
        [ForeignKey("Idorganization")]
        [InverseProperty("TMasterSalaryItemLocation")]
        public virtual TMasterOrganization IdorganizationNavigation { get; set; }
        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TMasterSalaryItemLocation")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
    }
}
