﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_ESS_Emp_Family")]
    public partial class TTrxEssEmpFamily
    {
        [Key]
        [Column("IDEmpFamily")]
        public long IdempFamily { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDFamRelation")]
        public int? IdfamRelation { get; set; }
        [Column("IDMaritalStatus")]
        public int? IdmaritalStatus { get; set; }
        [Column("IDEduLevel")]
        public int? IdeduLevel { get; set; }
        [Column("IDOccupation")]
        public int? Idoccupation { get; set; }
        [Column("FamilyNIK")]
        [StringLength(10)]
        public string FamilyNik { get; set; }
        [StringLength(200)]
        public string MemberName { get; set; }
        [StringLength(1)]
        public string Gender { get; set; }
        [Column("IDBirthPlace")]
        public int? IdbirthPlace { get; set; }
        [StringLength(50)]
        public string BirthPlace { get; set; }
        [Column(TypeName = "date")]
        public DateTime? BirthDate { get; set; }
        public bool IsAlive { get; set; }
        [StringLength(150)]
        public string Occupation { get; set; }
        [Column("IDCardNo")]
        [StringLength(50)]
        public string IdcardNo { get; set; }
        [StringLength(300)]
        public string OriginalAddress { get; set; }
        [StringLength(300)]
        public string ResidentialAddress { get; set; }
        [StringLength(50)]
        public string PhoneNo { get; set; }
        [StringLength(50)]
        public string MobileNo { get; set; }
        [Column(TypeName = "date")]
        public DateTime? MarriedDate { get; set; }
        [Column("IDAttachment")]
        public Guid? Idattachment { get; set; }
        [Required]
        public bool? IsMedicalClaim { get; set; }
        [Required]
        [Column("IsBPJSKes")]
        public bool? IsBpjskes { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        public bool IsRejected { get; set; }
        [StringLength(50)]
        public string RejectedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RejectedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idattachment")]
        [InverseProperty("TTrxEssEmpFamily")]
        public virtual TTrxAttachment IdattachmentNavigation { get; set; }
        [ForeignKey("IdeduLevel")]
        [InverseProperty("TTrxEssEmpFamily")]
        public virtual TMasterEduLevel IdeduLevelNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEssEmpFamily")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdfamRelation")]
        [InverseProperty("TTrxEssEmpFamily")]
        public virtual TMasterFamilyRelation IdfamRelationNavigation { get; set; }
        [ForeignKey("IdmaritalStatus")]
        [InverseProperty("TTrxEssEmpFamily")]
        public virtual TMasterMaritalStatus IdmaritalStatusNavigation { get; set; }
        [ForeignKey("Idoccupation")]
        [InverseProperty("TTrxEssEmpFamily")]
        public virtual TMasterOccupation IdoccupationNavigation { get; set; }
    }
}
