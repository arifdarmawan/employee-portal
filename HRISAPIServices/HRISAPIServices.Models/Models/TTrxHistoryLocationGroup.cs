﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_History_LocationGroup")]
    public partial class TTrxHistoryLocationGroup
    {
        public TTrxHistoryLocationGroup()
        {
            TMasterJobtitleMapping = new HashSet<TMasterJobtitleMapping>();
        }

        [Column("IDHistoryLocGroup")]
        public long IdhistoryLocGroup { get; set; }
        [Column("IDLocationGroup")]
        public int? IdlocationGroup { get; set; }
        [Column("IDLocation")]
        public int? Idlocation { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdlocationGroup")]
        [InverseProperty("TTrxHistoryLocationGroup")]
        public virtual TMasterLocationGroup IdlocationGroupNavigation { get; set; }
        [ForeignKey("Idlocation")]
        [InverseProperty("TTrxHistoryLocationGroup")]
        public virtual TMasterLocation IdlocationNavigation { get; set; }
        [InverseProperty("IdhistoryLocGroupNavigation")]
        public virtual ICollection<TMasterJobtitleMapping> TMasterJobtitleMapping { get; set; }
    }
}
