﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Attendance_Type")]
    public partial class TMasterAttendanceType
    {
        public TMasterAttendanceType()
        {
            TTrxAttendance = new HashSet<TTrxAttendance>();
        }

        [Key]
        [Column("IDAttendanceType")]
        public int IdattendanceType { get; set; }
        [StringLength(10)]
        public string AttendTypeCode { get; set; }
        [StringLength(150)]
        public string AttendTypeName { get; set; }
        [StringLength(1)]
        public string AttendTypeGroup { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdattendanceTypeNavigation")]
        public virtual ICollection<TTrxAttendance> TTrxAttendance { get; set; }
    }
}
