﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Jobtitle")]
    public partial class TMasterJobtitle
    {
        public TMasterJobtitle()
        {
            TMasterJobtitleMappingIdjobtitleNavigation = new HashSet<TMasterJobtitleMapping>();
            TMasterJobtitleMappingIdparentJobtitleNavigation = new HashSet<TMasterJobtitleMapping>();
            TMasterSalaryItemLocation = new HashSet<TMasterSalaryItemLocation>();
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmployeeIdjobtitleNavigation = new HashSet<TTrxEmployee>();
            TTrxEmployeeIdreportingToNavigation = new HashSet<TTrxEmployee>();
        }

        [Key]
        [Column("IDJobtitle")]
        public int Idjobtitle { get; set; }
        [Column("IDJoblevel")]
        public int? Idjoblevel { get; set; }
        [Column("IDOrganization")]
        public int? Idorganization { get; set; }
        [Required]
        [StringLength(10)]
        public string JobtitleCode { get; set; }
        [Required]
        [StringLength(150)]
        public string JobtitleName { get; set; }
        [StringLength(150)]
        public string JobtitleAlias { get; set; }
        [StringLength(10)]
        public string ParentCode { get; set; }
        [StringLength(150)]
        public string ParentName { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        public string JobDescription { get; set; }

        [ForeignKey("Idjoblevel")]
        [InverseProperty("TMasterJobtitle")]
        public virtual TMasterJoblevel IdjoblevelNavigation { get; set; }
        [ForeignKey("Idorganization")]
        [InverseProperty("TMasterJobtitle")]
        public virtual TMasterOrganization IdorganizationNavigation { get; set; }
        [InverseProperty("IdjobtitleNavigation")]
        public virtual ICollection<TMasterJobtitleMapping> TMasterJobtitleMappingIdjobtitleNavigation { get; set; }
        [InverseProperty("IdparentJobtitleNavigation")]
        public virtual ICollection<TMasterJobtitleMapping> TMasterJobtitleMappingIdparentJobtitleNavigation { get; set; }
        [InverseProperty("IdjobtitleNavigation")]
        public virtual ICollection<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        [InverseProperty("IdjobtitleNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdjobtitleNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployeeIdjobtitleNavigation { get; set; }
        [InverseProperty("IdreportingToNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployeeIdreportingToNavigation { get; set; }
    }
}
