﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Allowance_Template")]
    public partial class TMasterAllowanceTemplate
    {
        [Key]
        [Column("IDAllowanceTmp")]
        public long IdallowanceTmp { get; set; }
        [Column("IDSalaryItem")]
        public int IdsalaryItem { get; set; }
        [StringLength(250)]
        public string TemplateName { get; set; }
        public bool IsCompany { get; set; }
        public bool IsLocation { get; set; }
        public bool IsJoblevel { get; set; }
        public bool IsEmploymentType { get; set; }
        public bool IsOrganization { get; set; }
        public bool IsJobtitle { get; set; }
        public bool IsMaritalStatus { get; set; }
        public bool IsGender { get; set; }
        public bool IsEmployee { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TMasterAllowanceTemplate")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
    }
}
