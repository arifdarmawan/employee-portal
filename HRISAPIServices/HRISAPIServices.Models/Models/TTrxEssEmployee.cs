﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_ESS_Employee")]
    public partial class TTrxEssEmployee
    {
        [Key]
        [Column("IDESSEmployee")]
        public long Idessemployee { get; set; }
        [Column("IDEmployee")]
        public long Idemployee { get; set; }
        [Column("IDBloodType")]
        public int? IdbloodType { get; set; }
        [Column("IDReligion")]
        public int? Idreligion { get; set; }
        [Column("IDRace")]
        public int? Idrace { get; set; }
        [Column("IDMaritalStatus")]
        public int? IdmaritalStatus { get; set; }
        [Column("IDBirthPlace")]
        public int? IdbirthPlace { get; set; }
        [StringLength(50)]
        public string BirthPlace { get; set; }
        [Column(TypeName = "date")]
        public DateTime? BirthDate { get; set; }
        [StringLength(1)]
        public string Gender { get; set; }
        [Column("IDHomeBase")]
        public int? IdhomeBase { get; set; }
        [StringLength(50)]
        public string HomeBase { get; set; }
        [Column("IDCitizenship")]
        public int? Idcitizenship { get; set; }
        [StringLength(50)]
        public string Citizenship { get; set; }
        [StringLength(150)]
        public string OfficeMail { get; set; }
        [StringLength(150)]
        public string PrivateMail { get; set; }
        [StringLength(50)]
        public string MobileNo { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        public bool IsRejected { get; set; }
        [StringLength(50)]
        public string RejectedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RejectedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdbirthPlace")]
        [InverseProperty("TTrxEssEmployeeIdbirthPlaceNavigation")]
        public virtual TMasterCity IdbirthPlaceNavigation { get; set; }
        [ForeignKey("IdbloodType")]
        [InverseProperty("TTrxEssEmployee")]
        public virtual TMasterBloodType IdbloodTypeNavigation { get; set; }
        [ForeignKey("Idcitizenship")]
        [InverseProperty("TTrxEssEmployee")]
        public virtual TMasterCountry IdcitizenshipNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEssEmployee")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdhomeBase")]
        [InverseProperty("TTrxEssEmployeeIdhomeBaseNavigation")]
        public virtual TMasterCity IdhomeBaseNavigation { get; set; }
        [ForeignKey("IdmaritalStatus")]
        [InverseProperty("TTrxEssEmployee")]
        public virtual TMasterMaritalStatus IdmaritalStatusNavigation { get; set; }
        [ForeignKey("Idrace")]
        [InverseProperty("TTrxEssEmployee")]
        public virtual TMasterRace IdraceNavigation { get; set; }
        [ForeignKey("Idreligion")]
        [InverseProperty("TTrxEssEmployee")]
        public virtual TMasterReligion IdreligionNavigation { get; set; }
    }
}
