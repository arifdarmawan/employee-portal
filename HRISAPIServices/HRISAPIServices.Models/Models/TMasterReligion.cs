﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Religion")]
    public partial class TMasterReligion
    {
        public TMasterReligion()
        {
            TMasterThrTmpDetail = new HashSet<TMasterThrTmpDetail>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
            TTrxEssEmployee = new HashSet<TTrxEssEmployee>();
        }

        [Key]
        [Column("IDReligion")]
        public int Idreligion { get; set; }
        [Required]
        [StringLength(10)]
        public string ReligionCode { get; set; }
        [Required]
        [StringLength(50)]
        public string ReligionName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdreligionNavigation")]
        public virtual ICollection<TMasterThrTmpDetail> TMasterThrTmpDetail { get; set; }
        [InverseProperty("IdreligionNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
        [InverseProperty("IdreligionNavigation")]
        public virtual ICollection<TTrxEssEmployee> TTrxEssEmployee { get; set; }
    }
}
