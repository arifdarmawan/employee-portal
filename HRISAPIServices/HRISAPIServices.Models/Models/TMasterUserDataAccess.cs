﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_User_Data_Access")]
    public partial class TMasterUserDataAccess
    {
        [Key]
        [Column("IDUserDataAccess")]
        public int IduserDataAccess { get; set; }
        [Column("IDGroup")]
        public int? Idgroup { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idgroup")]
        [InverseProperty("TMasterUserDataAccess")]
        public virtual TMasterGroup IdgroupNavigation { get; set; }
    }
}
