﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_History_Event_Log")]
    public partial class TTrxHistoryEventLog
    {
        [Column("EventID")]
        public long EventId { get; set; }
        [StringLength(50)]
        public string UserLogin { get; set; }
        [StringLength(200)]
        public string EventName { get; set; }
        [StringLength(50)]
        public string EventType { get; set; }
        [StringLength(200)]
        public string ModuleName { get; set; }
        public string Description { get; set; }
        [StringLength(50)]
        public string MethodType { get; set; }
        [StringLength(10)]
        public string StatusCode { get; set; }
        [StringLength(50)]
        public string Host { get; set; }
        public string BrowserType { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EventDate { get; set; }
    }
}
