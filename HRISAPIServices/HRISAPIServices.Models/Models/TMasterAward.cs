﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Award")]
    public partial class TMasterAward
    {
        [Key]
        [Column("IDAward")]
        public int Idaward { get; set; }
        [StringLength(10)]
        public string AwardCode { get; set; }
        [StringLength(150)]
        public string AwardName { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [StringLength(100)]
        public string AwardReceived { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
