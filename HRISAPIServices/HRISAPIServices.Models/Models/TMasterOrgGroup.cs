﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Org_Group")]
    public partial class TMasterOrgGroup
    {
        public TMasterOrgGroup()
        {
            TMasterOrganization = new HashSet<TMasterOrganization>();
        }

        [Key]
        [Column("IDOrgGroup")]
        public int IdorgGroup { get; set; }
        [StringLength(10)]
        public string OrgGroupCode { get; set; }
        [StringLength(50)]
        public string OrgGroupName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdorgGroupNavigation")]
        public virtual ICollection<TMasterOrganization> TMasterOrganization { get; set; }
    }
}
