﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_LangCriteria")]
    public partial class TMasterLangCriteria
    {
        [Key]
        [Column("IDLangCriteria")]
        public int IdlangCriteria { get; set; }
        [StringLength(1)]
        public string Grade { get; set; }
        public short? MinValue { get; set; }
        public short? MaxValue { get; set; }
        [StringLength(200)]
        public string Remark { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
