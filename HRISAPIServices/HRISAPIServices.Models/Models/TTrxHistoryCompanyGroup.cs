﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_History_CompanyGroup")]
    public partial class TTrxHistoryCompanyGroup
    {
        [Column("IDHistoryCompGroup")]
        public long IdhistoryCompGroup { get; set; }
        [Column("IDCompanyGroup")]
        public int? IdcompanyGroup { get; set; }
        [Column("IDCompany")]
        public int? Idcompany { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdcompanyGroup")]
        [InverseProperty("TTrxHistoryCompanyGroup")]
        public virtual TMasterCompanyGroup IdcompanyGroupNavigation { get; set; }
        [ForeignKey("Idcompany")]
        [InverseProperty("TTrxHistoryCompanyGroup")]
        public virtual TMasterCompany IdcompanyNavigation { get; set; }
    }
}
