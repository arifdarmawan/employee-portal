﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_Salary_Changes")]
    public partial class TTrxEmpSalaryChanges
    {
        public TTrxEmpSalaryChanges()
        {
            TTrxEmpSalChangesDetail = new HashSet<TTrxEmpSalChangesDetail>();
        }

        [Key]
        [Column("IDEmpSalaryChanges")]
        public long IdempSalaryChanges { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDTmpSalary")]
        public int? IdtmpSalary { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        [StringLength(50)]
        public string Initiator { get; set; }
        [Column(TypeName = "date")]
        public DateTime? AppliedDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EffectiveDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [StringLength(30)]
        public string DecisionNo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DecisionDate { get; set; }

        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpSalaryChanges")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdtmpSalary")]
        [InverseProperty("TTrxEmpSalaryChanges")]
        public virtual TMasterSalaryTemplate IdtmpSalaryNavigation { get; set; }
        [InverseProperty("IdempSalaryChangesNavigation")]
        public virtual ICollection<TTrxEmpSalChangesDetail> TTrxEmpSalChangesDetail { get; set; }
    }
}
