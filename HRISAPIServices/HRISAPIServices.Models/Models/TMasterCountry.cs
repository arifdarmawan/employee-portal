﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Country")]
    public partial class TMasterCountry
    {
        public TMasterCountry()
        {
            TMasterIsland = new HashSet<TMasterIsland>();
            TMasterLocation = new HashSet<TMasterLocation>();
            TMasterState = new HashSet<TMasterState>();
            TTrxEmpAddressIdcountryONavigation = new HashSet<TTrxEmpAddress>();
            TTrxEmpAddressIdcountryRNavigation = new HashSet<TTrxEmpAddress>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
            TTrxEssEmpAddressIdcountryONavigation = new HashSet<TTrxEssEmpAddress>();
            TTrxEssEmpAddressIdcountryRNavigation = new HashSet<TTrxEssEmpAddress>();
            TTrxEssEmployee = new HashSet<TTrxEssEmployee>();
        }

        [Key]
        [Column("IDCountry")]
        public int Idcountry { get; set; }
        [StringLength(10)]
        public string CountryCode { get; set; }
        [StringLength(50)]
        public string CountryName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdcountryNavigation")]
        public virtual ICollection<TMasterIsland> TMasterIsland { get; set; }
        [InverseProperty("IdcountryNavigation")]
        public virtual ICollection<TMasterLocation> TMasterLocation { get; set; }
        [InverseProperty("IdcountryNavigation")]
        public virtual ICollection<TMasterState> TMasterState { get; set; }
        [InverseProperty("IdcountryONavigation")]
        public virtual ICollection<TTrxEmpAddress> TTrxEmpAddressIdcountryONavigation { get; set; }
        [InverseProperty("IdcountryRNavigation")]
        public virtual ICollection<TTrxEmpAddress> TTrxEmpAddressIdcountryRNavigation { get; set; }
        [InverseProperty("IdcitizenshipNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
        [InverseProperty("IdcountryONavigation")]
        public virtual ICollection<TTrxEssEmpAddress> TTrxEssEmpAddressIdcountryONavigation { get; set; }
        [InverseProperty("IdcountryRNavigation")]
        public virtual ICollection<TTrxEssEmpAddress> TTrxEssEmpAddressIdcountryRNavigation { get; set; }
        [InverseProperty("IdcitizenshipNavigation")]
        public virtual ICollection<TTrxEssEmployee> TTrxEssEmployee { get; set; }
    }
}
