﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_ESS_Emp_Bank_Account")]
    public partial class TTrxEssEmpBankAccount
    {
        [Key]
        [Column("IDEmpBankAccount")]
        public long IdempBankAccount { get; set; }
        [Column("IDEmployee")]
        public long? Idemployee { get; set; }
        [Column("IDCurrency")]
        public int? Idcurrency { get; set; }
        [Column("IDBankGroup")]
        public int? IdbankGroup { get; set; }
        [Column("IDBankAccount")]
        public int? IdbankAccount { get; set; }
        [StringLength(150)]
        public string AccountNo { get; set; }
        [StringLength(150)]
        public string AccountName { get; set; }
        [StringLength(150)]
        public string Owner { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? TransferFee { get; set; }
        public bool IsDefaultAccount { get; set; }
        [Column("IsTHR")]
        public bool IsThr { get; set; }
        public bool IsActive { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        public bool IsRejected { get; set; }
        [StringLength(50)]
        public string RejectedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RejectedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdbankAccount")]
        [InverseProperty("TTrxEssEmpBankAccount")]
        public virtual TMasterBankAccount IdbankAccountNavigation { get; set; }
        [ForeignKey("IdbankGroup")]
        [InverseProperty("TTrxEssEmpBankAccount")]
        public virtual TMasterBankGroup IdbankGroupNavigation { get; set; }
        [ForeignKey("Idcurrency")]
        [InverseProperty("TTrxEssEmpBankAccount")]
        public virtual TMasterCurrency IdcurrencyNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEssEmpBankAccount")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
    }
}
