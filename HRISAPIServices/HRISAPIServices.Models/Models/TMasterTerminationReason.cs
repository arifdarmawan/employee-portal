﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Termination_Reason")]
    public partial class TMasterTerminationReason
    {
        public TMasterTerminationReason()
        {
            TTrxEmpTermination = new HashSet<TTrxEmpTermination>();
        }

        [Key]
        [Column("IDTermReason")]
        public int IdtermReason { get; set; }
        [Column("IDTermCategory")]
        public int? IdtermCategory { get; set; }
        [StringLength(10)]
        public string ReasonCode { get; set; }
        [StringLength(200)]
        public string ReasonDesc { get; set; }
        [StringLength(1)]
        public string ReasonType { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdtermCategory")]
        [InverseProperty("TMasterTerminationReason")]
        public virtual TMasterTerminationCategory IdtermCategoryNavigation { get; set; }
        [InverseProperty("IdtermReasonNavigation")]
        public virtual ICollection<TTrxEmpTermination> TTrxEmpTermination { get; set; }
    }
}
