﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Termination_Category")]
    public partial class TMasterTerminationCategory
    {
        public TMasterTerminationCategory()
        {
            TMasterTerminationReason = new HashSet<TMasterTerminationReason>();
        }

        [Key]
        [Column("IDTermCategory")]
        public int IdtermCategory { get; set; }
        [StringLength(150)]
        public string ReasonCategory { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [StringLength(10)]
        public string ReasonCategoryCode { get; set; }

        [InverseProperty("IdtermCategoryNavigation")]
        public virtual ICollection<TMasterTerminationReason> TMasterTerminationReason { get; set; }
    }
}
