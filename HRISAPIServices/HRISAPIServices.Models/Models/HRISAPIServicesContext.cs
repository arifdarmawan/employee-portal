﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HRISAPIServices.Models.Models
{
    public partial class HRISAPIServicesContext : DbContext
    {
        public HRISAPIServicesContext()
        {
        }

        public HRISAPIServicesContext(DbContextOptions<HRISAPIServicesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TMaster1721Item> TMaster1721Item { get; set; }
        public virtual DbSet<TMasterAllowanceJamsostek> TMasterAllowanceJamsostek { get; set; }
        public virtual DbSet<TMasterAllowanceTemplate> TMasterAllowanceTemplate { get; set; }
        public virtual DbSet<TMasterAttendanceType> TMasterAttendanceType { get; set; }
        public virtual DbSet<TMasterAward> TMasterAward { get; set; }
        public virtual DbSet<TMasterBankAccount> TMasterBankAccount { get; set; }
        public virtual DbSet<TMasterBankGroup> TMasterBankGroup { get; set; }
        public virtual DbSet<TMasterBloodType> TMasterBloodType { get; set; }
        public virtual DbSet<TMasterBpjskesProvider> TMasterBpjskesProvider { get; set; }
        public virtual DbSet<TMasterBpjskesProviderDetail> TMasterBpjskesProviderDetail { get; set; }
        public virtual DbSet<TMasterBpjskesTemplate> TMasterBpjskesTemplate { get; set; }
        public virtual DbSet<TMasterBpjskesTmpDetail> TMasterBpjskesTmpDetail { get; set; }
        public virtual DbSet<TMasterCardType> TMasterCardType { get; set; }
        public virtual DbSet<TMasterCity> TMasterCity { get; set; }
        public virtual DbSet<TMasterCompany> TMasterCompany { get; set; }
        public virtual DbSet<TMasterCompanyGroup> TMasterCompanyGroup { get; set; }
        public virtual DbSet<TMasterCostCenter> TMasterCostCenter { get; set; }
        public virtual DbSet<TMasterCostCenterGroup> TMasterCostCenterGroup { get; set; }
        public virtual DbSet<TMasterCountry> TMasterCountry { get; set; }
        public virtual DbSet<TMasterCurrency> TMasterCurrency { get; set; }
        public virtual DbSet<TMasterEduLevel> TMasterEduLevel { get; set; }
        public virtual DbSet<TMasterEduMajor> TMasterEduMajor { get; set; }
        public virtual DbSet<TMasterEduResult> TMasterEduResult { get; set; }
        public virtual DbSet<TMasterEduStatus> TMasterEduStatus { get; set; }
        public virtual DbSet<TMasterEliminationReason> TMasterEliminationReason { get; set; }
        public virtual DbSet<TMasterEmploymentType> TMasterEmploymentType { get; set; }
        public virtual DbSet<TMasterFamilyRelation> TMasterFamilyRelation { get; set; }
        public virtual DbSet<TMasterGroup> TMasterGroup { get; set; }
        public virtual DbSet<TMasterHolidays> TMasterHolidays { get; set; }
        public virtual DbSet<TMasterInstitution> TMasterInstitution { get; set; }
        public virtual DbSet<TMasterIsland> TMasterIsland { get; set; }
        public virtual DbSet<TMasterJamsostekProvider> TMasterJamsostekProvider { get; set; }
        public virtual DbSet<TMasterJamsostekProviderDetail> TMasterJamsostekProviderDetail { get; set; }
        public virtual DbSet<TMasterJamsostekTemplate> TMasterJamsostekTemplate { get; set; }
        public virtual DbSet<TMasterJamsostekTmpDetail> TMasterJamsostekTmpDetail { get; set; }
        public virtual DbSet<TMasterJoblevel> TMasterJoblevel { get; set; }
        public virtual DbSet<TMasterJoblevelGroup> TMasterJoblevelGroup { get; set; }
        public virtual DbSet<TMasterJobtitle> TMasterJobtitle { get; set; }
        public virtual DbSet<TMasterJobtitleMapping> TMasterJobtitleMapping { get; set; }
        public virtual DbSet<TMasterKpp> TMasterKpp { get; set; }
        public virtual DbSet<TMasterKppDetail> TMasterKppDetail { get; set; }
        public virtual DbSet<TMasterLangCriteria> TMasterLangCriteria { get; set; }
        public virtual DbSet<TMasterLanguage> TMasterLanguage { get; set; }
        public virtual DbSet<TMasterLoanPaymentMethod> TMasterLoanPaymentMethod { get; set; }
        public virtual DbSet<TMasterLoanPaymentSchedule> TMasterLoanPaymentSchedule { get; set; }
        public virtual DbSet<TMasterLoanType> TMasterLoanType { get; set; }
        public virtual DbSet<TMasterLocation> TMasterLocation { get; set; }
        public virtual DbSet<TMasterLocationGroup> TMasterLocationGroup { get; set; }
        public virtual DbSet<TMasterLookup> TMasterLookup { get; set; }
        public virtual DbSet<TMasterMaritalStatus> TMasterMaritalStatus { get; set; }
        public virtual DbSet<TMasterMedicalExpense> TMasterMedicalExpense { get; set; }
        public virtual DbSet<TMasterMedicalInstitution> TMasterMedicalInstitution { get; set; }
        public virtual DbSet<TMasterMedicalInstitutionType> TMasterMedicalInstitutionType { get; set; }
        public virtual DbSet<TMasterMedicalRoom> TMasterMedicalRoom { get; set; }
        public virtual DbSet<TMasterMedicalTemplate> TMasterMedicalTemplate { get; set; }
        public virtual DbSet<TMasterMedicalTmpDetail> TMasterMedicalTmpDetail { get; set; }
        public virtual DbSet<TMasterMedicalType> TMasterMedicalType { get; set; }
        public virtual DbSet<TMasterMenu> TMasterMenu { get; set; }
        public virtual DbSet<TMasterOccupation> TMasterOccupation { get; set; }
        public virtual DbSet<TMasterOrgGroup> TMasterOrgGroup { get; set; }
        public virtual DbSet<TMasterOrgLevel> TMasterOrgLevel { get; set; }
        public virtual DbSet<TMasterOrganization> TMasterOrganization { get; set; }
        public virtual DbSet<TMasterPcnType> TMasterPcnType { get; set; }
        public virtual DbSet<TMasterPositionExpense> TMasterPositionExpense { get; set; }
        public virtual DbSet<TMasterPph21Component> TMasterPph21Component { get; set; }
        public virtual DbSet<TMasterPph21Formula> TMasterPph21Formula { get; set; }
        public virtual DbSet<TMasterPph21Method> TMasterPph21Method { get; set; }
        public virtual DbSet<TMasterPph21Template> TMasterPph21Template { get; set; }
        public virtual DbSet<TMasterPtkp> TMasterPtkp { get; set; }
        public virtual DbSet<TMasterPtkpDetail> TMasterPtkpDetail { get; set; }
        public virtual DbSet<TMasterPtkpTemplate> TMasterPtkpTemplate { get; set; }
        public virtual DbSet<TMasterRace> TMasterRace { get; set; }
        public virtual DbSet<TMasterReligion> TMasterReligion { get; set; }
        public virtual DbSet<TMasterSalaryItem> TMasterSalaryItem { get; set; }
        public virtual DbSet<TMasterSalaryItemEmployee> TMasterSalaryItemEmployee { get; set; }
        public virtual DbSet<TMasterSalaryItemGroup> TMasterSalaryItemGroup { get; set; }
        public virtual DbSet<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        public virtual DbSet<TMasterSalaryItemType> TMasterSalaryItemType { get; set; }
        public virtual DbSet<TMasterSalaryTemplate> TMasterSalaryTemplate { get; set; }
        public virtual DbSet<TMasterSalaryTmpDetail> TMasterSalaryTmpDetail { get; set; }
        public virtual DbSet<TMasterSalaryType> TMasterSalaryType { get; set; }
        public virtual DbSet<TMasterSecurityPolicy> TMasterSecurityPolicy { get; set; }
        public virtual DbSet<TMasterSeveranceItem> TMasterSeveranceItem { get; set; }
        public virtual DbSet<TMasterSeveranceRate> TMasterSeveranceRate { get; set; }
        public virtual DbSet<TMasterState> TMasterState { get; set; }
        public virtual DbSet<TMasterTaxRate> TMasterTaxRate { get; set; }
        public virtual DbSet<TMasterTerminationCategory> TMasterTerminationCategory { get; set; }
        public virtual DbSet<TMasterTerminationNotice> TMasterTerminationNotice { get; set; }
        public virtual DbSet<TMasterTerminationReason> TMasterTerminationReason { get; set; }
        public virtual DbSet<TMasterThrTemplate> TMasterThrTemplate { get; set; }
        public virtual DbSet<TMasterThrTmpDetail> TMasterThrTmpDetail { get; set; }
        public virtual DbSet<TMasterUmp> TMasterUmp { get; set; }
        public virtual DbSet<TMasterUmpDetail> TMasterUmpDetail { get; set; }
        public virtual DbSet<TMasterUser> TMasterUser { get; set; }
        public virtual DbSet<TMasterUserDataAccess> TMasterUserDataAccess { get; set; }
        public virtual DbSet<TMasterUserDataTrustee> TMasterUserDataTrustee { get; set; }
        public virtual DbSet<TMasterUserGroup> TMasterUserGroup { get; set; }
        public virtual DbSet<TMasterUserMenuAccess> TMasterUserMenuAccess { get; set; }
        public virtual DbSet<TMasterUserMenuTrustee> TMasterUserMenuTrustee { get; set; }
        public virtual DbSet<TMasterViolation> TMasterViolation { get; set; }
        public virtual DbSet<TMasterWarningLevel> TMasterWarningLevel { get; set; }
        public virtual DbSet<TReportBonus> TReportBonus { get; set; }
        public virtual DbSet<TReportBonusDetail> TReportBonusDetail { get; set; }
        public virtual DbSet<TReportPayrollNonUpah> TReportPayrollNonUpah { get; set; }
        public virtual DbSet<TReportPayrollNonUpahDetail> TReportPayrollNonUpahDetail { get; set; }
        public virtual DbSet<TReportPayrollUpah> TReportPayrollUpah { get; set; }
        public virtual DbSet<TReportPayrollUpahDetail> TReportPayrollUpahDetail { get; set; }
        public virtual DbSet<TReportPph21Monthly> TReportPph21Monthly { get; set; }
        public virtual DbSet<TReportThr> TReportThr { get; set; }
        public virtual DbSet<TReportThrDetail> TReportThrDetail { get; set; }
        public virtual DbSet<TTrxAttachment> TTrxAttachment { get; set; }
        public virtual DbSet<TTrxAttendance> TTrxAttendance { get; set; }
        public virtual DbSet<TTrxBonus> TTrxBonus { get; set; }
        public virtual DbSet<TTrxBonusDetail> TTrxBonusDetail { get; set; }
        public virtual DbSet<TTrxCurrencyRate> TTrxCurrencyRate { get; set; }
        public virtual DbSet<TTrxEmpAddress> TTrxEmpAddress { get; set; }
        public virtual DbSet<TTrxEmpBankAccount> TTrxEmpBankAccount { get; set; }
        public virtual DbSet<TTrxEmpBankAccountChanges> TTrxEmpBankAccountChanges { get; set; }
        public virtual DbSet<TTrxEmpBpjskes> TTrxEmpBpjskes { get; set; }
        public virtual DbSet<TTrxEmpBpjskesChanges> TTrxEmpBpjskesChanges { get; set; }
        public virtual DbSet<TTrxEmpEducation> TTrxEmpEducation { get; set; }
        public virtual DbSet<TTrxEmpFamily> TTrxEmpFamily { get; set; }
        public virtual DbSet<TTrxEmpIdCard> TTrxEmpIdCard { get; set; }
        public virtual DbSet<TTrxEmpJamsostek> TTrxEmpJamsostek { get; set; }
        public virtual DbSet<TTrxEmpJamsostekChanges> TTrxEmpJamsostekChanges { get; set; }
        public virtual DbSet<TTrxEmpJobExp> TTrxEmpJobExp { get; set; }
        public virtual DbSet<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        public virtual DbSet<TTrxEmpPph> TTrxEmpPph { get; set; }
        public virtual DbSet<TTrxEmpPphChanges> TTrxEmpPphChanges { get; set; }
        public virtual DbSet<TTrxEmpPtkpChanges> TTrxEmpPtkpChanges { get; set; }
        public virtual DbSet<TTrxEmpSalChangesDetail> TTrxEmpSalChangesDetail { get; set; }
        public virtual DbSet<TTrxEmpSalary> TTrxEmpSalary { get; set; }
        public virtual DbSet<TTrxEmpSalaryChanges> TTrxEmpSalaryChanges { get; set; }
        public virtual DbSet<TTrxEmpSalaryDetail> TTrxEmpSalaryDetail { get; set; }
        public virtual DbSet<TTrxEmpSalaryMonthly> TTrxEmpSalaryMonthly { get; set; }
        public virtual DbSet<TTrxEmpSalaryMonthlyDetail> TTrxEmpSalaryMonthlyDetail { get; set; }
        public virtual DbSet<TTrxEmpTermination> TTrxEmpTermination { get; set; }
        public virtual DbSet<TTrxEmpViolation> TTrxEmpViolation { get; set; }
        public virtual DbSet<TTrxEmployee> TTrxEmployee { get; set; }
        public virtual DbSet<TTrxEssEmpAddress> TTrxEssEmpAddress { get; set; }
        public virtual DbSet<TTrxEssEmpBankAccount> TTrxEssEmpBankAccount { get; set; }
        public virtual DbSet<TTrxEssEmpEducation> TTrxEssEmpEducation { get; set; }
        public virtual DbSet<TTrxEssEmpFamily> TTrxEssEmpFamily { get; set; }
        public virtual DbSet<TTrxEssEmpIdCard> TTrxEssEmpIdCard { get; set; }
        public virtual DbSet<TTrxEssEmpJobExp> TTrxEssEmpJobExp { get; set; }
        public virtual DbSet<TTrxEssEmployee> TTrxEssEmployee { get; set; }
        public virtual DbSet<TTrxHistoryCompanyGroup> TTrxHistoryCompanyGroup { get; set; }
        public virtual DbSet<TTrxHistoryCompanyLocation> TTrxHistoryCompanyLocation { get; set; }
        public virtual DbSet<TTrxHistoryEventLog> TTrxHistoryEventLog { get; set; }
        public virtual DbSet<TTrxHistoryLocationGroup> TTrxHistoryLocationGroup { get; set; }
        public virtual DbSet<TTrxHistoryRate> TTrxHistoryRate { get; set; }
        public virtual DbSet<TTrxHistoryReport> TTrxHistoryReport { get; set; }
        public virtual DbSet<TTrxLoanApplication> TTrxLoanApplication { get; set; }
        public virtual DbSet<TTrxLoanInstallment> TTrxLoanInstallment { get; set; }
        public virtual DbSet<TTrxLoanScheduled> TTrxLoanScheduled { get; set; }
        public virtual DbSet<TTrxMedicalClaim> TTrxMedicalClaim { get; set; }
        public virtual DbSet<TTrxMedicalClaimExpense> TTrxMedicalClaimExpense { get; set; }
        public virtual DbSet<TTrxPayrollNonUpah> TTrxPayrollNonUpah { get; set; }
        public virtual DbSet<TTrxPayrollNonUpahDetail> TTrxPayrollNonUpahDetail { get; set; }
        public virtual DbSet<TTrxPayrollUpah> TTrxPayrollUpah { get; set; }
        public virtual DbSet<TTrxPayrollUpahDetail> TTrxPayrollUpahDetail { get; set; }
        public virtual DbSet<TTrxPph21Monthly> TTrxPph21Monthly { get; set; }
        public virtual DbSet<TTrxProcessedBonus> TTrxProcessedBonus { get; set; }
        public virtual DbSet<TTrxProcessedPayrollNonUpah> TTrxProcessedPayrollNonUpah { get; set; }
        public virtual DbSet<TTrxProcessedPayrollUpah> TTrxProcessedPayrollUpah { get; set; }
        public virtual DbSet<TTrxProcessedPph21Monthly> TTrxProcessedPph21Monthly { get; set; }
        public virtual DbSet<TTrxProcessedThr> TTrxProcessedThr { get; set; }
        public virtual DbSet<TTrxTempPph21Monthly> TTrxTempPph21Monthly { get; set; }
        public virtual DbSet<TTrxThr> TTrxThr { get; set; }
        public virtual DbSet<TTrxThrDetail> TTrxThrDetail { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=10.112.5.152;Database=DBHRIS_FAP;User Id=sa;Password=12345;ConnectRetryCount=0;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<TMaster1721Item>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsCalculated).HasDefaultValueSql("((1))");

                entity.Property(e => e.ItemCode).IsUnicode(false);

                entity.Property(e => e.ItemDesc).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterAllowanceJamsostek>(entity =>
            {
                entity.Property(e => e.Amount).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdjoblevelNavigation)
                    .WithMany(p => p.TMasterAllowanceJamsostek)
                    .HasForeignKey(d => d.Idjoblevel)
                    .HasConstraintName("FK_T_Master_Allowance_Jamsostek_T_Master_Joblevel");

                entity.HasOne(d => d.IdtmpJamsostekNavigation)
                    .WithMany(p => p.TMasterAllowanceJamsostek)
                    .HasForeignKey(d => d.IdtmpJamsostek)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Allowance_Jamsostek_T_Master_Jamsostek_Template");

                entity.HasOne(d => d.IdtmpSalaryNavigation)
                    .WithMany(p => p.TMasterAllowanceJamsostek)
                    .HasForeignKey(d => d.IdtmpSalary)
                    .HasConstraintName("FK_T_Master_Allowance_Jamsostek_T_Master_Salary_Template");
            });

            modelBuilder.Entity<TMasterAllowanceTemplate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TemplateName).IsUnicode(false);

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TMasterAllowanceTemplate)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Allowance_Template_T_Master_Salary_Item");
            });

            modelBuilder.Entity<TMasterAttendanceType>(entity =>
            {
                entity.Property(e => e.AttendTypeCode).IsUnicode(false);

                entity.Property(e => e.AttendTypeGroup).IsUnicode(false);

                entity.Property(e => e.AttendTypeName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterAward>(entity =>
            {
                entity.Property(e => e.AwardCode).IsUnicode(false);

                entity.Property(e => e.AwardName).IsUnicode(false);

                entity.Property(e => e.AwardReceived).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterBankAccount>(entity =>
            {
                entity.Property(e => e.AccountName).IsUnicode(false);

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.Bicode).IsUnicode(false);

                entity.Property(e => e.Branch).IsUnicode(false);

                entity.Property(e => e.BranchCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdbankGroupNavigation)
                    .WithMany(p => p.TMasterBankAccount)
                    .HasForeignKey(d => d.IdbankGroup)
                    .HasConstraintName("FK_T_Master_Bank_Account_T_Master_Bank_Group");

                entity.HasOne(d => d.IdcityNavigation)
                    .WithMany(p => p.TMasterBankAccount)
                    .HasForeignKey(d => d.Idcity)
                    .HasConstraintName("FK_T_Master_Bank_Account_T_Master_City");
            });

            modelBuilder.Entity<TMasterBankGroup>(entity =>
            {
                entity.HasKey(e => e.IdbankGroup)
                    .HasName("PK_T_Master_Bank");

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.BankGroupName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TransferFee).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TMasterBloodType>(entity =>
            {
                entity.Property(e => e.BloodTypeCode).IsUnicode(false);

                entity.Property(e => e.BloodTypeName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterBpjskesProvider>(entity =>
            {
                entity.HasKey(e => e.IdbpjskesProvider)
                    .HasName("PK_T_Master_BPJS_Provider");

                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EdabuUser).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OfficeCode).IsUnicode(false);

                entity.Property(e => e.OfficeName).IsUnicode(false);

                entity.Property(e => e.RegisterNo).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterBpjskesProviderDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdbpjskesProviderNavigation)
                    .WithMany(p => p.TMasterBpjskesProviderDetail)
                    .HasForeignKey(d => d.IdbpjskesProvider)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_BPJSKes_Provider_Detail_T_Master_BPJSKes_Provider_Detail");

                entity.HasOne(d => d.IdhistoryCompLocNavigation)
                    .WithMany(p => p.TMasterBpjskesProviderDetail)
                    .HasForeignKey(d => d.IdhistoryCompLoc)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_BPJSKes_Provider_Detail_T_Trx_History_CompanyLocation");

                entity.HasOne(d => d.IdjobLevelNavigation)
                    .WithMany(p => p.TMasterBpjskesProviderDetail)
                    .HasForeignKey(d => d.IdjobLevel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_BPJSKes_Provider_Detail_T_Master_Joblevel");
            });

            modelBuilder.Entity<TMasterBpjskesTemplate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TemplateCode).IsUnicode(false);

                entity.Property(e => e.TemplateName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterBpjskesTmpDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsPayroll).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdtmpBpjskesNavigation)
                    .WithMany(p => p.TMasterBpjskesTmpDetail)
                    .HasForeignKey(d => d.IdtmpBpjskes)
                    .HasConstraintName("FK_T_Master_BPJSKes_TmpDetail_T_Master_BPJSKes_TmpDetail");
            });

            modelBuilder.Entity<TMasterCardType>(entity =>
            {
                entity.Property(e => e.CardTypeCode).IsUnicode(false);

                entity.Property(e => e.CardTypeName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterCity>(entity =>
            {
                entity.Property(e => e.CityCode).IsUnicode(false);

                entity.Property(e => e.CityName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdstateNavigation)
                    .WithMany(p => p.TMasterCity)
                    .HasForeignKey(d => d.Idstate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_City_T_Master_State");
            });

            modelBuilder.Entity<TMasterCompany>(entity =>
            {
                entity.Property(e => e.AccountName).IsUnicode(false);

                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.CompanyCode).IsUnicode(false);

                entity.Property(e => e.CompanyName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Director).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Fax).IsUnicode(false);

                entity.Property(e => e.Image).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Npwp).IsUnicode(false);

                entity.Property(e => e.OracleCode).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.ZipCode).IsUnicode(false);

                entity.HasOne(d => d.IdbankAccountNavigation)
                    .WithMany(p => p.TMasterCompany)
                    .HasForeignKey(d => d.IdbankAccount)
                    .HasConstraintName("FK_T_Master_Company_T_Master_Bank_Account");
            });

            modelBuilder.Entity<TMasterCompanyGroup>(entity =>
            {
                entity.Property(e => e.AliasCode).IsUnicode(false);

                entity.Property(e => e.AliasName).IsUnicode(false);

                entity.Property(e => e.CompanyGroupCode).IsUnicode(false);

                entity.Property(e => e.CompanyGroupName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterCostCenter>(entity =>
            {
                entity.Property(e => e.CostCenterCode).IsUnicode(false);

                entity.Property(e => e.CostCenterInitial).IsUnicode(false);

                entity.Property(e => e.CostCenterName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcostCenterGroupNavigation)
                    .WithMany(p => p.TMasterCostCenter)
                    .HasForeignKey(d => d.IdcostCenterGroup)
                    .HasConstraintName("FK_T_Master_Cost_Center_T_Master_Cost_Center_Group");
            });

            modelBuilder.Entity<TMasterCostCenterGroup>(entity =>
            {
                entity.Property(e => e.CostGroupCode).IsUnicode(false);

                entity.Property(e => e.CostGroupName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterCountry>(entity =>
            {
                entity.Property(e => e.CountryCode).IsUnicode(false);

                entity.Property(e => e.CountryName).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterCurrency>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.CurrencyCode).IsUnicode(false);

                entity.Property(e => e.CurrencyName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterEduLevel>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EduLevelCode).IsUnicode(false);

                entity.Property(e => e.EduLevelName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdeduStatusNavigation)
                    .WithMany(p => p.TMasterEduLevel)
                    .HasForeignKey(d => d.IdeduStatus)
                    .HasConstraintName("FK_T_Master_EduLevel_T_Master_EduStatus");
            });

            modelBuilder.Entity<TMasterEduMajor>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EduMajorCode).IsUnicode(false);

                entity.Property(e => e.EduMajorName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterEduResult>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EduResultCode).IsUnicode(false);

                entity.Property(e => e.EduResultName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterEduStatus>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EduStatusCode).IsUnicode(false);

                entity.Property(e => e.EduStatusName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterEliminationReason>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ReasonCode).IsUnicode(false);

                entity.Property(e => e.ReasonName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterEmploymentType>(entity =>
            {
                entity.HasKey(e => e.IdemploymentType)
                    .HasName("PK_T_Master_Employement_Type");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TypeCode).IsUnicode(false);

                entity.Property(e => e.TypeName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterFamilyRelation>(entity =>
            {
                entity.HasKey(e => e.IdfamRelation)
                    .HasName("PK_T_Master_Family_Relationship");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.FamRelationCode).IsUnicode(false);

                entity.Property(e => e.FamRelationName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterGroup>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.GroupCode).IsUnicode(false);

                entity.Property(e => e.GroupName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterHolidays>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.HolidayName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.HasOne(d => d.IdhistoryCompLocNavigation)
                    .WithMany(p => p.TMasterHolidays)
                    .HasForeignKey(d => d.IdhistoryCompLoc)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Holidays_T_Trx_History_CompanyLocation");
            });

            modelBuilder.Entity<TMasterInstitution>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.InstitutionCode).IsUnicode(false);

                entity.Property(e => e.InstitutionName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Website).IsUnicode(false);

                entity.HasOne(d => d.IdcityNavigation)
                    .WithMany(p => p.TMasterInstitution)
                    .HasForeignKey(d => d.Idcity)
                    .HasConstraintName("FK_T_Master_Institution_T_Master_City");
            });

            modelBuilder.Entity<TMasterIsland>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IslandCode).IsUnicode(false);

                entity.Property(e => e.IslandName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcountryNavigation)
                    .WithMany(p => p.TMasterIsland)
                    .HasForeignKey(d => d.Idcountry)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Island_T_Master_Country");
            });

            modelBuilder.Entity<TMasterJamsostekProvider>(entity =>
            {
                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Jobtitle).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OfficeCode).IsUnicode(false);

                entity.Property(e => e.OfficeName).IsUnicode(false);

                entity.Property(e => e.RegisterNo).IsUnicode(false);

                entity.Property(e => e.Signature).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterJamsostekProviderDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdhistoryCompLocNavigation)
                    .WithMany(p => p.TMasterJamsostekProviderDetail)
                    .HasForeignKey(d => d.IdhistoryCompLoc)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Jamsostek_Provider_Detail_T_Trx_History_CompanyLocation");

                entity.HasOne(d => d.IdjamsostekProviderNavigation)
                    .WithMany(p => p.TMasterJamsostekProviderDetail)
                    .HasForeignKey(d => d.IdjamsostekProvider)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Jamsostek_Provider_Detail_T_Master_Jamsostek_Provider_Detail");

                entity.HasOne(d => d.IdjobLevelNavigation)
                    .WithMany(p => p.TMasterJamsostekProviderDetail)
                    .HasForeignKey(d => d.IdjobLevel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Jamsostek_Provider_Detail_T_Master_Joblevel");
            });

            modelBuilder.Entity<TMasterJamsostekTemplate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TemplateCode).IsUnicode(false);

                entity.Property(e => e.TemplateName).IsUnicode(false);

                entity.HasOne(d => d.TemplateByNavigation)
                    .WithMany(p => p.TMasterJamsostekTemplate)
                    .HasForeignKey(d => d.TemplateBy)
                    .HasConstraintName("FK_T_Master_Jamsostek_Template_T_Master_Lookup");
            });

            modelBuilder.Entity<TMasterJamsostekTmpDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdtmpJamsostekNavigation)
                    .WithMany(p => p.TMasterJamsostekTmpDetail)
                    .HasForeignKey(d => d.IdtmpJamsostek)
                    .HasConstraintName("FK_T_Master_Jamsostek_TmpDetail_T_Master_Jamsostek_Template");

                entity.HasOne(d => d.IdumpNavigation)
                    .WithMany(p => p.TMasterJamsostekTmpDetail)
                    .HasForeignKey(d => d.Idump)
                    .HasConstraintName("FK_T_Master_Jamsostek_TmpDetail_T_Master_UMP");
            });

            modelBuilder.Entity<TMasterJoblevel>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.JoblevelCode).IsUnicode(false);

                entity.Property(e => e.JoblevelName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdjobLevelGroupNavigation)
                    .WithMany(p => p.TMasterJoblevel)
                    .HasForeignKey(d => d.IdjobLevelGroup)
                    .HasConstraintName("FK_T_Master_Joblevel_T_Master_Joblevel_Group");
            });

            modelBuilder.Entity<TMasterJoblevelGroup>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.GroupCode).IsUnicode(false);

                entity.Property(e => e.GroupName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterJobtitle>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.JobDescription).IsUnicode(false);

                entity.Property(e => e.JobtitleAlias).IsUnicode(false);

                entity.Property(e => e.JobtitleCode).IsUnicode(false);

                entity.Property(e => e.JobtitleName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ParentCode).IsUnicode(false);

                entity.Property(e => e.ParentName).IsUnicode(false);

                entity.HasOne(d => d.IdjoblevelNavigation)
                    .WithMany(p => p.TMasterJobtitle)
                    .HasForeignKey(d => d.Idjoblevel)
                    .HasConstraintName("FK_T_Master_Jobtitle_T_Master_Joblevel");

                entity.HasOne(d => d.IdorganizationNavigation)
                    .WithMany(p => p.TMasterJobtitle)
                    .HasForeignKey(d => d.Idorganization)
                    .HasConstraintName("FK_T_Master_Jobtitle_T_Master_Organization");
            });

            modelBuilder.Entity<TMasterJobtitleMapping>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdhistoryLocGroupNavigation)
                    .WithMany(p => p.TMasterJobtitleMapping)
                    .HasForeignKey(d => d.IdhistoryLocGroup)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Jobtitle_Mapping_T_Trx_History_LocationGroup");

                entity.HasOne(d => d.IdjobtitleNavigation)
                    .WithMany(p => p.TMasterJobtitleMappingIdjobtitleNavigation)
                    .HasForeignKey(d => d.Idjobtitle)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Jobtitle_Mapping_T_Master_Jobtitle");

                entity.HasOne(d => d.IdparentJobtitleNavigation)
                    .WithMany(p => p.TMasterJobtitleMappingIdparentJobtitleNavigation)
                    .HasForeignKey(d => d.IdparentJobtitle)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<TMasterKpp>(entity =>
            {
                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EmployeeCode).IsUnicode(false);

                entity.Property(e => e.Fax).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Kppcode).IsUnicode(false);

                entity.Property(e => e.Kppname).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Npwp).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.ResponsiblePerson).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterKppDetail>(entity =>
            {
                entity.HasKey(e => e.Idkppdetail)
                    .HasName("PK_T_Master_KPP_Company");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdhistoryCompLocNavigation)
                    .WithMany(p => p.TMasterKppDetail)
                    .HasForeignKey(d => d.IdhistoryCompLoc)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_KPP_Detail_T_Trx_History_CompanyLocation");

                entity.HasOne(d => d.IdjoblevelNavigation)
                    .WithMany(p => p.TMasterKppDetail)
                    .HasForeignKey(d => d.Idjoblevel)
                    .HasConstraintName("FK_T_Master_KPP_Detail_T_Master_Joblevel");

                entity.HasOne(d => d.IdkppNavigation)
                    .WithMany(p => p.TMasterKppDetail)
                    .HasForeignKey(d => d.Idkpp)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_KPP_Company_T_Master_KPP");
            });

            modelBuilder.Entity<TMasterLangCriteria>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Grade).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Remark).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterLanguage>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.LanguageCode).IsUnicode(false);

                entity.Property(e => e.LanguageName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterLoanPaymentMethod>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PayMethodCode).IsUnicode(false);

                entity.Property(e => e.PayMethodName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterLoanPaymentSchedule>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PayScheduleCode).IsUnicode(false);

                entity.Property(e => e.PayScheduleName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterLoanType>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.LoanTypeCode).IsUnicode(false);

                entity.Property(e => e.LoanTypeName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TMasterLoanType)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .HasConstraintName("FK_T_Master_Loan_Type_T_Master_Salary_Item");
            });

            modelBuilder.Entity<TMasterLocation>(entity =>
            {
                entity.HasKey(e => e.Idlocation)
                    .HasName("PK_T_Master_Location_1");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.LocationCode).IsUnicode(false);

                entity.Property(e => e.LocationName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OracleCode).IsUnicode(false);

                entity.HasOne(d => d.IdcityNavigation)
                    .WithMany(p => p.TMasterLocation)
                    .HasForeignKey(d => d.Idcity)
                    .HasConstraintName("FK_T_Master_Location_T_Master_City");

                entity.HasOne(d => d.IdcountryNavigation)
                    .WithMany(p => p.TMasterLocation)
                    .HasForeignKey(d => d.Idcountry)
                    .HasConstraintName("FK_T_Master_Location_T_Master_Country");

                entity.HasOne(d => d.IdstateNavigation)
                    .WithMany(p => p.TMasterLocation)
                    .HasForeignKey(d => d.Idstate)
                    .HasConstraintName("FK_T_Master_Location_T_Master_State");
            });

            modelBuilder.Entity<TMasterLocationGroup>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.LocationGroupCode).IsUnicode(false);

                entity.Property(e => e.LocationGroupName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterLookup>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.LookupCode).IsUnicode(false);

                entity.Property(e => e.LookupDesc).IsUnicode(false);

                entity.Property(e => e.LookupName).IsUnicode(false);

                entity.Property(e => e.LookupType).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterMaritalStatus>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.MaritalStatusCode).IsUnicode(false);

                entity.Property(e => e.MaritalStatusName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterMedicalExpense>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ExpenseCode).IsUnicode(false);

                entity.Property(e => e.ExpenseName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdmedicalTypeNavigation)
                    .WithMany(p => p.TMasterMedicalExpense)
                    .HasForeignKey(d => d.IdmedicalType)
                    .HasConstraintName("FK_T_Master_Medical_Expense_T_Master_Medical_Type");
            });

            modelBuilder.Entity<TMasterMedicalInstitution>(entity =>
            {
                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.InstitutionCode).IsUnicode(false);

                entity.Property(e => e.InstitutionName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ZipCode).IsUnicode(false);

                entity.HasOne(d => d.IdcityNavigation)
                    .WithMany(p => p.TMasterMedicalInstitution)
                    .HasForeignKey(d => d.Idcity)
                    .HasConstraintName("FK_T_Master_Medical_Institution_T_Master_City");

                entity.HasOne(d => d.IdinstitutionTypeNavigation)
                    .WithMany(p => p.TMasterMedicalInstitution)
                    .HasForeignKey(d => d.IdinstitutionType)
                    .HasConstraintName("FK_T_Master_Medical_Institution_T_Master_Medical_Institution_Type");
            });

            modelBuilder.Entity<TMasterMedicalInstitutionType>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.InstitutionTypeCode).IsUnicode(false);

                entity.Property(e => e.InstitutionTypeName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterMedicalRoom>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.RoomClass).IsUnicode(false);

                entity.Property(e => e.RoomCode).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterMedicalTemplate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Initiator).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TemplateCode).IsUnicode(false);

                entity.Property(e => e.TemplateName).IsUnicode(false);

                entity.HasOne(d => d.IdjoblevelNavigation)
                    .WithMany(p => p.TMasterMedicalTemplate)
                    .HasForeignKey(d => d.Idjoblevel)
                    .HasConstraintName("FK_T_Master_Medical_Template_T_Master_Joblevel");
            });

            modelBuilder.Entity<TMasterMedicalTmpDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Gender).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdmaritalStatusNavigation)
                    .WithMany(p => p.TMasterMedicalTmpDetail)
                    .HasForeignKey(d => d.IdmaritalStatus)
                    .HasConstraintName("FK_T_Master_Medical_TmpDetail_T_Master_MaritalStatus");

                entity.HasOne(d => d.IdmedicalTypeNavigation)
                    .WithMany(p => p.TMasterMedicalTmpDetail)
                    .HasForeignKey(d => d.IdmedicalType)
                    .HasConstraintName("FK_T_Master_Medical_TmpDetail_T_Master_Medical_Type");

                entity.HasOne(d => d.IdtmpMedicalNavigation)
                    .WithMany(p => p.TMasterMedicalTmpDetail)
                    .HasForeignKey(d => d.IdtmpMedical)
                    .HasConstraintName("FK_T_Master_Medical_TmpDetail_T_Master_Medical_Template");
            });

            modelBuilder.Entity<TMasterMedicalType>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.MedicalTypeCode).IsUnicode(false);

                entity.Property(e => e.MedicalTypeName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.RunId).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterMenu>(entity =>
            {
                entity.HasKey(e => e.Idmenu)
                    .HasName("PK_T_Master_Menu_1");

                entity.Property(e => e.CodeMenu).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.GroupMenu).IsUnicode(false);

                entity.Property(e => e.ImageName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.NameMenu).IsUnicode(false);

                entity.Property(e => e.ParentMenu).IsUnicode(false);

                entity.Property(e => e.TitleMenu).IsUnicode(false);

                entity.Property(e => e.Urlmenu).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterOccupation>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OccupationCode).IsUnicode(false);

                entity.Property(e => e.OccupationName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterOrgGroup>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OrgGroupCode).IsUnicode(false);

                entity.Property(e => e.OrgGroupName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterOrgLevel>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OrgLevelCode).IsUnicode(false);

                entity.Property(e => e.OrgLevelName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterOrganization>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OrgCode).IsUnicode(false);

                entity.Property(e => e.OrgName).IsUnicode(false);

                entity.Property(e => e.ParentLevelCode).IsUnicode(false);

                entity.Property(e => e.ParentLevelName).IsUnicode(false);

                entity.Property(e => e.ParentOrgCode).IsUnicode(false);

                entity.Property(e => e.ParentOrgName).IsUnicode(false);

                entity.HasOne(d => d.IdorgGroupNavigation)
                    .WithMany(p => p.TMasterOrganization)
                    .HasForeignKey(d => d.IdorgGroup)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Organization_T_Master_Org_Group");

                entity.HasOne(d => d.IdorgLevelNavigation)
                    .WithMany(p => p.TMasterOrganization)
                    .HasForeignKey(d => d.IdorgLevel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Organization_T_Master_Org_Level");
            });

            modelBuilder.Entity<TMasterPcnType>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Pcntype).IsUnicode(false);

                entity.Property(e => e.PcntypeCode).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterPositionExpense>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterPph21Component>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PphItemCode).IsUnicode(false);

                entity.Property(e => e.PphItemName).IsUnicode(false);

                entity.HasOne(d => d.IditemNavigation)
                    .WithMany(p => p.TMasterPph21Component)
                    .HasForeignKey(d => d.Iditem)
                    .HasConstraintName("FK_T_Master_PPh21_Component_T_Master_1721_Item");

                entity.HasOne(d => d.IdpphMethodNavigation)
                    .WithMany(p => p.TMasterPph21Component)
                    .HasForeignKey(d => d.IdpphMethod)
                    .HasConstraintName("FK_T_Master_PPh21_Component_T_Master_PPh21_Method");

                entity.HasOne(d => d.IdpphTemplateNavigation)
                    .WithMany(p => p.TMasterPph21Component)
                    .HasForeignKey(d => d.IdpphTemplate)
                    .HasConstraintName("FK_T_Master_PPh21_Component_T_Master_PPh21_Template");

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TMasterPph21Component)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .HasConstraintName("FK_T_Master_PPh21_Component_T_Master_Salary_Item");
            });

            modelBuilder.Entity<TMasterPph21Formula>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Formula).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IditemNavigation)
                    .WithMany(p => p.TMasterPph21Formula)
                    .HasForeignKey(d => d.Iditem)
                    .HasConstraintName("FK_T_Master_PPh21_Formula_T_Master_1721_Item");
            });

            modelBuilder.Entity<TMasterPph21Method>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PphMethodCode).IsUnicode(false);

                entity.Property(e => e.PphMethodName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterPph21Template>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TemplateCode).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterPtkp>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterPtkpDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Formula).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Query).IsUnicode(false);

                entity.Property(e => e.TableName).IsUnicode(false);

                entity.HasOne(d => d.IdptkpNavigation)
                    .WithMany(p => p.TMasterPtkpDetail)
                    .HasForeignKey(d => d.Idptkp)
                    .HasConstraintName("FK_T_Master_PTKP_Detail_T_Master_PTKP");

                entity.HasOne(d => d.IdtmpPtkpNavigation)
                    .WithMany(p => p.TMasterPtkpDetail)
                    .HasForeignKey(d => d.IdtmpPtkp)
                    .HasConstraintName("FK_T_Master_PTKP_Detail_T_Master_PTKP_Template");
            });

            modelBuilder.Entity<TMasterPtkpTemplate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Ptkpcode).IsUnicode(false);

                entity.Property(e => e.Ptkpname).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterRace>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.RaceCode).IsUnicode(false);

                entity.Property(e => e.RaceName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterReligion>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ReligionCode).IsUnicode(false);

                entity.Property(e => e.ReligionName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterSalaryItem>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.HasOne(d => d.IdsalaryItemGroupNavigation)
                    .WithMany(p => p.TMasterSalaryItem)
                    .HasForeignKey(d => d.IdsalaryItemGroup)
                    .HasConstraintName("FK_T_Master_Salary_Item_T_Master_Salary_Item_Group");

                entity.HasOne(d => d.IdsalaryTypeNavigation)
                    .WithMany(p => p.TMasterSalaryItem)
                    .HasForeignKey(d => d.IdsalaryType)
                    .HasConstraintName("FK_T_Master_Salary_Item_T_Master_Salary_Type");
            });

            modelBuilder.Entity<TMasterSalaryItemEmployee>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TMasterSalaryItemEmployee)
                    .HasForeignKey(d => d.Idemployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Salary_Item_Employee_T_Trx_Employee");

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TMasterSalaryItemEmployee)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_Salary_Item_Employee_T_Master_Salary_Item");
            });

            modelBuilder.Entity<TMasterSalaryItemGroup>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.GroupCode).IsUnicode(false);

                entity.Property(e => e.GroupName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterSalaryItemLocation>(entity =>
            {
                entity.Property(e => e.Amount).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Gender).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcompanyNavigation)
                    .WithMany(p => p.TMasterSalaryItemLocation)
                    .HasForeignKey(d => d.Idcompany)
                    .HasConstraintName("FK_T_Master_Salary_Item_Location_T_Master_Company");

                entity.HasOne(d => d.IdemploymentTypeNavigation)
                    .WithMany(p => p.TMasterSalaryItemLocation)
                    .HasForeignKey(d => d.IdemploymentType)
                    .HasConstraintName("FK_T_Master_Salary_Item_Location_T_Master_Employment_Type");

                entity.HasOne(d => d.IdjobLevelNavigation)
                    .WithMany(p => p.TMasterSalaryItemLocation)
                    .HasForeignKey(d => d.IdjobLevel)
                    .HasConstraintName("FK_T_Master_Salary_Item_Location_T_Master_Joblevel");

                entity.HasOne(d => d.IdjobtitleNavigation)
                    .WithMany(p => p.TMasterSalaryItemLocation)
                    .HasForeignKey(d => d.Idjobtitle)
                    .HasConstraintName("FK_T_Master_Salary_Item_Location_T_Master_Jobtitle");

                entity.HasOne(d => d.IdlocationNavigation)
                    .WithMany(p => p.TMasterSalaryItemLocation)
                    .HasForeignKey(d => d.Idlocation)
                    .HasConstraintName("FK_T_Master_Salary_Item_Location_T_Master_Location");

                entity.HasOne(d => d.IdmaritalStatusNavigation)
                    .WithMany(p => p.TMasterSalaryItemLocation)
                    .HasForeignKey(d => d.IdmaritalStatus)
                    .HasConstraintName("FK_T_Master_Salary_Item_Location_T_Master_MaritalStatus");

                entity.HasOne(d => d.IdorganizationNavigation)
                    .WithMany(p => p.TMasterSalaryItemLocation)
                    .HasForeignKey(d => d.Idorganization)
                    .HasConstraintName("FK_T_Master_Salary_Item_Location_T_Master_Organization");

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TMasterSalaryItemLocation)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .HasConstraintName("FK_T_Master_Salary_Item_Location_T_Master_Salary_Item_Location");
            });

            modelBuilder.Entity<TMasterSalaryItemType>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ItemTypeCode).IsUnicode(false);

                entity.Property(e => e.ItemTypeName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterSalaryTemplate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Initiator).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDefault).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Payment).IsUnicode(false);

                entity.Property(e => e.TemplateCode).IsUnicode(false);

                entity.Property(e => e.TemplateName).IsUnicode(false);

                entity.HasOne(d => d.IdcurrencyNavigation)
                    .WithMany(p => p.TMasterSalaryTemplate)
                    .HasForeignKey(d => d.Idcurrency)
                    .HasConstraintName("FK_T_Master_Salary_Template_T_Master_Currency");
            });

            modelBuilder.Entity<TMasterSalaryTmpDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Formula).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Query).IsUnicode(false);

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TMasterSalaryTmpDetail)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .HasConstraintName("FK_T_Master_Salary_TmpDetail_T_Master_Salary_Item");

                entity.HasOne(d => d.IdtmpSalaryNavigation)
                    .WithMany(p => p.TMasterSalaryTmpDetail)
                    .HasForeignKey(d => d.IdtmpSalary)
                    .HasConstraintName("FK_T_Master_Salary_TmpDetail_T_Master_Salary_Template");
            });

            modelBuilder.Entity<TMasterSalaryType>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryTypeCode).IsUnicode(false);

                entity.Property(e => e.SalaryTypeName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterSecurityPolicy>(entity =>
            {
                entity.HasKey(e => e.Idpolicy)
                    .HasName("PK_T_Master_Security_Setting");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.GroupTitle).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Key).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PolicyGroup).IsUnicode(false);

                entity.Property(e => e.Title).IsUnicode(false);

                entity.Property(e => e.Value).IsUnicode(false);

                entity.Property(e => e.ValueType).IsUnicode(false);

                entity.Property(e => e.ValueUom).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterSeveranceItem>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ItemCode).IsUnicode(false);

                entity.Property(e => e.ItemName).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterSeveranceRate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterState>(entity =>
            {
                entity.HasKey(e => e.Idstate)
                    .HasName("PK_T_Master_Province");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.StateCode).IsUnicode(false);

                entity.Property(e => e.StateName).IsUnicode(false);

                entity.HasOne(d => d.IdcountryNavigation)
                    .WithMany(p => p.TMasterState)
                    .HasForeignKey(d => d.Idcountry)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_State_T_Master_Country");

                entity.HasOne(d => d.IdislandNavigation)
                    .WithMany(p => p.TMasterState)
                    .HasForeignKey(d => d.Idisland)
                    .HasConstraintName("FK_T_Master_State_T_Master_Island");
            });

            modelBuilder.Entity<TMasterTaxRate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterTerminationCategory>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ReasonCategory).IsUnicode(false);

                entity.Property(e => e.ReasonCategoryCode).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterTerminationNotice>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemploymentTypeNavigation)
                    .WithMany(p => p.TMasterTerminationNotice)
                    .HasForeignKey(d => d.IdemploymentType)
                    .HasConstraintName("FK_T_Master_Termination_Notice_T_Master_Employment_Type");

                entity.HasOne(d => d.IdjoblevelNavigation)
                    .WithMany(p => p.TMasterTerminationNotice)
                    .HasForeignKey(d => d.Idjoblevel)
                    .HasConstraintName("FK_T_Master_Termination_Notice_T_Master_Joblevel");
            });

            modelBuilder.Entity<TMasterTerminationReason>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ReasonCode).IsUnicode(false);

                entity.Property(e => e.ReasonDesc).IsUnicode(false);

                entity.Property(e => e.ReasonType).IsUnicode(false);

                entity.HasOne(d => d.IdtermCategoryNavigation)
                    .WithMany(p => p.TMasterTerminationReason)
                    .HasForeignKey(d => d.IdtermCategory)
                    .HasConstraintName("FK_T_Master_Termination_Reason_T_Master_Termination_Category");
            });

            modelBuilder.Entity<TMasterThrTemplate>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TemplateName).IsUnicode(false);

                entity.Property(e => e.ThrdateFrom).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterThrTmpDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Formula).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemploymentTypeNavigation)
                    .WithMany(p => p.TMasterThrTmpDetail)
                    .HasForeignKey(d => d.IdemploymentType)
                    .HasConstraintName("FK_T_Master_THR_TmpDetail_T_Master_Employment_Type");

                entity.HasOne(d => d.IdreligionNavigation)
                    .WithMany(p => p.TMasterThrTmpDetail)
                    .HasForeignKey(d => d.Idreligion)
                    .HasConstraintName("FK_T_Master_THR_TmpDetail_T_Master_Religion");

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TMasterThrTmpDetail)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_THR_TmpDetail_T_Master_Salary_Item");

                entity.HasOne(d => d.IdthrtemplateNavigation)
                    .WithMany(p => p.TMasterThrTmpDetail)
                    .HasForeignKey(d => d.Idthrtemplate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_THR_TmpDetail_T_Master_THR_Template");
            });

            modelBuilder.Entity<TMasterUmp>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Umpcode).IsUnicode(false);

                entity.Property(e => e.Umpname).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterUmpDetail>(entity =>
            {
                entity.Property(e => e.Amount).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdumpNavigation)
                    .WithMany(p => p.TMasterUmpDetail)
                    .HasForeignKey(d => d.Idump)
                    .HasConstraintName("FK_T_Master_UMP_Detail_T_Master_UMP_Detail");
            });

            modelBuilder.Entity<TMasterUser>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Pin).IsUnicode(false);

                entity.Property(e => e.UserName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterUserDataAccess>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdgroupNavigation)
                    .WithMany(p => p.TMasterUserDataAccess)
                    .HasForeignKey(d => d.Idgroup)
                    .HasConstraintName("FK_T_Master_User_Data_Access_T_Master_Group");
            });

            modelBuilder.Entity<TMasterUserDataTrustee>(entity =>
            {
                entity.HasIndex(e => new { e.Idcompany, e.Idlocation, e.IdjobLevel, e.Idgroup })
                    .HasName("IX_IDGROUP_USER_DATA_TRUSTEE");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcompanyNavigation)
                    .WithMany(p => p.TMasterUserDataTrustee)
                    .HasForeignKey(d => d.Idcompany)
                    .HasConstraintName("FK_T_Master_User_Data_Trustee_T_Master_Company");

                entity.HasOne(d => d.IdgroupNavigation)
                    .WithMany(p => p.TMasterUserDataTrustee)
                    .HasForeignKey(d => d.Idgroup)
                    .HasConstraintName("FK_T_Master_User_Data_Trustee_T_Master_Group");

                entity.HasOne(d => d.IdjobLevelNavigation)
                    .WithMany(p => p.TMasterUserDataTrustee)
                    .HasForeignKey(d => d.IdjobLevel)
                    .HasConstraintName("FK_T_Master_User_Data_Trustee_T_Master_Joblevel");

                entity.HasOne(d => d.IdlocationNavigation)
                    .WithMany(p => p.TMasterUserDataTrustee)
                    .HasForeignKey(d => d.Idlocation)
                    .HasConstraintName("FK_T_Master_User_Data_Trustee_T_Master_Location");

                entity.HasOne(d => d.IdorgLevelNavigation)
                    .WithMany(p => p.TMasterUserDataTrustee)
                    .HasForeignKey(d => d.IdorgLevel)
                    .HasConstraintName("FK_T_Master_User_Data_Trustee_T_Master_Org_Level");
            });

            modelBuilder.Entity<TMasterUserGroup>(entity =>
            {
                entity.HasKey(e => e.IduserGroup)
                    .HasName("PK_T_Master_User_Group_1");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdgroupNavigation)
                    .WithMany(p => p.TMasterUserGroup)
                    .HasForeignKey(d => d.Idgroup)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_User_Group_T_Master_Group");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.TMasterUserGroup)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_User_Group_T_Master_User");
            });

            modelBuilder.Entity<TMasterUserMenuAccess>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdgroupNavigation)
                    .WithMany(p => p.TMasterUserMenuAccess)
                    .HasForeignKey(d => d.Idgroup)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Master_User_Menu_Access_T_Master_Group");
            });

            modelBuilder.Entity<TMasterUserMenuTrustee>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdgroupNavigation)
                    .WithMany(p => p.TMasterUserMenuTrustee)
                    .HasForeignKey(d => d.Idgroup)
                    .HasConstraintName("FK_T_Master_User_Menu_Trustee_T_Master_Group");

                entity.HasOne(d => d.IdmenuNavigation)
                    .WithMany(p => p.TMasterUserMenuTrustee)
                    .HasForeignKey(d => d.Idmenu)
                    .HasConstraintName("FK_T_Master_User_Menu_Trustee_T_Master_Menu");
            });

            modelBuilder.Entity<TMasterViolation>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ViolationCode).IsUnicode(false);

                entity.Property(e => e.ViolationName).IsUnicode(false);
            });

            modelBuilder.Entity<TMasterWarningLevel>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.WarningCode).IsUnicode(false);

                entity.Property(e => e.WarningName).IsUnicode(false);
            });

            modelBuilder.Entity<TReportBonus>(entity =>
            {
                entity.Property(e => e.Idbonus).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.IsPosted).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Pph21).HasDefaultValueSql("((0))");

                entity.Property(e => e.Thp).HasDefaultValueSql("((0))");

                entity.Property(e => e.VerifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TReportBonusDetail>(entity =>
            {
                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.Property(e => e.SalaryType).IsUnicode(false);
            });

            modelBuilder.Entity<TReportPayrollNonUpah>(entity =>
            {
                entity.Property(e => e.Idpayroll).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.Allowance).HasDefaultValueSql("((0))");

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Thp).HasDefaultValueSql("((0))");

                entity.Property(e => e.VerifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TReportPayrollNonUpahDetail>(entity =>
            {
                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.Property(e => e.SalaryType).IsUnicode(false);
            });

            modelBuilder.Entity<TReportPayrollUpah>(entity =>
            {
                entity.HasIndex(e => new { e.Idpayroll, e.Nik, e.Nyear, e.Nmonth, e.IsPosted, e.IsPaid })
                    .HasName("IX_T_Report_Payroll_Upah");

                entity.Property(e => e.Idpayroll).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Pph21).HasDefaultValueSql("((0))");

                entity.Property(e => e.Thp).HasDefaultValueSql("((0))");

                entity.Property(e => e.VerifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TReportPayrollUpahDetail>(entity =>
            {
                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.Property(e => e.SalaryType).IsUnicode(false);
            });

            modelBuilder.Entity<TReportPph21Monthly>(entity =>
            {
                entity.HasKey(e => e.IdpphMonthly)
                    .HasName("PK_T_Report_PPh_Monthly");

                entity.HasIndex(e => new { e.IdpphMonthly, e._19, e.Nyear, e.Nmonth, e.Nik, e.IsPosted })
                    .HasName("IX_T_Report_PPh21_Monthly");

                entity.Property(e => e.IdpphMonthly).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TReportThr>(entity =>
            {
                entity.Property(e => e.Idthr).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.IsPosted).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Pph21).HasDefaultValueSql("((0))");

                entity.Property(e => e.Thp).HasDefaultValueSql("((0))");

                entity.Property(e => e.VerifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TReportThrDetail>(entity =>
            {
                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.Property(e => e.SalaryType).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxAttachment>(entity =>
            {
                entity.Property(e => e.Idattachment).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.FileName).IsUnicode(false);

                entity.Property(e => e.FileType).IsUnicode(false);

                entity.Property(e => e.FileUrl).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Sources).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxAttendance>(entity =>
            {
                entity.Property(e => e.Idattendance).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ApprovedBy).IsUnicode(false);

                entity.Property(e => e.AttendanceCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Notes).IsUnicode(false);

                entity.Property(e => e.RejectedBy).IsUnicode(false);

                entity.HasOne(d => d.IdattachmentNavigation)
                    .WithMany(p => p.TTrxAttendance)
                    .HasForeignKey(d => d.Idattachment)
                    .HasConstraintName("FK_T_Trx_Attendance_T_Trx_Attachment");

                entity.HasOne(d => d.IdattendanceTypeNavigation)
                    .WithMany(p => p.TTrxAttendance)
                    .HasForeignKey(d => d.IdattendanceType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Attendance_T_Trx_Attendance");
            });

            modelBuilder.Entity<TTrxBonus>(entity =>
            {
                entity.Property(e => e.Idbonus).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.IsPosted).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Pph21).HasDefaultValueSql("((0))");

                entity.Property(e => e.Thp).HasDefaultValueSql("((0))");

                entity.Property(e => e.VerifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxBonusDetail>(entity =>
            {
                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.Property(e => e.SalaryType).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxCurrencyRate>(entity =>
            {
                entity.HasKey(e => e.IdcurrencyRate)
                    .HasName("PK_T_Trx_CurrencyRate");

                entity.Property(e => e.UpdatedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcurrencyNavigation)
                    .WithMany(p => p.TTrxCurrencyRate)
                    .HasForeignKey(d => d.Idcurrency)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Currency_Rate_T_Master_Currency");
            });

            modelBuilder.Entity<TTrxEmpAddress>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Oaddress).IsUnicode(false);

                entity.Property(e => e.OzipCode).IsUnicode(false);

                entity.Property(e => e.Raddress).IsUnicode(false);

                entity.Property(e => e.RzipCode).IsUnicode(false);

                entity.HasOne(d => d.IdcityONavigation)
                    .WithMany(p => p.TTrxEmpAddressIdcityONavigation)
                    .HasForeignKey(d => d.IdcityO)
                    .HasConstraintName("FK_T_Trx_Emp_AddressO_T_Master_City");

                entity.HasOne(d => d.IdcityRNavigation)
                    .WithMany(p => p.TTrxEmpAddressIdcityRNavigation)
                    .HasForeignKey(d => d.IdcityR)
                    .HasConstraintName("FK_T_Trx_Emp_AddressR_T_Master_City");

                entity.HasOne(d => d.IdcountryONavigation)
                    .WithMany(p => p.TTrxEmpAddressIdcountryONavigation)
                    .HasForeignKey(d => d.IdcountryO)
                    .HasConstraintName("FK_T_Trx_Emp_AddressO_T_Master_Country");

                entity.HasOne(d => d.IdcountryRNavigation)
                    .WithMany(p => p.TTrxEmpAddressIdcountryRNavigation)
                    .HasForeignKey(d => d.IdcountryR)
                    .HasConstraintName("FK_T_Trx_Emp_AddressR_T_Master_Country");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpAddress)
                    .HasForeignKey(d => d.Idemployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_Address_T_Trx_Employee");

                entity.HasOne(d => d.IdstateONavigation)
                    .WithMany(p => p.TTrxEmpAddressIdstateONavigation)
                    .HasForeignKey(d => d.IdstateO)
                    .HasConstraintName("FK_T_Trx_Emp_AddressO_T_Master_State");

                entity.HasOne(d => d.IdstateRNavigation)
                    .WithMany(p => p.TTrxEmpAddressIdstateRNavigation)
                    .HasForeignKey(d => d.IdstateR)
                    .HasConstraintName("FK_T_Trx_Emp_AddressR_T_Master_State");
            });

            modelBuilder.Entity<TTrxEmpBankAccount>(entity =>
            {
                entity.Property(e => e.AccountName).IsUnicode(false);

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.HasOne(d => d.IdbankAccountNavigation)
                    .WithMany(p => p.TTrxEmpBankAccount)
                    .HasForeignKey(d => d.IdbankAccount)
                    .HasConstraintName("FK_T_Trx_Emp_Bank_Account_T_Master_Bank_Account");

                entity.HasOne(d => d.IdbankGroupNavigation)
                    .WithMany(p => p.TTrxEmpBankAccount)
                    .HasForeignKey(d => d.IdbankGroup)
                    .HasConstraintName("FK_T_Trx_Emp_Bank_Account_T_Master_Bank_Group");

                entity.HasOne(d => d.IdcurrencyNavigation)
                    .WithMany(p => p.TTrxEmpBankAccount)
                    .HasForeignKey(d => d.Idcurrency)
                    .HasConstraintName("FK_T_Trx_Emp_Bank_Account_T_Master_Currency");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpBankAccount)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Bank_Account_T_Trx_Employee");
            });

            modelBuilder.Entity<TTrxEmpBankAccountChanges>(entity =>
            {
                entity.Property(e => e.AccountName).IsUnicode(false);

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.HasOne(d => d.IdbankAccountNavigation)
                    .WithMany(p => p.TTrxEmpBankAccountChanges)
                    .HasForeignKey(d => d.IdbankAccount)
                    .HasConstraintName("FK_T_Trx_Emp_Bank_Account_Changes_T_Master_Bank_Account");

                entity.HasOne(d => d.IdbankGroupNavigation)
                    .WithMany(p => p.TTrxEmpBankAccountChanges)
                    .HasForeignKey(d => d.IdbankGroup)
                    .HasConstraintName("FK_T_Trx_Emp_Bank_Account_Changes_T_Master_Bank_Group");

                entity.HasOne(d => d.IdcurrencyNavigation)
                    .WithMany(p => p.TTrxEmpBankAccountChanges)
                    .HasForeignKey(d => d.Idcurrency)
                    .HasConstraintName("FK_T_Trx_Emp_Bank_Account_Changes_T_Master_Currency");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpBankAccountChanges)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Bank_Account_Changes_T_Trx_Employee");
            });

            modelBuilder.Entity<TTrxEmpBpjskes>(entity =>
            {
                entity.HasKey(e => e.IdempBpjskes)
                    .HasName("PK_T_Trx_Emp_BPJS");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.MemberNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Ppkcode).IsUnicode(false);

                entity.Property(e => e.Ppkname).IsUnicode(false);

                entity.HasOne(d => d.IdbpjskesProviderNavigation)
                    .WithMany(p => p.TTrxEmpBpjskes)
                    .HasForeignKey(d => d.IdbpjskesProvider)
                    .HasConstraintName("FK_T_Trx_Emp_BPJSKes_T_Master_BPJSKes_Provider");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpBpjskes)
                    .HasForeignKey(d => d.Idemployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_BPJSKes_T_Trx_Employee");

                entity.HasOne(d => d.IdtmpBpjskesNavigation)
                    .WithMany(p => p.TTrxEmpBpjskes)
                    .HasForeignKey(d => d.IdtmpBpjskes)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_BPJSKes_T_Master_BPJSKes_Template");
            });

            modelBuilder.Entity<TTrxEmpBpjskesChanges>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.MemberNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Ppkcode).IsUnicode(false);

                entity.Property(e => e.Ppkname).IsUnicode(false);

                entity.HasOne(d => d.IdbpjskesProviderNavigation)
                    .WithMany(p => p.TTrxEmpBpjskesChanges)
                    .HasForeignKey(d => d.IdbpjskesProvider)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_BPJSKes_Changes_T_Master_BPJSKes_Provider");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpBpjskesChanges)
                    .HasForeignKey(d => d.Idemployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_BPJSKes_Changes_T_Trx_Emp_BPJSKes_Changes");

                entity.HasOne(d => d.IdtmpBpjskesNavigation)
                    .WithMany(p => p.TTrxEmpBpjskesChanges)
                    .HasForeignKey(d => d.IdtmpBpjskes)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_BPJSKes_Changes_T_Master_BPJSKes_Template");
            });

            modelBuilder.Entity<TTrxEmpEducation>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Title).IsUnicode(false);

                entity.HasOne(d => d.IdcityNavigation)
                    .WithMany(p => p.TTrxEmpEducation)
                    .HasForeignKey(d => d.Idcity)
                    .HasConstraintName("FK_T_Trx_Emp_Education_T_Master_City");

                entity.HasOne(d => d.IdeduLevelNavigation)
                    .WithMany(p => p.TTrxEmpEducation)
                    .HasForeignKey(d => d.IdeduLevel)
                    .HasConstraintName("FK_T_Trx_Emp_Education_T_Master_EduLevel");

                entity.HasOne(d => d.IdeduMajorNavigation)
                    .WithMany(p => p.TTrxEmpEducation)
                    .HasForeignKey(d => d.IdeduMajor)
                    .HasConstraintName("FK_T_Trx_Emp_Education_T_Master_EduMajor");

                entity.HasOne(d => d.IdeduResultNavigation)
                    .WithMany(p => p.TTrxEmpEducation)
                    .HasForeignKey(d => d.IdeduResult)
                    .HasConstraintName("FK_T_Trx_Emp_Education_T_Master_EduResult");

                entity.HasOne(d => d.IdeduStatusNavigation)
                    .WithMany(p => p.TTrxEmpEducation)
                    .HasForeignKey(d => d.IdeduStatus)
                    .HasConstraintName("FK_T_Trx_Emp_Education_T_Master_EduStatus");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpEducation)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Education_T_Trx_Employee");

                entity.HasOne(d => d.IdinstitutionNavigation)
                    .WithMany(p => p.TTrxEmpEducation)
                    .HasForeignKey(d => d.Idinstitution)
                    .HasConstraintName("FK_T_Trx_Emp_Education_T_Master_Institution");
            });

            modelBuilder.Entity<TTrxEmpFamily>(entity =>
            {
                entity.Property(e => e.BirthPlace).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.FamilyNik).IsUnicode(false);

                entity.Property(e => e.Gender).IsUnicode(false);

                entity.Property(e => e.IdcardNo).IsUnicode(false);

                entity.Property(e => e.IsBpjskes).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsMedicalClaim).HasDefaultValueSql("((1))");

                entity.Property(e => e.MemberName).IsUnicode(false);

                entity.Property(e => e.MobileNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Occupation).IsUnicode(false);

                entity.Property(e => e.OriginalAddress).IsUnicode(false);

                entity.Property(e => e.PhoneNo).IsUnicode(false);

                entity.Property(e => e.ResidentialAddress).IsUnicode(false);

                entity.HasOne(d => d.IdeduLevelNavigation)
                    .WithMany(p => p.TTrxEmpFamily)
                    .HasForeignKey(d => d.IdeduLevel)
                    .HasConstraintName("FK_T_Trx_Emp_Family_T_Master_EduLevel");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpFamily)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Family_T_Trx_Employee");

                entity.HasOne(d => d.IdfamRelationNavigation)
                    .WithMany(p => p.TTrxEmpFamily)
                    .HasForeignKey(d => d.IdfamRelation)
                    .HasConstraintName("FK_T_Trx_Emp_Family_T_Master_Family_Relation");

                entity.HasOne(d => d.IdmaritalStatusNavigation)
                    .WithMany(p => p.TTrxEmpFamily)
                    .HasForeignKey(d => d.IdmaritalStatus)
                    .HasConstraintName("FK_T_Trx_Emp_Family_T_Master_MaritalStatus");

                entity.HasOne(d => d.IdoccupationNavigation)
                    .WithMany(p => p.TTrxEmpFamily)
                    .HasForeignKey(d => d.Idoccupation)
                    .HasConstraintName("FK_T_Trx_Emp_Family_T_Master_Occupation");
            });

            modelBuilder.Entity<TTrxEmpIdCard>(entity =>
            {
                entity.Property(e => e.CardNumber).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Publisher).IsUnicode(false);

                entity.HasOne(d => d.IdcardTypeNavigation)
                    .WithMany(p => p.TTrxEmpIdCard)
                    .HasForeignKey(d => d.IdcardType)
                    .HasConstraintName("FK_T_Trx_Emp_IdCard_T_Master_Card_Type");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpIdCard)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_IdCard_T_Trx_Employee");
            });

            modelBuilder.Entity<TTrxEmpJamsostek>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.MemberNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcurrencyNavigation)
                    .WithMany(p => p.TTrxEmpJamsostek)
                    .HasForeignKey(d => d.Idcurrency)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_Jamsostek_T_Master_Currency");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpJamsostek)
                    .HasForeignKey(d => d.Idemployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_Jamsostek_T_Trx_Employee");

                entity.HasOne(d => d.IdjamsostekProviderNavigation)
                    .WithMany(p => p.TTrxEmpJamsostek)
                    .HasForeignKey(d => d.IdjamsostekProvider)
                    .HasConstraintName("FK_T_Trx_Emp_Jamsostek_T_Master_Jamsostek_Provider");

                entity.HasOne(d => d.IdtmpJamsostekNavigation)
                    .WithMany(p => p.TTrxEmpJamsostek)
                    .HasForeignKey(d => d.IdtmpJamsostek)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_Jamsostek_T_Master_Jamsostek_Template");
            });

            modelBuilder.Entity<TTrxEmpJamsostekChanges>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.MemberNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcurrencyNavigation)
                    .WithMany(p => p.TTrxEmpJamsostekChanges)
                    .HasForeignKey(d => d.Idcurrency)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_Jamsostek_Changes_T_Master_Currency");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpJamsostekChanges)
                    .HasForeignKey(d => d.Idemployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_Jamsostek_Changes_T_Trx_Emp_Jamsostek_Changes");

                entity.HasOne(d => d.IdjamsostekProviderNavigation)
                    .WithMany(p => p.TTrxEmpJamsostekChanges)
                    .HasForeignKey(d => d.IdjamsostekProvider)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_Jamsostek_Changes_T_Master_Jamsostek_Provider");

                entity.HasOne(d => d.IdtmpJamsostekNavigation)
                    .WithMany(p => p.TTrxEmpJamsostekChanges)
                    .HasForeignKey(d => d.IdtmpJamsostek)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Emp_Jamsostek_Changes_T_Master_Jamsostek_Template");
            });

            modelBuilder.Entity<TTrxEmpJobExp>(entity =>
            {
                entity.Property(e => e.CompanyAddress).IsUnicode(false);

                entity.Property(e => e.CompanyName).IsUnicode(false);

                entity.Property(e => e.CompanyPhone).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Jobtitle).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.TerminationReason).IsUnicode(false);

                entity.Property(e => e.ZipCode).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpJobExp)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_JobExp_T_Trx_Employee");
            });

            modelBuilder.Entity<TTrxEmpPcn>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Initiator).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Pcnno).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.SignedOffName).IsUnicode(false);

                entity.Property(e => e.SignedOffNik).IsUnicode(false);

                entity.Property(e => e.Spkno).IsUnicode(false);

                entity.HasOne(d => d.IdcityLetterNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.IdcityLetter)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_City");

                entity.HasOne(d => d.IdcompanyNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.Idcompany)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_Company");

                entity.HasOne(d => d.IdcostCenterNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.IdcostCenter)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_Cost_Center");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Trx_Employee");

                entity.HasOne(d => d.IdemploymentTypeNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.IdemploymentType)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_Employment_Type");

                entity.HasOne(d => d.IdjoblevelNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.Idjoblevel)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_Joblevel");

                entity.HasOne(d => d.IdjobtitleNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.Idjobtitle)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_Jobtitle");

                entity.HasOne(d => d.IdlocationNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.Idlocation)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_Location");

                entity.HasOne(d => d.IdorganizationNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.Idorganization)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_Organization");

                entity.HasOne(d => d.IdpcntypeNavigation)
                    .WithMany(p => p.TTrxEmpPcn)
                    .HasForeignKey(d => d.Idpcntype)
                    .HasConstraintName("FK_T_Trx_Emp_PCN_T_Master_PCN_Type");
            });

            modelBuilder.Entity<TTrxEmpPph>(entity =>
            {
                entity.HasIndex(e => new { e.IdpphMethod, e.Idptkpdetail, e.Idkpp, e.Npwp, e.Idemployee })
                    .HasName("IX_T_Trx_Emp_PPh");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Liable).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Npwp).IsUnicode(false);

                entity.Property(e => e.Npwpaddress).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpPph)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_T_Trx_Employee");

                entity.HasOne(d => d.IdkppNavigation)
                    .WithMany(p => p.TTrxEmpPph)
                    .HasForeignKey(d => d.Idkpp)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_T_Master_KPP");

                entity.HasOne(d => d.IdpphMethodNavigation)
                    .WithMany(p => p.TTrxEmpPph)
                    .HasForeignKey(d => d.IdpphMethod)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_T_Master_PPh21_Method");

                entity.HasOne(d => d.IdpphTemplateNavigation)
                    .WithMany(p => p.TTrxEmpPph)
                    .HasForeignKey(d => d.IdpphTemplate)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_T_Master_PPh21_Template");

                entity.HasOne(d => d.IdptkpdetailNavigation)
                    .WithMany(p => p.TTrxEmpPph)
                    .HasForeignKey(d => d.Idptkpdetail)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_T_Master_PTKPDetail");
            });

            modelBuilder.Entity<TTrxEmpPphChanges>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsPayroll).HasDefaultValueSql("((1))");

                entity.Property(e => e.Liable).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Npwp).IsUnicode(false);

                entity.Property(e => e.Npwpaddress).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpPphChanges)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_Changes_T_Trx_Employee");

                entity.HasOne(d => d.IdkppNavigation)
                    .WithMany(p => p.TTrxEmpPphChanges)
                    .HasForeignKey(d => d.Idkpp)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_Changes_T_Master_KPP");

                entity.HasOne(d => d.IdpphMethodNavigation)
                    .WithMany(p => p.TTrxEmpPphChanges)
                    .HasForeignKey(d => d.IdpphMethod)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_Changes_T_Master_PPh21_Method");

                entity.HasOne(d => d.IdpphTemplateNavigation)
                    .WithMany(p => p.TTrxEmpPphChanges)
                    .HasForeignKey(d => d.IdpphTemplate)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_Changes_T_Master_PPh21_Template");

                entity.HasOne(d => d.IdptkpdetailNavigation)
                    .WithMany(p => p.TTrxEmpPphChanges)
                    .HasForeignKey(d => d.Idptkpdetail)
                    .HasConstraintName("FK_T_Trx_Emp_PPh_Changes_T_Master_PTKP_Detail");
            });

            modelBuilder.Entity<TTrxEmpPtkpChanges>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Initiator).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpPtkpChanges)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_PTKP_Changes_T_Trx_Emp_PTKP_Changes");

                entity.HasOne(d => d.IdptkpdetailNavigation)
                    .WithMany(p => p.TTrxEmpPtkpChanges)
                    .HasForeignKey(d => d.Idptkpdetail)
                    .HasConstraintName("FK_T_Trx_Emp_PTKP_Changes_T_Master_PTKP_Detail");
            });

            modelBuilder.Entity<TTrxEmpSalChangesDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdempSalaryChangesNavigation)
                    .WithMany(p => p.TTrxEmpSalChangesDetail)
                    .HasForeignKey(d => d.IdempSalaryChanges)
                    .HasConstraintName("FK_T_Trx_Emp_Sal_Changes_Detail_T_Trx_Emp_Salary_Changes");

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TTrxEmpSalChangesDetail)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .HasConstraintName("FK_T_Trx_Emp_Sal_Changes_Detail_T_Master_Salary_Item");
            });

            modelBuilder.Entity<TTrxEmpSalary>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpSalary)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_T_Trx_Employee");

                entity.HasOne(d => d.IdtmpSalaryNavigation)
                    .WithMany(p => p.TTrxEmpSalary)
                    .HasForeignKey(d => d.IdtmpSalary)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_T_Master_Salary_Template");
            });

            modelBuilder.Entity<TTrxEmpSalaryChanges>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.DecisionNo).IsUnicode(false);

                entity.Property(e => e.Initiator).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpSalaryChanges)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_Changes_T_Trx_Employee");

                entity.HasOne(d => d.IdtmpSalaryNavigation)
                    .WithMany(p => p.TTrxEmpSalaryChanges)
                    .HasForeignKey(d => d.IdtmpSalary)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_Changes_T_Trx_Emp_Salary_Changes");
            });

            modelBuilder.Entity<TTrxEmpSalaryDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdempSalaryNavigation)
                    .WithMany(p => p.TTrxEmpSalaryDetail)
                    .HasForeignKey(d => d.IdempSalary)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_Detail_T_Trx_Emp_Salary");

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TTrxEmpSalaryDetail)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_Detail_T_Master_Salary_Item");
            });

            modelBuilder.Entity<TTrxEmpSalaryMonthly>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpSalaryMonthly)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_Monthly_T_Trx_Employee");
            });

            modelBuilder.Entity<TTrxEmpSalaryMonthlyDetail>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdempSalaryMonthlyNavigation)
                    .WithMany(p => p.TTrxEmpSalaryMonthlyDetail)
                    .HasForeignKey(d => d.IdempSalaryMonthly)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_Monthly_Detail_T_Trx_Emp_Salary_Monthly");

                entity.HasOne(d => d.IdsalaryItemNavigation)
                    .WithMany(p => p.TTrxEmpSalaryMonthlyDetail)
                    .HasForeignKey(d => d.IdsalaryItem)
                    .HasConstraintName("FK_T_Trx_Emp_Salary_Monthly_Detail_T_Master_Salary_Item");
            });

            modelBuilder.Entity<TTrxEmpTermination>(entity =>
            {
                entity.Property(e => e.BpjsletterNo).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.EmpReason).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.ResignLetterNo).IsUnicode(false);

                entity.Property(e => e.ServicePeriod).IsUnicode(false);

                entity.Property(e => e.SignedOffName).IsUnicode(false);

                entity.Property(e => e.SignedOffNik).IsUnicode(false);

                entity.Property(e => e.TerminationNo).IsUnicode(false);

                entity.HasOne(d => d.IdcityLetterNavigation)
                    .WithMany(p => p.TTrxEmpTermination)
                    .HasForeignKey(d => d.IdcityLetter)
                    .HasConstraintName("FK_T_Trx_Emp_Termination_T_Master_City");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpTermination)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Termination_T_Trx_Employee");

                entity.HasOne(d => d.IdtermReasonNavigation)
                    .WithMany(p => p.TTrxEmpTermination)
                    .HasForeignKey(d => d.IdtermReason)
                    .HasConstraintName("FK_T_Trx_Emp_Termination_T_Master_Termination_Reason");
            });

            modelBuilder.Entity<TTrxEmpViolation>(entity =>
            {
                entity.Property(e => e.Clause).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ResponsibilityName).IsUnicode(false);

                entity.Property(e => e.ResponsibilityNik).IsUnicode(false);

                entity.Property(e => e.ViolationNo).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEmpViolation)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Emp_Violation_T_Trx_Employee");

                entity.HasOne(d => d.IdwarningNavigation)
                    .WithMany(p => p.TTrxEmpViolation)
                    .HasForeignKey(d => d.Idwarning)
                    .HasConstraintName("FK_T_Trx_Emp_Violation_T_Master_Warning_Level");
            });

            modelBuilder.Entity<TTrxEmployee>(entity =>
            {
                entity.HasIndex(e => new { e.Nik, e.EffectiveDate })
                    .HasName("IX_T_TRX_EMPLOYEE_NIK");

                entity.HasIndex(e => new { e.Idemployee, e.Nik, e.EmployeeName, e.Pcnno, e.Idpcntype, e.Idorganization, e.Idlocation, e.Idjobtitle, e.Idjoblevel, e.IdemploymentType, e.IdcostCenter, e.StartDate, e.JoinDate, e.EndDate, e.IsTerminateRehired, e.IsTerminated, e.Idcompany, e.IsPosted, e.EffectiveDate })
                    .HasName("IX_T_Trx_Employee");

                entity.Property(e => e.BirthPlace).IsUnicode(false);

                entity.Property(e => e.Citizenship).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.EmployeeName).IsUnicode(false);

                entity.Property(e => e.EmployeeReference).IsUnicode(false);

                entity.Property(e => e.Gender).IsUnicode(false);

                entity.Property(e => e.HomeBase).IsUnicode(false);

                entity.Property(e => e.ImagePath).IsUnicode(false);

                entity.Property(e => e.Initiator).IsUnicode(false);

                entity.Property(e => e.MobileNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.MotherName).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.OfficeMail).IsUnicode(false);

                entity.Property(e => e.OldNik).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Pcnno).IsUnicode(false);

                entity.Property(e => e.Pin).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.PrivateMail).IsUnicode(false);

                entity.Property(e => e.RefNo).IsUnicode(false);

                entity.Property(e => e.RefNote).IsUnicode(false);

                entity.Property(e => e.RehiredBy).IsUnicode(false);

                entity.Property(e => e.ResetPinby).IsUnicode(false);

                entity.Property(e => e.ResetPwdBy).IsUnicode(false);

                entity.Property(e => e.SignedOffName).IsUnicode(false);

                entity.Property(e => e.SignedOffNik).IsUnicode(false);

                entity.Property(e => e.Spkno).IsUnicode(false);

                entity.Property(e => e.Spktype).IsUnicode(false);

                entity.Property(e => e.TerminateRehiredBy).IsUnicode(false);

                entity.Property(e => e.TerminatedBy).IsUnicode(false);

                entity.HasOne(d => d.IdbirthPlaceNavigation)
                    .WithMany(p => p.TTrxEmployeeIdbirthPlaceNavigation)
                    .HasForeignKey(d => d.IdbirthPlace)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_City");

                entity.HasOne(d => d.IdbloodTypeNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.IdbloodType)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Blood_Type");

                entity.HasOne(d => d.IdcitizenshipNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.Idcitizenship)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Country");

                entity.HasOne(d => d.IdcompanyNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.Idcompany)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Company");

                entity.HasOne(d => d.IdcostCenterNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.IdcostCenter)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Cost_Center");

                entity.HasOne(d => d.IdemploymentTypeNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.IdemploymentType)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Employment_Type");

                entity.HasOne(d => d.IdhomeBaseNavigation)
                    .WithMany(p => p.TTrxEmployeeIdhomeBaseNavigation)
                    .HasForeignKey(d => d.IdhomeBase)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_City1");

                entity.HasOne(d => d.IdjoblevelNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.Idjoblevel)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Joblevel");

                entity.HasOne(d => d.IdjobtitleNavigation)
                    .WithMany(p => p.TTrxEmployeeIdjobtitleNavigation)
                    .HasForeignKey(d => d.Idjobtitle)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Jobtitle");

                entity.HasOne(d => d.IdlocationNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.Idlocation)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Location");

                entity.HasOne(d => d.IdmaritalStatusNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.IdmaritalStatus)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_MaritalStatus");

                entity.HasOne(d => d.IdorganizationNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.Idorganization)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Organization");

                entity.HasOne(d => d.IdpcntypeNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.Idpcntype)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_PCN_Type");

                entity.HasOne(d => d.IdplacesNavigation)
                    .WithMany(p => p.TTrxEmployeeIdplacesNavigation)
                    .HasForeignKey(d => d.Idplaces)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_City2");

                entity.HasOne(d => d.IdpoHNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.IdpoH)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_State");

                entity.HasOne(d => d.IdraceNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.Idrace)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Race");

                entity.HasOne(d => d.IdreligionNavigation)
                    .WithMany(p => p.TTrxEmployee)
                    .HasForeignKey(d => d.Idreligion)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Religion");

                entity.HasOne(d => d.IdreportingToNavigation)
                    .WithMany(p => p.TTrxEmployeeIdreportingToNavigation)
                    .HasForeignKey(d => d.IdreportingTo)
                    .HasConstraintName("FK_T_Trx_Employee_T_Master_Jobtitle1");
            });

            modelBuilder.Entity<TTrxEssEmpAddress>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Oaddress).IsUnicode(false);

                entity.Property(e => e.OzipCode).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Raddress).IsUnicode(false);

                entity.Property(e => e.RejectedBy).IsUnicode(false);

                entity.Property(e => e.RzipCode).IsUnicode(false);

                entity.HasOne(d => d.IdcityONavigation)
                    .WithMany(p => p.TTrxEssEmpAddressIdcityONavigation)
                    .HasForeignKey(d => d.IdcityO)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_AddressO_T_Master_City");

                entity.HasOne(d => d.IdcityRNavigation)
                    .WithMany(p => p.TTrxEssEmpAddressIdcityRNavigation)
                    .HasForeignKey(d => d.IdcityR)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_AddressR_T_Master_City");

                entity.HasOne(d => d.IdcountryONavigation)
                    .WithMany(p => p.TTrxEssEmpAddressIdcountryONavigation)
                    .HasForeignKey(d => d.IdcountryO)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_AddressO_T_Master_Country");

                entity.HasOne(d => d.IdcountryRNavigation)
                    .WithMany(p => p.TTrxEssEmpAddressIdcountryRNavigation)
                    .HasForeignKey(d => d.IdcountryR)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_AddressR_T_Master_Country");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEssEmpAddress)
                    .HasForeignKey(d => d.Idemployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Address_T_Trx_Employee");

                entity.HasOne(d => d.IdstateONavigation)
                    .WithMany(p => p.TTrxEssEmpAddressIdstateONavigation)
                    .HasForeignKey(d => d.IdstateO)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_AddressO_T_Master_State");

                entity.HasOne(d => d.IdstateRNavigation)
                    .WithMany(p => p.TTrxEssEmpAddressIdstateRNavigation)
                    .HasForeignKey(d => d.IdstateR)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_AddressR_T_Master_State");
            });

            modelBuilder.Entity<TTrxEssEmpBankAccount>(entity =>
            {
                entity.Property(e => e.AccountName).IsUnicode(false);

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.RejectedBy).IsUnicode(false);

                entity.HasOne(d => d.IdbankAccountNavigation)
                    .WithMany(p => p.TTrxEssEmpBankAccount)
                    .HasForeignKey(d => d.IdbankAccount)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Bank_Account_T_Master_Bank_Account");

                entity.HasOne(d => d.IdbankGroupNavigation)
                    .WithMany(p => p.TTrxEssEmpBankAccount)
                    .HasForeignKey(d => d.IdbankGroup)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Bank_Account_T_Master_Bank_Group");

                entity.HasOne(d => d.IdcurrencyNavigation)
                    .WithMany(p => p.TTrxEssEmpBankAccount)
                    .HasForeignKey(d => d.Idcurrency)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Bank_Account_T_Master_Currency");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEssEmpBankAccount)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Bank_Account_T_Trx_Employee");
            });

            modelBuilder.Entity<TTrxEssEmpEducation>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.RejectedBy).IsUnicode(false);

                entity.Property(e => e.Title).IsUnicode(false);

                entity.HasOne(d => d.IdattachmentNavigation)
                    .WithMany(p => p.TTrxEssEmpEducation)
                    .HasForeignKey(d => d.Idattachment)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Education_T_Trx_Attachment");

                entity.HasOne(d => d.IdcityNavigation)
                    .WithMany(p => p.TTrxEssEmpEducation)
                    .HasForeignKey(d => d.Idcity)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Education_T_Master_City");

                entity.HasOne(d => d.IdeduLevelNavigation)
                    .WithMany(p => p.TTrxEssEmpEducation)
                    .HasForeignKey(d => d.IdeduLevel)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Education_T_Master_EduLevel");

                entity.HasOne(d => d.IdeduMajorNavigation)
                    .WithMany(p => p.TTrxEssEmpEducation)
                    .HasForeignKey(d => d.IdeduMajor)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Education_T_Master_EduMajor");

                entity.HasOne(d => d.IdeduResultNavigation)
                    .WithMany(p => p.TTrxEssEmpEducation)
                    .HasForeignKey(d => d.IdeduResult)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Education_T_Master_EduResult");

                entity.HasOne(d => d.IdeduStatusNavigation)
                    .WithMany(p => p.TTrxEssEmpEducation)
                    .HasForeignKey(d => d.IdeduStatus)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Education_T_Master_EduStatus");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEssEmpEducation)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Education_T_Trx_Employee");

                entity.HasOne(d => d.IdinstitutionNavigation)
                    .WithMany(p => p.TTrxEssEmpEducation)
                    .HasForeignKey(d => d.Idinstitution)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Education_T_Master_Institution");
            });

            modelBuilder.Entity<TTrxEssEmpFamily>(entity =>
            {
                entity.Property(e => e.BirthPlace).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.FamilyNik).IsUnicode(false);

                entity.Property(e => e.Gender).IsUnicode(false);

                entity.Property(e => e.IdcardNo).IsUnicode(false);

                entity.Property(e => e.IsBpjskes).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsMedicalClaim).HasDefaultValueSql("((1))");

                entity.Property(e => e.MemberName).IsUnicode(false);

                entity.Property(e => e.MobileNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Occupation).IsUnicode(false);

                entity.Property(e => e.OriginalAddress).IsUnicode(false);

                entity.Property(e => e.PhoneNo).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.RejectedBy).IsUnicode(false);

                entity.Property(e => e.ResidentialAddress).IsUnicode(false);

                entity.HasOne(d => d.IdattachmentNavigation)
                    .WithMany(p => p.TTrxEssEmpFamily)
                    .HasForeignKey(d => d.Idattachment)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Family_T_Trx_Attachment");

                entity.HasOne(d => d.IdeduLevelNavigation)
                    .WithMany(p => p.TTrxEssEmpFamily)
                    .HasForeignKey(d => d.IdeduLevel)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Family_T_Master_EduLevel");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEssEmpFamily)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Family_T_Trx_Employee");

                entity.HasOne(d => d.IdfamRelationNavigation)
                    .WithMany(p => p.TTrxEssEmpFamily)
                    .HasForeignKey(d => d.IdfamRelation)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Family_T_Master_Family_Relation");

                entity.HasOne(d => d.IdmaritalStatusNavigation)
                    .WithMany(p => p.TTrxEssEmpFamily)
                    .HasForeignKey(d => d.IdmaritalStatus)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Family_T_Master_MaritalStatus");

                entity.HasOne(d => d.IdoccupationNavigation)
                    .WithMany(p => p.TTrxEssEmpFamily)
                    .HasForeignKey(d => d.Idoccupation)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_Family_T_Master_Occupation");
            });

            modelBuilder.Entity<TTrxEssEmpIdCard>(entity =>
            {
                entity.Property(e => e.CardNumber).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Publisher).IsUnicode(false);

                entity.HasOne(d => d.IdattachmentNavigation)
                    .WithMany(p => p.TTrxEssEmpIdCard)
                    .HasForeignKey(d => d.Idattachment)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_IdCard_T_Trx_Attachment");

                entity.HasOne(d => d.IdcardTypeNavigation)
                    .WithMany(p => p.TTrxEssEmpIdCard)
                    .HasForeignKey(d => d.IdcardType)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_IdCard_T_Master_Card_Type");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEssEmpIdCard)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_IdCard_T_Trx_Employee");
            });

            modelBuilder.Entity<TTrxEssEmpJobExp>(entity =>
            {
                entity.Property(e => e.CompanyAddress).IsUnicode(false);

                entity.Property(e => e.CompanyName).IsUnicode(false);

                entity.Property(e => e.CompanyPhone).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Jobtitle).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.TerminationReason).IsUnicode(false);

                entity.Property(e => e.ZipCode).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEssEmpJobExp)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_ESS_Emp_JobExp_T_Trx_Employee");
            });

            modelBuilder.Entity<TTrxEssEmployee>(entity =>
            {
                entity.Property(e => e.BirthPlace).IsUnicode(false);

                entity.Property(e => e.Citizenship).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Gender).IsUnicode(false);

                entity.Property(e => e.HomeBase).IsUnicode(false);

                entity.Property(e => e.MobileNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OfficeMail).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.PrivateMail).IsUnicode(false);

                entity.Property(e => e.RejectedBy).IsUnicode(false);

                entity.HasOne(d => d.IdbirthPlaceNavigation)
                    .WithMany(p => p.TTrxEssEmployeeIdbirthPlaceNavigation)
                    .HasForeignKey(d => d.IdbirthPlace)
                    .HasConstraintName("FK_T_Trx_ESS_Employee_T_Master_City_BirthPlace");

                entity.HasOne(d => d.IdbloodTypeNavigation)
                    .WithMany(p => p.TTrxEssEmployee)
                    .HasForeignKey(d => d.IdbloodType)
                    .HasConstraintName("FK_T_Trx_ESS_Employee_T_Master_Blood_Type");

                entity.HasOne(d => d.IdcitizenshipNavigation)
                    .WithMany(p => p.TTrxEssEmployee)
                    .HasForeignKey(d => d.Idcitizenship)
                    .HasConstraintName("FK_T_Trx_ESS_Employee_T_Master_Country");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxEssEmployee)
                    .HasForeignKey(d => d.Idemployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_ESS_Employee_T_Trx_Employee");

                entity.HasOne(d => d.IdhomeBaseNavigation)
                    .WithMany(p => p.TTrxEssEmployeeIdhomeBaseNavigation)
                    .HasForeignKey(d => d.IdhomeBase)
                    .HasConstraintName("FK_T_Trx_ESS_Employee_T_Master_City_HomeBase");

                entity.HasOne(d => d.IdmaritalStatusNavigation)
                    .WithMany(p => p.TTrxEssEmployee)
                    .HasForeignKey(d => d.IdmaritalStatus)
                    .HasConstraintName("FK_T_Trx_ESS_Employee_T_Master_MaritalStatus");

                entity.HasOne(d => d.IdraceNavigation)
                    .WithMany(p => p.TTrxEssEmployee)
                    .HasForeignKey(d => d.Idrace)
                    .HasConstraintName("FK_T_Trx_ESS_Employee_T_Master_Race");

                entity.HasOne(d => d.IdreligionNavigation)
                    .WithMany(p => p.TTrxEssEmployee)
                    .HasForeignKey(d => d.Idreligion)
                    .HasConstraintName("FK_T_Trx_ESS_Employee_T_Master_Religion");
            });

            modelBuilder.Entity<TTrxHistoryCompanyGroup>(entity =>
            {
                entity.HasKey(e => e.IdhistoryCompGroup)
                    .HasName("PK_T_Trx_History_Company");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcompanyNavigation)
                    .WithMany(p => p.TTrxHistoryCompanyGroup)
                    .HasForeignKey(d => d.Idcompany)
                    .HasConstraintName("FK_T_Trx_History_Company_T_Master_Company");

                entity.HasOne(d => d.IdcompanyGroupNavigation)
                    .WithMany(p => p.TTrxHistoryCompanyGroup)
                    .HasForeignKey(d => d.IdcompanyGroup)
                    .HasConstraintName("FK_T_Trx_History_Company_T_Master_Company_Group");
            });

            modelBuilder.Entity<TTrxHistoryCompanyLocation>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdcompanyNavigation)
                    .WithMany(p => p.TTrxHistoryCompanyLocation)
                    .HasForeignKey(d => d.Idcompany)
                    .HasConstraintName("FK_T_Trx_History_CompanyLocation_T_Master_Company");

                entity.HasOne(d => d.IdlocationNavigation)
                    .WithMany(p => p.TTrxHistoryCompanyLocation)
                    .HasForeignKey(d => d.Idlocation)
                    .HasConstraintName("FK_T_Trx_History_CompanyLocation_T_Master_Location");
            });

            modelBuilder.Entity<TTrxHistoryEventLog>(entity =>
            {
                entity.HasKey(e => e.EventId)
                    .HasName("PK_T_Trx_History_Event_Log_1");

                entity.Property(e => e.BrowserType).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.EventName).IsUnicode(false);

                entity.Property(e => e.EventType).IsUnicode(false);

                entity.Property(e => e.Host).IsUnicode(false);

                entity.Property(e => e.MethodType).IsUnicode(false);

                entity.Property(e => e.ModuleName).IsUnicode(false);

                entity.Property(e => e.StatusCode).IsUnicode(false);

                entity.Property(e => e.UserLogin).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxHistoryLocationGroup>(entity =>
            {
                entity.HasKey(e => e.IdhistoryLocGroup)
                    .HasName("PK_T_Trx_History_Location");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdlocationNavigation)
                    .WithMany(p => p.TTrxHistoryLocationGroup)
                    .HasForeignKey(d => d.Idlocation)
                    .HasConstraintName("FK_T_Trx_History_Location_T_Master_Location");

                entity.HasOne(d => d.IdlocationGroupNavigation)
                    .WithMany(p => p.TTrxHistoryLocationGroup)
                    .HasForeignKey(d => d.IdlocationGroup)
                    .HasConstraintName("FK_T_Trx_History_Location_T_Master_Location_Group");
            });

            modelBuilder.Entity<TTrxHistoryRate>(entity =>
            {
                entity.HasKey(e => e.IdhistoryRate)
                    .HasName("PK_T_Trx_History_Rate_1");

                entity.Property(e => e.CurrencyName).IsUnicode(false);

                entity.Property(e => e.UpdatedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxHistoryReport>(entity =>
            {
                entity.Property(e => e.Idreport).ValueGeneratedNever();

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.PrintedBy).IsUnicode(false);

                entity.Property(e => e.ReportName).IsUnicode(false);

                entity.Property(e => e.ReportNo).IsUnicode(false);

                entity.Property(e => e.ReportType).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxLoanApplication>(entity =>
            {
                entity.Property(e => e.ApplicantId).IsUnicode(false);

                entity.Property(e => e.ApplicantName).IsUnicode(false);

                entity.Property(e => e.ApplicationNo).IsUnicode(false);

                entity.Property(e => e.ApprovedBy).IsUnicode(false);

                entity.Property(e => e.ApprovedNo).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.DisbursedBy).IsUnicode(false);

                entity.Property(e => e.DisbursedNo).IsUnicode(false);

                entity.Property(e => e.EliminatedAmount).HasDefaultValueSql("((0))");

                entity.Property(e => e.EliminatedBy).IsUnicode(false);

                entity.Property(e => e.EliminatedNo).IsUnicode(false);

                entity.Property(e => e.EliminatedReason).IsUnicode(false);

                entity.Property(e => e.EliminatedRemark).IsUnicode(false);

                entity.Property(e => e.InterestPeriod).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsScheduled).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.RejectedBy).IsUnicode(false);

                entity.Property(e => e.RejectedNo).IsUnicode(false);

                entity.Property(e => e.SubmittedBy).IsUnicode(false);

                entity.Property(e => e.WitnessName).IsUnicode(false);

                entity.Property(e => e.WitnessNik).IsUnicode(false);

                entity.HasOne(d => d.IdcurrencyNavigation)
                    .WithMany(p => p.TTrxLoanApplication)
                    .HasForeignKey(d => d.Idcurrency)
                    .HasConstraintName("FK_T_Trx_Loan_Application_T_Master_Currency");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxLoanApplication)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Loan_Application_T_Trx_Employee");

                entity.HasOne(d => d.IdloanTypeNavigation)
                    .WithMany(p => p.TTrxLoanApplication)
                    .HasForeignKey(d => d.IdloanType)
                    .HasConstraintName("FK_T_Trx_Loan_Application_T_Master_Loan_Type");

                entity.HasOne(d => d.IdplacesNavigation)
                    .WithMany(p => p.TTrxLoanApplication)
                    .HasForeignKey(d => d.Idplaces)
                    .HasConstraintName("FK_T_Trx_Loan_Application_T_Master_City");
            });

            modelBuilder.Entity<TTrxLoanInstallment>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EndBalance).HasDefaultValueSql("((0))");

                entity.Property(e => e.InterestPortion).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.OutstandingInterest).HasDefaultValueSql("((0))");

                entity.Property(e => e.OutstandingPrincipal).HasDefaultValueSql("((0))");

                entity.Property(e => e.PrincipalPortion).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReceivedAmount).HasDefaultValueSql("((0))");

                entity.Property(e => e.StartBalance).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.IdloanScheduledNavigation)
                    .WithMany(p => p.TTrxLoanInstallment)
                    .HasForeignKey(d => d.IdloanScheduled)
                    .HasConstraintName("FK_T_Trx_Loan_Installment_T_Trx_Loan_Scheduled");
            });

            modelBuilder.Entity<TTrxLoanScheduled>(entity =>
            {
                entity.HasKey(e => e.IdloanScheduled)
                    .HasName("PK_T_Trx_Loan_Rescheduled");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.ScheduleNo).IsUnicode(false);

                entity.HasOne(d => d.IdapplicationNavigation)
                    .WithMany(p => p.TTrxLoanScheduled)
                    .HasForeignKey(d => d.Idapplication)
                    .HasConstraintName("FK_T_Trx_Loan_Scheduled_T_Trx_Loan_Application");

                entity.HasOne(d => d.IdpayMethodNavigation)
                    .WithMany(p => p.TTrxLoanScheduled)
                    .HasForeignKey(d => d.IdpayMethod)
                    .HasConstraintName("FK_T_Trx_Loan_Scheduled_T_Master_Loan_Payment_Method");

                entity.HasOne(d => d.IdpayScheduleNavigation)
                    .WithMany(p => p.TTrxLoanScheduled)
                    .HasForeignKey(d => d.IdpaySchedule)
                    .HasConstraintName("FK_T_Trx_Loan_Scheduled_T_Master_Loan_Payment_Schedule");
            });

            modelBuilder.Entity<TTrxMedicalClaim>(entity =>
            {
                entity.Property(e => e.ApprovedBy).IsUnicode(false);

                entity.Property(e => e.ClaimAmount).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.CurrentLimit).HasDefaultValueSql("((0))");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.MedicalNo).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.TTrxMedicalClaim)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_T_Trx_Medical_Claim_T_Trx_Employee");

                entity.HasOne(d => d.IdmedicalTypeNavigation)
                    .WithMany(p => p.TTrxMedicalClaim)
                    .HasForeignKey(d => d.IdmedicalType)
                    .HasConstraintName("FK_T_Trx_Medical_Claim_T_Master_Medical_Type");
            });

            modelBuilder.Entity<TTrxMedicalClaimExpense>(entity =>
            {
                entity.Property(e => e.ClaimAmount).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.PayByComp).HasDefaultValueSql("((0))");

                entity.Property(e => e.PayByEmp).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.IdfamRelationNavigation)
                    .WithMany(p => p.TTrxMedicalClaimExpense)
                    .HasForeignKey(d => d.IdfamRelation)
                    .HasConstraintName("FK_T_Trx_Medical_Claim_Expense_T_Master_Family_Relation");

                entity.HasOne(d => d.IdinstitutionNavigation)
                    .WithMany(p => p.TTrxMedicalClaimExpense)
                    .HasForeignKey(d => d.Idinstitution)
                    .HasConstraintName("FK_T_Trx_Medical_Claim_Expense_T_Master_Institution");

                entity.HasOne(d => d.IdmedicalClaimNavigation)
                    .WithMany(p => p.TTrxMedicalClaimExpense)
                    .HasForeignKey(d => d.IdmedicalClaim)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Medical_Claim_Expense_T_Trx_Medical_Claim");

                entity.HasOne(d => d.IdmedicalExpenseNavigation)
                    .WithMany(p => p.TTrxMedicalClaimExpense)
                    .HasForeignKey(d => d.IdmedicalExpense)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Trx_Medical_Claim_Expense_T_Master_Medical_Expense");
            });

            modelBuilder.Entity<TTrxPayrollNonUpah>(entity =>
            {
                entity.Property(e => e.Idpayroll).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.Allowance).HasDefaultValueSql("((0))");

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Thp).HasDefaultValueSql("((0))");

                entity.Property(e => e.VerifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxPayrollNonUpahDetail>(entity =>
            {
                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.Property(e => e.SalaryType).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxPayrollUpah>(entity =>
            {
                entity.Property(e => e.Idpayroll).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Pph21).HasDefaultValueSql("((0))");

                entity.Property(e => e.Thp).HasDefaultValueSql("((0))");

                entity.Property(e => e.VerifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxPayrollUpahDetail>(entity =>
            {
                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.Property(e => e.SalaryType).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxPph21Monthly>(entity =>
            {
                entity.HasKey(e => e.IdpphMonthly)
                    .HasName("PK_T_Trx_PPh_Monthly");

                entity.Property(e => e.IdpphMonthly).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxProcessedBonus>(entity =>
            {
                entity.Property(e => e.IdprocessBonus).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxProcessedPayrollNonUpah>(entity =>
            {
                entity.HasKey(e => e.IdprocessNonUpah)
                    .HasName("PK_T_Trx_Processed_Payroll_NonUpah_1");

                entity.Property(e => e.IdprocessNonUpah).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxProcessedPayrollUpah>(entity =>
            {
                entity.HasKey(e => e.IdprocessUpah)
                    .HasName("PK_T_Trx_Processed_Payroll_Upah_1");

                entity.Property(e => e.IdprocessUpah).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxProcessedPph21Monthly>(entity =>
            {
                entity.Property(e => e.IdprocessPph21).ValueGeneratedNever();

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxProcessedThr>(entity =>
            {
                entity.Property(e => e.IdprocessThr).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxTempPph21Monthly>(entity =>
            {
                entity.Property(e => e.IdpphMonthly).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.Method).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxThr>(entity =>
            {
                entity.Property(e => e.Idthr).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AccountNo).IsUnicode(false);

                entity.Property(e => e.BankGroupCode).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.EditedBy).IsUnicode(false);

                entity.Property(e => e.IsPosted).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.Nik).IsUnicode(false);

                entity.Property(e => e.Nmonth).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Nyear).IsUnicode(false);

                entity.Property(e => e.Owner).IsUnicode(false);

                entity.Property(e => e.PostedBy).IsUnicode(false);

                entity.Property(e => e.Pph21).HasDefaultValueSql("((0))");

                entity.Property(e => e.Thp).HasDefaultValueSql("((0))");

                entity.Property(e => e.VerifiedBy).IsUnicode(false);
            });

            modelBuilder.Entity<TTrxThrDetail>(entity =>
            {
                entity.Property(e => e.ModifiedBy).IsUnicode(false);

                entity.Property(e => e.SalaryItemCode).IsUnicode(false);

                entity.Property(e => e.SalaryItemName).IsUnicode(false);

                entity.Property(e => e.SalaryType).IsUnicode(false);
            });
        }
    }
}
