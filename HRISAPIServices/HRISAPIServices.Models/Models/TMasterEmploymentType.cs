﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Employment_Type")]
    public partial class TMasterEmploymentType
    {
        public TMasterEmploymentType()
        {
            TMasterSalaryItemLocation = new HashSet<TMasterSalaryItemLocation>();
            TMasterTerminationNotice = new HashSet<TMasterTerminationNotice>();
            TMasterThrTmpDetail = new HashSet<TMasterThrTmpDetail>();
            TTrxEmpPcn = new HashSet<TTrxEmpPcn>();
            TTrxEmployee = new HashSet<TTrxEmployee>();
        }

        [Column("IDEmploymentType")]
        public int IdemploymentType { get; set; }
        [StringLength(10)]
        public string TypeCode { get; set; }
        [StringLength(50)]
        public string TypeName { get; set; }
        public short? DueMonth { get; set; }
        public bool DueStatus { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdemploymentTypeNavigation")]
        public virtual ICollection<TMasterSalaryItemLocation> TMasterSalaryItemLocation { get; set; }
        [InverseProperty("IdemploymentTypeNavigation")]
        public virtual ICollection<TMasterTerminationNotice> TMasterTerminationNotice { get; set; }
        [InverseProperty("IdemploymentTypeNavigation")]
        public virtual ICollection<TMasterThrTmpDetail> TMasterThrTmpDetail { get; set; }
        [InverseProperty("IdemploymentTypeNavigation")]
        public virtual ICollection<TTrxEmpPcn> TTrxEmpPcn { get; set; }
        [InverseProperty("IdemploymentTypeNavigation")]
        public virtual ICollection<TTrxEmployee> TTrxEmployee { get; set; }
    }
}
