﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Loan_Type")]
    public partial class TMasterLoanType
    {
        public TMasterLoanType()
        {
            TTrxLoanApplication = new HashSet<TTrxLoanApplication>();
        }

        [Key]
        [Column("IDLoanType")]
        public int IdloanType { get; set; }
        [Column("IDSalaryItem")]
        public int? IdsalaryItem { get; set; }
        [StringLength(10)]
        public string LoanTypeCode { get; set; }
        [StringLength(150)]
        public string LoanTypeName { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? LoanBalance { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TMasterLoanType")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
        [InverseProperty("IdloanTypeNavigation")]
        public virtual ICollection<TTrxLoanApplication> TTrxLoanApplication { get; set; }
    }
}
