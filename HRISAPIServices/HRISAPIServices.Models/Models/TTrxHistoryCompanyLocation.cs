﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_History_CompanyLocation")]
    public partial class TTrxHistoryCompanyLocation
    {
        public TTrxHistoryCompanyLocation()
        {
            TMasterBpjskesProviderDetail = new HashSet<TMasterBpjskesProviderDetail>();
            TMasterHolidays = new HashSet<TMasterHolidays>();
            TMasterJamsostekProviderDetail = new HashSet<TMasterJamsostekProviderDetail>();
            TMasterKppDetail = new HashSet<TMasterKppDetail>();
        }

        [Key]
        [Column("IDHistoryCompLoc")]
        public long IdhistoryCompLoc { get; set; }
        [Column("IDCompany")]
        public int? Idcompany { get; set; }
        [Column("IDLocation")]
        public int? Idlocation { get; set; }
        public bool? IsActive { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idcompany")]
        [InverseProperty("TTrxHistoryCompanyLocation")]
        public virtual TMasterCompany IdcompanyNavigation { get; set; }
        [ForeignKey("Idlocation")]
        [InverseProperty("TTrxHistoryCompanyLocation")]
        public virtual TMasterLocation IdlocationNavigation { get; set; }
        [InverseProperty("IdhistoryCompLocNavigation")]
        public virtual ICollection<TMasterBpjskesProviderDetail> TMasterBpjskesProviderDetail { get; set; }
        [InverseProperty("IdhistoryCompLocNavigation")]
        public virtual ICollection<TMasterHolidays> TMasterHolidays { get; set; }
        [InverseProperty("IdhistoryCompLocNavigation")]
        public virtual ICollection<TMasterJamsostekProviderDetail> TMasterJamsostekProviderDetail { get; set; }
        [InverseProperty("IdhistoryCompLocNavigation")]
        public virtual ICollection<TMasterKppDetail> TMasterKppDetail { get; set; }
    }
}
