﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Group")]
    public partial class TMasterGroup
    {
        public TMasterGroup()
        {
            TMasterUserDataAccess = new HashSet<TMasterUserDataAccess>();
            TMasterUserDataTrustee = new HashSet<TMasterUserDataTrustee>();
            TMasterUserGroup = new HashSet<TMasterUserGroup>();
            TMasterUserMenuAccess = new HashSet<TMasterUserMenuAccess>();
            TMasterUserMenuTrustee = new HashSet<TMasterUserMenuTrustee>();
        }

        [Key]
        [Column("IDGroup")]
        public int Idgroup { get; set; }
        [Column("IDUserType")]
        public int? IduserType { get; set; }
        [StringLength(5)]
        public string GroupCode { get; set; }
        [StringLength(100)]
        public string GroupName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdgroupNavigation")]
        public virtual ICollection<TMasterUserDataAccess> TMasterUserDataAccess { get; set; }
        [InverseProperty("IdgroupNavigation")]
        public virtual ICollection<TMasterUserDataTrustee> TMasterUserDataTrustee { get; set; }
        [InverseProperty("IdgroupNavigation")]
        public virtual ICollection<TMasterUserGroup> TMasterUserGroup { get; set; }
        [InverseProperty("IdgroupNavigation")]
        public virtual ICollection<TMasterUserMenuAccess> TMasterUserMenuAccess { get; set; }
        [InverseProperty("IdgroupNavigation")]
        public virtual ICollection<TMasterUserMenuTrustee> TMasterUserMenuTrustee { get; set; }
    }
}
