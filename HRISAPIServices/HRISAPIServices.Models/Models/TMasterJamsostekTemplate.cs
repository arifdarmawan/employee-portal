﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Jamsostek_Template")]
    public partial class TMasterJamsostekTemplate
    {
        public TMasterJamsostekTemplate()
        {
            TMasterAllowanceJamsostek = new HashSet<TMasterAllowanceJamsostek>();
            TMasterJamsostekTmpDetail = new HashSet<TMasterJamsostekTmpDetail>();
            TTrxEmpJamsostek = new HashSet<TTrxEmpJamsostek>();
            TTrxEmpJamsostekChanges = new HashSet<TTrxEmpJamsostekChanges>();
        }

        [Key]
        [Column("IDTmpJamsostek")]
        public int IdtmpJamsostek { get; set; }
        [StringLength(10)]
        public string TemplateCode { get; set; }
        [Required]
        [StringLength(50)]
        public string TemplateName { get; set; }
        public bool IsPayroll { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        public int? TemplateBy { get; set; }

        [ForeignKey("TemplateBy")]
        [InverseProperty("TMasterJamsostekTemplate")]
        public virtual TMasterLookup TemplateByNavigation { get; set; }
        [InverseProperty("IdtmpJamsostekNavigation")]
        public virtual ICollection<TMasterAllowanceJamsostek> TMasterAllowanceJamsostek { get; set; }
        [InverseProperty("IdtmpJamsostekNavigation")]
        public virtual ICollection<TMasterJamsostekTmpDetail> TMasterJamsostekTmpDetail { get; set; }
        [InverseProperty("IdtmpJamsostekNavigation")]
        public virtual ICollection<TTrxEmpJamsostek> TTrxEmpJamsostek { get; set; }
        [InverseProperty("IdtmpJamsostekNavigation")]
        public virtual ICollection<TTrxEmpJamsostekChanges> TTrxEmpJamsostekChanges { get; set; }
    }
}
