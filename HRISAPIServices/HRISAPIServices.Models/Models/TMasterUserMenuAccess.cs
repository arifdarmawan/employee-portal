﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_User_Menu_Access")]
    public partial class TMasterUserMenuAccess
    {
        [Key]
        [Column("IDUserMenuAccess")]
        public int IduserMenuAccess { get; set; }
        [Column("IDGroup")]
        public int Idgroup { get; set; }
        public bool IsView { get; set; }
        public bool IsInput { get; set; }
        public bool IsDetail { get; set; }
        public bool IsCreate { get; set; }
        public bool IsUpdate { get; set; }
        public bool IsDelete { get; set; }
        public bool IsPosting { get; set; }
        public bool IsUnposting { get; set; }
        public bool IsVerify { get; set; }
        public bool IsExport { get; set; }
        public bool IsImport { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("Idgroup")]
        [InverseProperty("TMasterUserMenuAccess")]
        public virtual TMasterGroup IdgroupNavigation { get; set; }
    }
}
