﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Report_PPh21_Monthly")]
    public partial class TReportPph21Monthly
    {
        [Column("IDPPhMonthly")]
        public Guid IdpphMonthly { get; set; }
        [Required]
        [Column("NYear")]
        [StringLength(4)]
        public string Nyear { get; set; }
        [Required]
        [Column("NMonth")]
        [StringLength(2)]
        public string Nmonth { get; set; }
        [Required]
        [Column("NIK")]
        [StringLength(10)]
        public string Nik { get; set; }
        [Column("1", TypeName = "numeric(18, 2)")]
        public decimal _1 { get; set; }
        [Column("2", TypeName = "numeric(18, 2)")]
        public decimal _2 { get; set; }
        [Column("3", TypeName = "numeric(18, 2)")]
        public decimal _3 { get; set; }
        [Column("3.a", TypeName = "numeric(18, 2)")]
        public decimal _3A { get; set; }
        [Column("3.b", TypeName = "numeric(18, 2)")]
        public decimal _3B { get; set; }
        [Column("4", TypeName = "numeric(18, 2)")]
        public decimal _4 { get; set; }
        [Column("5", TypeName = "numeric(18, 2)")]
        public decimal _5 { get; set; }
        [Column("6", TypeName = "numeric(18, 2)")]
        public decimal _6 { get; set; }
        [Column("7", TypeName = "numeric(18, 2)")]
        public decimal _7 { get; set; }
        [Column("7.a", TypeName = "numeric(18, 2)")]
        public decimal _7A { get; set; }
        [Column("7.b", TypeName = "numeric(18, 2)")]
        public decimal _7B { get; set; }
        [Column("8", TypeName = "numeric(18, 2)")]
        public decimal _8 { get; set; }
        [Column("8.a", TypeName = "numeric(18, 2)")]
        public decimal _8A { get; set; }
        [Column("9", TypeName = "numeric(18, 2)")]
        public decimal _9 { get; set; }
        [Column("9.a", TypeName = "numeric(18, 2)")]
        public decimal _9A { get; set; }
        [Column("9.b", TypeName = "numeric(18, 2)")]
        public decimal _9B { get; set; }
        [Column("10", TypeName = "numeric(18, 2)")]
        public decimal _10 { get; set; }
        [Column("11", TypeName = "numeric(18, 2)")]
        public decimal _11 { get; set; }
        [Column("11.a", TypeName = "numeric(18, 2)")]
        public decimal _11A { get; set; }
        [Column("12", TypeName = "numeric(18, 2)")]
        public decimal _12 { get; set; }
        [Column("13", TypeName = "numeric(18, 2)")]
        public decimal _13 { get; set; }
        [Column("14", TypeName = "numeric(18, 2)")]
        public decimal _14 { get; set; }
        [Column("15", TypeName = "numeric(18, 2)")]
        public decimal _15 { get; set; }
        [Column("16", TypeName = "numeric(18, 2)")]
        public decimal _16 { get; set; }
        [Column("17", TypeName = "numeric(18, 2)")]
        public decimal _17 { get; set; }
        [Column("18", TypeName = "numeric(18, 2)")]
        public decimal _18 { get; set; }
        [Column("19", TypeName = "numeric(18, 2)")]
        public decimal _19 { get; set; }
        [Column("19.a", TypeName = "numeric(18, 2)")]
        public decimal _19A { get; set; }
        [Column("19.b", TypeName = "numeric(18, 2)")]
        public decimal _19B { get; set; }
        [Column("19.c", TypeName = "numeric(18, 2)")]
        public decimal _19C { get; set; }
        [Column("20", TypeName = "numeric(18, 2)")]
        public decimal _20 { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        public bool IsEdited { get; set; }
        [StringLength(50)]
        public string EditedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EditedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
