﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Jamsostek_TmpDetail")]
    public partial class TMasterJamsostekTmpDetail
    {
        [Key]
        [Column("IDTmpJamDetail")]
        public long IdtmpJamDetail { get; set; }
        [Column("IDTmpJamsostek")]
        public int? IdtmpJamsostek { get; set; }
        [Column("JKK", TypeName = "decimal(5, 2)")]
        public decimal? Jkk { get; set; }
        [Column("JKM", TypeName = "decimal(5, 2)")]
        public decimal? Jkm { get; set; }
        [Column("JHTC", TypeName = "decimal(5, 2)")]
        public decimal? Jhtc { get; set; }
        [Column("JHTE", TypeName = "decimal(5, 2)")]
        public decimal? Jhte { get; set; }
        [Column("JPC", TypeName = "decimal(5, 2)")]
        public decimal? Jpc { get; set; }
        [Column("JPE", TypeName = "decimal(5, 2)")]
        public decimal? Jpe { get; set; }
        [Column("MaxJP", TypeName = "numeric(18, 2)")]
        public decimal? MaxJp { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("IDUMP")]
        public int? Idump { get; set; }

        [ForeignKey("IdtmpJamsostek")]
        [InverseProperty("TMasterJamsostekTmpDetail")]
        public virtual TMasterJamsostekTemplate IdtmpJamsostekNavigation { get; set; }
        [ForeignKey("Idump")]
        [InverseProperty("TMasterJamsostekTmpDetail")]
        public virtual TMasterUmp IdumpNavigation { get; set; }
    }
}
