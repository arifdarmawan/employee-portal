﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Cost_Center_Group")]
    public partial class TMasterCostCenterGroup
    {
        public TMasterCostCenterGroup()
        {
            TMasterCostCenter = new HashSet<TMasterCostCenter>();
        }

        [Key]
        [Column("IDCostCenterGroup")]
        public int IdcostCenterGroup { get; set; }
        [StringLength(30)]
        public string CostGroupCode { get; set; }
        [StringLength(150)]
        public string CostGroupName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdcostCenterGroupNavigation")]
        public virtual ICollection<TMasterCostCenter> TMasterCostCenter { get; set; }
    }
}
