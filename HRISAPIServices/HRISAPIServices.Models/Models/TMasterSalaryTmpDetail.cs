﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Salary_TmpDetail")]
    public partial class TMasterSalaryTmpDetail
    {
        [Key]
        [Column("IDTmpSalDetail")]
        public int IdtmpSalDetail { get; set; }
        [Column("IDTmpSalary")]
        public int? IdtmpSalary { get; set; }
        [Column("IDSalaryItem")]
        public int? IdsalaryItem { get; set; }
        [StringLength(800)]
        public string Formula { get; set; }
        [StringLength(200)]
        public string Query { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("IdsalaryItem")]
        [InverseProperty("TMasterSalaryTmpDetail")]
        public virtual TMasterSalaryItem IdsalaryItemNavigation { get; set; }
        [ForeignKey("IdtmpSalary")]
        [InverseProperty("TMasterSalaryTmpDetail")]
        public virtual TMasterSalaryTemplate IdtmpSalaryNavigation { get; set; }
    }
}
