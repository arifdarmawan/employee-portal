﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Tax_Rate")]
    public partial class TMasterTaxRate
    {
        [Key]
        [Column("IDTaxRate")]
        public int IdtaxRate { get; set; }
        [StringLength(4)]
        public string Nyear { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? StartValue { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? EndValue { get; set; }
        [Column("PercentageNPWP", TypeName = "decimal(5, 2)")]
        public decimal PercentageNpwp { get; set; }
        [Column("PercentageNonNPWP", TypeName = "decimal(5, 2)")]
        public decimal PercentageNonNpwp { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        public bool IsUsed { get; set; }
    }
}
