﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_Emp_BPJSKes_Changes")]
    public partial class TTrxEmpBpjskesChanges
    {
        [Key]
        [Column("IDEmpBPJSKesChanges")]
        public long IdempBpjskesChanges { get; set; }
        [Column("IDEmployee")]
        public long Idemployee { get; set; }
        [Column("IDTmpBPJSKes")]
        public int IdtmpBpjskes { get; set; }
        [Column("IDBPJSKesProvider")]
        public int IdbpjskesProvider { get; set; }
        [Required]
        [StringLength(150)]
        public string MemberNo { get; set; }
        [Column(TypeName = "date")]
        public DateTime MembershipDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EffectiveDate { get; set; }
        public bool IsPosted { get; set; }
        [StringLength(50)]
        public string PostedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PostedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("PPKCode")]
        [StringLength(10)]
        public string Ppkcode { get; set; }
        [Column("PPKName")]
        [StringLength(250)]
        public string Ppkname { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }

        [ForeignKey("IdbpjskesProvider")]
        [InverseProperty("TTrxEmpBpjskesChanges")]
        public virtual TMasterBpjskesProvider IdbpjskesProviderNavigation { get; set; }
        [ForeignKey("Idemployee")]
        [InverseProperty("TTrxEmpBpjskesChanges")]
        public virtual TTrxEmployee IdemployeeNavigation { get; set; }
        [ForeignKey("IdtmpBpjskes")]
        [InverseProperty("TTrxEmpBpjskesChanges")]
        public virtual TMasterBpjskesTemplate IdtmpBpjskesNavigation { get; set; }
    }
}
