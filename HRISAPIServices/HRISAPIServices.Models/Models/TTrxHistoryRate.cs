﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Trx_History_Rate")]
    public partial class TTrxHistoryRate
    {
        [Column("IDHistoryRate")]
        public long IdhistoryRate { get; set; }
        [StringLength(50)]
        public string CurrencyName { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? Rate { get; set; }
        public bool IsActive { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RateDate { get; set; }
        [StringLength(50)]
        public string UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
    }
}
