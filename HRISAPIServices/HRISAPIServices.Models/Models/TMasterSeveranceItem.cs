﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Severance_Item")]
    public partial class TMasterSeveranceItem
    {
        [Key]
        [Column("IDSeveranceItem")]
        public int IdseveranceItem { get; set; }
        [StringLength(10)]
        public string ItemCode { get; set; }
        [StringLength(150)]
        public string ItemName { get; set; }
        [Column("IsTHP")]
        public bool IsThp { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
