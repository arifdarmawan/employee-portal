﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRISAPIServices.Models.Models
{
    [Table("T_Master_Family_Relation")]
    public partial class TMasterFamilyRelation
    {
        public TMasterFamilyRelation()
        {
            TTrxEmpFamily = new HashSet<TTrxEmpFamily>();
            TTrxEssEmpFamily = new HashSet<TTrxEssEmpFamily>();
            TTrxMedicalClaimExpense = new HashSet<TTrxMedicalClaimExpense>();
        }

        [Column("IDFamRelation")]
        public int IdfamRelation { get; set; }
        [StringLength(10)]
        public string FamRelationCode { get; set; }
        [StringLength(50)]
        public string FamRelationName { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }

        [InverseProperty("IdfamRelationNavigation")]
        public virtual ICollection<TTrxEmpFamily> TTrxEmpFamily { get; set; }
        [InverseProperty("IdfamRelationNavigation")]
        public virtual ICollection<TTrxEssEmpFamily> TTrxEssEmpFamily { get; set; }
        [InverseProperty("IdfamRelationNavigation")]
        public virtual ICollection<TTrxMedicalClaimExpense> TTrxMedicalClaimExpense { get; set; }
    }
}
