﻿using HRISAPIServices.Models.GeneralModels;
using HRISAPIServices.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HRISAPIServices.Repository.Abstract
{
    public interface IEmployeeRepository
    {
        Task<BaseResponse> GetEmployeeInfoByEmail(string email);
        Task<BaseResponse> GetEmployeeGeneralInfoByEmail(string email);
        Task<BaseResponse> GetEmployeeGeneralInfoInputByEmail(string email);
        Task<BaseResponse> GetEmployeeIDCardListByEmail(string email);
        Task<BaseResponse> GetEmployeeFamilyListByEmail(string email);
        Task<BaseResponse> GetEmployeeFamilyInputByIDEmpFamily(long id);
        Task<BaseResponse> GetEmployeeEducationListByEmail(string email);
        Task<BaseResponse> GetEmployeeJobExperienceListByEmail(string email);
        Task<BaseResponse> GetEmployeeBankAccountListByEmail(string email);
        Task<BaseResponse> GetEmployeeStructureByEmail(string email);
        Task<BaseResponse> GetEmployeeJobDescByEmail(string email);
        Task<BaseResponse> GetDataMasterCityList();
        Task<BaseResponse> GetDataMasterBloodTypeList();
        Task<BaseResponse> GetDataMasterCountryList();
        Task<BaseResponse> GetDataMasterMaritalStatusList();
        Task<BaseResponse> GetDataMasterReligionList();
        Task<BaseResponse> GetDataMasterCompanyList();
        Task<BaseResponse> GetDataMasterCompanyListParam(string OracleCode);
        Task<BaseResponse> GetDataMasterFamilyRelationList();
        Task<BaseResponse> GetDataMasterEducationLevelList();
        Task<BaseResponse> GetDataMasterEducationStatusList();
        Task<BaseResponse> GetDataMasterEducationMajorList();
        Task<BaseResponse> GetDataMasterInstitutionList();
        Task<BaseResponse> GetDataMasterCardTypeList();
        Task<BaseResponse> GetDataMasterEducationResultList();
        Task<BaseResponse> GetDataMasterOccupationList();
        Task<BaseResponse> Checkdata(string email);
        Task<BaseResponse> InsertIDCard(TTrxEssEmpIdCard data); 
        Task<BaseResponse> InsertFamily(EmployeeFamilyInputModel data);
        Task<BaseResponse> UpdateGeneralInfo(GeneralInfoModel data);
    }
}
