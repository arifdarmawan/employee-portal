﻿using FirstResource.System.Library.EnumMaster;
using HRISAPIServices.Models.GeneralModels;
using HRISAPIServices.Models.Models;
using HRISAPIServices.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HRISAPIServices.Repository.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        readonly HRISAPIServicesContext _context;
        public EmployeeRepository()
        {
            _context = new HRISAPIServicesContext();
        }

        #region Base Get Functions
        public async Task<BaseResponse> GetEmployeeInfoByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email)
                    .Include(a => a.IdcompanyNavigation)
                    .Include(b => b.IdlocationNavigation)
                    .Include(c => c.IdemploymentTypeNavigation)
                    .Include(d => d.IdjoblevelNavigation)
                    .Include(e => e.IdjobtitleNavigation);

                var result = await (from data in EmployeeList

                                    select new
                                    {
                                        EmployeeInfo = GenEmployeeInfo(data),
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                    response.Code = 103;
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeGeneralInfoByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email)
                    .Include(a => a.IdbirthPlaceNavigation)
                    .Include(b => b.IdbloodTypeNavigation)
                    .Include(c => c.IdcitizenshipNavigation)
                    .Include(d => d.IdmaritalStatusNavigation)
                    .Include(e => e.IdreligionNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fa => fa.IdbirthPlaceNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fb => fb.IdbloodTypeNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fb => fb.IdcitizenshipNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fd => fd.IdmaritalStatusNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fe => fe.IdreligionNavigation)
                    .Include(g => g.TTrxEssEmpAddress);

                var result = await (from data in EmployeeList
                                    select new
                                    {
                                        GeneralInfo = GenGeneralInfo(data),
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeGeneralInfoInputByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email)
                    .Include(a => a.IdbirthPlaceNavigation)
                    .Include(b => b.IdbloodTypeNavigation)
                    .Include(c => c.IdcitizenshipNavigation)
                    .Include(d => d.IdmaritalStatusNavigation)
                    .Include(e => e.IdreligionNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fa => fa.IdbirthPlaceNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fb => fb.IdbloodTypeNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fb => fb.IdcitizenshipNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fd => fd.IdmaritalStatusNavigation)
                    .Include(f => f.TTrxEssEmployee)
                    .ThenInclude(fe => fe.IdreligionNavigation)
                    .Include(f => f.TTrxEssEmpIdCard)
                    .Include(g => g.TTrxEssEmpAddress);

                var result = await (from data in EmployeeList

                                    select new
                                    {
                                        GeneralInfo = GenGeneralInfoInput(data),
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeIDCardListByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email)
                    .Include(a => a.TTrxEssEmpIdCard).ThenInclude(aa => aa.IdcardTypeNavigation)
                    .Include(b => b.TTrxEmpIdCard).ThenInclude(bb => bb.IdcardTypeNavigation);

                var result = await (from data in EmployeeList

                                    select new
                                    {
                                        IDCardList = GenIDCardList(data),
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeFamilyListByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email)
                    .Include(a => a.TTrxEmpFamily).ThenInclude(aa => aa.IdeduLevelNavigation)
                    .Include(a => a.TTrxEmpFamily).ThenInclude(ab => ab.IdfamRelationNavigation)
                    .Include(a => a.TTrxEmpFamily).ThenInclude(ac => ac.IdmaritalStatusNavigation)
                    .Include(a => a.TTrxEmpFamily).ThenInclude(ad => ad.IdoccupationNavigation)
                    .Include(b => b.TTrxEssEmpFamily).ThenInclude(aa => aa.IdeduLevelNavigation)
                    .Include(b => b.TTrxEssEmpFamily).ThenInclude(ab => ab.IdfamRelationNavigation)
                    .Include(b => b.TTrxEssEmpFamily).ThenInclude(ac => ac.IdmaritalStatusNavigation)
                    .Include(b => b.TTrxEssEmpFamily).ThenInclude(ad => ad.IdoccupationNavigation);

                var result = await (from data in EmployeeList
                                        //where data.OfficeMail == email
                                    select new
                                    {
                                        FamilyList = GenFamilyList(data),
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeFamilyInputByIDEmpFamily(long id)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEssEmpFamily.Where(ss => ss.IdempFamily == id)
                    .Include(a => a.IdemployeeNavigation)
                    .Include(b => b.IdeduLevelNavigation)
                    .Include(c => c.IdattachmentNavigation)
                    .Include(d => d.IdfamRelationNavigation)
                    .Include(e => e.IdmaritalStatusNavigation)
                    .Include(f => f.IdoccupationNavigation);

                var result = await (from data in EmployeeList

                                    select new
                                    {
                                        GeneralInfo = GenFamilyInput(data),
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeEducationListByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email)
                    .Include(a => a.TTrxEmpEducation).ThenInclude(aa => aa.IdeduLevelNavigation).ThenInclude(aaa => aaa.IdeduStatusNavigation)
                    .Include(a => a.TTrxEmpEducation).ThenInclude(ab => ab.IdeduMajorNavigation)
                    .Include(a => a.TTrxEmpEducation).ThenInclude(ac => ac.IdinstitutionNavigation)
                    .Include(a => a.TTrxEmpEducation).ThenInclude(ad => ad.IdcityNavigation)
                    .Include(a => a.TTrxEmpEducation).ThenInclude(ae => ae.IdeduResultNavigation)
                    .Include(b => b.TTrxEssEmpEducation).ThenInclude(ba => ba.IdeduLevelNavigation).ThenInclude(aaa => aaa.IdeduStatusNavigation)
                    .Include(b => b.TTrxEssEmpEducation).ThenInclude(bb => bb.IdeduMajorNavigation)
                    .Include(b => b.TTrxEssEmpEducation).ThenInclude(bc => bc.IdinstitutionNavigation)
                    .Include(b => b.TTrxEssEmpEducation).ThenInclude(bd => bd.IdcityNavigation)
                    .Include(b => b.TTrxEssEmpEducation).ThenInclude(be => be.IdeduResultNavigation);

                var result = await (from data in EmployeeList
                                        //where data.OfficeMail == email
                                    select new
                                    {
                                        EducationList = GenEducationList(data),
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeJobExperienceListByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email)
                   .Include(a => a.TTrxEmpJobExp)
                   .Include(b => b.TTrxEssEmpJobExp);

                var result = await (from data in EmployeeList
                                        //where data.OfficeMail == email
                                    select new
                                    {
                                        JobExperienceList = GenJobExperienceList(data),
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeBankAccountListByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email)
                    .Include(a => a.TTrxEmpBankAccount).ThenInclude(aa => aa.IdcurrencyNavigation)
                    .Include(a => a.TTrxEmpBankAccount).ThenInclude(ab => ab.IdbankGroupNavigation)
                    .Include(a => a.TTrxEmpBankAccount).ThenInclude(ac => ac.IdbankAccountNavigation);

                var result = await (from data in EmployeeList
                                        //where data.OfficeMail == email
                                    select new
                                    {
                                        BankAccountList = GenBankAccountList(data),
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public Task<BaseResponse> GetEmployeeStructureByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public Task<BaseResponse> GetEmployeeJobDescByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public async Task<BaseResponse> GetDataMasterCityList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TMasterCity.Where(ss => ss.IsActive == true);

                var result = await (from data in _context.TMasterCity.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.Idcity,
                                        Text = data.CityName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterBloodTypeList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TMasterCity.Where(ss => ss.IsActive == true);

                var result = await (from data in _context.TMasterBloodType.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.IdbloodType,
                                        Text = data.BloodTypeName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterCountryList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TMasterCity.Where(ss => ss.IsActive == true);

                var result = await (from data in _context.TMasterCountry.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.Idcountry,
                                        Text = data.CountryName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterMaritalStatusList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TMasterCity.Where(ss => ss.IsActive == true);

                var result = await (from data in _context.TMasterMaritalStatus.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.IdmaritalStatus,
                                        Text = data.MaritalStatusName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterReligionList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TMasterCity.Where(ss => ss.IsActive == true);

                var result = await (from data in _context.TMasterReligion.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.Idreligion,
                                        Text = data.ReligionName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterCompanyList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TMasterCompany.Where(ss => ss.IsActive == true);

                var result = await (from data in _context.TMasterCompany.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        data.Idcompany,
                                        data.IdbankAccount,
                                        data.CompanyCode,
                                        data.CompanyName,
                                        data.OracleCode,
                                        data.Npwp,
                                        data.Address,
                                        data.ZipCode,
                                        data.Phone,
                                        data.Fax,
                                        data.Email,
                                        data.Image,
                                        data.Director,
                                        data.AccountNo,
                                        data.AccountName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterCompanyListParam(string OracleCode)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TMasterCompany.Where(ss => ss.IsActive == true);

                var result = await (from data in _context.TMasterCompany.Where(ss => ss.IsActive == true)
                                    where data.OracleCode.ToUpper().Trim() == OracleCode.Trim().ToUpper()
                                    select new
                                    {
                                        data.Idcompany,
                                        data.IdbankAccount,
                                        data.CompanyCode,
                                        data.CompanyName,
                                        data.OracleCode,
                                        data.Npwp,
                                        data.Address,
                                        data.ZipCode,
                                        data.Phone,
                                        data.Fax,
                                        data.Email,
                                        data.Image,
                                        data.Director,
                                        data.AccountNo,
                                        data.AccountName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterFamilyRelationList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var EmployeeList = _context.TMasterCity.Where(ss => ss.IsActive == true);

                var result = await (from data in _context.TMasterFamilyRelation.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.IdfamRelation,
                                        Text = data.FamRelationName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterEducationLevelList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from data in _context.TMasterEduLevel.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.IdeduLevel,
                                        Text = data.EduLevelName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterOccupationList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from data in _context.TMasterOccupation.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.Idoccupation,
                                        Text = data.OccupationName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterEducationStatusList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from data in _context.TMasterEduStatus.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.IdeduStatus,
                                        Text = data.EduStatusName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterEducationMajorList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from data in _context.TMasterEduMajor.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.IdeduMajor,
                                        Text = data.EduMajorName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterEducationResultList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from data in _context.TMasterEduResult.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.IdeduResult,
                                        Text = data.EduResultName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterInstitutionList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from data in _context.TMasterInstitution.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.Idinstitution,
                                        Text = data.InstitutionName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataMasterCardTypeList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from data in _context.TMasterCardType.Where(ss => ss.IsActive == true)
                                    select new
                                    {
                                        ID = data.IdcardType,
                                        Text = data.CardTypeName
                                    }).ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> Checkdata(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                //var EmployeeList = _context.TTrxEmployee.Where(ss => ss.OfficeMail.Split('@')[0] == email);

                var result = new { IsExist = await _context.TTrxEmployee.AnyAsync(ss => ss.OfficeMail.Split('@')[0] == email) };
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (!result.IsExist)
                {
                    response.Code = (int)CustomHttpStatusCode.EmployeeDataNotExist;
                    response.Message = "Data Not Exist!";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
        #endregion

        #region Base Insert Functions
        public async Task<BaseResponse> InsertIDCard(TTrxEssEmpIdCard data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (!_context.TTrxEssEmpIdCard.Any(ss => ss.Idemployee == data.Idemployee && ss.IdcardType == data.IdcardType))
                    {
                        TTrxEssEmpIdCard toInsert = new TTrxEssEmpIdCard
                        {
                            Idemployee = data.Idemployee,
                            IdcardType = data.IdcardType,
                            CardNumber = data.CardNumber,
                            Description = data.Description,
                            IsPosted = false,
                            Publisher = data.Publisher,
                            ExpiredDate = data.ExpiredDate,
                            CreatedBy = data.CreatedBy,
                            CreatedDate = DateTime.Now,
                        };

                        await _context.TTrxEssEmpIdCard.AddAsync(toInsert);
                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.SAVE_SUCCESS;
                    }
                    else
                    {
                        response.Code = (int)CustomHttpStatusCode.InsertFailDataAlreadyExist;
                        response.Status = CustomHttpStatusCode.InsertFailDataAlreadyExist.ToString();
                        response.Message = "Card already exist.";

                        dbcxtransaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
        public async Task<BaseResponse> InsertFamily(EmployeeFamilyInputModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    TTrxEmployee toAddEmp = _context.TTrxEmployee.Find(data.Idemployee);
                    TTrxEmpFamily RealData = _context.TTrxEmpFamily.Find(data.Idemployee);

                    TTrxEssEmpFamily toUpdate = new TTrxEssEmpFamily();
                    toUpdate = new TTrxEssEmpFamily
                    {
                        Idemployee = data.Idemployee,
                        IdfamRelation = data.FamilyRelation,
                        IdmaritalStatus = data.MaritalStatus,
                        IdeduLevel = data.EducationLevel,
                        Idoccupation = data.Occupation,
                        FamilyNik = "",
                        MemberName = data.FamilyMemberName,
                        Gender = data.Gender,
                        IdbirthPlace = data.BirthPlace,
                        BirthPlace = _context.TMasterCity.Find(data.BirthPlace).CityName,
                        BirthDate = data.BirthDate,
                        IsAlive = true,
                        Occupation = _context.TMasterOccupation.Find(data.Occupation).OccupationName,
                        IdcardNo = data.IDCardNo,
                        OriginalAddress = data.OriginalAddress,
                        ResidentialAddress = data.ResidentialAddress,
                        PhoneNo = data.PhoneNo,
                        MobileNo = data.MobileNo,
                        MarriedDate = data.MarriedDate,
                        Idattachment = Guid.Parse("E88B20DA-E3A8-EA11-BA9E-2C4D544CC5A5"),
                        IsMedicalClaim = false,
                        IsBpjskes = false,
                        IsPosted = false,
                        IsRejected = false,

                        CreatedBy = toAddEmp.Nik,
                        CreatedDate = DateTime.Now
                    };
                    await _context.TTrxEssEmpFamily.AddAsync(toUpdate);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response = ResponseConstant.SAVE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
        #endregion

        #region Base Update Functions
        public async Task<BaseResponse> UpdateGeneralInfo(GeneralInfoModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    TTrxEssEmployee toUpdate = _context.TTrxEssEmployee.Where(ss => ss.Idemployee == data.Idemployee && !ss.IsPosted).OrderByDescending(ss => ss.CreatedDate).FirstOrDefault();
                    TTrxEmployee toAdd = _context.TTrxEmployee.Find(data.Idemployee);
                    if (toUpdate != null)
                    {
                        toUpdate.IdbirthPlace = data.IDBirthPlace;
                        toUpdate.BirthDate = data.birthDate;
                        toUpdate.IdbloodType = data.IDBloodType;
                        toUpdate.Gender = data.gender;
                        toUpdate.Idcitizenship = data.IDNationality;
                        toUpdate.IdmaritalStatus = data.IDMaritalStatus;
                        toUpdate.MobileNo = data.mobileNo;
                        toUpdate.Idreligion = data.IDReligion;

                        toUpdate.ModifiedBy = toAdd.Nik;
                        toUpdate.ModifiedDate = DateTime.Now;

                        _context.Entry(toUpdate).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        toUpdate = new TTrxEssEmployee
                        {
                            Idemployee = data.Idemployee,
                            IdbloodType = data.IDBloodType,
                            Idreligion = data.IDReligion,
                            Idrace = toAdd.Idrace,
                            IdmaritalStatus = data.IDMaritalStatus,
                            IdbirthPlace = data.IDBirthPlace,
                            BirthPlace = _context.TMasterCity.Find(data.IDBirthPlace).CityName,
                            BirthDate = data.birthDate,
                            Gender = data.gender,
                            IdhomeBase = toAdd.IdhomeBase,
                            HomeBase = toAdd.HomeBase,
                            Idcitizenship = data.IDNationality,
                            Citizenship = _context.TMasterCountry.Find(data.IDNationality).CountryName,
                            OfficeMail = toAdd.OfficeMail,
                            PrivateMail = toAdd.PrivateMail,
                            MobileNo = data.mobileNo,
                            IsPosted = false,
                            IsRejected = false,

                            CreatedBy = toAdd.Nik,
                            CreatedDate = DateTime.Now
                        };

                        await _context.TTrxEssEmployee.AddAsync(toUpdate);
                        await _context.SaveChangesAsync();
                    }

                    TTrxEssEmpAddress toUpdateCard = _context.TTrxEssEmpAddress.Where(ss => ss.Idemployee == data.Idemployee && !ss.IsPosted).OrderByDescending(ss => ss.CreatedDate).FirstOrDefault();
                    TTrxEmpAddress toAddCard = _context.TTrxEmpAddress.Find(data.Idemployee);
                    if (toUpdateCard != null)
                    {
                        toUpdateCard.Oaddress = data.IdCardAddress != null ? data.IdCardAddress : "";
                        toUpdateCard.Raddress = data.CurrentAddress != null ? data.CurrentAddress : "";

                        toUpdateCard.ModifiedBy = toAdd.Nik;
                        toUpdateCard.ModifiedDate = DateTime.Now;

                        _context.Entry(toUpdateCard).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        toUpdateCard = new TTrxEssEmpAddress
                        {
                            Idemployee = data.Idemployee,
                            IdcountryO = toAddCard != null ? toAddCard.IdcountryO : toAdd.Idcitizenship,
                            IdstateO = toAddCard != null ? toAddCard.IdstateO : toAdd.Idcitizenship,
                            IdcityO = toAddCard != null ? toAddCard.IdcityO : toAdd.IdbirthPlace,
                            Oaddress = data.IdCardAddress,
                            OzipCode = toAddCard != null ? toAddCard.OzipCode : data.IdCardAddress,
                            IdcountryR = toAddCard != null ? toAddCard.IdcountryR : toAdd.Idcitizenship,
                            IdstateR = toAddCard != null ? toAddCard.IdstateR : toAdd.Idcitizenship,
                            IdcityR = toAddCard != null ? toAddCard.IdcityR : toAdd.IdbirthPlace,
                            Raddress = data.CurrentAddress,
                            RzipCode = toAddCard != null ? toAddCard.RzipCode : data.CurrentAddress,
                            IsPosted = false,

                            CreatedBy = toAdd.Nik,
                            CreatedDate = DateTime.Now
                        };

                        await _context.TTrxEssEmpAddress.AddAsync(toUpdateCard);
                        await _context.SaveChangesAsync();
                    }

                    dbcxtransaction.Commit();

                    response = ResponseConstant.SAVE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
        #endregion

        #region Functions

        /// <summary>
        /// Genereate Data Employee Info
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object GenEmployeeInfo(TTrxEmployee data)
        {
            return new
            {
                NIK = data.Nik,
                EmployeeName = data.EmployeeName,
                JoinDate = data.JoinDate,
                Company = data.IdcompanyNavigation?.CompanyName,
                Location = data.IdlocationNavigation?.LocationName,
                EmploymentType = data.IdemploymentTypeNavigation?.TypeName,
                BirthDate = data.BirthDate,
                JobLevel = $"{data.IdjobtitleNavigation?.JobtitleCode}-{ data.IdjoblevelNavigation?.JoblevelName}",
                JobTitle = data.IdjobtitleNavigation?.JobtitleName
            };
        }
        /// <summary>
        /// Generate Data General Employee Info
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object GenGeneralInfo(TTrxEmployee data)
        {
            return new
            {
                Idemployee = data.Idemployee,
                IdbirthPlace = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdbirthPlace : data.IdbirthPlace,
                BirthPlace = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdbirthPlaceNavigation?.CityName : data.IdbirthPlaceNavigation?.CityName,
                BirthDate = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().BirthDate : data.BirthDate,
                IdbloodType = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdbloodType : data.IdbloodType,
                BloodType = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdbloodTypeNavigation?.BloodTypeName : data.IdbloodTypeNavigation?.BloodTypeName,
                Gender = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().Gender : data.Gender,
                IdNationality = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().Idcitizenship : data.Idcitizenship,
                Nationality = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdcitizenshipNavigation?.CountryName : data.IdcitizenshipNavigation?.CountryName,
                IdMaritalStatus = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdmaritalStatus : data.IdmaritalStatus,
                MaritalStatus = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdmaritalStatusNavigation?.MaritalStatusName : data.IdmaritalStatusNavigation?.MaritalStatusName,
                IDCardAddress = data.TTrxEssEmpAddress.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmpAddress.Where(ss => !ss.IsPosted).FirstOrDefault().Oaddress : data.TTrxEmpAddress?.FirstOrDefault()?.Oaddress,
                CurrentAddress = data.TTrxEssEmpAddress.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmpAddress.Where(ss => !ss.IsPosted).FirstOrDefault().Raddress : data.TTrxEmpAddress?.FirstOrDefault()?.Raddress,
                IDCardNumber = data.TTrxEssEmpIdCard.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmpIdCard.Where(ss => !ss.IsPosted).FirstOrDefault().CardNumber : data.TTrxEssEmpIdCard?.FirstOrDefault()?.CardNumber,
                Email = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().OfficeMail : data.OfficeMail,
                MobileNo = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().MobileNo : data.MobileNo,
                IdReligion = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().Idreligion : data.Idreligion,
                Religion = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdreligionNavigation?.ReligionName : data.IdreligionNavigation?.ReligionName
            };
        }
        private object GenGeneralInfoInput(TTrxEmployee data)
        {
            return new
            {
                Idemployee = data.Idemployee,
                BirthPlace = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdbirthPlace : data.IdbirthPlace,
                BirthDate = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().BirthDate : data.BirthDate,
                BloodType = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdbloodType : data.IdbloodType,
                Gender = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().Gender : data.Gender,
                Nationality = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().Idcitizenship : data.Idcitizenship,
                MaritalStatus = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().IdmaritalStatus : data.IdmaritalStatus,
                IDCardAddress = data.TTrxEssEmpAddress.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmpAddress.Where(ss => !ss.IsPosted).FirstOrDefault().Oaddress : data.TTrxEmpAddress?.FirstOrDefault()?.Oaddress,
                CurrentAddress = data.TTrxEssEmpAddress.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmpAddress.Where(ss => !ss.IsPosted).FirstOrDefault().Raddress : data.TTrxEmpAddress?.FirstOrDefault()?.Raddress,
                IDCardNumber = data.TTrxEssEmpIdCard.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmpIdCard.Where(ss => !ss.IsPosted).FirstOrDefault().CardNumber : data.TTrxEssEmpIdCard?.FirstOrDefault()?.CardNumber,
                Email = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().OfficeMail : data.OfficeMail,
                MobileNo = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().MobileNo : data.MobileNo,
                Religion = data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault() != null ? data.TTrxEssEmployee.Where(ss => !ss.IsPosted).FirstOrDefault().Idreligion : data.Idreligion
            };
        }
        /// <summary>
        /// Generate Data ID Card Employee
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object GenIDCardList(TTrxEmployee data)
        {
            //Not Posted Data IDCard
            var data1 = (from a in data.TTrxEssEmpIdCard.Where(ss => !ss.IsPosted)
                         select a).Distinct().ToList();

            //Posted Data IDCard
            var data2 = (from a in data.TTrxEmpIdCard
                         from b in data.TTrxEssEmpIdCard.Where(ss => ss.IdcardType == a.IdcardType && ss.Idemployee == a.Idemployee && ss.IsPosted && !data1.Select(xx => xx.IdcardType).Contains(ss.IdcardType))
                         select b).ToList();

            var data3 = data1.Union(data2).ToList();

            return (from a in data1.Union(data2)
                    select new
                    {
                        Id = a.IdempCard,
                        Idemployee = a.Idemployee,
                        CardTypeName = a.IdcardTypeNavigation?.CardTypeName,
                        CardNo = a.CardNumber,
                        Publisher = a.Publisher,
                        ExpiredDate = a.ExpiredDate,
                        Description = a.Description,
                        CreatedBy = a.CreatedBy,
                        CreatedDate = a.CreatedDate,
                        IsPosted = a.IsPosted,
                    });
        }
        /// <summary>
        /// Generate Data Family Employee
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object GenFamilyList(TTrxEmployee data)
        {
            //Not Posted Data Family
            var data1 = (from a in data.TTrxEssEmpFamily.Where(ss => !ss.IsPosted)
                         select a).Distinct().ToList();

            //Posted Data Family
            var data2 = (from a in data.TTrxEmpFamily
                         from b in data.TTrxEssEmpFamily.Where(ss => ss.IdfamRelation == a.IdfamRelation && ss.Idemployee == a.Idemployee && ss.IsPosted && !data1.Select(xx => xx.IdfamRelation).Contains(ss.IdfamRelation))
                         select b).ToList();

            return (from b in data1.Union(data2)
                    select new
                    {
                        IDEmpFamily = b.IdempFamily,
                        FamilyRelation = b.IdfamRelationNavigation?.FamRelationName,
                        FamilyMemberName = b.MemberName,
                        BirthPlace = b.BirthPlace,
                        BirthDate = b.BirthDate,
                        Gender = b.Gender,
                        MaritalStatus = b.IdmaritalStatusNavigation?.MaritalStatusName,
                        IDCardNo = b.IdcardNo,
                        Occupation = b.IdoccupationNavigation?.OccupationName,
                        OriginalAddress = b.OriginalAddress,
                        ResidentialAddress = b.ResidentialAddress,
                        PhoneNo = b.PhoneNo,
                        MobileNo = b.MobileNo,
                        IsPosted = b.IsPosted
                    }).ToList();
        }
        private object GenFamilyInput(TTrxEssEmpFamily data)
        {
            return new
            {
                Idemployee = data.Idemployee,
                FamilyRelation = data.IdfamRelation,
                FamilyMemberName = data.MemberName,
                BirthPlace = data.IdbirthPlace,
                BirthDate = data.BirthDate,
                Gender = data.Gender,
                MaritalStatus = data.IdmaritalStatus,
                IDCardNo = data.IdcardNo,
                EducationLevel = data.IdeduLevel,
                Occupation = data.Idoccupation,
                OriginalAddress = data.OriginalAddress,
                ResidentialAddress = data.ResidentialAddress,
                PhoneNo = data.PhoneNo,
                MobileNo = data.MobileNo,
                IsAlive = data.IsAlive,
                MarriedDate = data.MarriedDate,
            };
        }
        /// <summary>
        /// Generate Data Education Employee
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object GenEducationList(TTrxEmployee data)
        {
            //Not Posted Data Education
            var data1 = (from a in data.TTrxEssEmpEducation.Where(ss => !ss.IsPosted)
                         select a).Distinct().ToList();

            //Posted Data Education
            var data2 = (from a in data.TTrxEmpEducation
                         from b in data.TTrxEssEmpEducation.Where(ss => ss.IdeduLevel == a.IdeduLevel && ss.Idemployee == a.Idemployee && ss.IsPosted && !data1.Select(xx => xx.IdeduLevel).Contains(ss.IdeduLevel))
                         select b).ToList();

            return (from a in data1.Union(data2)
                    select new
                    {
                        EducationStatus = a.IdeduLevelNavigation?.IdeduStatusNavigation?.EduStatusName,
                        EducationLevel = a.IdeduLevelNavigation?.EduLevelName,
                        EducationMajor = a.IdeduMajorNavigation?.EduMajorName,
                        InstitutionName = a.IdinstitutionNavigation?.InstitutionName,
                        City = a.IdcityNavigation?.CityName,
                        EducationResult = a.IdeduResultNavigation?.EduResultName,
                        Graduation = a.Graduation,
                        GradePoint = a.GradePoint,
                        Title = a.Title,
                        IsPosted = a.IsPosted
                    }).ToList();
        }
        /// <summary>
        /// Generate Data Job Experience Employee
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object GenJobExperienceList(TTrxEmployee data)
        {
            //Not Posted Data Education
            var data1 = (from a in data.TTrxEssEmpJobExp.Where(ss => !ss.IsPosted)
                         select a).Distinct().ToList();

            //Posted Data Education
            var data2 = (from a in data.TTrxEmpJobExp
                         from b in data.TTrxEssEmpJobExp.Where(ss => ss.StartDate == a.StartDate && ss.Idemployee == a.Idemployee && ss.IsPosted && (!data1.Select(xx => xx.StartDate).Contains(ss.StartDate)))
                         select b).ToList();

            return (from a in data1.Union(data2)
                    select new
                    {
                        JobTitle = a.Jobtitle,
                        StartDate = a.StartDate,
                        EndDate = a.EndDate,
                        TerminationReason = a.TerminationReason,
                        CompanyName = a.CompanyName,
                        CompanyAddress = a.CompanyAddress,
                        ZipCode = a.ZipCode,
                        CompanyPhone = a.CompanyPhone,
                        IsPosted = a.IsPosted
                    }).ToList();
        }
        /// <summary>
        /// Generate Data Bank Account Employee
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private object GenBankAccountList(TTrxEmployee data)
        {
            return (from a in data.TTrxEmpBankAccount
                    select new
                    {
                        CurrencyName = a.IdcurrencyNavigation?.CurrencyName,
                        BankGroup = a.IdbankGroupNavigation?.BankGroupCode,
                        Branch = a.IdbankAccountNavigation?.Branch,
                        AccountNo = a.IdbankAccountNavigation?.AccountNo,
                        AccountName = a.IdbankAccountNavigation?.AccountName,
                        Owner = a.Owner
                    }).ToList();
        }
        #endregion
    }
}
